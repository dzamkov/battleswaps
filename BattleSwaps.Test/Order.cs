﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BattleSwaps.Test
{
    using Util = BattleSwaps.Engine.Util;

    [TestClass]
    public class Order
    {
        [TestMethod]
        public void Scenario1()
        {
            const int size = 100000;
            Util.Order[] orders = new Util.Order[size];
            orders[0] = Util.Order.Create();
            for (int i = 1; i < size; i++)
            {
                orders[i] = orders[i - 1].After();
                Assert.IsTrue(orders[i - 1] < orders[i]);
            }
            for (int i = 1; i < size; i++)
                Assert.IsTrue(orders[i - 1] < orders[i]);
        }

        [TestMethod]
        public void ParallelScenario1()
        {
            const int size = 10000;
            Util.Order baseOrder = Util.Order.Create();
            Util.Order[] highOrders = new Util.Order[size];
            Util.Order[] midOrders = new Util.Order[size];
            Util.Order[] lowOrders = new Util.Order[size];
            uint counter = 0;
            object counterLock = new object();
            for (int i = 0; i < size; i++)
            {
                int cur = i;
                ThreadPool.QueueUserWorkItem(delegate
                {
                    midOrders[cur] = baseOrder.After();
                    highOrders[cur] = midOrders[cur].After();
                    lowOrders[cur] = midOrders[cur].Before();

                    Assert.IsTrue(lowOrders[cur] < highOrders[cur]);
                    Assert.IsTrue(highOrders[cur] > lowOrders[cur]);

                    Monitor.Enter(counterLock);
                    counter++;
                    Monitor.PulseAll(counterLock);
                    Monitor.Exit(counterLock);
                });
            }

            Monitor.Enter(counterLock);
            while (counter < size)
                Monitor.Wait(counterLock);
            Monitor.Exit(counterLock);
            
            for (int i = 0; i < size; i++)
            {
                Assert.IsTrue(baseOrder < midOrders[i]);
                Assert.IsTrue(baseOrder < lowOrders[i]);
                Assert.IsTrue(baseOrder < highOrders[i]);
                Assert.IsTrue(lowOrders[i] < midOrders[i]);
                Assert.IsTrue(midOrders[i] < highOrders[i]);
            }
        }
    }
}
