﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Engine.Util
{
    /// <summary>
    /// A low-level implementation of a hash table with items of the given type.
    /// </summary>
    /// <typeparam name="TBucket">The type of an item or empty bucket in the hash table. The default value for this
    /// type should be an empty bucket.</typeparam>
    /// <typeparam name="TKey">The key type used to index items.</typeparam>
    public struct HashTable<TBucket, TKey>
        where TBucket : IHashBucket<TKey>
    {
        public HashTable(uint Capacity)
        {
            this.Buckets = new TBucket[Capacity];
            this.Count = 0;
        }

        /// <summary>
        /// The buckets in this hash table.
        /// </summary>
        public TBucket[] Buckets;

        /// <summary>
        /// The number of non-empty items in this hash table.
        /// </summary>
        public uint Count;

        /// <summary>
        /// The empty hash table.
        /// </summary>
        public static readonly HashTable<TBucket, TKey> Empty = new HashTable<TBucket, TKey>(1);

        /// <summary>
        /// Tries getting one of the items with the given key in this hash table, possibly removing it.
        /// </summary>
        /// <param name="Key">The key to search for. If successful, this will be updated to the (equivalent) key used
        /// by the returned item.</param>
        /// <param name="ShouldRemove">Should the item be removed if found?</param>
        private bool _TryGet(IEqualityComparer<TKey> Comparer, ref TKey Key, out TBucket Bucket, bool ShouldRemove)
        {
            uint index = unchecked((uint)Comparer.GetHashCode(Key)) % (uint)this.Buckets.Length;
            uint holeIndex = uint.MaxValue;
            TBucket temp = default(TBucket);

            // Inspect item at index
            look:
            TKey otherKey;
            Bucket = this.Buckets[index];
            HashBucketState state = Bucket.TryGetKey(out otherKey);
            if (state == HashBucketState.Empty)
            {
                // Hole does not need filling
                if (holeIndex < uint.MaxValue)
                    this.Buckets[holeIndex] = default(TBucket);
                return false;
            }
            else if (state == HashBucketState.Alive)
            {
                // Check if this bucket is what we're looking for
                if (Comparer.Equals(Key, otherKey))
                {
                    Key = otherKey;
                    if (holeIndex < uint.MaxValue)
                    {
                        if (ShouldRemove)
                        {
                            // Swap holes
                            this.Buckets[holeIndex] = temp;
                            holeIndex = index;
                        }
                        else
                        {
                            // Begin filling holes with this entry
                            uint targetIndex = unchecked((uint)Comparer.GetHashCode(otherKey)) %
                                (uint)this.Buckets.Length;
                            if (unchecked(targetIndex - holeIndex < index - holeIndex))
                            {
                                this.Buckets[holeIndex] = Bucket;
                                holeIndex = index;
                            }
                        }
                    }
                    else
                    {
                        if (ShouldRemove)
                        {
                            this.Count--;
                            holeIndex = index;
                        }
                        else
                        {
                            // No filling to do
                            return true;
                        }
                    }
                    index = (index + 1) % (uint)this.Buckets.Length;
                    goto fill;
                }

                // Fill in holes when possible
                else if (holeIndex < uint.MaxValue)
                {
                    uint targetIndex = unchecked((uint)Comparer.GetHashCode(otherKey)) % (uint)this.Buckets.Length;
                    if (unchecked(targetIndex - holeIndex < index - holeIndex))
                    {
                        this.Buckets[holeIndex] = Bucket;
                        holeIndex = index;
                    }
                }
            }
            else if (state == HashBucketState.Dead)
            {
                // Remove dead items when possible
                if (holeIndex == uint.MaxValue)
                {
                    holeIndex = index;
                    temp = Bucket;
                    this.Count--;
                }
            }
            index = (index + 1) % (uint)this.Buckets.Length;
            goto look;

            // Fill hole and return true
            fill:
            temp = this.Buckets[index];
            state = temp.TryGetKey(out otherKey);
            if (state == HashBucketState.Empty)
            {
                // Hole does not need filling
                this.Buckets[holeIndex] = default(TBucket);
                return true;
            }
            else if (state == HashBucketState.Alive)
            {
                // Fill in holes when possible
                uint targetIndex = unchecked((uint)Comparer.GetHashCode(otherKey)) % (uint)this.Buckets.Length;
                if (unchecked(targetIndex - holeIndex < index - holeIndex))
                {
                    this.Buckets[holeIndex] = Bucket;
                    holeIndex = index;
                }
            }
            index = (index + 1) % (uint)this.Buckets.Length;
            goto fill;
        }

        /// <summary>
        /// Tries getting one of the items with the given key in this hash table.
        /// </summary>
        /// <param name="Key">The key to search for. If successful, this will be updated to the (equivalent) key used
        /// by the returned item.</param>
        public bool TryGet(IEqualityComparer<TKey> Comparer, ref TKey Key, out TBucket Bucket)
        {
            return this._TryGet(Comparer, ref Key, out Bucket, false);
        }

        /// <summary>
        /// Tries taking one of the items with the given key in this hash table. This will remove it from the
        /// hash table.
        /// </summary>
        /// <param name="Key">The key to search for. If successful, this will be updated to the (equivalent) key used
        /// by the returned item.</param>
        public bool TryTake(IEqualityComparer<TKey> Comparer, ref TKey Key, out TBucket Bucket)
        {
            return this._TryGet(Comparer, ref Key, out Bucket, true);
        }

        /// <summary>
        /// Indicates whether an item with the given key exists in this hash table.
        /// </summary>
        public bool Contains(IEqualityComparer<TKey> Comparer, TKey Key)
        {
            TBucket item;
            return this.TryGet(Comparer, ref Key, out item);
        }

        /// <summary>
        /// A general-purpose low-level function for adding or replacing an item with a specific key in this hash
        /// table. Note that this function does no automatic resizing.
        /// </summary>
        /// <param name="Item">The item to add or replace. If an item with the given key is already in the hash
        /// table, this will be set to that item.</param>
        /// <param name="Key">The key of the given item. If an item with the given key is already in the hash
        /// table, this will be set to that item's (equivalent) key.</param>
        /// <param name="ShouldAdd">If an item with the given key does not yet exist in the hash table, should
        /// it be added?</param>
        /// <param name="ShouldReplace">If an item with the given key already exists in the hash table, should
        /// it be replaced by the given item?</param>
        /// <returns>Indicates whether an item with the given key already was already in the hash table.</returns>
        public bool Exchange(
            IEqualityComparer<TKey> Comparer,
            ref TBucket Item, ref TKey Key,
            bool ShouldAdd, bool ShouldReplace)
        {
            uint index = unchecked((uint)Comparer.GetHashCode(Key)) % (uint)Buckets.Length;
            while (true)
            {
                TKey otherKey;
                TBucket cur = Buckets[index];
                HashBucketState state = cur.TryGetKey(out otherKey);
                if (state == HashBucketState.Dead ||
                    state == HashBucketState.Empty)
                {
                    if (ShouldAdd)
                    {
                        Buckets[index] = Item;
                        this.Count++;
                    }
                    return false;
                }
                else if (Comparer.Equals(Key, otherKey))
                {
                    if (ShouldReplace)
                        Buckets[index] = Item;
                    Item = cur;
                    Key = otherKey;
                    return true;
                }
                else
                {
                    Debug.Assert(state == HashBucketState.Alive);
                    index = (index + 1) % (uint)Buckets.Length;
                }
            }
        }

        /// <summary>
        /// Recreates the bucket array for this hash table. This will also clean up all dead items.
        /// </summary>
        public void Rehash(IEqualityComparer<TKey> Comparer, uint Capacity)
        {
            TBucket[] oldBuckets = this.Buckets;
            this.Buckets = new TBucket[Capacity];
            this.Count = 0;
            for (int i = 0; i < oldBuckets.Length; i++)
            {
                TBucket item;
                TKey key;
                if ((item = oldBuckets[i]).TryGetKey(out key) == HashBucketState.Alive)
                    this.Update(Comparer, item, key);
            }
        }

        /// <summary>
        /// Adds an item to this hash table, or replaces another item with the same key, if one exists. Note that this
        /// function does no automatic resizing.
        /// </summary>
        public void Update(IEqualityComparer<TKey> Comparer, TBucket Item, TKey Key)
        {
            this.Exchange(Comparer, ref Item, ref Key, true, true);
        }

        /// <summary>
        /// Adds an item to this hash table, or replaces another item with the same key, if one exists. Note that this
        /// function does no automatic resizing.
        /// </summary>
        public void Update(IEqualityComparer<TKey> Comparer, TBucket Item)
        {
            TKey key;
            if (Item.TryGetKey(out key) == HashBucketState.Alive)
                this.Update(Comparer, Item, key);
        }

        /// <summary>
        /// If the portion of buckets in this hashtable is above the given load factor, resizes the hash table
        /// to be at least twice as large.
        /// </summary>
        public void MaintainLoadFactor(IEqualityComparer<TKey> Comparer, double LoadFactor)
        {
            if ((double)this.Count / (double)this.Buckets.Length > LoadFactor)
                this.Rehash(Comparer, (uint)this.Buckets.Length * 2 + 1);
        }

        /// <summary>
        /// If the portion of buckets in this hashtable will be above the given load factor after adding the given
        /// amount of extra items, resizes the hash table to be at least twice as large as needed.
        /// </summary>
        public void MaintainLoadFactor(IEqualityComparer<TKey> Comparer, uint Extra, double LoadFactor)
        {
            if ((double)(this.Count + Extra) / (double)this.Buckets.Length > LoadFactor)
                this.Rehash(Comparer, (uint)(this.Buckets.Length + Extra) * 2 + 1);
        }
    }

    /// <summary>
    /// An interface to an item, or empty bucket, in a hash table.
    /// </summary>
    public interface IHashBucket<TKey>
    {
        /// <summary>
        /// Tries getting the key for this bucket.
        /// </summary>
        HashBucketState TryGetKey(out TKey Key);
    }

    /// <summary>
    /// Gives the state of a bucket in a hash table.
    /// </summary>
    public enum HashBucketState
    {
        /// <summary>
        /// The bucket is empty, with no key or data.
        /// </summary>
        Empty,

        /// <summary>
        /// The bucket is not empty, but no longer has a key or data and should be removed. Buckets may change from
        /// being <see cref="Alive"/> to <see cref="Dead"/> at any time without being prompted by the hash table.
        /// </summary>
        Dead,

        /// <summary>
        /// The bucket contains an item with a key and useful data.
        /// </summary>
        Alive
    }
}