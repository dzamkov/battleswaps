﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace BattleSwaps.Engine.Util
{
    /// <summary>
    /// Contains utility functions and extensions related to reflection.
    /// </summary>
    public static class Reflection
    {
        /// <summary>
        /// Calls the given action for all assemblies that are part of the program, including ones that are loaded
        /// later on. The action will only ever be called on one thread at a time.
        /// </summary>
        public static void AssemblyWatch(Action<System.Reflection.Assembly> Action)
        {
            // TODO
            Action(System.Reflection.Assembly.GetAssembly(typeof(Reflection)));
        }

        /// <summary>
        /// Given a generic type parameter and a type, determines whether the parameter can be substituted to the
        /// be the given type.
        /// </summary>
        public static bool CanSubstitute(Type Parameter, Type Instance)
        {
            // TODO: Type constraints
            return true;
        }

        /// <summary>
        /// Gets an arbitrary concrete type that can be substituted for the given generic parameter.
        /// </summary>
        public static Type GetDefault(Type Parameter)
        {
            // TODO: Type constraints
            return typeof(object);
        }

        /// <summary>
        /// Tries finding a substitution such that, when applied to the given generic pattern type, will yield the
        /// given closed instance type. This returns false if not possible.
        /// </summary>
        private static bool _TryMatch(Dictionary<Type, Type> Substitution, Type Pattern, Type Instance)
        {
            if (Pattern.IsGenericParameter)
            {
                Type cur;
                if (Substitution.TryGetValue(Pattern, out cur))
                {
                    return cur == Instance;
                }
                else if (CanSubstitute(Pattern, Instance))
                {
                    Substitution.Add(Pattern, Instance);
                    return true;
                }
            }
            else if (Pattern.IsGenericType)
            {
                // TODO: Check if this works for nested generic types
                Type patternDef = Pattern.GetGenericTypeDefinition();
                if (Instance.IsGenericType && patternDef == Instance.GetGenericTypeDefinition())
                {
                    Type[] patternArgs = Pattern.GetGenericArguments();
                    Type[] instanceArgs = Instance.GetGenericArguments();
                    for (int i = 0; i < patternArgs.Length; i++)
                    {
                        if (!_TryMatch(Substitution, patternArgs[i], instanceArgs[i]))
                            return false;
                    }
                    return true;
                }
            }
            else if (Pattern.IsArray)
            {
                if (Instance.IsArray)
                {
                    return _TryMatch(Substitution, Pattern.GetElementType(), Instance.GetElementType());
                }
            }
            else
            {
                return Pattern == Instance;
            }
            return false;
        }

        /// <summary>
        /// Tries finding a substitution such that, when applied to the given generic pattern type, will yield the
        /// given closed instance type. This returns false if not possible.
        /// </summary>
        public static bool TryMatch(Type Pattern, Type Instance, out Dictionary<Type, Type> Substitution)
        {
            Substitution = new Dictionary<Type, Type>(5);
            return _TryMatch(Substitution, Pattern, Instance);
        }

        /// <summary>
        /// Applies a substitution to a pattern type.
        /// </summary>
        public static Type Subsitute(Type Pattern, Dictionary<Type, Type> Substitution)
        {
            if (Pattern.IsGenericParameter)
            {
                Type res;
                if (Substitution.TryGetValue(Pattern, out res))
                    return res;
                return GetDefault(Pattern);
            }
            else if (Pattern.IsGenericType)
            {
                Type patternDef = Pattern.GetGenericTypeDefinition();
                Type[] args = Pattern.GetGenericArguments();
                for (int i = 0; i < args.Length; i++)
                    args[i] = Subsitute(args[i], Substitution);
                return patternDef.MakeGenericType(args);
            }
            else
            {
                return Pattern;
            }
        }
    }
}
