﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Engine.Util
{
    /// <summary>
    /// Implements a min-heap, which can be used as a priority queue.
    /// </summary>
    /// <remarks>The comparer used for min-heap operations must be used consistently for all operations.</remarks>
    public struct MinHeap<T>
    {
        /// <summary>
        /// The bins in this heap. This always has a size which is one less than a power of two. This may be null
        /// for empty heaps.
        /// </summary>
        public T[] Bins;

        /// <summary>
        /// The number of items in this heap.
        /// </summary>
        public int Count;

        /// <summary>
        /// Tries returning the minimum item in the heap, returning false if the heap is empty.
        /// </summary>
        public bool TryGetMin(out T Item)
        {
            if (this.Count > 0)
            {
                Item = this.Bins[0];
                return true;
            }
            Item = default(T);
            return false;
        }

        /// <summary>
        /// Tries returning and removes the minimum item in the heap, returning false if the heap is empty.
        /// </summary>
        public bool TryTakeMin(IComparer<T> Comparer, out T Item)
        {
            if (this.Count > 0)
            {
                Item = this.Bins[0];
                if (this.Count > 1)
                {
                    // Heapify down
                    this.Count--;
                    T replacement = this.Bins[this.Count];
                    this.Bins[this.Count] = default(T);
                    int index = 0;
                    int leftChildIndex;
                    while ((leftChildIndex = index * 2 + 1) < this.Count)
                    {
                        T leftChild = this.Bins[leftChildIndex];
                        int rightChildIndex = index * 2 + 2;
                        if (rightChildIndex < this.Count)
                        {
                            T rightChild = this.Bins[rightChildIndex];
                            if (Comparer.Compare(replacement, leftChild) > 0)
                            {
                                if (Comparer.Compare(rightChild, leftChild) < 0)
                                {
                                    this.Bins[index] = rightChild;
                                    index = rightChildIndex;
                                }
                                else
                                {
                                    this.Bins[index] = leftChild;
                                    index = leftChildIndex;
                                }
                            }
                            else if (Comparer.Compare(replacement, rightChild) > 0)
                            {
                                this.Bins[index] = rightChild;
                                index = rightChildIndex;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            if (Comparer.Compare(replacement, leftChild) > 0)
                            {
                                this.Bins[index] = leftChild;
                                index = leftChildIndex;
                            }
                            break;
                        }
                    }
                    this.Bins[index] = replacement;
                }
                else
                {
                    this.Bins = null;
                    this.Count = 0;
                }
                return true;
            }
            Item = default(T);
            return false;
        }

        /// <summary>
        /// Adds an item to the heap.
        /// </summary>
        public void Add(IComparer<T> Comparer, T Item)
        {
            // Resize bins if needed
            if (this.Bins == null)
            {
                Debug.Assert(this.Count == 0);
                this.Bins = new T[1];
            }
            else if (this.Count == Bins.Length)
            {
                T[] nBins = new T[this.Bins.Length * 2 + 1];
                Array.Copy(this.Bins, 0, nBins, 0, this.Bins.Length);
                this.Bins = nBins;
            }

            // Heapify up
            int index = this.Count;
            T parent;
            int parentIndex;
            while (index > 0 && Comparer.Compare(Item, parent = this.Bins[parentIndex = (index + 1) / 2 - 1]) < 0)
            {
                this.Bins[index] = parent;
                index = parentIndex;
            }
            this.Bins[index] = Item;
            this.Count++;
        }
    }
}
