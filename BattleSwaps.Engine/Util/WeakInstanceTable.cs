﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace BattleSwaps.Engine.Util
{
    /// <summary>
    /// A mapping from equivalence classes of objects of a given type (as defined by a given comparer) to the
    /// preferred representative of the class, which is weakly referenced. This is useful for ensuring that objects
    /// are reused when possible.
    /// </summary>
    public sealed class WeakInstanceTable<T>
        where T : class
    {
        private HashTable<_HashBucket, T> _HashTable;
        public WeakInstanceTable(IEqualityComparer<T> Comparer)
        {
            this._HashTable = new HashTable<_HashBucket, T>(5);
            this.Comparer = Comparer;
        }

        /// <summary>
        /// A bucket in the hash table for an instance table.
        /// </summary>
        private struct _HashBucket : IHashBucket<T>
        {
            public WeakReference<T> Instance;
            public _HashBucket(WeakReference<T> Instance)
            {
                this.Instance = Instance;
            }

            HashBucketState IHashBucket<T>.TryGetKey(out T Key)
            {
                if (this.Instance != null)
                {
                    if (this.Instance.TryGetTarget(out Key))
                        return HashBucketState.Alive;
                    else
                        return HashBucketState.Dead;
                }
                Key = null;
                return HashBucketState.Empty;
            }
        }

        /// <summary>
        /// The equality comparer used to define the equivalence classes used by this table.
        /// </summary>
        public readonly IEqualityComparer<T> Comparer;

        /// <summary>
        /// Gets the preferred representative for the equivalence class the given object is in. If no such
        /// representative has been defined yet, it will be set to the given object.
        /// </summary>
        public T Coalesce(T Prototype)
        {
            // TODO: No contention for simultaneous readers
            lock (this)
            {
                _HashBucket item;
                T res = Prototype;
                if (!this._HashTable.TryGet(this.Comparer, ref res, out item))
                {
                    this._HashTable.Update(Comparer, new _HashBucket(new WeakReference<T>(res)), res);
                    this._HashTable.MaintainLoadFactor(this.Comparer, 0.6);
                }
                return res;
            }
        }
    }
}
