﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

namespace BattleSwaps.Engine.Util
{
    /// <summary>
    /// An element in an order-maintenance structure. This class is thread-safe.
    /// </summary>
    public sealed class Order
    {
        private Order(Order Base, _ListNode Node, ulong Label)
        {
            this._Base = Base;
            this._Node = Node;
            this._Label = Label;
        }

        private Order(_ListNode Node, ulong Label)
        {
            this._Base = this;
            this._Node = Node;
            this._Label = Label;
        }

        /// <summary>
        /// The "base" order for the list this order is in, as defined by the Dietz-Sleator algorithm.
        /// </summary>
        private readonly Order _Base;

        /// <summary>
        /// The list node corresponding to this order.
        /// </summary>
        /// <remarks>The seperation of nodes and orders is necessary for garbage collection. Otherwise, a usage
        /// of any order would prevent collection of all of them in the same list.</remarks>
        private _ListNode _Node;

        /// <summary>
        /// The "label" of this order, as defined by the Dietz-Sleator algorithm.
        /// </summary>
        private ulong _Label;

        /// <summary>
        /// A node in a Dietz-Sleator circular linked list.
        /// </summary>
        /// <remarks>See https://www.cs.cmu.edu/~sleator/papers/maintaining-order.pdf for details</remarks>
        private sealed class _ListNode
        {
            /// <summary>
            /// The order this node belongs to. If this has been collected, the node can be removed.
            /// </summary>
            public WeakReference<Order> Order;

            /// <summary>
            /// The next node in the list.
            /// </summary>
            public _ListNode Next;
        }

        /// <summary>
        /// Creates a new order which is unrelated to all existing orders.
        /// </summary>
        public static Order Create()
        {
            // Created circular linked list with two nodes
            _ListNode bNode = new _ListNode();
            _ListNode resNode = new _ListNode();
            bNode.Next = resNode;
            resNode.Next = bNode;

            // Create base order
            Order b = new Order(bNode, 0);
            bNode.Order = new WeakReference<Order>(b);

            // Create result order
            Order res = new Order(b, resNode, ulong.MaxValue / 2);
            resNode.Order = new WeakReference<Order>(res);

            // Don't return until we are sure the list structure is ready
            Thread.MemoryBarrier();
            return res;
        }

        /// <summary>
        /// Tries locking the order and corresponding node following the given node. This assumes the current node
        /// is locked.
        /// </summary>
        private static bool _TryLockNext(_ListNode Current, out Order Order)
        {
            _ListNode next = Current.Next;
            Order = default(Order);
            if (!Monitor.TryEnter(next))
                return false;
            while (!next.Order.TryGetTarget(out Order))
            {
                _ListNode after = next.Next;
                try
                {
                    if (!Monitor.TryEnter(after))
                        return false;
                }
                finally
                {
                    Monitor.Exit(next);
                }
                next = after;
            }
            if (!Monitor.TryEnter(Order))
            {
                Monitor.Exit(next);
                return false;
            }
            Debug.Assert(Order._Node == next);
            return true;
        }

        /// <summary>
        /// Assuming (only) the given order and its corresponding node are locked, this will balance the labels of the
        /// node with its following nodes as described in the Dietz-Sleator paper.
        /// </summary>
        private static void _Balance(ulong StartLabel, Order Order)
        {
            ulong i = 2;
            ulong remGap = 0;
            Stack<Order> toBalance = new Stack<Order>();

            // Traverse list until the gap is over i * i
            Order current = Order;
            while (true)
            {
                Order next;
                if (!_TryLockNext(current._Node, out next))
                    goto balanceBefore;

                ulong gap = unchecked(next._Label - StartLabel);
                remGap = gap;
                if (gap >= i * i)
                {
                    Monitor.Exit(next._Node);
                    Monitor.Exit(next);
                    goto balance;
                }

                toBalance.Push(current);
                Monitor.Exit(current._Node);
                Monitor.Exit(current);
                current = next;
                i++;
            }

        balance:
            // Balances "current" and all nodes currently in the "toBalance" stack. Assumes "current" is locked.
            Debug.Assert(remGap / i > 0);
            remGap -= remGap / i;
            ulong curGap = unchecked(current._Label - StartLabel);
            if (remGap < curGap)
                remGap = curGap;
            else
                current._Label = StartLabel + remGap;

        balanceBefore:
            if (toBalance.Count > 0)
            {
                _ListNode lastNode = current._Node;
                Monitor.Exit(current._Node);
                Monitor.Exit(current);
                Order before = toBalance.Pop();
                Monitor.Enter(before);
                Monitor.Enter(before._Node);
                if (before._Node.Next != lastNode)
                {
                    // List was modified, return prematurely
                    Monitor.Exit(before._Node);
                    Monitor.Exit(before);
                    Monitor.Enter(Order);
                    Monitor.Enter(Order._Node);
                    return;
                }
                i--;
                current = before;
                goto balance;
            }
        }

        /// <summary>
        /// Creates a new node derived from this node with a given relationship.
        /// </summary>
        private Order _CreateDerived(bool IsAfter)
        {
        retry:
            Monitor.Enter(this);
            Monitor.Enter(this._Node);
            ulong thisLabel = this._Label;

        retryWithThisLock:
            Order next;
            if (!_TryLockNext(this._Node, out next))
            {
                Monitor.Exit(this._Node);
                Monitor.Exit(this);
                goto retry;
            }

        retryWithBothLocks:
            ulong diff = unchecked(next._Label - thisLabel);
            if (diff >= 2)
            {
                if (IsAfter)
                {
                    _ListNode middle = new _ListNode();
                    Order res = new Order(this._Base, middle, unchecked(thisLabel + diff / 2));
                    middle.Order = new WeakReference<Order>(res);
                    this._Node.Next = middle;
                    middle.Next = next._Node;

                    Monitor.Exit(next._Node);
                    Monitor.Exit(next);
                    Monitor.Exit(this._Node);
                    Monitor.Exit(this);
                    return res;
                }
                else
                {
                    _ListNode middle = new _ListNode();
                    Order res = new Order(this._Base, this._Node, unchecked(thisLabel));
                    this._Label = thisLabel + diff / 2;
                    middle.Order = this._Node.Order;
                    this._Node = middle;
                    res._Node.Order = new WeakReference<Order>(res);
                    res._Node.Next = middle;
                    middle.Next = next._Node;

                    Monitor.Exit(next._Node);
                    Monitor.Exit(next);
                    Monitor.Exit(res._Node);
                    Monitor.Exit(this);
                    return res;
                }
            }
            else
            {
                Monitor.Exit(this._Node);
                Monitor.Exit(this);
                _Balance(thisLabel, next);
                if (!Monitor.TryEnter(this))
                {
                    Monitor.Exit(next._Node);
                    Monitor.Exit(next);
                    goto retry;
                }
                Monitor.Enter(this._Node);
                thisLabel = this._Label;
                if (this._Node.Next != next._Node)
                {
                    Monitor.Exit(next._Node);
                    Monitor.Exit(next);
                    goto retryWithThisLock;
                }
                goto retryWithBothLocks;
            }
        }

        /// <summary>
        /// Creates a new order immediately after this one.
        /// </summary>
        public Order After()
        {
            return this._CreateDerived(true);
        }

        /// <summary>
        /// Creates a new order immediately before this one.
        /// </summary>
        public Order Before()
        {
            return this._CreateDerived(false);
        }

        /// <summary>
        /// Safetly gets a lock on two orders.
        /// </summary>
        private static void _LockTwo(Order A, Order B)
        {
            while (true)
            {
                Monitor.Enter(A);
                if (Monitor.TryEnter(B)) return;
                Monitor.Exit(A);
            }
        }

        public static bool operator <(Order A, Order B)
        {
            _LockTwo(A, B);
            try
            {
                Debug.Assert(A._Base == B._Base);
                ulong baseLabel = A._Base._Label;
                return unchecked(A._Label - baseLabel < B._Label - baseLabel);
            }
            finally
            {
                Monitor.Exit(A);
                Monitor.Exit(B);
            }
        }

        public static bool operator >(Order A, Order B)
        {
            _LockTwo(A, B);
            try
            {
                Debug.Assert(A._Base == B._Base);
                ulong baseLabel = A._Base._Label;
                return unchecked(A._Label - baseLabel > B._Label - baseLabel);
            }
            finally
            {
                Monitor.Exit(A);
                Monitor.Exit(B);
            }
        }

        public static bool operator <=(Order A, Order B)
        {
            return A == B || A < B;
        }

        public static bool operator >=(Order A, Order B)
        {
            return A == B || A > B;
        }

        public override string ToString()
        {
            return String.Format("<0x{0:x16}>", (this._Label - this._Base._Label));
        }
    }
}
