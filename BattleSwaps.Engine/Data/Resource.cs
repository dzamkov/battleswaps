﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using BattleSwaps.Engine.Util;

namespace BattleSwaps.Engine.Data
{
    /// <summary>
    /// The non-parameteric base class for <see cref="Resource{T}"/>
    /// </summary>
    public abstract class Resource
    {
        internal uint _Usages;
        internal ResourceState _State;
        protected Resource()
        {
            this._Usages = 0;
            this._State = ResourceState.Waiting;
        }

        /// <summary>
        /// Begins loading the object for this resource. This should eventually call
        /// <see cref="Resource{T}.Provide"/> to provide the loaded object.
        /// </summary>
        protected abstract void Load();

        /// <summary>
        /// Prepares this resource to receive an object. If this returns true, a call to
        /// <see cref="Resource{T}.Provide"/> is required to follow.
        /// </summary>
        protected bool Prepare()
        {
            lock (this)
            {
                if (this._State == ResourceState.Waiting)
                {
                    this._State = ResourceState.Loading;
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Releases a usage of this resource.
        /// </summary>
        internal void _Release()
        {
            lock (this)
            {
                Debug.Assert(this._State == ResourceState.Loading || this._State == ResourceState.Available);
                Debug.Assert(this._Usages > 0);
                this._Usages--;

                // TODO: Garbage collection for unused available resources
            }
        }
    }

    /// <summary>
    /// Identifies the general state of a resource.
    /// </summary>
    public enum ResourceState
    {
        /// <summary>
        /// The resource is not ready for use and has not been requested.
        /// </summary>
        Waiting,

        /// <summary>
        /// The resource has been requested, but is not ready for use.
        /// </summary>
        Loading,

        /// <summary>
        /// The resource is available for use.
        /// </summary>
        Available
    }

    /// <summary>
    /// Identifies an immutable object of the given type that may not yet be loaded into memory or ready for use,
    /// but can be made available on demand.
    /// </summary>
    public abstract class Resource<T> : Resource
    {
        private T _Object;
        protected Resource()
        { }

        /// <summary>
        /// Gets a usage for this resource. This will request the object for the resource to be loaded if it
        /// is not yet available.
        /// </summary>
        public Usage<T> Use()
        {
            bool needRequest = true;
            lock (this)
            {
                this._Usages++;
                if (this._State == ResourceState.Waiting)
                {
                    this._State = ResourceState.Loading;
                    needRequest = true;
                }
            }
            if (needRequest)
                this.Load();
            return new Usage<T>(this);
        }

        /// <summary>
        /// Gets the object for this resource. This assumes that the object can not be disposed, as it may become
        /// invalid otherwise.
        /// </summary>
        public T Peek()
        {
            using (Usage<T> usage = this.Use())
            {
                return usage.Wait();
            }
        }

        /// <summary>
        /// Provides an object for this resource.
        /// </summary>
        protected void Provide(T Object)
        {
            lock (this)
            {
                if (this._State == ResourceState.Loading)
                {
                    this._Object = Object;
                    this._State = ResourceState.Available;
                    Monitor.PulseAll(this);
                }
            }
        }

        /// <summary>
        /// Waits until the object for this resource becomes available. This requires the resource to have at least
        /// one usage.
        /// </summary>
        internal T _Wait()
        {
            lock (this)
            {
                Debug.Assert(this._Usages > 0);
                while (this._State == ResourceState.Loading)
                    Monitor.Wait(this);
                Debug.Assert(this._State == ResourceState.Available);
                return this._Object;
            }
        }
    }

    /// <summary>
    /// A reference to the object for a resource. This must be disposed after use.
    /// </summary>
    public struct Usage<T> : IDisposable
    {
        public Usage(Resource<T> Resource)
        {
            this.Resource = Resource;
        }

        /// <summary>
        /// The resource for this usage.
        /// </summary>
        public Resource<T> Resource;

        /// <summary>
        /// Indicates whether this is a valid resource usage.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this.Resource != null;
            }
        }

        /// <summary>
        /// A special usage value indicating an invalid reference to a resource.
        /// </summary>
        public static readonly Usage<T> Invalid = new Usage<T>(null);

        /// <summary>
        /// Waits until the object for this usage becomes available, and then returns it.
        /// </summary>
        public T Wait()
        {
            return this.Resource._Wait();
        }

        /// <summary>
        /// Releases this resource reference.
        /// </summary>
        public void Dispose()
        {
            if (this.Resource != null)
            {
                this.Resource._Release();
                this.Resource = null;
            }
        }
    }
}