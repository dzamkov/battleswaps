﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;

namespace BattleSwaps.Engine.Data
{
    /// <summary>
    /// Describes the information needed to serialize a struct or sealed class.
    /// </summary>
    public struct StructLayout
    {
        public Field[] Fields;
        public StructLayout(Field[] Fields)
        {
            this.Fields = Fields;
        }

        /// <summary>
        /// Describes a field to be considered in a struct layout.
        /// </summary>
        public struct Field
        {
            public readonly FieldInfo Info;
            public readonly Serializer Serializer;
            public Field(FieldInfo Info, Serializer Serializer)
            {
                this.Info = Info;
                this.Serializer = Serializer;
            }
        }

        /// <summary>
        /// Creates a struct layout for the given type.
        /// </summary>
        private static StructLayout _Create(Type Type)
        {
            if (Type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic).Length > 0)
                throw new Exception("Struct must have no private fields");
            if (!Type.IsValueType && !Type.IsSealed)
                throw new Exception("Class must be sealed");
            if (!Type.IsValueType && Type.GetConstructor(Type.EmptyTypes) == null)
                throw new Exception("Class must have a public default constructor");
            FieldInfo[] fieldInfos = Type.GetFields(BindingFlags.Instance | BindingFlags.Public);
            Array.Sort(fieldInfos, (a, b) => a.MetadataToken.CompareTo(b.MetadataToken));
            Field[] fields = new Field[fieldInfos.Length];
            for (int i = 0; i < fields.Length; i++)
            {
                Serializer serializer;
                FieldInfo fieldInfo = fieldInfos[i];
                if (!Serializer.TryGetDefault(fieldInfo.FieldType, out serializer))
                    throw new Exception("Inner field does not have a default serializer");
                fields[i] = new Field(fieldInfo, serializer);
            }
            return new StructLayout(fields);
        }

        /// <summary>
        /// Caches the struct layout for a given type.
        /// </summary>
        private static class _Cache<T>
        {
            public static StructLayout Layout = _Create(typeof(T));
        }

        /// <summary>
        /// Gets the struct layout for the given type, asserting that a valid layout exists.
        /// </summary>
        public static StructLayout Get<T>()
        {
            return _Cache<T>.Layout;
        }
    }

    /// <summary>
    /// A serializer which serializes a struct or sealed class as an ordered collection of fields, using the default
    /// serializer for each field.
    /// </summary>
    public sealed class StructSerializer<T> : Serializer<T>
    {
        private StructSerializer()
            : base(StructEqualityComparer<T>.Instance)
        { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly StructSerializer<T> Instance = new StructSerializer<T>();

        public override object ReadUntyped(ByteReader Reader)
        {
            StructLayout layout = StructLayout.Get<T>();
            object obj = Activator.CreateInstance(typeof(T));
            for (int i = 0; i < layout.Fields.Length; i++)
            {
                StructLayout.Field field = layout.Fields[i];
                field.Info.SetValue(obj, field.Serializer.ReadUntyped(Reader));
            }
            return obj;
        }

        public override void WriteUntyped(ByteWriter Writer, object Value)
        {
            StructLayout layout = StructLayout.Get<T>();
            Debug.Assert(Value.GetType() == typeof(T));
            for (int i = 0; i < layout.Fields.Length; i++)
            {
                StructLayout.Field field = layout.Fields[i];
                field.Serializer.WriteUntyped(Writer, field.Info.GetValue(Value));
            }
        }

        public override T Read(ByteReader Reader)
        {
            return (T)this.ReadUntyped(Reader);
        }

        public override void Write(ByteWriter Writer, T Value)
        {
            this.WriteUntyped(Writer, (object)Value);
        }
    }

    /// <summary>
    /// An equality comparer which compares structs or sealed classes by examining them as an ordered collection of
    /// fields, using the default equality comparer for each field.
    /// </summary>
    public sealed class StructEqualityComparer<T> : EqualityComparer<T>
    {
        private StructEqualityComparer() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly StructEqualityComparer<T> Instance = new StructEqualityComparer<T>();

        public override bool Equals(T A, T B)
        {
            StructLayout layout = StructLayout.Get<T>();
            for (int i = 0; i < layout.Fields.Length; i++)
            {
                StructLayout.Field field = layout.Fields[i];
                if (!field.Serializer.Comparer.Equals(field.Info.GetValue(A), field.Info.GetValue(B)))
                    return false;
            }
            return true;
        }

        public override int GetHashCode(T Obj)
        {
            StructLayout layout = StructLayout.Get<T>();
            unchecked
            {
                uint hash = 2147285903;
                for (int i = 0; i < layout.Fields.Length; i++)
                {
                    StructLayout.Field field = layout.Fields[i];
                    hash ^= (uint)field.Serializer.Comparer.GetHashCode(field.Info.GetValue(Obj));
                    hash *= 16777619;
                }
                return (int)hash;
            }
        }
    }
}
