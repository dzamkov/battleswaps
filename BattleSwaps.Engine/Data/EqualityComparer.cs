﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Engine.Data
{
    /// <summary>
    /// Contains functions for getting and constructing equality comparers.
    /// </summary>
    public static class EqualityComparer
    {
        /// <summary>
        /// Gets an equality comparer that uses the <see cref="IEquatable{T}"/> interface.
        /// </summary>
        public static EqualityComparer<T> Equatable<T>()
            where T : IEquatable<T>
        {
            return EquatableComparer<T>.Instance;
        }

        /// <summary>
        /// Gets an equality comparer which considers all values equal.
        /// </summary>
        public static EqualityComparer<T> Trivial<T>()
        {
            return TrivialEqualityComparer<T>.Instance;
        }

        /// <summary>
        /// Gets an equality comparer which compares objects by reference.
        /// </summary>
        public static EqualityComparer<T> Reference<T>()
            where T : class
        {
            return ReferenceEqualityComparer<T>.Instance;
        }

        /// <summary>
        /// Gets an equality comparer which compares structs and sealed classes field by field.
        /// </summary>
        public static EqualityComparer<T> Struct<T>()
        {
            return StructEqualityComparer<T>.Instance;
        }

        /// <summary>
        /// Gets an equality comparer which compares immutable objects based on a set of fields and properties from
        /// which they may be constructed.
        /// </summary>
        public static EqualityComparer<T> Immutable<T>()
        {
            return ImmutableEqualityComparer<T>.Instance;
        }

        /// <summary>
        /// Constructs an equality comparer which compares enums by casting them to their underlying integral type.
        /// </summary>
        public static EqualityComparer<T> Enum<T, TBase>(IEqualityComparer<TBase> Base)
            where T : struct
            where TBase : struct
        {
            return new EnumEqualityComparer<T, TBase>(Base);
        }

        /// <summary>
        /// Constructs an equality comparer which compares arrays item-by-item.
        /// </summary>
        public static EqualityComparer<T[]> Array<T>(IEqualityComparer<T> Item)
        {
            return new ArrayEqualityComparer<T>(Item);
        }

        /// <summary>
        /// Constructs an equality comparer which compares nullable values.
        /// </summary>
        public static EqualityComparer<T?> Nullable<T>(IEqualityComparer<T> Inner)
            where T : struct
        {
            return new NullableEqualityComparer<T>(Inner);
        }
    }

    /// <summary>
    /// An equality comparer that uses the <see cref="IEquatable{T}"/> interface.
    /// </summary>
    public sealed class EquatableComparer<T> : EqualityComparer<T>
        where T : IEquatable<T>
    {
        private EquatableComparer() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly EquatableComparer<T> Instance = new EquatableComparer<T>();

        public override bool Equals(T A, T B)
        {
            return A.Equals(B);
        }

        public override int GetHashCode(T Obj)
        {
            return Obj.GetHashCode();
        }
    }

    /// <summary>
    /// An equality comparer which considers all objects equals.
    /// </summary>
    public sealed class TrivialEqualityComparer<T> : EqualityComparer<T>
    {
        private TrivialEqualityComparer() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly TrivialEqualityComparer<T> Instance = new TrivialEqualityComparer<T>();

        public override bool Equals(T A, T B)
        {
            return true;
        }

        public override int GetHashCode(T Obj)
        {
            return 0;
        }
    }

    /// <summary>
    /// An equality comparer which compares objects by reference.
    /// </summary>
    public sealed class ReferenceEqualityComparer<T> : EqualityComparer<T>
        where T : class
    {
        private ReferenceEqualityComparer() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly ReferenceEqualityComparer<T> Instance = new ReferenceEqualityComparer<T>();

        public override bool Equals(T A, T B)
        {
            return object.ReferenceEquals(A, B);
        }

        public override int GetHashCode(T Obj)
        {
            return System.Runtime.CompilerServices.RuntimeHelpers.GetHashCode(Obj);
        }
    }

    /// <summary>
    /// A serializer for an enum value.
    /// </summary>
    /// <typeparam name="TBase">The underlying integral type for the enum.</typeparam>
    public sealed class EnumEqualityComparer<T, TBase> : EqualityComparer<T>
        where T : struct
        where TBase : struct
    {
        public EnumEqualityComparer(IEqualityComparer<TBase> Base)
        {
            this.Base = Base;
        }

        /// <summary>
        /// The equality comparer for the underlying integral type for the enum.
        /// </summary>
        public IEqualityComparer<TBase> Base;

        public override bool Equals(T A, T B)
        {
            return this.Base.Equals((TBase)(object)A, (TBase)(object)B);
        }

        public override int GetHashCode(T Obj)
        {
            return this.Base.GetHashCode((TBase)(object)Obj);
        }
    }

    /// <summary>
    /// An item-by-item equality comparer for arrays.
    /// </summary>
    public sealed class ArrayEqualityComparer<T> : EqualityComparer<T[]>
    {
        public ArrayEqualityComparer(IEqualityComparer<T> Item)
        {
            this.Item = Item;
        }

        /// <summary>
        /// The equality comparer for items.
        /// </summary>
        public readonly IEqualityComparer<T> Item;

        public override bool Equals(T[] A, T[] B)
        {
            if (A.Length != B.Length)
                return false;
            for (int i = 0; i < A.Length; i++)
                if (!this.Item.Equals(A[i], B[i]))
                    return false;
            return true;
        }

        public override int GetHashCode(T[] Obj)
        {
            unchecked
            {
                uint hash = 2166136261;
                for (int i = 0; i < Obj.Length; i++)
                {
                    hash ^= (uint)Obj.GetHashCode();
                    hash *= 16777619;
                }
                return (int)hash;
            }
        }
    }

    /// <summary>
    /// An equality comparer for nullable values.
    /// </summary>
    public sealed class NullableEqualityComparer<T> : EqualityComparer<T?>
        where T : struct
    {
        public NullableEqualityComparer(IEqualityComparer<T> Inner)
        {
            this.Inner = Inner;
        }

        /// <summary>
        /// The equality comparer for the inner value.
        /// </summary>
        public readonly IEqualityComparer<T> Inner;

        public override bool Equals(T? A, T? B)
        {
            if (A.HasValue)
            {
                if (B.HasValue)
                    return this.Inner.Equals(A.Value, B.Value);
                else
                    return false;
            }
            else
            {
                return !B.HasValue;
            }
        }

        public override int GetHashCode(T? Obj)
        {
            if (Obj.HasValue)
                return this.Inner.GetHashCode(Obj.Value);
            else
                return unchecked((int)2652284429);
        }
    }
}
