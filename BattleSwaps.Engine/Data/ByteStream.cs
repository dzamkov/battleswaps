﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Engine.Data
{
    /// <summary>
    /// A streaming interface which allows the reading of bytes one-by-one. This interface is not thread-safe.
    /// </summary>
    public abstract class ByteReader
    {
        /// <summary>
        /// Gets the next byte from this reader, and advances the reader by one byte.
        /// </summary>
        public abstract byte ReadByte();

        /// <summary>
        /// Reads a 16-bit unsigned integer from this reader, and advances the reader beyond the value.
        /// </summary>
        public virtual ushort ReadUInt16()
        {
            unchecked
            {
                return (ushort)(
                    (this.ReadByte() << 0) |
                    (this.ReadByte() << 8));
            }
        }

        /// <summary>
        /// Reads a 16-bit signed integer from this reader, and advances the reader beyond the value.
        /// </summary>
        public short ReadInt16()
        {
            unchecked
            {
                return (short)this.ReadUInt16();
            }
        }

        /// <summary>
        /// Reads a 16-bit character from this reader, and advances the reader beyond the value.
        /// </summary>
        public char ReadChar()
        {
            return Convert.ToChar(this.ReadUInt16());
        }

        /// <summary>
        /// Reads a 32-bit unsigned integer from this reader, and advances the reader beyond the value.
        /// </summary>
        public virtual uint ReadUInt32()
        {
            unchecked
            {
                return (uint)(
                    (this.ReadByte() << 0) |
                    (this.ReadByte() << 8) |
                    (this.ReadByte() << 16) |
                    (this.ReadByte() << 24));
            }
        }

        /// <summary>
        /// Reads a 32-bit signed integer from this reader, and advances the reader beyond the value.
        /// </summary>
        public int ReadInt32()
        {
            unchecked
            {
                return (int)this.ReadUInt32();
            }
        }

        /// <summary>
        /// Reads a 64-bit unsigned integer from this reader, and advances the reader beyond the value.
        /// </summary>
        public virtual ulong ReadUInt64()
        {
            unchecked
            {
                return (uint)(
                    (this.ReadByte() << 0) |
                    (this.ReadByte() << 8) |
                    (this.ReadByte() << 16) |
                    (this.ReadByte() << 24) |
                    (this.ReadByte() << 32) |
                    (this.ReadByte() << 40) |
                    (this.ReadByte() << 48) |
                    (this.ReadByte() << 56));
            }
        }

        /// <summary>
        /// Reads a 64-bit signed integer from this reader, and advances the reader beyond the value.
        /// </summary>
        public long ReadInt64()
        {
            unchecked
            {
                return (long)this.ReadUInt64();
            }
        }

        /// <summary>
        /// Reads a string from this reader, and advances the reader beyond the value.
        /// </summary>
        public virtual string ReadString()
        {
            ushort length = this.ReadUInt16();
            char[] chars = new char[length];
            for (ushort i = 0; i < length; i++)
                chars[i] = this.ReadChar();
            return new string(chars);
        }

        /// <summary>
        /// Reads a double from this reader, and advances the reader beyond the value.
        /// </summary>
        public virtual double ReadDouble()
        {
            // TODO: Remove this as it isn't cross platform
            return BitConverter.Int64BitsToDouble(this.ReadInt64());
        }

        /// <summary>
        /// Reads a value of the given type from this reader using the given serializer, and advances the reader
        /// beyond the value.
        /// </summary>
        public T Read<T>(Serializer<T> Serializer)
        {
            return Serializer.Read(this);
        }
    }

    /// <summary>
    /// A streaming interface which allows the writing of bytes one-by-one. This interface is not thread-safe.
    /// </summary>
    public abstract class ByteWriter
    {
        /// <summary>
        /// Writes a byte using this writer.
        /// </summary>
        public abstract void WriteByte(byte Value);

        /// <summary>
        /// Writes a 16-bit unsigned integer to this writer.
        /// </summary>
        public virtual void WriteUInt16(ushort Value)
        {
            unchecked
            {
                this.WriteByte((byte)Value);
                this.WriteByte((byte)(Value >> 8));
            }
        }

        /// <summary>
        /// Writes a 16-bit signed integer to this writer.
        /// </summary>
        public void WriteInt16(short Value)
        {
            unchecked
            {
                this.WriteUInt16((ushort)Value);
            }
        }

        /// <summary>
        /// Writes a 16-bit character to this writer.
        /// </summary>
        public void WriteChar(char Value)
        {
            this.WriteUInt16(Convert.ToUInt16(Value));
        }

        /// <summary>
        /// Writes a 32-bit unsigned integer to this writer.
        /// </summary>
        public virtual void WriteUInt32(uint Value)
        {
            unchecked
            {
                this.WriteByte((byte)(Value >> 0));
                this.WriteByte((byte)(Value >> 8));
                this.WriteByte((byte)(Value >> 16));
                this.WriteByte((byte)(Value >> 24));
            }
        }

        /// <summary>
        /// Writes a 32-bit signed integer to this writer.
        /// </summary>
        public void WriteInt32(int Value)
        {
            unchecked
            {
                this.WriteUInt32((uint)Value);
            }
        }

        /// <summary>
        /// Writes a 64-bit unsigned integer to this writer.
        /// </summary>
        public virtual void WriteUInt64(ulong Value)
        {
            unchecked
            {
                this.WriteByte((byte)(Value >> 0));
                this.WriteByte((byte)(Value >> 8));
                this.WriteByte((byte)(Value >> 16));
                this.WriteByte((byte)(Value >> 24));
                this.WriteByte((byte)(Value >> 32));
                this.WriteByte((byte)(Value >> 40));
                this.WriteByte((byte)(Value >> 48));
                this.WriteByte((byte)(Value >> 56));
            }
        }

        /// <summary>
        /// Writes a 64-bit signed integer to this writer.
        /// </summary>
        public void WriteInt64(long Value)
        {
            unchecked
            {
                this.WriteUInt64((ulong)Value);
            }
        }

        /// <summary>
        /// Writes a string to this writer.
        /// </summary>
        public virtual void WriteString(string Value)
        {
            Debug.Assert(Value.Length <= ushort.MaxValue);
            this.WriteUInt16((ushort)Value.Length);
            for (int i = 0; i < Value.Length; i++)
                this.WriteChar(Value[i]);
        }

        /// <summary>
        /// Writes a double to this writer.
        /// </summary>
        public virtual void WriteDouble(double Value)
        {
            // TODO: Remove this as it isn't cross platform
            this.WriteInt64(BitConverter.DoubleToInt64Bits(Value));
        }

        /// <summary>
        /// Reads a value of the given type to this writer using the given serializer.
        /// </summary>
        public void Write<T>(Serializer<T> Serializer, T Value)
        {
            Serializer.Write(this, Value);
        }
    }

    /// <summary>
    /// A byte writer which generates a 64-bit hash of the byte sequence written to it.
    /// </summary>
    public sealed class HashByteWriter : ByteWriter
    {
        public HashByteWriter(ulong Initial)
        {
            this.Hash = Initial;
        }

        public HashByteWriter()
            : this(14695981039346656037)
        { }

        /// <summary>
        /// The current hash for this writer.
        /// </summary>
        public ulong Hash;

        public override void WriteByte(byte Value)
        {
            // From https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
            unchecked
            {
                this.Hash *= (ulong)1099511628211;
                this.Hash ^= Value;
            }
        }
    }
}
