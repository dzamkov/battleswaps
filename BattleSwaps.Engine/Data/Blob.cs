﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.IO;

using BattleSwaps.Engine.Util;

namespace BattleSwaps.Engine.Data
{
    /// <summary>
    /// Describes raw binary data without interpretation that is available through the resource system.
    /// </summary>
    public struct Blob : IDisposable
    {
        public Blob(Stream Stream)
        {
            this.Stream = Stream;
        }

        /// <summary>
        /// The stream containing the data for this blob.
        /// </summary>
        public Stream Stream;

        /// <summary>
        /// Performs an action that accesses this data with a stream. The stream will initially be at the
        /// beginning of the data.
        /// </summary>
        public void Use(Action<Stream> Action)
        {
            lock (this.Stream)
            {
                this.Stream.Seek(0, SeekOrigin.Begin);
                Action(this.Stream);
            }
        }

        /// <summary>
        /// Performs an action that accesses this data, interpreting it as text.
        /// </summary>
        public void Use(Encoding Encoding, Action<TextReader> Action)
        {
            this.Use(delegate(Stream stream)
            {
                // Don't dispose reader, since we don't want to close the underlying stream
                var reader = new StreamReader(stream, Encoding ?? Encoding.Default);
                Action(reader);
            });
        }

        /// <summary>
        /// Interprets this data as text.
        /// </summary>
        public string GetText(Encoding Encoding)
        {
            string text = null;
            this.Use(Encoding, delegate(TextReader reader)
            {
                text = reader.ReadToEnd();
            });
            return text;
        }

        /// <summary>
        /// Gets a representation of this binary data as an array of bytes.
        /// </summary>
        public byte[] Bytes
        {
            get
            {
                byte[] result = null;
                this.Use(delegate(Stream stream)
                {
                    result = stream.AsBytes();
                });
                return result;
            }
        }


        /// <summary>
        /// Interprets this data as text.
        /// </summary>
        public string Text
        {
            get
            {
                return this.GetText(null);
            }
        }

        /// <summary>
        /// The base file path to find blobs at.
        /// </summary>
        private static string _BasePath = ".";

        /// <summary>
        /// A resource for a blob loaded from an external source by its path.
        /// </summary>
        [Tag(typeof(Resource<Blob>), (uint)2283804372)]
        public sealed class LoadResource : Resource<Blob>
        {
            [DefaultSerializer]
            public static readonly Serializer<LoadResource> Serializer =
                Data.Serializer.Immutable<LoadResource>();
            private static readonly WeakInstanceTable<LoadResource> _Instances =
                new WeakInstanceTable<LoadResource>(EqualityComparer.Immutable<LoadResource>());
            private LoadResource(AbsolutePath Path)
            {
                this.Path = Path;
            }

            /// <summary>
            /// The path of the blob to load.
            /// </summary>
            public readonly AbsolutePath Path;

            /// <summary>
            /// Gets the load resource for the given path.
            /// </summary>
            public static LoadResource Get(AbsolutePath Path)
            {
                return _Instances.Coalesce(new LoadResource(Path));
            }

            protected override void Load()
            {
                string fullPath = this.Path.GetIOPathString(_BasePath);
                Stream stream = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                this.Provide(new Blob(stream));
            }
        }

        /// <summary>
        /// Gets a blob resource that is loaded from an external source by its path.
        /// </summary>
        public static Resource<Blob> Load(AbsolutePath Path)
        {
            return LoadResource.Get(Path);
        }

        /// <summary>
        /// Deletes the underlying stream for the blob.
        /// </summary>
        public void Dispose()
        {
            this.Stream.Dispose();
        }

        public static implicit operator byte[](Blob Source)
        {
            return Source.Bytes;
        }
    }

    /// <summary>
    /// Identifies an absolute path from which a resource blob can be loaded.
    /// </summary>
    public struct AbsolutePath
    {
        [DefaultSerializer]
        public static readonly Serializer<AbsolutePath> Serializer =
            StructSerializer<AbsolutePath>.Instance;
        public AbsolutePath(string[] Parts)
        {
            this.Parts = Parts;
        }

        /// <summary>
        /// The parts of this path.
        /// </summary>
        public string[] Parts;

        /// <summary>
        /// Constructs a path string for use on the current system.
        /// </summary>
        /// <param name="Base">The base system path to apply this path to.</param>
        public string GetIOPathString(string Base)
        {
            return Path.Combine(Base, Path.Combine(this.Parts));
        }

        /// <summary>
        /// Converts a path string into an absolute path, using '/' as a delimiter.
        /// </summary>
        public static implicit operator AbsolutePath(string PathString)
        {
            return new AbsolutePath(PathString.Split('/'));
        }

        /// <summary>
        /// Extends an absolute path.
        /// </summary>
        public static AbsolutePath operator /(AbsolutePath A, string B)
        {
            return A / (RelativePath)B;
        }
    }

    /// <summary>
    /// Identifies a relative path from which a resource blob can be loaded.
    /// </summary>
    public struct RelativePath
    {
        [DefaultSerializer]
        public static readonly Serializer<RelativePath> Serializer =
            StructSerializer<RelativePath>.Instance;
        public RelativePath(uint Up, string[] Parts)
        {
            this.Up = Up;
            this.Parts = Parts;
        }

        /// <summary>
        /// Indicates how many levels to go up before applying this path.
        /// </summary>
        public uint Up;

        /// <summary>
        /// The parts of this path.
        /// </summary>
        public string[] Parts;

        /// <summary>
        /// Indicates the value used for <see cref="Up"/> to indicate that this relative path should start from
        /// the root.
        /// </summary>
        /// <remarks>This isn't <see cref="uint.MaxValue"/> in order to avoid overflow issues.</remarks>
        private static readonly uint _RootUp = uint.MaxValue / 2;

        /// <summary>
        /// A relative path which navigates to the root directory.
        /// </summary>
        public static readonly RelativePath Root = new RelativePath(_RootUp, Array.Empty<string>());

        /// <summary>
        /// A relative path which does no navigation, leaving the current directory unchanged.
        /// </summary>
        public static readonly RelativePath Identity = new RelativePath(0, Array.Empty<string>());

        /// <summary>
        /// A relative path which goes up one directory.
        /// </summary>
        public static readonly RelativePath GoUp = new RelativePath(1, Array.Empty<string>());

        /// <summary>
        /// Converts a path string into an relative path, using '/' as a delimiter, "~" as the marker for the root
        /// path, ".." for up one path, "..." for up two paths, and so on.
        /// </summary>
        public static implicit operator RelativePath(string PathString)
        {
            uint up = 0;
            List<string> parts = new List<string>();
            foreach (string part in PathString.Split('/'))
            {
                if (part == "~")
                {
                    up = _RootUp;
                    parts.Clear();
                    continue;
                }
                else if (part.StartsWith("."))
                {
                    for (int i = 1; i < part.Length; i++)
                    {
                        if (part[i] != '.')
                            goto normal;
                    }
                    uint upAmount = (uint)part.Length - 1;
                    if (parts.Count <= upAmount)
                    {
                        up += upAmount - (uint)parts.Count;
                        parts.Clear();
                    }
                    else
                    {
                        parts.RemoveRange(parts.Count - (int)upAmount, (int)upAmount);
                    }
                    continue;
                }
            normal: parts.Add(part);
            }
            return new RelativePath(up, parts.ToArray());
        }

        /// <summary>
        /// Extends a relative path.
        /// </summary>
        public static RelativePath operator /(RelativePath A, string B)
        {
            return A / (RelativePath)B;
        }

        /// <summary>
        /// Combines two relative paths.
        /// </summary>
        public static RelativePath operator /(RelativePath A, RelativePath B)
        {
            if (B.Up < A.Parts.Length)
            {
                uint diff = (uint)(A.Parts.Length - B.Up);
                string[] nParts = new string[diff + B.Parts.Length];
                Array.Copy(A.Parts, 0, nParts, 0, diff);
                Array.Copy(B.Parts, 0, nParts, diff, B.Parts.Length);
                return new RelativePath(A.Up, nParts);
            }
            else
            {
                return new RelativePath((uint)(A.Up + B.Up - A.Parts.Length), B.Parts);
            }
        }

        /// <summary>
        /// Combines an absolute paths with a relative path.
        /// </summary>
        public static AbsolutePath operator /(AbsolutePath A, RelativePath B)
        {
            if (B.Up < A.Parts.Length)
            {
                uint diff = (uint)(A.Parts.Length - B.Up);
                string[] nParts = new string[diff + B.Parts.Length];
                Array.Copy(A.Parts, 0, nParts, 0, diff);
                Array.Copy(B.Parts, 0, nParts, diff, B.Parts.Length);
                return new AbsolutePath(nParts);
            }
            else
            {
                return new AbsolutePath(B.Parts);
            }
        }
    }

    /// <summary>
    /// Contains extension functions to Stream.
    /// </summary>
    public static class StreamEx
    {
        /// <summary>
        /// Copies the contents of this stream to the given target stream.
        /// </summary>
        /// <remarks>This is a method in .Net 4.0, but we are targeting 3.5</remarks>
        public static void CopyTo(this Stream Source, Stream Target)
        {
            byte[] buffer = new byte[16 * 1024];
            int read;
            while ((read = Source.Read(buffer, 0, buffer.Length)) > 0)
                Target.Write(buffer, 0, read);
        }

        /// <summary>
        /// Reads the contents of this stream as a byte array.
        /// </summary>
        public static byte[] AsBytes(this Stream Source)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                Source.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
}
