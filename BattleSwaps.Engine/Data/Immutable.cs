﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace BattleSwaps.Engine.Data
{
    /// <summary>
    /// Describes the information needed to serialize an immutable object.
    /// </summary>
    public struct ImmutableLayout
    {
        public Func<object[], object> Getter;
        public Parameter[] Parameters;
        public ImmutableLayout(Func<object[], object> Getter, Parameter[] Parameters)
        {
            this.Getter = Getter;
            this.Parameters = Parameters;
        }

        /// <summary>
        /// Describes a parameter to be considered in an immutable serializer.
        /// </summary>
        public struct Parameter
        {
            public readonly MemberInfo Info;
            public readonly Serializer Serializer;
            public Parameter(MemberInfo Info, Serializer Serializer)
            {
                this.Info = Info;
                this.Serializer = Serializer;
            }
        }

        /// <summary>
        /// Tries creating an immutable layout based on the given getter, which takes the given parameters.
        /// </summary>
        private static bool _TryCreate(
            Type Type, Func<object[], object> Getter,
            ParameterInfo[] ParameterInfos,
            out ImmutableLayout Layout)
        {
            Layout = default(ImmutableLayout);
            Parameter[] parameters = new Parameter[ParameterInfos.Length];
            for (int i = 0; i < parameters.Length; i++)
            {
                Serializer serializer;
                ParameterInfo parameterInfo = ParameterInfos[i];
                if (!Serializer.TryGetDefault(parameterInfo.ParameterType, out serializer))
                    return false;

                FieldInfo field = Type.GetField(parameterInfo.Name);
                PropertyInfo property = Type.GetProperty(parameterInfo.Name);
                if (field != null && field.FieldType == parameterInfo.ParameterType)
                    parameters[i] = new Parameter(field, serializer);
                else if (property != null && property.PropertyType == parameterInfo.ParameterType)
                    parameters[i] = new Parameter(property, serializer);
                else
                    return false;
            }
            Layout = new ImmutableLayout(Getter, parameters);
            return true;
        }

        /// <summary>
        /// Creates an immutable layout for the given type.
        /// </summary>
        private static ImmutableLayout _Create(Type Type)
        {
            ImmutableLayout layout;

            // Search for getter method
            var getterMethod = Type.GetMethod("Get", BindingFlags.Static | BindingFlags.Public);
            if (getterMethod != null && getterMethod.ReturnType == Type)
            {
                ParameterInfo[] parameterInfos = getterMethod.GetParameters();
                Func<object[], object> getter = parameters => getterMethod.Invoke(null, parameters);
                if (_TryCreate(Type, getter, parameterInfos, out layout))
                    return layout;
            }

            // Search constructors
            foreach (var constructor in Type.GetConstructors())
            {
                ParameterInfo[] parameterInfos = constructor.GetParameters();
                Func<object[], object> getter = parameters => constructor.Invoke(parameters);
                if (_TryCreate(Type, getter, parameterInfos, out layout))
                    return layout;
            }

            throw new Exception("No applicable constructor or getter found");
        }

        /// <summary>
        /// Caches the immutable layout for a given type.
        /// </summary>
        private static class _Cache<T>
        {
            public static ImmutableLayout Layout = _Create(typeof(T));
        }

        /// <summary>
        /// Gets the struct layout for the given type, asserting that a valid layout exists.
        /// </summary>
        public static ImmutableLayout Get<T>()
        {
            return _Cache<T>.Layout;
        }
    }

    /// <summary>
    /// A serializer for immutable objects of the given type which uses a public constructor for which all
    /// parameters are given by a corresponding field or property.
    /// </summary>
    public sealed class ImmutableSerializer<T> : Serializer<T>
    {
        private ImmutableSerializer()
            : base(ImmutableEqualityComparer<T>.Instance)
        { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly ImmutableSerializer<T> Instance = new ImmutableSerializer<T>();

        public override object ReadUntyped(ByteReader Reader)
        {
            ImmutableLayout layout = ImmutableLayout.Get<T>();
            object[] parameters = new object[layout.Parameters.Length];
            for (int i = 0; i < parameters.Length; i++)
                parameters[i] = layout.Parameters[i].Serializer.ReadUntyped(Reader);
            return layout.Getter(parameters);
        }

        public override void WriteUntyped(ByteWriter Writer, object Value)
        {
            ImmutableLayout layout = ImmutableLayout.Get<T>();
            Debug.Assert(Value.GetType() == typeof(T));
            for (int i = 0; i < layout.Parameters.Length; i++)
            {
                ImmutableLayout.Parameter param = layout.Parameters[i];
                if (param.Info is FieldInfo)
                    param.Serializer.WriteUntyped(Writer, ((FieldInfo)param.Info).GetValue(Value));
                else
                    param.Serializer.WriteUntyped(Writer, ((PropertyInfo)param.Info).GetValue(Value, null));
            }
        }

        public override T Read(ByteReader Reader)
        {
            return (T)this.ReadUntyped(Reader);
        }

        public override void Write(ByteWriter Writer, T Value)
        {
            this.WriteUntyped(Writer, (object)Value);
        }
    }

    /// <summary>
    /// An equality comparer which compares immutable objects by examining a set of fields and properties which
    /// are provided by and sufficient to construct the object.
    /// </summary>
    public sealed class ImmutableEqualityComparer<T> : EqualityComparer<T>
    {
        private ImmutableEqualityComparer() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly ImmutableEqualityComparer<T> Instance = new ImmutableEqualityComparer<T>();

        public override bool Equals(T A, T B)
        {
            ImmutableLayout layout = ImmutableLayout.Get<T>();
            for (int i = 0; i < layout.Parameters.Length; i++)
            {
                ImmutableLayout.Parameter param = layout.Parameters[i];
                if (param.Info is FieldInfo)
                {
                    FieldInfo field = (FieldInfo)param.Info;
                    if (!param.Serializer.Comparer.Equals(field.GetValue(A), field.GetValue(B)))
                        return false;
                }
                else
                {
                    PropertyInfo property = (PropertyInfo)param.Info;
                    if (!param.Serializer.Comparer.Equals(property.GetValue(A, null), property.GetValue(B, null)))
                        return false;
                }
            }
            return true;
        }

        public override int GetHashCode(T Obj)
        {
            ImmutableLayout layout = ImmutableLayout.Get<T>();
            unchecked
            {
                uint hash = 2147285903;
                for (int i = 0; i < layout.Parameters.Length; i++)
                {
                    ImmutableLayout.Parameter param = layout.Parameters[i];
                    if (param.Info is FieldInfo)
                    {
                        FieldInfo field = (FieldInfo)param.Info;
                        hash ^= (uint)param.Serializer.Comparer.GetHashCode(field.GetValue(Obj));
                    }
                    else
                    {
                        PropertyInfo property = (PropertyInfo)param.Info;
                        hash ^= (uint)param.Serializer.Comparer.GetHashCode(property.GetValue(Obj, null));
                    }

                    hash *= 16777619;
                }
                return (int)hash;
            }
        }
    }
}
