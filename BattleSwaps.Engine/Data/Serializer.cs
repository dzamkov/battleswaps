﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using BattleSwaps.Engine.Util;

namespace BattleSwaps.Engine.Data
{
    /// <summary>
    /// A serializer for an unspecified set of types or values.
    /// </summary>
    public abstract class Serializer
    {
        public Serializer(IEqualityComparer Comparer)
        {
            this.Comparer = Comparer;
        }

        /// <summary>
        /// The equality comparer associated with this serializer. The serializer is guranteed to preserve equality
        /// relationships (of this comparer) through serialization and deserialization. Note that this does not
        /// imply that equal objects need to serialize to the same byte sequence, only that they need to deserialize
        /// to objects that are equal to each other and the original objects.
        /// </summary>
        public readonly IEqualityComparer Comparer;

        /// <summary>
        /// Reads a value from the given byte reader using this serializer.
        /// </summary>
        public abstract object ReadUntyped(ByteReader Reader);

        /// <summary>
        /// Writes a value to the given byte writer using this serializer.
        /// </summary>
        public abstract void WriteUntyped(ByteWriter Writer, object Value);

        /// <summary>
        /// Describes a set of serializer for types which fit a certain pattern.
        /// </summary>
        private struct _Resolver
        {
            public _Resolver(Type Pattern, Func<Dictionary<Type, Type>, Serializer> Resolve)
            {
                this.Pattern = Pattern;
                this.Resolve = Resolve;
            }

            /// <summary>
            /// The pattern for the types for which this resolver may provide a serializer. This pattern make include
            /// generic type parameters as placeholders for closed types.
            /// </summary>
            public Type Pattern;

            /// <summary>
            /// Given a substitution of generic type parameters in <see cref="Pattern"/> to closed types, returns
            /// a serializer for the resulting instance type, or null if this resolver has no serializer for the
            /// given type.
            /// </summary>
            public Func<Dictionary<Type, Type>, Serializer> Resolve;
        }

        /// <summary>
        /// The set of all known default serializer resolvers.
        /// </summary>
        private static List<_Resolver> _DefaultResolvers = new List<_Resolver>();

        /// <summary>
        /// A cache of all known default type serializers.
        /// </summary>
        private static Dictionary<Type, Serializer> _DefaultSerializers = new Dictionary<Type, Serializer>();

        /// <summary>
        /// Tries getting the default serializer for the given type, returning false if none exists.
        /// </summary>
        public static bool TryGetDefault(Type Type, out Serializer Serializer)
        {
            lock (_DefaultSerializers)
            {
                // Check cache
                if (_DefaultSerializers.TryGetValue(Type, out Serializer))
                    return true;

                // Find resolver
                Serializer = null;
                foreach (var resolver in _DefaultResolvers)
                {
                    Dictionary<Type, Type> subs;
                    if (Util.Reflection.TryMatch(resolver.Pattern, Type, out subs))
                    {
                        Serializer nSerializer = resolver.Resolve(subs);
                        if (Serializer == null) Serializer = nSerializer;
                        else if (nSerializer != null) throw new Exception("Ambigious default serializer for type");
                    }
                }

                // Update cache and return
                if (Serializer != null)
                {
                    _DefaultSerializers[Type] = Serializer;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Given the type of a serializer, gets the type of the thing it serializes. This returns false if the
        /// given type is not a serializer type.
        /// </summary>
        private static bool _TryGetSerializerType(Type SerializerType, out Type InnerType)
        {
            while (!SerializerType.IsGenericType || SerializerType.GetGenericTypeDefinition() != typeof(Serializer<>))
            {
                SerializerType = SerializerType.BaseType;
                if (SerializerType == null)
                {
                    InnerType = default(Type);
                    return false;
                }
            }
            InnerType = SerializerType.GetGenericArguments()[0];
            return true;
        }

        static Serializer()
        {
            Reflection.AssemblyWatch(delegate(Assembly assembly)
            {
                lock (_DefaultSerializers)
                {
                    // Find default serializers
                    foreach (var type in assembly.GetTypes())
                    {
                        Type curType = type;
                        var flags = BindingFlags.Static | BindingFlags.Public | BindingFlags.DeclaredOnly;
                        foreach (var field in type.GetFields(flags))
                        {
                            string fieldName = field.Name;
                            foreach (DefaultSerializerAttribute attr in
                                field.GetCustomAttributes(typeof(DefaultSerializerAttribute), false))
                            {
                                Type innerType;
                                if (!_TryGetSerializerType(field.FieldType, out innerType))
                                    throw new Exception("Field does not contain a serializer");
                                _DefaultResolvers.Add(new _Resolver(innerType, subs =>
                                {
                                    FieldInfo finField = Util.Reflection
                                        .Subsitute(curType, subs)
                                        .GetField(fieldName);
                                    return (Serializer)finField.GetValue(null);
                                }));
                            }
                        }
                        foreach (var property in type.GetProperties(flags))
                        {
                            string propertyName = property.Name;
                            foreach (DefaultSerializerAttribute attr in
                                property.GetCustomAttributes(typeof(DefaultSerializerAttribute), false))
                            {
                                Type innerType;
                                if (!_TryGetSerializerType(property.PropertyType, out innerType))
                                    throw new Exception("Property does not contain a serializer");
                                _DefaultResolvers.Add(new _Resolver(innerType, subs =>
                                {
                                    PropertyInfo finProp = Util.Reflection
                                        .Subsitute(curType, subs)
                                        .GetProperty(propertyName);
                                    return (Serializer)finProp.GetValue(null, null);
                                }));
                            }
                        }
                    }
                }
            });
        }

        #region Primitive Serializers
        public static Serializer<bool> Bool = BoolSerializer.Instance;
        public static Serializer<byte> Byte = ByteSerializer.Instance;
        public static Serializer<uint> UInt32 = UInt32Serializer.Instance;
        public static Serializer<int> Int32 = Int32Serializer.Instance;
        public static Serializer<ulong> UInt64 = UInt64Serializer.Instance;
        public static Serializer<double> Double = DoubleSerializer.Instance;
        public static Serializer<string> String = StringSerializer.Instance;
        #endregion

        /// <summary>
        /// Constructs a serializer which may only serializer the given value.
        /// </summary>
        public static Serializer<T> Trivial<T>(T Value)
        {
            return new TrivialSerializer<T>(Value);
        }

        /// <summary>
        /// Constructs a serializer which may only serialize the default value of the given type.
        /// </summary>
        public static Serializer<T> TrivialDefault<T>()
        {
            return TrivialSerializer<T>.Default;
        }

        /// <summary>
        /// Constructs a serializer which serializes enums by casting them to their underlying integral type.
        /// </summary>
        public static Serializer<T> Enum<T, TBase>(Serializer<TBase> Base)
            where T : struct
            where TBase : struct
        {
            return new EnumSerializer<T, TBase>(Base);
        }

        /// <summary>
        /// Constructs a serializer which serializes arrays item-by-item.
        /// </summary>
        public static Serializer<T[]> Array<T>(Serializer<T> Item, EqualityComparer<T[]> Comparer)
        {
            return new ArraySerializer<T>(Item, Comparer);
        }

        /// <summary>
        /// Constructs a serializer which serializes arrays item-by-item.
        /// </summary>
        public static Serializer<T[]> Array<T>(Serializer<T> Item)
        {
            return new ArraySerializer<T>(Item);
        }

        /// <summary>
        /// Constructs a serializer for nullable values.
        /// </summary>
        public static Serializer<T?> Nullable<T>(Serializer<T> Inner)
            where T : struct
        {
            return new NullableSerializer<T>(Inner);
        }

        /// <summary>
        /// Gets a serializer for structs and sealed classes. The object must have a default public constructor, and
        /// be composed entirely of public fields which all have default serializers.
        /// </summary>
        public static Serializer<T> Struct<T>()
        {
            return StructSerializer<T>.Instance;
        }

        /// <summary>
        /// Gets a serializer for immutable objects. The object must have a public constructor whose parameters all
        /// have default serializers and can all be filled by fields and properties of the object.
        /// </summary>
        public static Serializer<T> Immutable<T>()
        {
            return ImmutableSerializer<T>.Instance;
        }

        /// <summary>
        /// Gets a serializer for types whose subtypes have tags applied with <see cref="TagAttribute"/>. Every
        /// tagged subtype must also have a default serializer.
        /// </summary>
        /// <typeparam name="TTag">The type of tag applied to subtypes.</typeparam>
        public static Serializer<T> Open<TTag, T>()
        {
            return OpenSerializer<TTag, T>.Instance;
        }

        /// <summary>
        /// Gets the default serializer for the given type, or the trivial serializer if none is defined.
        /// </summary>
        public static Serializer<T> Default<T>()
        {
            return Serializer<T>.Default;
        }
    }
    /// <summary>
    /// Describes a method of representing values of the given type as byte sequences of implied length.
    /// </summary>
    public abstract class Serializer<T> : Serializer
    {
        public Serializer(EqualityComparer<T> Comparer)
            : base(Comparer)
        { }

        /// <summary>
        /// Gets the equality comparer associated with this serializer. The serializer is guranteed to preserve
        /// equality relationships (of this comparer) through serialization and deserialization. Note that this does
        /// not imply that equal objects need to serialize to the same byte sequence, only that they need to
        /// deserialize to objects that are equal to each other and the original objects.
        /// </summary>
        public new EqualityComparer<T> Comparer
        {
            get
            {
                return (EqualityComparer<T>)base.Comparer;
            }
        }

        /// <summary>
        /// Reads a value from the given byte reader using this serializer.
        /// </summary>
        public abstract T Read(ByteReader Reader);

        /// <summary>
        /// Writes a value to the given byte writer using this serializer.
        /// </summary>
        public abstract void Write(ByteWriter Writer, T Value);

        /// <summary>
        /// The cache for the default serializer for this type.
        /// </summary>
        private static Serializer<T> _Default;

        /// <summary>
        /// Tries getting the default serializer for the given type, returning false if none exists. This should be
        /// used when a specific serializer can not be specified, such as for fields in automatically-generated
        /// serializers. The only formal requirement of this serializer is that, if the type in question has an
        /// "==" operator, then the equality comparer for the resulting serializer must be consistent with it.
        /// </summary>
        public static bool TryGetDefault(out Serializer<T> Serializer)
        {
            Serializer = _Default;
            if (Serializer != null)
                return true;
            Serializer untyped;
            if (Data.Serializer.TryGetDefault(typeof(T), out untyped))
            {
                _Default = Serializer = (Serializer<T>)untyped;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the default serializer for this type if it is defined, or the trivial serializer otherwise.
        /// </summary>
        public static Serializer<T> Default
        {
            get
            {
                Serializer<T> res;
                if (!TryGetDefault(out res))
                    res = TrivialSerializer<T>.Default;
                return res;
            }
        }

        public override object ReadUntyped(ByteReader Reader)
        {
            return (object)this.Read(Reader);
        }

        public override void WriteUntyped(ByteWriter Writer, object Value)
        {
            this.Write(Writer, (T)Value);
        }
    }

    /// <summary>
    /// Identifies a field or property as containing the default serializer for some type. This serializer should
    /// work on all valid values of the type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class DefaultSerializerAttribute : Attribute
    {

    }

    /// <summary>
    /// A serializer for a boolean value.
    /// </summary>
    public sealed class BoolSerializer : Serializer<bool>
    {
        private BoolSerializer()
            : base(EqualityComparer.Equatable<bool>())
        { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        [DefaultSerializer]
        public static readonly BoolSerializer Instance = new BoolSerializer();

        public override bool Read(ByteReader Reader)
        {
            return Reader.ReadByte() > 0;
        }

        public override void Write(ByteWriter Writer, bool Value)
        {
            Writer.WriteByte(Value ? (byte)0xFF : (byte)0x00);
        }
    }

    /// <summary>
    /// A serializer for a byte.
    /// </summary>
    public sealed class ByteSerializer : Serializer<byte>
    {
        private ByteSerializer()
            : base(EqualityComparer.Equatable<byte>())
        { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        [DefaultSerializer]
        public static readonly ByteSerializer Instance = new ByteSerializer();

        public override byte Read(ByteReader Reader)
        {
            return Reader.ReadByte();
        }

        public override void Write(ByteWriter Writer, byte Value)
        {
            Writer.WriteByte(Value);
        }
    }

    /// <summary>
    /// A serializer for a 32-bit unsigned integer.
    /// </summary>
    public sealed class UInt32Serializer : Serializer<uint>
    {
        private UInt32Serializer()
            : base(EqualityComparer.Equatable<uint>())
        { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        [DefaultSerializer]
        public static readonly UInt32Serializer Instance = new UInt32Serializer();

        public override uint Read(ByteReader Reader)
        {
            return Reader.ReadUInt32();
        }

        public override void Write(ByteWriter Writer, uint Value)
        {
            Writer.WriteUInt32(Value);
        }
    }

    /// <summary>
    /// A serializer for a 32-bit signed integer.
    /// </summary>
    public sealed class Int32Serializer : Serializer<int>
    {
        private Int32Serializer()
            : base(EqualityComparer.Equatable<int>())
        { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        [DefaultSerializer]
        public static readonly Int32Serializer Instance = new Int32Serializer();

        public override int Read(ByteReader Reader)
        {
            return Reader.ReadInt32();
        }

        public override void Write(ByteWriter Writer, int Value)
        {
            Writer.WriteInt32(Value);
        }
    }

    /// <summary>
    /// A serializer for a 64-bit unsigned integer.
    /// </summary>
    public sealed class UInt64Serializer : Serializer<ulong>
    {
        private UInt64Serializer()
            : base(EqualityComparer.Equatable<ulong>())
        { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        [DefaultSerializer]
        public static readonly UInt64Serializer Instance = new UInt64Serializer();

        public override ulong Read(ByteReader Reader)
        {
            return Reader.ReadUInt64();
        }

        public override void Write(ByteWriter Writer, ulong Value)
        {
            Writer.WriteUInt64(Value);
        }
    }

    /// <summary>
    /// A serializer for a double floating point value.
    /// </summary>
    public sealed class DoubleSerializer : Serializer<double>
    {
        private DoubleSerializer()
            : base(EqualityComparer.Equatable<double>())
        { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        [DefaultSerializer]
        public static readonly DoubleSerializer Instance = new DoubleSerializer();

        public override double Read(ByteReader Reader)
        {
            return Reader.ReadDouble();
        }

        public override void Write(ByteWriter Writer, double Value)
        {
            Writer.WriteDouble(Value);
        }
    }

    /// <summary>
    /// A serializer for a string.
    /// </summary>
    public sealed class StringSerializer : Serializer<string>
    {
        private StringSerializer()
            : base(EqualityComparer.Equatable<string>())
        { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        [DefaultSerializer]
        public static readonly StringSerializer Instance = new StringSerializer();

        public override string Read(ByteReader Reader)
        {
            return Reader.ReadString();
        }

        public override void Write(ByteWriter Writer, string Value)
        {
            Writer.WriteString(Value);
        }
    }

    /// <summary>
    /// A serializer which uses trivial equality (everything is equal) as its definition of equality and stores no
    /// data for any object.
    /// </summary>
    public sealed class TrivialSerializer<T> : Serializer<T>
    {
        public TrivialSerializer(T Representative)
            : base(EqualityComparer.Trivial<T>())
        {
            this.Representative = Representative;
        }

        /// <summary>
        /// A representative of set of objects this serializer is valid for.
        /// </summary>
        public readonly T Representative;

        /// <summary>
        /// A trivial serializer with the default value as its representative.
        /// </summary>
        public static readonly new TrivialSerializer<T> Default = new TrivialSerializer<T>(default(T));

        public override T Read(ByteReader Reader)
        {
            return this.Representative;
        }

        public override void Write(ByteWriter Writer, T Value)
        {
            // Nothing to write
        }
    }

    /// <summary>
    /// A serializer for an enum value.
    /// </summary>
    /// <typeparam name="TBase">The underlying integral type for the enum.</typeparam>
    public sealed class EnumSerializer<T, TBase> : Serializer<T>
        where T : struct
        where TBase : struct
    {
        public EnumSerializer(Serializer<TBase> Base)
            : base(EqualityComparer.Enum<T, TBase>(Base.Comparer))
        {
            this.Base = Base;
        }

        /// <summary>
        /// The serializer for the underlying integral type for the enum.
        /// </summary>
        public readonly Serializer<TBase> Base;

        public override T Read(ByteReader Reader)
        {
            return (T)(object)this.Base.Read(Reader);
        }

        public override void Write(ByteWriter Writer, T Value)
        {
            this.Base.Write(Writer, (TBase)(object)Value);
        }
    }

    /// <summary>
    /// A serializer for array values.
    /// </summary>
    public sealed class ArraySerializer<T> : Serializer<T[]>
    {
        public ArraySerializer(Serializer<T> Item, EqualityComparer<T[]> Comparer)
            : base(Comparer)
        {
            this.Item = Item;
        }

        public ArraySerializer(Serializer<T> Item)
            : this(Item, EqualityComparer.Array(Item.Comparer))
        { }

        /// <summary>
        /// The inner serializer for this serializer.
        /// </summary>
        public readonly Serializer<T> Item;

        /// <summary>
        /// The default serializer for this type.
        /// </summary>
        [DefaultSerializer]
        public static readonly new ArraySerializer<T> Default =
            new ArraySerializer<T>(Serializer<T>.Default);

        public override T[] Read(ByteReader Reader)
        {
            uint count = Reader.ReadUInt32();
            T[] res = new T[count];
            for (uint i = 0; i < count; i++)
                res[i] = Reader.Read(this.Item);
            return res;
        }

        public override void Write(ByteWriter Writer, T[] Value)
        {
            Writer.WriteUInt32((uint)Value.Length);
            for (int i = 0; i < Value.Length; i++)
                Writer.Write(this.Item, Value[i]);
        }
    }

    /// <summary>
    /// A serializer for a nullable value.
    /// </summary>
    public sealed class NullableSerializer<T> : Serializer<T?>
        where T : struct
    {
        public NullableSerializer(Serializer<T> Inner)
            : base(EqualityComparer.Nullable<T>(Inner.Comparer))
        {
            this.Inner = Inner;
        }

        /// <summary>
        /// The inner serializer for this serializer.
        /// </summary>
        public readonly Serializer<T> Inner;

        /// <summary>
        /// The default serializer for this type.
        /// </summary>
        [DefaultSerializer]
        public static readonly new NullableSerializer<T> Default =
            new NullableSerializer<T>(Serializer<T>.Default);

        public override T? Read(ByteReader Reader)
        {
            if (Reader.ReadByte() > 0)
                return Reader.Read(this.Inner);
            else
                return null;
        }

        public override void Write(ByteWriter Writer, T? Value)
        {
            if (Value.HasValue)
            {
                Writer.WriteByte(0xFF);
                Writer.Write(this.Inner, Value.Value);
            }
            else
            {
                Writer.WriteByte(0x00);
            }
        }
    }
}