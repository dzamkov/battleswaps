﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

using BattleSwaps.Engine.Util;

namespace BattleSwaps.Engine.Data
{
    /// <summary>
    /// Identifies the tag for a type or value when using open equality or serialization on a base type.
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Class | AttributeTargets.Struct |
        AttributeTargets.Interface | AttributeTargets.Field |
        AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
    public sealed class TagAttribute : Attribute
    {
        public TagAttribute(Type Base, object Tag)
        {
            this.Base = Base;
            this.Tag = Tag;
        }

        /// <summary>
        /// The base type this tag applies to.
        /// </summary>
        public readonly Type Base;

        /// <summary>
        /// The value of this tag.
        /// </summary>
        public readonly object Tag;
    }

    /// <summary>
    /// Describes the information needed to serialize a base type with tags for all of its sub-types and values.
    /// </summary>
    /// <typeparam name="TTag">The type of tag being used.</typeparam>
    public sealed class TagLayout<TTag>
    {
        public TagLayout()
        {
            this.SerializerByTag = new Dictionary<TTag, Serializer>(Serializer<TTag>.Default.Comparer);
            this.TagForType = new Dictionary<Type, TTag>();
            this.TagForValue = new Dictionary<object, TTag>();
        }

        /// <summary>
        /// A mapping from tags to serializers for the subset of objects to which the tag applies.
        /// </summary>
        public readonly Dictionary<TTag, Serializer> SerializerByTag;

        /// <summary>
        /// A mapping from types to tags which have serializers that can be used for those types.
        /// </summary>
        public readonly Dictionary<Type, TTag> TagForType;

        /// <summary>
        /// A mapping from values to tags which have serializers that can be used for those values.
        /// </summary>
        public readonly Dictionary<object, TTag> TagForValue;

        /// <summary>
        /// Gets the tag for the given value.
        /// </summary>
        /// <param name="IsSingular">Indicates whether this is the only tag for the object.</param>
        public TTag GetTag(object Value, out bool IsSingular)
        {
            TTag tag;
            if (this.TagForValue.TryGetValue(Value, out tag))
            {
                IsSingular = true;
                return tag;
            }
            else
            {
                IsSingular = false;
                Type type = Value.GetType();
                while (type != null)
                {
                    if (this.TagForType.TryGetValue(Value.GetType(), out tag))
                        return tag;
                    type = type.BaseType;
                }
            }
            throw new Exception("No tag exists for this object");
        }

        /// <summary>
        /// Creates a copy of this tag layout.
        /// </summary>
        private TagLayout<TTag> _Copy()
        {
            TagLayout<TTag> res = new TagLayout<TTag>();
            foreach (var kvp in this.SerializerByTag)
                res.SerializerByTag.Add(kvp.Key, kvp.Value);
            foreach (var kvp in this.TagForType)
                res.TagForType.Add(kvp.Key, kvp.Value);
            foreach (var kvp in this.TagForValue)
                res.TagForValue.Add(kvp.Key, kvp.Value);
            return res;
        }

        /// <summary>
        /// Adds a tag/subtype mapping to this store.
        /// </summary>
        private void _Add(TTag Tag, Type SubType)
        {
            Serializer serializer;
            if (!Serializer.TryGetDefault(SubType, out serializer))
                throw new Exception("Subtype defines no default serializer");
            this.SerializerByTag.Add(Tag, serializer);
            this.TagForType.Add(SubType, Tag);
        }

        /// <summary>
        /// Adds a tag/value mapping to this store.
        /// </summary>
        private void _Add<T>(TTag Tag, T Value)
        {
            this.SerializerByTag.Add(Tag, new TrivialSerializer<T>(Value));
            this.TagForValue.Add(Value, Tag);
        }

        /// <summary>
        /// Caches the tag layout for a given type.
        /// </summary>
        private static class _Cache<T>
        {
            public static TagLayout<TTag> Layout = new TagLayout<TTag>();
            static _Cache()
            {
                // Begin watching for tag attributes
                Reflection.AssemblyWatch(delegate(Assembly assembly)
                {
                    TagLayout<TTag> nLayout = Layout._Copy();

                    // Add tags
                    foreach (var type in assembly.GetTypes())
                    {
                        // Check type tags
                        foreach (TagAttribute attr in type.GetCustomAttributes(typeof(TagAttribute), false))
                        {
                            if (attr.Base == typeof(T) && attr.Tag is TTag)
                                nLayout._Add((TTag)attr.Tag, type);
                        }

                        // Check value tags
                        var flags = BindingFlags.Static | BindingFlags.Public | BindingFlags.DeclaredOnly;
                        foreach (var field in type.GetFields(flags))
                        {
                            foreach (TagAttribute attr in field.GetCustomAttributes(typeof(TagAttribute), false))
                                if (attr.Base == typeof(T) && attr.Tag is TTag)
                                    nLayout._Add((TTag)attr.Tag, (T)field.GetValue(null));
                        }
                        foreach (var property in type.GetProperties(flags))
                        {
                            foreach (TagAttribute attr in property.GetCustomAttributes(typeof(TagAttribute), false))
                                if (attr.Base == typeof(T) && attr.Tag is TTag)
                                    nLayout._Add((TTag)attr.Tag, (T)property.GetValue(null, null));
                        }
                    }

                    // Update mapping
                    Layout = nLayout;
                });
            }
        }

        /// <summary>
        /// Gets the tag layout for the given type, asserting that a valid layout exists.
        /// </summary>
        public static TagLayout<TTag> Get<T>()
        {
            return _Cache<T>.Layout;
        }
    }

    /// <summary>
    /// A serializer for the given type which uses tags to distinguish between subtypes and values, where each subtype
    /// is serialized using its default serializer. Tags are defined using <see cref="TagAttribute"/>.
    /// </summary>
    /// <typeparam name="TTag">The type of tag used to distinguish subtypes and values.</typeparam>
    /// <typeparam name="T">The base type the serializer is applicable to.</typeparam>
    public sealed class OpenSerializer<TTag, T> : Serializer<T>
    {
        private OpenSerializer()
            : base(OpenEqualityComparer<TTag, T>.Instance)
        { }


        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly OpenSerializer<TTag, T> Instance = new OpenSerializer<TTag, T>();

        public override object ReadUntyped(ByteReader Reader)
        {
            TTag tag = Serializer<TTag>.Default.Read(Reader);
            return TagLayout<TTag>.Get<T>().SerializerByTag[tag].ReadUntyped(Reader);
        }

        public override void WriteUntyped(ByteWriter Writer, object Value)
        {
            TagLayout<TTag> layout = TagLayout<TTag>.Get<T>();
            bool isSingular;
            TTag tag = layout.GetTag(Value, out isSingular);
            Serializer<TTag>.Default.Write(Writer, tag);
            if (!isSingular)
                layout.SerializerByTag[tag].WriteUntyped(Writer, Value);
        }

        public override T Read(ByteReader Reader)
        {
            return (T)this.ReadUntyped(Reader);
        }

        public override void Write(ByteWriter Writer, T Value)
        {
            this.WriteUntyped(Writer, (object)Value);
        }
    }

    /// <summary>
    /// An equality comparer for the given type which uses tags to distinguish between subtypes and values, and each
    /// subtype is compared using its default equality comparer. Tags are defined using <see cref="TagAttribute"/>.
    /// </summary>
    /// <typeparam name="TTag">The type of tag used to distinguish subtypes and values.</typeparam>
    /// <typeparam name="T">The base type this equality comparer applies to.</typeparam>
    public sealed class OpenEqualityComparer<TTag, T> : EqualityComparer<T>
    {
        private OpenEqualityComparer() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly OpenEqualityComparer<TTag, T> Instance = new OpenEqualityComparer<TTag, T>();

        public override bool Equals(T A, T B)
        {
            bool isSingular;
            TagLayout<TTag> layout = TagLayout<TTag>.Get<T>();
            TTag tagA = layout.GetTag(A, out isSingular);
            TTag tagB = layout.GetTag(B, out isSingular);
            if (!Serializer<TTag>.Default.Comparer.Equals(tagA, tagB))
                return false;
            return isSingular || layout.SerializerByTag[tagA].Comparer.Equals(A, B);
        }

        public override int GetHashCode(T Obj)
        {
            bool isSingular;
            TagLayout<TTag> layout = TagLayout<TTag>.Get<T>();
            TTag tag = layout.GetTag(Obj, out isSingular);
            int tagHash = Serializer<TTag>.Default.Comparer.GetHashCode(tag);
            return isSingular ? tagHash : tagHash ^ layout.SerializerByTag[tag].Comparer.GetHashCode(Obj);
        }
    }
}
