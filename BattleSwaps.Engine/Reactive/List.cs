﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Engine.Reactive
{
    /// <summary>
    /// An interface for creating a deconstruction of a dynamic list at a particular moment.
    /// </summary>
    public interface IDynamicListDeconstructor<T>
    {
        /// <summary>
        /// Appends an item to this deconstruction.
        /// </summary>
        void Append(T Item);

        /// <summary>
        /// Appends a sequence of items to this deconstruction.
        /// </summary>
        void Append(IEnumerable<T> Items);

        /// <summary>
        /// Appends a list to this deconstruction.
        /// </summary>
        void Append(Dynamic<List<T>> List);
    }

    /// <summary>
    /// Contains functions related to dynamic lists.
    /// </summary>
    public static class DynamicList
    {
        /// <summary>
        /// Creates a deconstruction of this list at the current moment.
        /// </summary>
        /// <param name="SourceReader">The reader used to read the values this dynamic depends on</param>
        /// <param name="Invalidate">Will be unioned with a latch that is triggered when the deconstruction has
        /// become invalid.</param>
        internal static void _ReadDeconstruct<T>(
            this Dynamic<List<T>> List,
            IDynamicReader SourceReader, IDynamicListDeconstructor<T> Deconstructor,
            OrLatchBuilder Invalidate)
        {
            DynamicList<T> dynamicList = List as DynamicList<T>;
            if (dynamicList != null)
            {
                dynamicList.ReadDeconstruct(SourceReader, Deconstructor, Invalidate);
            }
            else
            {
                List<T> list = List.Read(SourceReader, Invalidate);
                foreach (var item in list)
                    Deconstructor.Append(item);
            }
        }
    }

    /// <summary>
    /// Identifies a list value that varies over time.
    /// </summary>
    public abstract class DynamicList<T> : Dynamic<List<T>>
    {
        public DynamicList(DynamicHint Hint)
            : base(Hint)
        { }

        /// <summary>
        /// Creates a deconstruction of this list at the current moment.
        /// </summary>
        /// <param name="SourceReader">The reader used to read the values this dynamic depends on</param>
        /// <param name="Invalidate">Will be unioned with a latch that is triggered when the deconstruction has
        /// become invalid.</param>
        protected internal abstract void ReadDeconstruct(
            IDynamicReader SourceReader, IDynamicListDeconstructor<T> Deconstructor,
            OrLatchBuilder Invalidate);
    }
}
