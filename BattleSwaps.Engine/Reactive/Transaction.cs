﻿using System;

namespace BattleSwaps.Engine.Reactive
{
    /// <summary>
    /// Identifies a transactional reactive variable of the given type. As implied by the name, these variables are
    /// modified discretely in transactions. Variables are used to introduce state into a reactive system.
    /// </summary>
    public abstract class Variable<T> : Dynamic<T>
    {
        public Variable(DynamicHint Hint)
            : base(Hint)
        { }
    }

    /// <summary>
    /// An interface which allows the creation of new variables.
    /// </summary>
    public interface IVarBuilder
    {
        /// <summary>
        /// Creates a new variable of the given type and assigns it the given initial value.
        /// </summary>
        Variable<T> Declare<T>(T Initial);
    }

    /// <summary>
    /// An interface which can be used to modify variables at a particular state of a reactive system.
    /// </summary>
    public interface IStateWriter : IVarBuilder
    {
        /// <summary>
        /// Sets the value of the given variable at the state associated with this writer.
        /// </summary>
        T Write<T>(Variable<T> Variable, T Value);
    }

    /// <summary>
    /// An interface for defining a transaction in a reactive system. This is simply the combination of a
    /// <see cref="IDynamicReader"/> with a <see cref="IStateWriter"/>, both associated with the same state. Note that
    /// it is illegal to read a variable after modifying it in the same transaction.
    /// </summary>
    public interface ITransactionBuilder : IDynamicReader, IStateWriter { }

    /// <summary>
    /// Describes an operation which atomically reads from a reactive system and makes modifications to a subset of
    /// its variables.
    /// </summary>
    public delegate void Transaction(ITransactionBuilder Transaction);
}