﻿using System;
using System.Diagnostics;

using BattleSwaps.Engine.Data;

namespace BattleSwaps.Engine.Reactive
{
    /// <summary>
    /// Identifies an absolute clock-like time or a time span within a reactive system that has a notion of time.
    /// </summary>
    public struct Time
    {
        [DefaultSerializer]
        public static readonly Serializer<Time> Serializer = Data.Serializer.Struct<Time>();
        public Time(ulong Ticks)
        {
            this.Ticks = Ticks;
        }

        /// <summary>
        /// The number of ticks for this time.
        /// </summary>
        public ulong Ticks;

        /// <summary>
        /// The number of ticks in a second.
        /// </summary>
        public const ulong TicksPerSecond = 1000000;

        /// <summary>
        /// Gets the length of this time in seconds.
        /// </summary>
        public double Seconds
        {
            get
            {
                return (double)this.Ticks / (double)TicksPerSecond;
            }
        }

        /// <summary>
        /// The largest possible time length.
        /// </summary>
        public static readonly Time MaxValue = new Time(ulong.MaxValue);

        /// <summary>
        /// A zero-length time.
        /// </summary>
        public static readonly Time Zero = new Time(0);

        /// <summary>
        /// An arbitrary time representing an "origin" moment.
        /// </summary>
        public static readonly Time Origin = new Time(0);

        /// <summary>
        /// Constructs a time length from the given amount of seconds.
        /// </summary>
        public static Time FromSeconds(double Seconds)
        {
            return new Time((ulong)(Seconds * TicksPerSecond));
        }

        /// <summary>
        /// Gets the earliest time after the given time.
        /// </summary>
        public static Time Next(Time Time)
        {
            return new Time(Time.Ticks + 1);
        }

        /// <summary>
        /// Gets the earlier/shorter of two times.
        /// </summary>
        public static Time Earlier(Time A, Time B)
        {
            return A < B ? A : B;
        }

        /// <summary>
        /// Gets the later/longer of two times.
        /// </summary>
        public static Time Later(Time A, Time B)
        {
            return A < B ? B : A;
        }

        public static Time operator +(Time A, Time B)
        {
            return new Time(A.Ticks + B.Ticks);
        }

        public static Time operator -(Time A, Time B)
        {
            Debug.Assert(A >= B);
            return new Time(A.Ticks - B.Ticks);
        }

        public static bool operator <(Time A, Time B)
        {
            return A.Ticks < B.Ticks;
        }

        public static bool operator >(Time A, Time B)
        {
            return A.Ticks > B.Ticks;
        }

        public static bool operator <=(Time A, Time B)
        {
            return A.Ticks <= B.Ticks;
        }

        public static bool operator >=(Time A, Time B)
        {
            return A.Ticks >= B.Ticks;
        }

        public static bool operator ==(Time A, Time B)
        {
            return A.Ticks == B.Ticks;
        }

        public static bool operator !=(Time A, Time B)
        {
            return A.Ticks != B.Ticks;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Time))
                return false;
            return this == (Time)obj;
        }

        public override int GetHashCode()
        {
            return this.Ticks.GetHashCode();
        }

        public override string ToString()
        {
            return this.Seconds.ToString("0.000000");
        }
    }

    /// <summary>
    /// Describes an analytical progression of a value of the given type over time.
    /// </summary>
    public interface ISpan<T>
    {
        /// <summary>
        /// Gets the time at which this span begins to be applicable.
        /// </summary>
        Time Start { get; }

        /// <summary>
        /// Gets the time at which this span ceases to be applicable, or infinity if this never happens.
        /// </summary>
        Time End { get; }

        /// <summary>
        /// Gets the value of this span at the given time.
        /// </summary>
        T this[Time Time] { get; }
    }
}
