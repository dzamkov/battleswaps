﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Engine.Reactive
{
    /// <summary>
    /// The non-parameteric base class for dynamic values.
    /// </summary>
    public abstract class Dynamic
    {
        public Dynamic(DynamicHint Hint)
        {
            this.Hint = Hint;
        }

        /// <summary>
        /// Describes the general behavior of this dynamic value.
        /// </summary>
        public readonly DynamicHint Hint;

        /// <summary>
        /// Creates a dynamic value wrapping the given constant value.
        /// </summary>
        public static Dynamic<T> Const<T>(T Value)
        {
            return new ConstDynamic<T>(Value);
        }

        /// <summary>
        /// Gets a dynamic value that switches between two source values depending on a boolean condition.
        /// </summary>
        public static Dynamic<T> Switch<T>(Dynamic<bool> Condition, Dynamic<T> OnFalse, Dynamic<T> OnTrue)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Describes the general behavior of a dynamic value.
    /// </summary>
    [Flags]
    public enum DynamicHint : uint
    {
        /// <summary>
        /// Indicates that a dynamic value never changes.
        /// </summary>
        Constant = 0x00000001,

        /// <summary>
        /// Indicates that a dynamic value can change, but only discretely at particular moments.
        /// </summary>
        Discrete = 0x00000003,

        /// <summary>
        /// Indicates that a dynamic value can change either discretely or continuously, possibly switching between
        /// the two over time.
        /// </summary>
        Mixed = 0x00000007,

        /// <summary>
        /// Indicates that a dynamic value changes continuously over time.
        /// </summary>
        Continuous = 0x0000000F,

        /// <summary>
        /// A mask which isolated the variability information from a hint.
        /// </summary>
        VariabilityMask = 0x0000000F,


        /// <summary>
        /// Indicates that a dynamic value is fast to retreive, taking a comparable time to looking up from a cache,
        /// and thus should not be cached.
        /// </summary>
        FastLookup = 0x00000010,

        /// <summary>
        /// Indicates that a dynamic value is fast to compute or retreive, but may be cached.
        /// </summary>
        Fast = 0x00000030,

        /// <summary>
        /// Indicates that a dynamic value is slow to compute or retreive, and should probably be cached.
        /// </summary>
        Slow = 0x00000070,

        /// <summary>
        /// A mask which isolates the performance information from a hint.
        /// </summary>
        PerformanceMask = 0x000000F0,

        /// <summary>
        /// Indicates that a listener can be bound to a latch.
        /// </summary>
        LatchListenable = 0x00000100,
    }

    /// <summary>
    /// Identifies a value of the given type that varies over time.
    /// </summary>
    public abstract class Dynamic<T> : Dynamic
    {
        public Dynamic(DynamicHint Hint)
            : base(Hint)
        { }

        /// <summary>
        /// Computes or retreives the current value of this dynamic value, using the given source reader to read
        /// the values this dynamic depends on.
        /// </summary>
        /// <param name="SourceReader">The reader used to read the values this dynamic depends on</param>
        /// <param name="Invalidate">Will be unioned with a latch that is triggered when the returned value becomes
        /// invalid. If this dynamic varies continuously, the returned latch will already be triggered</param>
        protected internal abstract T Read(IDynamicReader SourceReader, OrLatchBuilder Invalidate);

        public static implicit operator Dynamic<T>(T Source)
        {
            return Dynamic.Const(Source);
        }
    }

    /// <summary>
    /// A constant wrapped as a dynamic value.
    /// </summary>
    public sealed class ConstDynamic<T> : Dynamic<T>
    {
        public ConstDynamic(T Value)
            : base(DynamicHint.Constant | DynamicHint.FastLookup)
        {
            this.Value = Value;
        }

        /// <summary>
        /// The value of this constant.
        /// </summary>
        public readonly T Value;

        protected internal override T Read(IDynamicReader SourceReader, OrLatchBuilder Invalidate)
        {
            return this.Value;
        }
    }

    /// <summary>
    /// An interface to a context which allows the reading of dynamic values at a certain moment. This interface is
    /// thread-safe.
    /// </summary>
    public interface IDynamicReader
    {
        /// <summary>
        /// Gets the current value of the given dynamic value.
        /// </summary>
        /// <param name="Invalidate">Will be unioned with a latch that is triggered when the returned value becomes
        /// invalid. If this dynamic varies continuously, the returned latch will already be triggered</param>
        T Read<T>(Dynamic<T> Dynamic, OrLatchBuilder Invalidate);

        /// <summary>
        /// Iterates over all entries currently in the given bag in no particular order, and returns a bag that
        /// represents the items that have not yet been retreived (initially empty).
        /// </summary>
        void ReadIterate<T>(
            Dynamic<Bag<T>> Bag, Action<Entry<T>> Yield,
            out Dynamic<Bag<T>> Remainder);

        /// <summary>
        /// Creates a deconstruction of the given list.
        /// </summary>
        /// <param name="Invalidate">Will be unioned with a latch that is triggered when the deconstruction has
        /// become invalid.</param>
        void ReadDeconstruct<T>(
            Dynamic<List<T>> List, IDynamicListDeconstructor<T> Deconstructor,
            OrLatchBuilder Invalidate);
    }

    /// <summary>
    /// Contains extensions for <see cref="IDynamicReader"/>. 
    /// </summary>
    public static class DynamicReader
    {
        /// <summary>
        /// Gets the current value of the given dynamic value.
        /// </summary>
        public static T Read<T>(this IDynamicReader Reader, Dynamic<T> Dynamic)
        {
            return Reader.Read(Dynamic, OrLatchBuilder.AlwaysTrue);
        }

        /// <summary>
        /// A dynamic reader that reads dynamic values directly, at the current program moment, without caching
        /// or analysis.
        /// </summary>
        public static DirectDynamicReader Direct = new DirectDynamicReader();
    }

    /// <summary>
    /// A dynamic reader that reads dynamic values directly, at the current program moment, without caching
    /// or analysis.
    /// </summary>
    public struct DirectDynamicReader : IDynamicReader
    {
        /// <inheritdoc/>
        public T Read<T>(Dynamic<T> Dynamic, OrLatchBuilder Invalidate)
        {
            return Dynamic.Read(this, Invalidate);
        }

        /// <inheritdoc/>
        public void ReadIterate<T>(
            Dynamic<Bag<T>> Bag, Action<Entry<T>> Yield,
            out Dynamic<Bag<T>> Remainder)
        {
            Bag._ReadIterate(this, Yield, out Remainder);
        }

        /// <inheritdoc/>
        public void ReadDeconstruct<T>(
            Dynamic<List<T>> List,
            IDynamicListDeconstructor<T> Deconstructor,
            OrLatchBuilder Invalidate)
        {
            List._ReadDeconstruct(this, Deconstructor, Invalidate);
        }
    }
}
