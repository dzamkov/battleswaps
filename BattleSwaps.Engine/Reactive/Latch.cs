﻿using System;
using System.Diagnostics;

namespace BattleSwaps.Engine.Reactive
{
    /// <summary>
    /// Identifies a dynamic boolean value subject to the restriction that, once the latch becomes true, it will
    /// remain so.
    /// </summary>
    public abstract class Latch : Dynamic<bool>
    {
        public Latch(DynamicHint Hint)
            : base(Hint)
        {
            // A latch must be constant or discrete, since it can not vary continuously.
            Debug.Assert((Hint & DynamicHint.VariabilityMask) <= DynamicHint.Discrete);
        }

        /// <summary>
        /// A latch that is always true.
        /// </summary>
        public static readonly Latch ConstTrue = ConstLatch.True;

        /// <summary>
        /// A latch that is always false.
        /// </summary>
        public static readonly Latch ConstFalse = ConstLatch.False;

        /// <summary>
        /// Constructs a latch from a dynamic boolean value. The dynamic value must satisfy the property that,
        /// once it becomes true, it will remain so, but this is not checked.
        /// </summary>
        public static Latch FromDynamic(Dynamic<bool> Source)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Constructs a latch that is closed iff both of the given latches are closed.
        /// </summary>
        public static Latch operator &(Latch A, Latch B)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Constructs a latch that is closed iff either of the given latches are closed.
        /// </summary>
        public static Latch operator |(Latch A, Latch B)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Binds a function to be called when this latch is triggered (becomes "true"), or immediately if the latch 
        /// has already been triggered. This can only be used when <see cref="Dynamic.Hint"/> has the flag
        /// <see cref="DynamicHint.LatchListenable"/>. 
        /// </summary>
        /// <remarks>If not called immediately, the given action may be weakly referenced. Thus, an external
        /// strong reference to it is needed if it must still be called.</remarks>
        /// <param name="SourceReader">The reader used to read the values this dynamic depends on</param>
        public abstract void Listen(IDynamicReader SourceReader, Action Action);
    }

    /// <summary>
    /// A constant boolean wrapped as a latch.
    /// </summary>
    public sealed class ConstLatch : Latch
    {
        private ConstLatch(bool Value)
            : base(DynamicHint.Constant | DynamicHint.FastLookup | DynamicHint.LatchListenable)
        {
            this.Value = Value;
        }

        /// <summary>
        /// The only instance of a constant latch with a true value.
        /// </summary>
        public static readonly ConstLatch True = new ConstLatch(true);

        /// <summary>
        /// The only instance of a constant latch with a false value.
        /// </summary>
        public static readonly ConstLatch False = new ConstLatch(false);

        /// <summary>
        /// The value of this constant latch.
        /// </summary>
        public readonly bool Value;

        protected internal override bool Read(IDynamicReader SourceReader, OrLatchBuilder Invalidate)
        {
            return this.Value;
        }

        public override void Listen(IDynamicReader SourceReader, Action Action)
        {
            if (this.Value)
                Action();
        }
    }

    /// <summary>
    /// A helper class for constructing a latch as the "or" of several source latches.
    /// </summary>
    public sealed class OrLatchBuilder
    {
        public OrLatchBuilder()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// A latch builder that always produces <see cref="Latch.ConstTrue"/>. This builder may be reused multiple
        /// times.
        /// </summary>
        public static readonly OrLatchBuilder AlwaysTrue;

        /// <summary>
        /// Adds the given latch to the union produced by this builder.
        /// </summary>
        public void OrWith(Latch Latch)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the latch that was constructed using this builder. Unless this is a special builder
        /// (i.e. <see cref="Latch.ConstTrue"/>), the builder may no longer be used after this call. 
        /// </summary>
        public Latch Finish()
        {
            throw new NotImplementedException();
        }
    }
}
