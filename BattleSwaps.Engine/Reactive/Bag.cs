﻿using System;

namespace BattleSwaps.Engine.Reactive
{
    /// <summary>
    /// Describes an unordered collection of items of the given type such that duplicates are allowed.
    /// </summary>
    public struct Bag<T>
    {
        public Bag(T[] Items)
        {
            this.Items = Items;
        }

        /// <summary>
        /// The items in this bag.
        /// </summary>
        public T[] Items;
    }

    /// <summary>
    /// Describes an entry in a bag.
    /// </summary>
    public struct Entry<T>
    {
        public Entry(T Value, Latch Remove)
        {
            this.Value = Value;
            this.Remove = Remove;
        }

        /// <summary>
        /// The value of this entry.
        /// </summary>
        public T Value;

        /// <summary>
        /// A latch that is triggered when this entry is removed.
        /// </summary>
        public Latch Remove;
    }

    /// <summary>
    /// Contains functions related to dynamic bags.
    /// </summary>
    public static class DynamicBag
    {
        /// <summary>
        /// Gets the always-empty dynamic bag of the given type.
        /// </summary>
        public static Dynamic<Bag<T>> Empty<T>()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Iterates over all entries currently in this bag in no particular order, and returns a bag that represents
        /// the items that have not yet been retreived (initially empty).
        /// </summary>
        /// <param name="SourceReader">The reader used to read the values this dynamic depends on</param>
        internal static void _ReadIterate<T>(
            this Dynamic<Bag<T>> Bag,
            IDynamicReader SourceReader, Action<Entry<T>> Yield,
            out Dynamic<Bag<T>> Remainder)
        {
            DynamicBag<T> dynamicBag = Bag as DynamicBag<T>;
            if (dynamicBag != null)
            {
                dynamicBag.ReadIterate(SourceReader, Yield, out Remainder);
            }
            else
            {
                OrLatchBuilder invalidateBuilder = new OrLatchBuilder();
                Bag<T> bag = Bag.Read(SourceReader, invalidateBuilder);
                Latch invalidate = invalidateBuilder.Finish();
                foreach (var item in bag.Items)
                    Yield(new Entry<T>(item, invalidate));
                Remainder = Dynamic.Switch(invalidate, DynamicBag.Empty<T>(), bag);
            }
        }
    }

    /// <summary>
    /// Identifies a bag value that varies over time.
    /// </summary>
    public abstract class DynamicBag<T> : Dynamic<Bag<T>>
    {
        public DynamicBag(DynamicHint Hint)
            : base(Hint)
        { }

        /// <summary>
        /// Iterates over all entries currently in this bag in no particular order, and returns a bag that represents
        /// the items that have not yet been retreived (initially empty).
        /// </summary>
        /// <param name="SourceReader">The reader used to read the values this dynamic depends on</param>
        protected internal abstract void ReadIterate(
            IDynamicReader SourceReader, Action<Entry<T>> Yield,
            out Dynamic<Bag<T>> Remainder);
    }
}
