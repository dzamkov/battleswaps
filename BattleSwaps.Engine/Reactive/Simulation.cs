﻿using System;
using System.Collections.Generic;

using BattleSwaps.Engine.Util;

namespace BattleSwaps.Engine.Reactive
{
    /// <summary>
    /// Contains functions related to event-driven reactive systems.
    /// </summary>
    public static class Simulation
    {
        /// <summary>
        /// Creates and runs a simulation with the given setup procedure.
        /// </summary>
        /// <param name="Setup">Sets up the variables and events for the simulation</param>
        public static ISimulationStream Run(Action<IVarBuilder, IEventBuilder> Setup)
        {
            return _SimpleSimulation.Run(Setup);
        }
    }

    /// <summary>
    /// A dynamic which can be disposed by its user. This is useful for locking a simulation at a particular moment
    /// for reading, and informing the simulation when reading is finished.
    /// </summary>
    public interface IDisposableDynamicReader : IDynamicReader, IDisposable { }

    /// <summary>
    /// An interface for reading from or writing to a simulation in a stream-like fashion (with increasing time
    /// values).
    /// </summary>
    public interface ISimulationStream : IDisposable
    {
        /// <summary>
        /// Gets or sets the current simulation time for this stream interface. This may not be set to a lower
        /// value. For best performance, this should be increased as early as possible, allowing the simulation stream
        /// to start updating sooner.
        /// </summary>
        Time Current { get; set; }

        /// <summary>
        /// Creates a reader to read from the simulation at the current time.
        /// </summary>
        IDisposableDynamicReader ReadCurrent();

        /// <summary>
        /// Applies the given transaction to the simulation at current time.
        /// </summary>
        void Apply(Transaction Transaction);
    }

    /// <summary>
    /// Contains classes and functions related to simple single-stream simulations.
    /// </summary>
    internal static class _SimpleSimulation
    {
        /// <summary>
        /// Creates and runs a simulation with the given setup procedure.
        /// </summary>
        /// <param name="Setup">Sets up the variables and events for the simulation</param>
        public static ISimulationStream Run(Action<VarBuilder, IEventBuilder> Setup)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// A <see cref="Reactive.Variable{T}"/> for a simple simulation. 
        /// </summary>
        public sealed class Variable<T> : Reactive.Variable<T>
        {
            public Variable(T Initial)
                : base(DynamicHint.Discrete | DynamicHint.FastLookup)
            {
                this.Current = Initial;
            }

            /// <summary>
            /// The current value of this variable.
            /// </summary>
            public T Current;

            protected internal override T Read(IDynamicReader SourceReader, OrLatchBuilder Invalidate)
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// A <see cref="IVarBuilder"/> for a simple simulation. 
        /// </summary>
        public sealed class VarBuilder : IVarBuilder
        {
            private VarBuilder() { }

            /// <summary>
            /// The only instance of this class.
            /// </summary>
            public static readonly VarBuilder Instance = new VarBuilder();

            Reactive.Variable<T> IVarBuilder.Declare<T>(T Initial)
            {
                return new Variable<T>(Initial);
            }
        }

        /// <summary>
        /// A <see cref="ISimulationStream"/> for a simple simulation. 
        /// </summary>
        public sealed class Stream : ISimulationStream
        {
            /// <summary>
            /// Describes a pending simulation event.
            /// </summary>
            public struct Event
            {
                /// <summary>
                /// The moment for this event.
                /// </summary>
                public Moment Moment;
            }

            Time ISimulationStream.Current
            {
                get
                {
                    throw new NotImplementedException();
                }

                set
                {
                    throw new NotImplementedException();
                }
            }

            IDisposableDynamicReader ISimulationStream.ReadCurrent()
            {
                throw new NotImplementedException();
            }

            void ISimulationStream.Apply(Transaction Transaction)
            {
                throw new NotImplementedException();
            }

            void IDisposable.Dispose()
            {
                throw new NotImplementedException();
            }
        }
    }
}
