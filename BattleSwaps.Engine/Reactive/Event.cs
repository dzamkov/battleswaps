﻿using System;

namespace BattleSwaps.Engine.Reactive
{
    /// <summary>
    /// Identifies a particular state and time within a transactional reactive system that has a notion of time.
    /// </summary>
    public struct Moment
    {
        public static bool operator ==(Moment A, Moment B)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(Moment A, Moment B)
        {
            throw new NotImplementedException();
        }

        public static bool operator <(Moment A, Moment B)
        {
            throw new NotImplementedException();
        }

        public static bool operator >(Moment A, Moment B)
        {
            throw new NotImplementedException();
        }

        public static bool operator <=(Moment A, Moment B)
        {
            throw new NotImplementedException();
        }

        public static bool operator >=(Moment A, Moment B)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object Other)
        {
            if (!(Other is Moment))
                return false;
            return this == (Moment)Other;
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// An interface for defining events in a transactional reactive system that progresses in <see cref="Moment"/>s. 
    /// </summary>
    public interface IEventBuilder
    {
        /// <summary>
        /// Includes all events from the given event source.
        /// </summary>
        void Include(EventSource Source);

        /// <summary>
        /// Defines an event occuring at the given time and applying the given transaction.
        /// </summary>
        /// <remarks>When events are set to be applied at the same time, they will be tie-broken by order of
        /// emission within the event builder that defined them. All events in an event source produced by
        /// <see cref="Include(EventSource)"/> will be tie-broken as a group around surronding calls to
        /// <see cref="Include(EventSource)"/> and <see cref="Emit(Time, Transaction)"/>.</remarks>
        /// <param name="Time">The clock time the event should occur at. If this is before the time the event was
        /// emitted, then the event will be applied immediately.</param>
        /// <param name="Transaction">The transaction to be applied when the event occurs.</param>
        void Emit(Time Time, Transaction Transaction);
    }

    /// <summary>
    /// An action which produces a set of events based on a given reactive state.
    /// </summary>
    public delegate void EventSource(IDynamicReader Reader, IEventBuilder Events);
}
