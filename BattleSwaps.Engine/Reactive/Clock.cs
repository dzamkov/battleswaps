﻿using System;
using System.Threading;
using System.Diagnostics;

namespace BattleSwaps.Engine.Reactive
{
    /// <summary>
    /// Identifies an ascending dynamic <see cref="Reactive.Time"/> value. 
    /// </summary>
    public abstract class Clock
    {
        /// <summary>
        /// Constructs a clock from a dynamic <see cref="Reactive.Time"/> value. The dynamic value must be ascending,
        /// but this is not checked.
        /// </summary>
        public static Clock FromDynamic(Dynamic<Time> Time)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Constructs a latch that will trigger when this clock reaches the given time.
        /// </summary>
        public Latch GetAlarm(Time Time)
        {
            throw new NotImplementedException();
        }
    }
}
