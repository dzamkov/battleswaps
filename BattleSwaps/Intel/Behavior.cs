﻿using System;
using System.Collections.Generic;
using System.Reflection;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;

namespace BattleSwaps.Intel
{
    /// <summary>
    /// Identifies a control parameter which can influence some aspect of a unit's behavior.
    /// </summary>
    public abstract class Parameter
    {
        [DefaultSerializer]
        public static readonly Serializer<Parameter> Serializer = Data.Serializer.Open<uint, Parameter>();

        /// <summary>
        /// Reads a behavior involving this parameter.
        /// </summary>
        internal abstract ParameterBehavior _ReadBehavior(ByteReader Reader);
    }

    /// <summary>
    /// Identifies a control parameter (with values of the given type) which can influence some aspect of a
    /// unit's behavior.
    /// </summary>
    public class Parameter<T> : Parameter
    {
        public Parameter(IEqualityComparer<T> Comparer)
        {
            this.Comparer = Comparer;
        }

        /// <summary>
        /// The equality comparer used for parameter values.
        /// </summary>
        public readonly IEqualityComparer<T> Comparer;

        internal override ParameterBehavior _ReadBehavior(ByteReader Reader)
        {
            return new ParameterBehavior<T>(this, ParameterBehavior<T>.ValueSerializer.Read(Reader));
        }
    }

    /// <summary>
    /// Provide control parameter values to units based on their identity, properties and environment.
    /// </summary>
    public abstract class Behavior
    {
        [DefaultSerializer]
        public static readonly Serializer<Behavior> Serializer = Data.Serializer.Open<byte, Behavior>();

        /// <summary>
        /// A behavior that does not define any control parameters.
        /// </summary>
        public static Behavior Empty = EmptyBehavior.Instance;

        /// <summary>
        /// Gets the value of a control parameter for the given unit using this behavior. The resulting signal may
        /// vary depending on the properties and environment of the unit.
        /// </summary>
        public abstract PartialSignal<T> GetParameterSignal<T>(Parameter<T> Parameter, Unit Unit);

        /// <summary>
        /// Sets this behavior to have the given parameter undefined where it was previously defined to be the
        /// given value.
        /// </summary>
        public abstract Behavior Remove<T>(Parameter<T> Parameter, T Value);

        /// <summary>
        /// Decorates this behavior so it only applies to units in the given group.
        /// </summary>
        public Behavior For(Group Group)
        {
            if (Group == Group.Empty)
                return Empty;
            return new GroupBehavior(Group, this);
        }

        /// <summary>
        /// Constructs a behavior defining a single parameter.
        /// </summary>
        public static Behavior Define<T>(Parameter<T> Parameter, T Value)
        {
            return new ParameterBehavior<T>(Parameter, Value);
        }

        public static Behavior operator +(Behavior Base, Behavior Patch)
        {
            if (Base == Empty) return Patch;
            if (Patch == Empty) return Base;
            return new OverrideBehavior(Base, Patch);
        }
    }

    /// <summary>
    /// A behavior which does not define any parameter signals for any unit.
    /// </summary>
    public sealed class EmptyBehavior : Behavior
    {
        private EmptyBehavior() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        [Tag(typeof(Behavior), (byte)(0))]
        public static readonly EmptyBehavior Instance = new EmptyBehavior();

        public override PartialSignal<T> GetParameterSignal<T>(Parameter<T> Parameter, Unit Unit)
        {
            return PartialSignal<T>.Undefined;
        }

        public override Behavior Remove<T>(Parameter<T> Parameter, T Value)
        {
            return this;
        }
    }

    /// <summary>
    /// A behavior which defines a single parameter as a static value for all units.
    /// </summary>
    [Tag(typeof(Behavior), (byte)(1))]
    public abstract class ParameterBehavior : Behavior, IEquatable<ParameterBehavior>
    {
        [DefaultSerializer]
        public static readonly new Serializer<ParameterBehavior> Serializer = new _Serializer();

        /// <summary>
        /// A serializer for a parameter behavior.
        /// </summary>
        private sealed class _Serializer : Serializer<ParameterBehavior>
        {
            public _Serializer()
                : base(EqualityComparer.Equatable<ParameterBehavior>())
            { }

            public override ParameterBehavior Read(ByteReader Reader)
            {
                return Parameter.Serializer.Read(Reader)._ReadBehavior(Reader);
            }

            public override void Write(ByteWriter Writer, ParameterBehavior Value)
            {
                Value._Write(Writer);
            }
        }

        /// <summary>
        /// Indicates whether this parameter behavior is the same as another.
        /// </summary>
        public abstract bool Equals(ParameterBehavior Other);

        /// <summary>
        /// Writes this behavior to a stream.
        /// </summary>
        internal abstract void _Write(ByteWriter Writer);
    }

    /// <summary>
    /// A behavior which defines a single parameter as a static value for all units.
    /// </summary>
    public sealed class ParameterBehavior<T> : ParameterBehavior
    {
        public ParameterBehavior(Parameter<T> Parameter, T Value)
        {
            this.Parameter = Parameter;
            this.Value = Value;
        }

        /// <summary>
        /// The parameter defined by this behavior.
        /// </summary>
        public readonly Parameter<T> Parameter;

        /// <summary>
        /// The value for the parameter defined by this behavior.
        /// </summary>
        public readonly T Value;

        /// <summary>
        /// The default serializer for the values in this behavior.
        /// </summary>
        public static readonly Serializer<T> ValueSerializer = Serializer<T>.Default;

        public override PartialSignal<TN> GetParameterSignal<TN>(Parameter<TN> Parameter, Unit Unit)
        {
            if ((Parameter)Parameter == (Parameter)this.Parameter)
                return new PartialSignal<TN>(Signal<TN>.Static((TN)(object)this.Value));
            return PartialSignal<TN>.Undefined;
        }

        public override Behavior Remove<TN>(Parameter<TN> Parameter, TN Value)
        {
            if ((Parameter)Parameter == (Parameter)this.Parameter &&
                this.Parameter.Comparer.Equals(this.Value, (T)(object)(TN)Value))
            {
                return Behavior.Empty;
            }
            return this;
        }

        public override bool Equals(ParameterBehavior Other)
        {
            ParameterBehavior<T> typedOther = Other as ParameterBehavior<T>;
            if (typedOther == null)
                return false;
            return this.Parameter == typedOther.Parameter &&
                ValueSerializer.Comparer.Equals(this.Value, typedOther.Value);
        }

        internal override void _Write(ByteWriter Writer)
        {
            Intel.Parameter.Serializer.Write(Writer, this.Parameter);
            ValueSerializer.Write(Writer, this.Value);
        }
    }

    /// <summary>
    /// A behavior which is only defined for units in a certain group.
    /// </summary>
    [Tag(typeof(Behavior), (byte)(2))]
    public sealed class GroupBehavior : Behavior
    {
        [DefaultSerializer]
        public static new readonly Serializer<GroupBehavior> Serializer =
            Data.Serializer.Immutable<GroupBehavior>();
        public GroupBehavior(Group Target, Behavior Source)
        {
            this.Target = Target;
            this.Source = Source;
        }

        /// <summary>
        /// The group of units to which this behavior applies.
        /// </summary>
        public readonly Group Target;

        /// <summary>
        /// The behavior applied to units in the target group for this behavior.
        /// </summary>
        public readonly Behavior Source;

        public override PartialSignal<T> GetParameterSignal<T>(Parameter<T> Parameter, Unit Unit)
        {
            bool isMemberStatic;
            Signal<bool> isMember = this.Target.Contains(Unit);
            if (isMember.TryGetStatic(out isMemberStatic))
            {
                if (isMemberStatic)
                    return this.Source.GetParameterSignal(Parameter, Unit);
                else
                    return PartialSignal<T>.Undefined;
            }
            else
            {
                Signal<Maybe<T>> source = this.Source.GetParameterSignal(Parameter, Unit).AsSignal;
                return new PartialSignal<T>(Signal.Custom(
                    delegate(Moment moment, OrLatchBuilder invalidate, ref bool isDiscrete)
                {
                    if (isMember.Get(moment, invalidate, ref isDiscrete))
                        return source.Get(moment, invalidate, ref isDiscrete);
                    else
                        return Maybe<T>.Nothing;
                }));
            }
        }

        public override Behavior Remove<T>(Parameter<T> Parameter, T Value)
        {
            Behavior nSource = this.Source.Remove(Parameter, Value);
            if (this.Source != nSource)
                return nSource.For(this.Target);
            return this;
        }
    }

    /// <summary>
    /// A behavior which combines control parameters from two behaviors, giving priority to one of them.
    /// </summary>
    [Tag(typeof(Behavior), (byte)(3))]
    public sealed class OverrideBehavior : Behavior
    {
        [DefaultSerializer]
        public static new readonly Serializer<OverrideBehavior> Serializer =
            Data.Serializer.Immutable<OverrideBehavior>();
        public OverrideBehavior(Behavior Base, Behavior Patch)
        {
            this.Base = Base;
            this.Patch = Patch;
        }

        /// <summary>
        /// The behavior to be applied for control parameters that are not present in the patch for this override
        /// behavior.
        /// </summary>
        public readonly Behavior Base;

        /// <summary>
        /// The primary behavior to check for control parameters.
        /// </summary>
        public readonly Behavior Patch;

        public override PartialSignal<T> GetParameterSignal<T>(Parameter<T> Parameter, Unit Unit)
        {
            PartialSignal<T> partialPatchSignal = this.Patch.GetParameterSignal(Parameter, Unit);
            if (partialPatchSignal.IsTotal)
                return partialPatchSignal;
            PartialSignal<T> partialBaseSignal = this.Base.GetParameterSignal(Parameter, Unit);
            if (partialBaseSignal.IsTotal)
            {
                Signal<Maybe<T>> patchSignal = partialPatchSignal.AsSignal;
                Signal<T> baseSignal = partialBaseSignal.AsTotalSignal;
                return new PartialSignal<T>(Signal.Custom(
                    delegate(Moment moment, OrLatchBuilder invalidate, ref bool isDiscrete)
                {
                    Maybe<T> patchValue = patchSignal.Get(moment, invalidate, ref isDiscrete);
                    if (patchValue.IsJust)
                        return patchValue.Value;
                    else
                        return baseSignal.Get(moment, invalidate, ref isDiscrete);
                }));
            }
            else
            {
                Signal<Maybe<T>> patchSignal = partialPatchSignal.AsSignal;
                Signal<Maybe<T>> baseSignal = partialBaseSignal.AsSignal;
                return new PartialSignal<T>(Signal.Custom(
                    delegate(Moment moment, OrLatchBuilder invalidate, ref bool isDiscrete)
                {
                    Maybe<T> patchValue = patchSignal.Get(moment, invalidate, ref isDiscrete);
                    if (patchValue.IsJust)
                        return patchValue;
                    else
                        return baseSignal.Get(moment, invalidate, ref isDiscrete);
                }));
            }
        }

        public override Behavior Remove<T>(Parameter<T> Parameter, T Value)
        {
            Behavior nBase = this.Base.Remove(Parameter, Value);
            Behavior nPatch = this.Patch.Remove(Parameter, Value);
            if (this.Base != nBase || this.Patch != nPatch)
                return nBase + nPatch;
            return this;
        }
    }
}
