﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;

namespace BattleSwaps.Intel
{
    /// <summary>
    /// The non-parametric base class for control captures.
    /// </summary>
    public abstract class Capture
    {
        public Capture(Initializer Initializer, NameRef<Capture> NameRef, CaptureParameter Parameter)
        {
            this.NameRef = NameRef;
            NameRef.Put(Initializer, this);

            this.Parameter = Parameter;
            this.Cancel = new ExplicitLatch();
        }

        /// <summary>
        /// The named reference for this capture.
        /// </summary>
        public readonly NameRef<Capture> NameRef;
        
        /// <summary>
        /// The control parameter affected by this capture.
        /// </summary>
        public readonly CaptureParameter Parameter;

        /// <summary>
        /// A latch which is triggered when this control capture is cancelled.
        /// </summary>
        public readonly ExplicitLatch Cancel;
    }

    /// <summary>
    /// Identifies a control capture for a set of units where a control signal of the given type is provided. This
    /// allows manually control of a unit from an external source.
    /// </summary>
    public sealed class Capture<T> : Capture
    {
        public Capture(Initializer Initializer, NameRef<Capture> NameRef, CaptureParameter<T> Parameter, T Initial)
            : base(Initializer, NameRef, Parameter)
        {
            this.Input = new ExplicitSignal<T>(Initializer, Initial);
        }

        /// <summary>
        /// Gets the control parameter affected by this capture.
        /// </summary>
        public new CaptureParameter<T> Parameter
        {
            get
            {
                return (CaptureParameter<T>)base.Parameter;
            }
        }

        /// <summary>
        /// The control input signal provided by this capture.
        /// </summary>
        public readonly ExplicitSignal<T> Input;
    }

    /// <summary>
    /// A control parameter which modifies a unit's behavior by providing it an input signal which can be used to
    /// control it manually.
    /// </summary>
    public abstract class CaptureParameter : Parameter<NameRef<Capture>>
    {
        public CaptureParameter()
            : base(EqualityComparer<NameRef<Capture>>.Default)
        { }

        /// <summary>
        /// Reads a capture prototype involving this parameter.
        /// </summary>
        internal abstract CapturePrototype _ReadCapturePrototype(NameRef<Capture> NameRef, ByteReader Reader);
    }

    /// <summary>
    /// A control parameter which modifies a unit's behavior by providing it an input signal of the given type
    /// which can be used to control it manually.
    /// </summary>
    public sealed class CaptureParameter<T> : CaptureParameter
    {
        public CaptureParameter(Serializer<T> BeatSerializer)
        {
            this.BeatSerializer = BeatSerializer;
        }

        /// <summary>
        /// The serializer for input signal values for this parameter.
        /// </summary>
        public readonly Serializer<T> BeatSerializer;

        internal override CapturePrototype _ReadCapturePrototype(NameRef<Capture> NameRef, ByteReader Reader)
        {
            return new CapturePrototype<T>
            {
                NameRef = NameRef,
                Parameter = this,
                Beat = this.BeatSerializer.Read(Reader)
            };
        }
    }

    /// <summary>
    /// A full static description of a control capture at a particular reactive state.
    /// </summary>
    public abstract class CapturePrototype : IEquatable<CapturePrototype>
    {
        /// <summary>
        /// The name for the capture.
        /// </summary>
        public NameRef<Capture> NameRef;

        /// <summary>
        /// The default serializer for parameter behaviors.
        /// </summary>
        [DefaultSerializer]
        public static readonly Serializer<CapturePrototype> Serializer = new _Serializer();

        /// <summary>
        /// A serializer for a parameter behavior.
        /// </summary>
        private sealed class _Serializer : Serializer<CapturePrototype>
        {
            public _Serializer()
                : base(EqualityComparer.Equatable<CapturePrototype>())
            { }

            public override CapturePrototype Read(ByteReader Reader)
            {
                NameRef<Capture> nameRef = NameRef<Capture>.Serializer.Read(Reader);
                return ((CaptureParameter)Parameter.Serializer.Read(Reader))
                    ._ReadCapturePrototype(nameRef, Reader);
            }

            public override void Write(ByteWriter Writer, CapturePrototype Value)
            {
                NameRef<Capture>.Serializer.Write(Writer, Value.NameRef);
                Value._Write(Writer);
            }
        }

        /// <summary>
        /// Creates an instance of the capture associated with this prototype.
        /// </summary>
        public abstract Capture Instantiate(Initializer Initializer);

        /// <summary>
        /// Begins a capture with this prototype using the given interface.
        /// </summary>
        public abstract void Begin(Transaction Transaction, Interface Interface, Group Subject);

        /// <summary>
        /// Updates a capture to this prototype using the given interface.
        /// </summary>
        public abstract void Update(Transaction Transaction, Interface Interface);

        /// <summary>
        /// Indicates whether this capture prototype is the same as another.
        /// </summary>
        public abstract bool Equals(CapturePrototype Other);

        /// <summary>
        /// Writes this prototype to a stream.
        /// </summary>
        internal abstract void _Write(ByteWriter Writer);
    }

    /// <summary>
    /// A full static description of a control capture at a particular reactive state.
    /// </summary>
    public sealed class CapturePrototype<T> : CapturePrototype
    {
        /// <summary>
        /// The parameter that is affected by the capture.
        /// </summary>
        public CaptureParameter<T> Parameter;

        /// <summary>
        /// The current value of the signal provided by the capture.
        /// </summary>
        public T Beat;

        public override Capture Instantiate(Initializer Initializer)
        {
            return new Capture<T>(Initializer, this.NameRef, this.Parameter, this.Beat);
        }

        public override void Begin(Transaction Transaction, Interface Interface, Group Subject)
        {
            Interface.BeginCapture(Transaction, Subject, this.Parameter, this.NameRef, this.Beat);
        }

        public override void Update(Transaction Transaction, Interface Interface)
        {
            Interface.UpdateCapture(Transaction, this.Parameter, this.NameRef, this.Beat);
        }

        public override bool Equals(CapturePrototype Other)
        {
            CapturePrototype<T> typedOther = Other as CapturePrototype<T>;
            if (typedOther == null)
                return false;
            return
                this.NameRef == typedOther.NameRef &&
                this.Parameter == typedOther.Parameter &&
                this.Parameter.BeatSerializer.Comparer.Equals(this.Beat, typedOther.Beat);
        }

        internal override void _Write(ByteWriter Writer)
        {
            Intel.Parameter.Serializer.Write(Writer, this.Parameter);
            this.Parameter.BeatSerializer.Write(Writer, this.Beat);
        }
    }
}
