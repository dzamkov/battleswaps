﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Intel
{
    /// <summary>
    /// Identifies and describes an entity that is "important" to gameplay to the extent that it may be controlled and
    /// inspected by players.
    /// </summary>
    public sealed class Unit : IUnit
    {
        private Signal<Rectangle> _Rectangle;
        private Controller _Controller;
        private static readonly InteriorRef<Signal<Rectangle>> _RectangleRef =
            (obj, act) => act(ref ((Unit)obj)._Rectangle);
        public Unit(Initializer Initializer, NameRef<Unit> NameRef)
        {
            this.NameRef = NameRef;
            NameRef.Put(Initializer, this);

            Property.Introduce(Ref.Create(this, _RectangleRef));
        }

        /// <summary>
        /// The named reference for this unit.
        /// </summary>
        public readonly NameRef<Unit> NameRef;

        /// <summary>
        /// Gets or sets the rectangle defining the visual bounds of the unit.
        /// </summary>
        public Property<Signal<Rectangle>> Rectangle
        {
            get
            {
                return Property.Get(Ref.Create(this, _RectangleRef), ref this._Rectangle);
            }
            set
            {
                Property.Define(Ref.Create(this, _RectangleRef), ref this._Rectangle, value);
            }
        }

        /// <summary>
        /// Gets or sets the controller for this unit. This should be set only once, before the unit is in use.
        /// </summary>
        public Controller Controller
        {
            get
            {
                Debug.Assert(this._Controller != null);
                return this._Controller;
            }
            set
            {
                Debug.Assert(this._Controller == null);
                value.Environment = this;
                this._Controller = value;
            }
        }

        NameRef<Unit> IUnit.NameRef
        {
            get
            {
                return this.NameRef;
            }
        }

        Signal<Rectangle> IUnit.Rectangle
        {
            get
            {
                return this._Rectangle;
            }
        }

        IController IUnit.Controller
        {
            get
            {
                return this.Controller;
            }
        }
    }

    /// <summary>
    /// A read-only interface for a unit, possibly withholding some information. This may also identify a unit
    /// within a certain context.
    /// </summary>
    public interface IUnit
    {
        /// <summary>
        /// Gets a named reference for this unit. The reference should not be accessed directly.
        /// </summary>
        NameRef<Unit> NameRef { get; }

        /// <summary>
        /// Gets a rectangle defining the visual bounds of the unit, or null if unknown.
        /// </summary>
        Signal<Rectangle> Rectangle { get; }

        /// <summary>
        /// Gets the controller for this unit.
        /// </summary>
        IController Controller { get; }
    }
}
