﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics;
using BattleSwaps.Graphics.Scene;

namespace BattleSwaps.Intel
{
    /// <summary>
    /// Describes a dynamic subset of information in an intel world, which can be taken to be the knowledge of a
    /// player/faction. This may include stored observations (e.g. last known unit positions) along with
    /// ongoing observations. The perspective itself is a limited interface to the world based on the observations
    /// it stores.
    /// </summary>
    public class Perspective : IWorld
    {
        private World _World;
        private static readonly InteriorRef<World> _WorldRef =
            (obj, act) => act(ref ((Perspective)obj)._World);
        public Perspective()
        {
            Property.Introduce(Ref.Create(this, _WorldRef));
        }

        /// <summary>
        /// Gets or sets the intel world this perspective is for.
        /// </summary>
        public Property<World> World
        {
            get
            {
                return Property.Get(Ref.Create(this, _WorldRef), ref this._World);
            }
            set
            {
                Property.Define(Ref.Create(this, _WorldRef), ref this._World, value);
            }
        }

        Bag<IUnit> IWorld.Units
        {
            get
            {
                return ((IWorld)this._World).Units;
            }
        }

        Composition IWorld.Scene
        {
            get
            {
                return ((IWorld)this._World).Scene;
            }
        }

        IUnit IWorld.GetUnit(Query Query, NameRef<Unit> NameRef)
        {
            throw new NotImplementedException();
        }

        IFaction IWorld.GetFaction(Query Query, NameRef<Faction> NameRef)
        {
            throw new NotImplementedException();
        }
    }
}