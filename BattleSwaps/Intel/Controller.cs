﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Intel
{
    /// <summary>
    /// Provides context-dependent low-level control signals and commands to a unit on behalf of its faction, and
    /// exposes control-related information.
    /// </summary>
    public abstract class Controller : IController
    {
        private readonly ExplicitSignal<NameRef<Faction>> _FactionRef;
        private Unit _Unit;
        private Signal<Faction> _Faction;
        private static readonly InteriorRef<Unit> _UnitRef =
            (obj, act) => act(ref ((Controller)obj)._Unit);
        public Controller(Initializer Initializer, ControllerState State)
        {
            this._FactionRef = new ExplicitSignal<NameRef<Faction>>(Initializer, State.Faction);

            Property.Introduce(Ref.Create(this, _UnitRef));
        }

        /// <summary>
        /// Gets the faction that this controller is currently acting under.
        /// </summary>
        public Signal<Faction> Faction
        {
            get
            {
                Signal<Faction> value = this._Faction;
                if (value == null)
                {
                    value = this._FactionRef.DeRef();
                    value = Interlocked.CompareExchange(ref this._Faction, value, null) ?? value;
                }
                return value;
            }
        }

        /// <summary>
        /// Gets or sets the unit this controller is for.
        /// </summary>
        public Property<Unit> Unit
        {
            get
            {
                return Property.Get(Ref.Create(this, _UnitRef), ref this._Unit);
            }
            set
            {
                Property.Define(Ref.Create(this, _UnitRef), ref this._Unit, value);
            }
        }

        /// <summary>
        /// Sets the environment for this controller.
        /// </summary>
        public Unit Environment
        {
            set
            {
                this.Unit = value;
            }
        }

        /// <summary>
        /// Gets a control parameter for this controller. These parameters are set by the faction that owns the
        /// controller.
        /// </summary>
        public Property<Signal<Maybe<T>>> GetParameter<T>(Parameter<T> Parameter)
        {
            return this.Unit.Map(unit =>
            {
                var behavior = this.Faction.Map(f => (Signal<Behavior>)f.Behavior).Switch();
                return behavior.Map(b => b.GetParameterSignal<T>(Parameter, unit).AsSignal).Switch();
            });
        }

        /// <summary>
        /// Gets the control capture source for this controller, or null if not applicable.
        /// </summary>
        public Property<Signal<Capture<T>>> GetCapture<T>(CaptureParameter<T> Parameter)
        {
            return this.GetParameter<NameRef<Capture>>(Parameter).Map(source =>
            {
                return Signal.Custom(SignalKind.Discrete,
                    delegate(Moment moment, OrLatchBuilder invalidate, ref bool isDiscrete)
                {
                    Maybe<NameRef<Capture>> cur = source.Get(moment, invalidate, ref isDiscrete);
                    if (cur.IsJust)
                        return (Capture<T>)cur.Value.Get(moment.State);
                    else
                        return null;
                });
            });
        }

        /// <summary>
        /// Gets the set of captures this controller responds to.
        /// </summary>
        public abstract Property<Bag<Capture>> ActiveCaptures { get; }

        PartialSignal<ControllerState> IController.State
        {
            get
            {
                return this._FactionRef.Map(f => new ControllerState(f));
            }
        }

        PartialSignal<bool> IController.IsCaptured
        {
            get
            {
                return this.ActiveCaptures.Map(ac => ac.GetArbitrary(null).Map(c => c != null)).RuntimeValue;
            }
        }

        bool IController.Is<T>()
        {
            return this is T;
        }
    }

    /// <summary>
    /// Describes the state of a controller.
    /// </summary>
    public struct ControllerState
    {
        public ControllerState(NameRef<Faction> Faction)
        {
            this.Faction = Faction;
        }

        /// <summary>
        /// The faction this controller belongs to.
        /// </summary>
        public NameRef<Faction> Faction;
    }

    /// <summary>
    /// A read-only interface for a controller, possibly withholding some information.
    /// </summary>
    public interface IController
    {
        /// <summary>
        /// Gets the state of this controller, when known.
        /// </summary>
        PartialSignal<ControllerState> State { get; }

        /// <summary>
        /// Indicates whether this controller is responding to any capture sources.
        /// </summary>
        PartialSignal<bool> IsCaptured { get; }

        /// <summary>
        /// Indicates whether this controller is of the given derived type. This provides information about the
        /// general behavior of the controller.
        /// </summary>
        bool Is<T>() where T : Controller;
    }

    /// <summary>
    /// A controller that provides vehicle-related control signals to a unit.
    /// </summary>
    public sealed class DriveController : Controller
    {
        private Signal<Capture<DriveBeat>> _Capture;
        private Signal<DriveBeat> _Input;
        public DriveController(Initializer Initializer, ControllerState State)
            : base(Initializer, State)
        { }

        /// <summary>
        /// The capture parameter which can be used to capture a drive controller.
        /// </summary>
        [Tag(typeof(Parameter), (uint)183655455)]
        public static readonly CaptureParameter<DriveBeat> CaptureParameter =
            new CaptureParameter<DriveBeat>(DriveBeat.Serializer);

        /// <summary>
        /// The input produced by this controller when idle.
        /// </summary>
        private static readonly Signal<DriveBeat> _IdleInput = DriveBeat.Idle;

        /// <summary>
        /// Gets the capture source for this controller, or null if not applicable.
        /// </summary>
        public Property<Signal<Capture<DriveBeat>>> Capture
        {
            get
            {
                if (this._Capture != null)
                    return this._Capture;
                return Property.From(delegate(out Signal<Capture<DriveBeat>> res)
                {
                    res = this._Capture;
                    if (res != null)
                        return true;

                    if (this.GetCapture(CaptureParameter).TryGetValue(out res))
                    {
                        res = Interlocked.CompareExchange(ref this._Capture, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets the drive input produced by this controller.
        /// </summary>
        public Property<Signal<DriveBeat>> Input
        {
            get
            {
                if (this._Input != null)
                    return this._Input;
                return Property.From(delegate(out Signal<DriveBeat> res)
                {
                    res = this._Input;
                    if (res != null)
                        return true;

                    Signal<Capture<DriveBeat>> capture;
                    if (this.Capture.TryGetValue(out capture))
                    {
                        res = capture.Map(c => c == null ? _IdleInput : c.Input).Switch();
                        res = Interlocked.CompareExchange(ref this._Input, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        public override Property<Bag<Capture>> ActiveCaptures
        {
            get
            {
                return this.Capture.Map(capture =>
                    capture.Map(c => c != null ? Bag.Singleton<Capture>(c) : Bag<Capture>.Empty).Switch());
            }
        }
    }

    /// <summary>
    /// Describes the input to a drive control signal at a given moment.
    /// </summary>
    public struct DriveBeat
    {
        [DefaultSerializer]
        public static readonly Serializer<DriveBeat> Serializer = Data.Serializer.Struct<DriveBeat>();
        public DriveBeat(double Forward, double Turn)
        {
            Debug.Assert(-1.0 <= Forward && Forward <= 1.0);
            Debug.Assert(-1.0 <= Turn && Turn <= 1.0);

            this.Forward = Forward;
            this.Turn = Turn;
        }

        /// <summary>
        /// The amount to drive in the forward direction, in the range [-1, 1], where 1 indicates full speed ahead
        /// and -1 indicates full brake/reverse.
        /// </summary>
        public double Forward;

        /// <summary>
        /// The amount to turn by, in the range [-1, 1], where -1 indicates maximum right and 1 indicates
        /// maximum left.
        /// </summary>
        public double Turn;

        /// <summary>
        /// The input corresponding to an idle vehicle.
        /// </summary>
        public static readonly DriveBeat Idle = new DriveBeat(0.0, 0.0);
    }

    /// <summary>
    /// A controller that provides weapon-related control signals to a unit.
    /// </summary>
    public sealed class WeaponController : Controller
    {
        private Signal<Capture<WeaponBeat>> _Capture;
        private Signal<WeaponBeat> _Input;
        public WeaponController(Initializer Initializer, ControllerState State)
            : base(Initializer, State)
        { }

        /// <summary>
        /// The capture parameter which can be used to capture a weapon controller.
        /// </summary>
        [Tag(typeof(Parameter), (uint)949425352)]
        public static readonly CaptureParameter<WeaponBeat> CaptureParameter =
            new CaptureParameter<WeaponBeat>(WeaponBeat.Serializer);

        /// <summary>
        /// The input produced by this controller when idle.
        /// </summary>
        private static readonly Signal<WeaponBeat> _IdleInput = WeaponBeat.Idle;

        /// <summary>
        /// Gets the capture source for this controller, or null if not applicable.
        /// </summary>
        public Property<Signal<Capture<WeaponBeat>>> Capture
        {
            get
            {
                if (this._Capture != null)
                    return this._Capture;
                return Property.From(delegate(out Signal<Capture<WeaponBeat>> res)
                {
                    res = this._Capture;
                    if (res != null)
                        return true;

                    if (this.GetCapture(CaptureParameter).TryGetValue(out res))
                    {
                        res = Interlocked.CompareExchange(ref this._Capture, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets the drive input produced by this controller.
        /// </summary>
        public Property<Signal<WeaponBeat>> Input
        {
            get
            {
                if (this._Input != null)
                    return this._Input;
                return Property.From(delegate(out Signal<WeaponBeat> res)
                {
                    res = this._Input;
                    if (res != null)
                        return true;

                    Signal<Capture<WeaponBeat>> capture;
                    if (this.Capture.TryGetValue(out capture))
                    {
                        res = capture.Map(c => c == null ? _IdleInput : c.Input).Switch();
                        res = Interlocked.CompareExchange(ref this._Input, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        public override Property<Bag<Capture>> ActiveCaptures
        {
            get
            {
                return this.Capture.Map(capture =>
                    capture.Map(c => c != null ? Bag.Singleton<Capture>(c) : Bag<Capture>.Empty).Switch());
            }
        }
    }

    /// <summary>
    /// Describes the input to a weapon control signal at a given moment.
    /// </summary>
    public struct WeaponBeat
    {
        [DefaultSerializer]
        public static readonly Serializer<WeaponBeat> Serializer = Data.Serializer.Struct<WeaponBeat>();
        public WeaponBeat(DetailVector? Aim, bool Fire)
        {
            this.Aim = Aim;
            this.Fire = Fire;
        }

        /// <summary>
        /// The point in world space to aim at.
        /// </summary>
        public DetailVector? Aim;

        /// <summary>
        /// Indicates whether the weapon should fire if possible.
        /// </summary>
        public bool Fire;

        /// <summary>
        /// The input corresponding to an idle weapon.
        /// </summary>
        public static readonly WeaponBeat Idle = new WeaponBeat(null, false);
    }
}
