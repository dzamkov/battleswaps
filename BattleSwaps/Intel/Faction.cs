﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;

namespace BattleSwaps.Intel
{
    /// <summary>
    /// Provides low-level control parameters for a dynamic set of units, allowing the units to work together
    /// coherently to accomplish goals. Every unit (controllable or not) is assigned to a faction from
    /// which it receives its control parameters.
    /// </summary>
    public sealed class Faction : IFaction
    {
        private IWorld _World;
        private DirectInterface _Interface;
        private static readonly InteriorRef<IWorld> _WorldRef =
            (obj, act) => act(ref ((Faction)obj)._World);
        public Faction(Initializer Initializer, NameRef<Faction> NameRef, FactionState State)
        {
            this.NameRef = NameRef;
            NameRef.Put(Initializer, this);

            ExplicitBagSetup<Capture> setup;
            this.Behavior = new ExplicitSignal<Behavior>(Initializer, State.Behavior);
            this.Captures = new ExplicitBag<Capture>(Initializer, out setup);
            for (int i = 0; i < State.Captures.Length; i++)
            {
                Capture capture = State.Captures[i].Instantiate(Initializer);
                setup.Add(Initializer, new Entry<Capture>(capture, capture.Cancel));
            }

            Property.Introduce(Ref.Create(this, _WorldRef));
        }

        /// <summary>
        /// The named reference for this faction.
        /// </summary>
        public readonly NameRef<Faction> NameRef;

        /// <summary>
        /// The behavior for units in this faction.
        /// </summary>
        public readonly ExplicitSignal<Behavior> Behavior;

        /// <summary>
        /// The set of control captures in effect for this faction.
        /// </summary>
        public readonly ExplicitBag<Capture> Captures;

        /// <summary>
        /// Gets or sets the interface this faction uses to read from the intel world in order to produce its
        /// control signals and commands. This represents the knowledge available to the faction.
        /// </summary>
        public Property<IWorld> World
        {
            get
            {
                return Property.Get(Ref.Create(this, _WorldRef), ref this._World);
            }
            set
            {
                Property.Define(Ref.Create(this, _WorldRef), ref this._World, value);
            }
        }

        /// <summary>
        /// Sets the environment for this faction.
        /// </summary>
        public World Environment
        {
            set
            {
                this.World = value;
            }
        }

        /// <summary>
        /// Gets an interface which allows action on behalf of this faction.
        /// </summary>
        public DirectInterface Interface
        {
            get
            {
                DirectInterface value = this._Interface;
                if (value == null)
                {
                    value = new DirectInterface(this);
                    value = Interlocked.CompareExchange(ref this._Interface, value, null) ?? value;
                }
                return value;
            }
        }

        NameRef<Faction> IFaction.NameRef
        {
            get
            {
                return this.NameRef;
            }
        }

        PartialSignal<Behavior> IFaction.Behavior
        {
            get
            {
                return this.Behavior;
            }
        }
    }

    /// <summary>
    /// Describes the state of a faction at a particular reactive state.
    /// </summary>
    public struct FactionState
    {
        /// <summary>
        /// The behavior of the units for the faction.
        /// </summary>
        public Behavior Behavior;

        /// <summary>
        /// The ongoing control captures currently in effect for the faction.
        /// </summary>
        public CapturePrototype[] Captures;
    }

    /// <summary>
    /// A read-only interface for a unit, possibly withholding some information.
    /// </summary>
    public interface IFaction
    {
        /// <summary>
        /// Gets a named reference for this faction. The reference should not be accessed directly.
        /// </summary>
        NameRef<Faction> NameRef { get; }

        /// <summary>
        /// Gets the behavior used by units of this faction, when known.
        /// </summary>
        PartialSignal<Behavior> Behavior { get; }
    }

    /// <summary>
    /// A full static description of an faction at a particular reactive state.
    /// </summary>
    public struct FactionPrototype
    {
        public FactionPrototype(NameRef<Faction> NameRef, FactionState State)
        {
            this.NameRef = NameRef;
            this.State = State;
        }

        /// <summary>
        /// The named reference for the faction.
        /// </summary>
        public NameRef<Faction> NameRef;

        /// <summary>
        /// The state of the faction.
        /// </summary>
        public FactionState State;

        /// <summary>
        /// Creates an instance of the faction described by this prototype. The only missing information that
        /// needs to be supplied before the entity can be put to use is its environment.
        /// </summary>
        public Faction Instantiate(Initializer Initializer)
        {
            return new Faction(Initializer, this.NameRef, this.State);
        }

        /// <summary>
        /// Creates an instance of the entity described by this prototype.
        /// </summary>
        public Faction Instantiate(Initializer Initializer, World Environment)
        {
            Faction faction = this.Instantiate(Initializer);
            faction.Environment = Environment;
            return faction;
        }
    }
}
