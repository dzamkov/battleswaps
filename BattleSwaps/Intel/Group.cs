﻿using System;
using System.Collections.Generic;

using BattleSwaps.Data;
using BattleSwaps.Reactive;

namespace BattleSwaps.Intel
{
    // TODO: Serialization

    /// <summary>
    /// Identifies a set of units, which may possibly vary over time.
    /// </summary>
    public abstract class Group
    {
        [DefaultSerializer]
        public static readonly Serializer<Group> Serializer = Data.Serializer.Open<byte, Group>();

        /// <summary>
        /// The empty group.
        /// </summary>
        public static readonly Group Empty = StaticGroup.Empty;

        /// <summary>
        /// Gets a signal which indicates whether the given unit is in this group.
        /// </summary>
        public abstract Signal<bool> Contains(Unit Unit);

        /// <summary>
        /// Constructs a group containing a single unit.
        /// </summary>
        public static Group Singleton(NameRef<Unit> Unit)
        {
            return StaticGroup.Singleton(Unit);
        }

        /// <summary>
        /// Constructs a group containing a single unit.
        /// </summary>
        public static Group Singleton(IUnit Unit)
        {
            return StaticGroup.Singleton(Unit);
        }
    }

    /// <summary>
    /// Statically identifies a set of units such that the membership of a unit does not change over time, and can
    /// be determined solely by the unit's name.
    /// </summary>
    [Tag(typeof(Group), (byte)0)]
    public sealed class StaticGroup : Group
    {
        // TODO: More efficient single-unit insertion/removal
        private readonly HashSet<NameRef<Unit>> _Members;
        public StaticGroup(HashSet<NameRef<Unit>> Members)
        {
            this._Members = Members;
        }

        /// <summary>
        /// The empty static group.
        /// </summary>
        public static readonly new StaticGroup Empty = new StaticGroup(
            new HashSet<NameRef<Unit>>(NameRef<Unit>.Serializer.Comparer));

        /// <summary>
        /// Indicates whether this is an empty group.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this._Members.Count == 0;
            }
        }

        /// <summary>
        /// Indicates whether this group contains a single member.
        /// </summary>
        public bool IsSingleton
        {
            get
            {
                return this._Members.Count == 1;
            }
        }

        /// <summary>
        /// Indicates whether the given unit is in this group.
        /// </summary>
        public bool StaticContains(IUnit Unit)
        {
            return this._Members.Contains(Unit.NameRef);
        }

        /// <summary>
        /// Indicates whether the unit with the given name is in this group.
        /// </summary>
        public bool StaticContains(NameRef<Unit> Unit)
        {
            return this._Members.Contains(Unit);
        }

        /// <summary>
        /// Constructs a group containing a single unit.
        /// </summary>
        public static new StaticGroup Singleton(NameRef<Unit> Unit)
        {
            return new StaticGroup(new HashSet<NameRef<Unit>> { Unit });
        }

        /// <summary>
        /// Constructs a group containing a single unit.
        /// </summary>
        public static new StaticGroup Singleton(IUnit Unit)
        {
            return Singleton(Unit.NameRef);
        }

        /// <summary>
        /// The serializer for this type.
        /// </summary>
        [DefaultSerializer]
        public static readonly new Serializer<StaticGroup> Serializer = new _Serializer();

        private class _Serializer : Serializer<StaticGroup>
        {
            public _Serializer()
                : base(new _EqualityComparer())
            { }

            public override StaticGroup Read(ByteReader Reader)
            {
                uint count = Reader.ReadUInt32();
                if (count == 0)
                    return StaticGroup.Empty;
                HashSet<NameRef<Unit>> members = new HashSet<NameRef<Unit>>(NameRef<Unit>.Serializer.Comparer);
                for (uint i = 0; i < count; i++)
                    members.Add(Reader.Read(NameRef<Unit>.Serializer));
                return new StaticGroup(members);
            }

            public override void Write(ByteWriter Writer, StaticGroup Value)
            {
                Writer.WriteUInt32((uint)Value._Members.Count);
                foreach (var member in Value._Members)
                    Writer.Write(NameRef<Unit>.Serializer, member);
            }
        }

        private class _EqualityComparer : EqualityComparer<StaticGroup>
        {
            public readonly IEqualityComparer<HashSet<NameRef<Unit>>> SetComparer =
                HashSet<NameRef<Unit>>.CreateSetComparer();

            public override bool Equals(StaticGroup A, StaticGroup B)
            {
                return SetComparer.Equals(A._Members, B._Members);
            }

            public override int GetHashCode(StaticGroup Obj)
            {
                return SetComparer.GetHashCode(Obj._Members);
            }
        }

        public override Signal<bool> Contains(Unit Unit)
        {
            return this.StaticContains(Unit);
        }

        public static StaticGroup operator +(StaticGroup A, StaticGroup B)
        {
            if (A.IsEmpty) return B;
            if (B.IsEmpty) return A;
            HashSet<NameRef<Unit>> members = new HashSet<NameRef<Unit>>(NameRef<Unit>.Serializer.Comparer);
            members.UnionWith(A._Members);
            members.UnionWith(B._Members);
            return new StaticGroup(members);
        }

        public static StaticGroup operator -(StaticGroup A, StaticGroup B)
        {
            if (A.IsEmpty || B.IsEmpty) return A;
            HashSet<NameRef<Unit>> members = new HashSet<NameRef<Unit>>(NameRef<Unit>.Serializer.Comparer);
            members.UnionWith(A._Members);
            members.ExceptWith(B._Members);
            return new StaticGroup(members);
        }
    }
}
