﻿using System;
using System.Collections.Generic;

using BattleSwaps.Data;
using BattleSwaps.Reactive;

namespace BattleSwaps.Intel
{
    /// <summary>
    /// Describes an external stimuli that can be applied to a game simulation through an interface.
    /// </summary>
    public abstract class Command
    {
        [DefaultSerializer]
        public static readonly Serializer<Command> Serializer = Data.Serializer.Open<byte, Command>();

        /// <summary>
        /// Applies this command to the given interface. If the interface does not have the expected state for
        /// the command, this will throw an exception or invalidate the transaction.
        /// </summary>
        public abstract void Apply(Transaction Transaction, Interface Interface);
    }

    /// <summary>
    /// A command which updates the behavior for a faction.
    /// </summary>
    [Tag(typeof(Command), (byte)(0))]
    public sealed class UpdateBehaviorCommand : Command
    {
        [DefaultSerializer]
        public static readonly new Serializer<UpdateBehaviorCommand> Serializer =
            Data.Serializer.Struct<UpdateBehaviorCommand>();
        public UpdateBehaviorCommand() { }
        public UpdateBehaviorCommand(Behavior Behavior)
        {
            this.Behavior = Behavior;
        }

        /// <summary>
        /// The behavior applied by this command.
        /// </summary>
        public Behavior Behavior;

        public override void Apply(Transaction Transaction, Interface Interface)
        {
            Interface.UpdateBehavior(Transaction, this.Behavior);
        }
    }

    /// <summary>
    /// A command which selectively overrides the behavior for a faction.
    /// </summary>
    [Tag(typeof(Command), (byte)(1))]
    public sealed class OverrideBehaviorCommand : Command
    {
        [DefaultSerializer]
        public static readonly new Serializer<OverrideBehaviorCommand> Serializer =
            Data.Serializer.Struct<OverrideBehaviorCommand>();
        public OverrideBehaviorCommand() { }
        public OverrideBehaviorCommand(Behavior Patch)
        {
            this.Patch = Patch;
        }

        /// <summary>
        /// The patch applied by this command.
        /// </summary>
        public Behavior Patch;

        public override void Apply(Transaction Transaction, Interface Interface)
        {
            Interface.OverrideBehavior(Transaction, this.Patch);
        }
    }

    /// <summary>
    /// A command which begins control capture.
    /// </summary>
    [Tag(typeof(Command), (byte)(2))]
    public sealed class BeginCaptureCommand : Command
    {
        [DefaultSerializer]
        public static readonly new Serializer<BeginCaptureCommand> Serializer =
            Data.Serializer.Struct<BeginCaptureCommand>();
        public BeginCaptureCommand() { }
        public BeginCaptureCommand(Group Subject, CapturePrototype Prototype)
        {
            this.Subject = Subject;
            this.Prototype = Prototype;
        }

        /// <summary>
        /// The group of units affected by capture.
        /// </summary>
        public Group Subject;

        /// <summary>
        /// The prototype for the capture this command initiates.
        /// </summary>
        public CapturePrototype Prototype;

        public override void Apply(Transaction Transaction, Interface Interface)
        {
            this.Prototype.Begin(Transaction, Interface, this.Subject);
        }
    }

    /// <summary>
    /// A command which updates the signal for a control capture.
    /// </summary>
    [Tag(typeof(Command), (byte)(3))]
    public sealed class UpdateCaptureCommand : Command
    {
        [DefaultSerializer]
        public static readonly new Serializer<UpdateCaptureCommand> Serializer =
            Data.Serializer.Struct<UpdateCaptureCommand>();
        public UpdateCaptureCommand() { }
        public UpdateCaptureCommand(CapturePrototype Prototype)
        {
            this.Prototype = Prototype;
        }

        /// <summary>
        /// The prototype for the capture after this command is applied.
        /// </summary>
        public CapturePrototype Prototype;

        public override void Apply(Transaction Transaction, Interface Interface)
        {
            this.Prototype.Update(Transaction, Interface);
        }
    }

    /// <summary>
    /// A command which cancels control capture.
    /// </summary>
    [Tag(typeof(Command), (byte)(4))]
    public sealed class CancelCaptureCommand : Command
    {
        [DefaultSerializer]
        public static readonly new Serializer<CancelCaptureCommand> Serializer =
            Data.Serializer.Struct<CancelCaptureCommand>();
        public CancelCaptureCommand() { }
        public CancelCaptureCommand(NameRef<Capture> Capture)
        {
            this.Capture = Capture;
        }

        /// <summary>
        /// The name of the capture to cancel.
        /// </summary>
        public NameRef<Capture> Capture;

        public override void Apply(Transaction Transaction, Interface Interface)
        {
            Interface.CancelCapture(Transaction, this.Capture);
        }
    }

    /// <summary>
    /// An interface which packages manipulations as <see cref="Command"/>'s.
    /// </summary>
    public sealed class CommandInterface : Interface
    {
        public CommandInterface(IWorld World, IFaction Faction, Stimulus<Command> Transmit)
            : base(World, Faction)
        {
            this.Transmit = Transmit;
        }

        /// <summary>
        /// The stimulus to transmit a command that is issued through this interface.
        /// </summary>
        public readonly Stimulus<Command> Transmit;

        public override void UpdateBehavior(Transaction Transaction, Behavior Behavior)
        {
            this.Transmit(Transaction, new UpdateBehaviorCommand(Behavior));
        }

        public override void OverrideBehavior(Transaction Transaction, Behavior Patch)
        {
            this.Transmit(Transaction, new OverrideBehaviorCommand(Patch));
        }

        public override void BeginCapture<T>(
            Transaction Transaction,
            Group Subject, CaptureParameter<T> Parameter,
            NameRef<Capture> NameRef, T Initial)
        {
            this.Transmit(Transaction, new BeginCaptureCommand(Subject, new CapturePrototype<T>
            {
                NameRef = NameRef,
                Parameter = Parameter,
                Beat = Initial
            }));
        }

        public override void UpdateCapture<T>(
            Transaction Transaction,
            CaptureParameter<T> Parameter,
            NameRef<Capture> NameRef, T Beat)
        {
            this.Transmit(Transaction, new UpdateCaptureCommand(new CapturePrototype<T>
            {
                NameRef = NameRef,
                Parameter = Parameter,
                Beat = Beat
            }));
        }

        public override void CancelCapture(Transaction Transaction, NameRef<Capture> NameRef)
        {
            this.Transmit(Transaction, new CancelCaptureCommand(NameRef));
        }
    }
}