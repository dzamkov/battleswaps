﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics;
using BattleSwaps.Graphics.Scene;

namespace BattleSwaps.Intel
{
    /// <summary>
    /// Identifies and describes the intel-related component of a game world.
    /// </summary>
    public sealed class World : IWorld
    {
        private Terrain.World _Terrain;
        private Assemblage _Assemblage;
        private Composition _Scene;
        private Bag<IUnit> _IUnits;
        private static readonly InteriorRef<Terrain.World> _TerrainRef =
            (obj, act) => act(ref ((World)obj)._Terrain);
        private static readonly InteriorRef<Assemblage> _AssemblageRef =
            (obj, act) => act(ref ((World)obj)._Assemblage);
        public World()
        {
            Property.Introduce(Ref.Create(this, _TerrainRef));
            Property.Introduce(Ref.Create(this, _AssemblageRef));
        }

        /// <summary>
        /// Gets or sets the terrain for this world.
        /// </summary>
        public Property<Terrain.World> Terrain
        {
            get
            {
                return Property.Get(Ref.Create(this, _TerrainRef), ref this._Terrain);
            }
            set
            {
                Property.Define(Ref.Create(this, _TerrainRef), ref this._Terrain, value);
            }
        }

        /// <summary>
        /// Gets or sets the full assemblage of content in this intel-based world.
        /// </summary>
        public Property<Assemblage> Assemblage
        {
            get
            {
                return Property.Get(Ref.Create(this, _AssemblageRef), ref this._Assemblage);
            }
            set
            {
                Property.Define(Ref.Create(this, _AssemblageRef), ref this._Assemblage, value);
            }
        }

        /// <summary>
        /// Gets the set of all units in this world.
        /// </summary>
        public Property<Bag<Unit>> Units
        {
            get
            {
                return Property.From(delegate(out Bag<Unit> units)
                {
                    Assemblage assemblage;
                    if (this.Assemblage.TryGetValue(out assemblage))
                    {
                        units = assemblage.Units;
                        return true;
                    }
                    units = default(Bag<Unit>);
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets a visual representation of this world.
        /// </summary>
        public Property<Composition> Scene
        {
            get
            {
                return Property.From(delegate(out Composition res)
                {
                    lock (this)
                    {
                        res = this._Scene;
                        if (res.IsValid)
                            return true;

                        Terrain.World terrain;
                        Assemblage assemblage;
                        AmbientLight ambientLight;
                        DirectionalLight directionalLight;
                        if (this.Terrain.TryGetValue(out terrain) &&
                            this.Assemblage.TryGetValue(out assemblage) &&
                            terrain.AmbientLight.TryGetValue(out ambientLight) &&
                            terrain.DirectionalLight.TryGetValue(out directionalLight))
                        {
                            this._Scene = res = new Composition
                            {
                                AmbientLight = ambientLight,
                                DirectionalLight = directionalLight,
                                HeightRange = new HeightRange(0.0, 4.0), // TODO
                                Content = assemblage.Content
                            };
                            return true;
                        }
                        return false;
                    }
                });
            }
        }

        Bag<IUnit> IWorld.Units
        {
            get
            {
                Bag<IUnit> value = this._IUnits;
                if (value == null)
                {
                    value = this._Assemblage.Units.Map<IUnit>(u => u);
                    value = Interlocked.CompareExchange(ref this._IUnits, value, null) ?? value;
                }
                return value;
            }
        }

        Composition IWorld.Scene
        {
            get
            {
                return this.Scene.RuntimeValue;
            }
        }


        IUnit IWorld.GetUnit(Query Query, NameRef<Unit> NameRef)
        {
            return NameRef[Query];
        }

        IFaction IWorld.GetFaction(Query Query, NameRef<Faction> NameRef)
        {
            return NameRef[Query];
        }
    }

    /// <summary>
    /// A read-only interface for an intel world, possibly withholding some information.
    /// </summary>
    public interface IWorld
    {
        /// <summary>
        /// Gets the set of all known units in the world.
        /// </summary>
        Bag<IUnit> Units { get; }

        /// <summary>
        /// Gets a partial visual representation of the world, with only known objects being visible.
        /// </summary>
        Graphics.Scene.Composition Scene { get; }

        /// <summary>
        /// Gets the unit with the given name.
        /// </summary>
        IUnit GetUnit(Query Query, NameRef<Unit> NameRef);

        /// <summary>
        /// Gets the faction with the given name.
        /// </summary>
        IFaction GetFaction(Query Query, NameRef<Faction> NameRef);
    }

    /// <summary>
    /// Describes the intel-related contents of a world, or a portion of a world.
    /// </summary>
    public struct Assemblage
    {
        /// <summary>
        /// The units presented by this assemblage.
        /// </summary>
        public Bag<Unit> Units;

        /// <summary>
        /// The graphical contributions of this assemblage.
        /// </summary>
        public Content Content;

        /// <summary>
        /// Gets a latch such that this assemblage is empty at all states where the latch is closed.
        /// </summary>
        public Latch Dissolve
        {
            get
            {
                return
                    this.Units.Dissolve &
                    this.Content.Dissolve;
            }
        }

        /// <summary>
        /// Indicates whether this structure is valid.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this.Units != null && this.Content != null;
            }
        }

        /// <summary>
        /// The empty assemblage.
        /// </summary>
        public static readonly Assemblage Empty = new Assemblage
        {
            Units = Bag<Intel.Unit>.Empty,
            Content = Graphics.Scene.Content.Empty
        };

        /// <summary>
        /// Gets the union of a bag of assemblages.
        /// </summary>
        public static Assemblage Union(Bag<Assemblage> Sources)
        {
            return new Assemblage
            {
                Units = Bag.Union(Sources.Map(a => a.Units)),
                Content = Graphics.Scene.Content.Union(Sources.Map(a => a.Content))
            };
        }

        public static Assemblage operator +(Assemblage A, Assemblage B)
        {
            return new Assemblage
            {
                Units = A.Units + B.Units,
                Content = A.Content + B.Content
            };
        }
    }
}
