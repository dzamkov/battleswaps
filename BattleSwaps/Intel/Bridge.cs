﻿using System;
using System.Collections.Generic;
using System.Threading;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics;

namespace BattleSwaps.Intel
{
    /// <summary>
    /// A read-only interface to a world in a reactive system different from the externally-visible reactive system.
    /// </summary>
    public sealed class BridgeWorld : IWorld
    {
        private Bag<IUnit> _Units;
        public BridgeWorld(Bridge Bridge, IWorld Inner)
        {
            this.Bridge = Bridge;
            this.Inner = Inner;
        }

        /// <summary>
        /// The bridge providing the mapping between the externally-visible reactive system and the internal
        /// simulation system.
        /// </summary>
        public readonly Bridge Bridge;

        /// <summary>
        /// The interface to the world in the "internal" reactive system.
        /// </summary>
        public readonly IWorld Inner;

        /// <summary>
        /// Gets the external interface to the given unit in the internal interface.
        /// </summary>
        public BridgeUnit GetExternal(IUnit Inner)
        {
            // TODO: Reuse units maybe?
            return new BridgeUnit(this.Bridge, Inner);
        }

        Bag<IUnit> IWorld.Units
        {
            get
            {
                Bag<IUnit> value = this._Units;
                if (value == null)
                {
                    value = this.Bridge.InverseApply(this.Inner.Units).Map(unit => (IUnit)this.GetExternal(unit));
                    value = Interlocked.CompareExchange(ref this._Units, value, null) ?? value;
                }
                return value;
            }
        }

        Graphics.Scene.Composition IWorld.Scene
        {
            get
            {
                return Graphics.Scene.Composition.InverseApplyBridge(this.Bridge, this.Inner.Scene);
            }
        }

        IUnit IWorld.GetUnit(Query Query, NameRef<Unit> Name)
        {
            return this.GetExternal(this.Bridge.Inquire(Query, query => this.Inner.GetUnit(query, Name)));
        }

        IFaction IWorld.GetFaction(Query Query, NameRef<Faction> NameRef)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A read-only interface to a unit in a reactive system different from the externally-visible reactive system.
    /// </summary>
    public sealed class BridgeUnit : IUnit
    {
        private Signal<Rectangle> _Rectangle;
        private BridgeController _Controller;
        public BridgeUnit(Bridge Bridge, IUnit Inner)
        {
            this.Bridge = Bridge;
            this.Inner = Inner;
        }

        /// <summary>
        /// The bridge providing the mapping between the externally-visible reactive system and the internal
        /// simulation system.
        /// </summary>
        public readonly Bridge Bridge;

        /// <summary>
        /// The interface to the unit in the "internal" reactive system.
        /// </summary>
        public readonly IUnit Inner;

        /// <summary>
        /// Gets the name of the unit for this interface.
        /// </summary>
        public NameRef<Unit> NameRef
        {
            get
            {
                return this.Inner.NameRef;
            }
        }

        public static bool operator ==(BridgeUnit A, BridgeUnit B)
        {
            return A.Bridge == B.Bridge && A.Inner == B.Inner;
        }

        public static bool operator !=(BridgeUnit A, BridgeUnit B)
        {
            return !(A == B);
        }

        public override bool Equals(object Other)
        {
            if (Other is BridgeUnit)
                return this == (BridgeUnit)Other;
            return false;
        }

        public override int GetHashCode()
        {
            return this.Bridge.GetHashCode() ^ this.Inner.GetHashCode();
        }

        Signal<Rectangle> IUnit.Rectangle
        {
            get
            {
                Signal<Rectangle> value = this._Rectangle;
                if (value == null)
                {
                    value = this.Bridge.InverseApply(this.Inner.Rectangle);
                    value = Interlocked.CompareExchange(ref this._Rectangle, value, null) ?? value;
                }
                return value;
            }
        }

        IController IUnit.Controller
        {
            get
            {
                BridgeController value = this._Controller;
                if (value == null)
                {
                    value = new BridgeController(this.Bridge, this.Inner.Controller);
                    value = Interlocked.CompareExchange(ref this._Controller, value, null) ?? value;
                }
                return value;
            }
        }
    }

    /// <summary>
    /// A read-only interface to a controller in a reactive system different from the externally-visible reactive
    /// system.
    /// </summary>
    public sealed class BridgeController : IController
    {
        private PartialSignal<ControllerState> _State;
        private PartialSignal<bool> _IsCaptured;
        public BridgeController(Bridge Bridge, IController Inner)
        {
            this.Bridge = Bridge;
            this.Inner = Inner;
        }

        /// <summary>
        /// The bridge providing the mapping between the externally-visible reactive system and the internal
        /// simulation system.
        /// </summary>
        public readonly Bridge Bridge;

        /// <summary>
        /// The interface to the controller in the "internal" reactive system.
        /// </summary>
        public readonly IController Inner;

        PartialSignal<ControllerState> IController.State
        {
            get
            {
                PartialSignal<ControllerState> value = this._State;
                if (value == null)
                {
                    value = this.Bridge.InverseApply(this.Inner.State);
                    value = Interlocked.CompareExchange(ref this._State, value, null) ?? value;
                }
                return value;
            }
        }

        PartialSignal<bool> IController.IsCaptured
        {
            get
            {
                PartialSignal<bool> value = this._IsCaptured;
                if (value == null)
                {
                    value = this.Bridge.InverseApply(this.Inner.IsCaptured);
                    value = Interlocked.CompareExchange(ref this._IsCaptured, value, null) ?? value;
                }
                return value;
            }
        }

        bool IController.Is<T>()
        {
            return this.Inner.Is<T>();
        }
    }

    /// <summary>
    /// A read-only interface to a faction in a reactive system different from the externally-visible reactive
    /// system.
    /// </summary>
    public sealed class BridgeFaction : IFaction
    {
        public BridgeFaction(Bridge Bridge, IFaction Inner)
        {
            this.Bridge = Bridge;
            this.Inner = Inner;
        }

        /// <summary>
        /// The bridge providing the mapping between the externally-visible reactive system and the internal
        /// simulation system.
        /// </summary>
        public readonly Bridge Bridge;

        /// <summary>
        /// The interface to the faction in the "internal" reactive system.
        /// </summary>
        public readonly IFaction Inner;

        NameRef<Faction> IFaction.NameRef
        {
            get
            {
                return this.Inner.NameRef;
            }
        }

        PartialSignal<Behavior> IFaction.Behavior
        {
            get { throw new NotImplementedException(); }
        }
    }
}
