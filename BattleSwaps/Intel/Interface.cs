﻿using System;
using System.Collections.Generic;
using System.Threading;

using BattleSwaps.Reactive;

namespace BattleSwaps.Intel
{
    /// <summary>
    /// An interface which allows acting in a game world through a faction.
    /// </summary>
    public abstract class Interface
    {
        public Interface(IWorld World, IFaction Faction)
        {
            this.World = World;
            this.Faction = Faction;
        }

        /// <summary>
        /// A limited-knowledge perspective of the world this interface is in.
        /// </summary>
        public readonly IWorld World;

        /// <summary>
        /// The faction this interface is for within its limited-knowledge world perspective.
        /// </summary>
        public readonly IFaction Faction;

        /// <summary>
        /// Replaces the behavior used by the faction for this interface.
        /// </summary>
        public abstract void UpdateBehavior(Transaction Transaction, Behavior Behavior);

        /// <summary>
        /// Ammends the behavior used by the faction for this interface.
        /// </summary>
        public abstract void OverrideBehavior(Transaction Transaction, Behavior Patch);

        /// <summary>
        /// Begins control capture for a group of units.
        /// </summary>
        /// <param name="Subject">The group of units to capture.</param>
        /// <param name="Parameter">The capture parameter to set for captured units.</param>
        /// <param name="Initial">The initial value of the control signal for the capture.</param>
        public abstract void BeginCapture<T>(
            Transaction Transaction, Group Subject,
            CaptureParameter<T> Parameter,
            NameRef<Capture> NameRef, T Initial);

        /// <summary>
        /// Updates the signal for a control capture.
        /// </summary>
        public abstract void UpdateCapture<T>(
            Transaction Transaction,
            CaptureParameter<T> Parameter,
            NameRef<Capture> NameRef, T Beat);

        /// <summary>
        /// Releases control capture for a set of units. This will remove all references to the capture in the
        /// faction behavior.
        /// </summary>
        public abstract void CancelCapture(Transaction Transaction, NameRef<Capture> NameRef);
    }

    /// <summary>
    /// An interface which directly references a faction.
    /// </summary>
    public sealed class DirectInterface : Interface
    {
        public DirectInterface(Faction Faction)
            : base(Faction.World.RuntimeValue, Faction)
        { }

        /// <summary>
        /// Gets the faction for this interface.
        /// </summary>
        public new Faction Faction
        {
            get
            {
                return (Faction)base.Faction;
            }
        }

        public override void UpdateBehavior(Transaction Transaction, Behavior Behavior)
        {
            this.Faction.Behavior[Transaction] = Behavior;
        }

        public override void OverrideBehavior(Transaction Transaction, Behavior Patch)
        {
            Behavior cur = this.Faction.Behavior[Transaction];
            this.Faction.Behavior[Transaction] = cur + Patch;
        }

        public override void BeginCapture<T>(
            Transaction Transaction, Group Subject,
            CaptureParameter<T> Parameter,
            NameRef<Capture> NameRef, T Initial)
        {
            // Create capture
            Capture<T> capture = new Capture<T>(Transaction.Initializer, NameRef, Parameter, Initial);
            this.Faction.Captures.Add(Transaction, new Entry<Capture>(capture, capture.Cancel));

            // Ammend behavior
            Behavior cur = this.Faction.Behavior[Transaction];
            this.Faction.Behavior[Transaction] = cur + Behavior.Define(Parameter, NameRef).For(Subject);
        }

        public override void UpdateCapture<T>(
            Transaction Transaction,
            CaptureParameter<T> Parameter,
            NameRef<Capture> NameRef, T Beat)
        {
            // TODO: Ensure faction owns the capture
            Capture<T> capture = (Capture<T>)NameRef[Transaction];
            if (capture.Parameter != Parameter)
                throw new Exception("Wrong capture parameter");
            capture.Input[Transaction] = Beat;
        }

        public override void CancelCapture(Transaction Transaction, NameRef<Capture> NameRef)
        {
            // TODO: Ensure faction owns the capture
            Capture capture = NameRef[Transaction];
            capture.Cancel.Trigger(Transaction);

            Behavior cur = this.Faction.Behavior[Transaction];
            this.Faction.Behavior[Transaction] = cur.Remove(capture.Parameter, NameRef);
        }
    }
}