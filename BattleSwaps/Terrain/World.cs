﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Graphics.Scene;

namespace BattleSwaps.Terrain
{
    /// <summary>
    /// Identifies and describes the terrain-related component of a game world.
    /// </summary>
    public sealed class World
    {
        private AmbientLight _AmbientLight;
        private DirectionalLight _DirectionalLight;
        private HeightLayer _HeightLayer;
        private static readonly InteriorRef<AmbientLight> _AmbientLightRef =
            (obj, act) => act(ref ((World)obj)._AmbientLight);
        private static readonly InteriorRef<DirectionalLight> _DirectionalLightRef =
            (obj, act) => act(ref ((World)obj)._DirectionalLight);
        private static readonly InteriorRef<HeightLayer> _HeightLayerRef =
            (obj, act) => act(ref ((World)obj)._HeightLayer);
        public World()
        {
            Property.Introduce(Ref.Create(this, _AmbientLightRef));
            Property.Introduce(Ref.Create(this, _DirectionalLightRef));
            Property.Introduce(Ref.Create(this, _HeightLayerRef));
        }

        /// <summary>
        /// Gets or sets the ambient lighting of the world.
        /// </summary>
        public Property<AmbientLight> AmbientLight
        {
            get
            {
                return Property.Get(Ref.Create(this, _AmbientLightRef), ref this._AmbientLight);
            }
            set
            {
                Property.Define(Ref.Create(this, _AmbientLightRef), ref this._AmbientLight, value);
            }
        }


        /// <summary>
        /// Gets or sets the directional lighting of the world.
        /// </summary>
        public Property<DirectionalLight> DirectionalLight
        {
            get
            {
                return Property.Get(Ref.Create(this, _DirectionalLightRef), ref this._DirectionalLight);
            }
            set
            {
                Property.Define(Ref.Create(this, _DirectionalLightRef), ref this._DirectionalLight, value);
            }
        }

        /// <summary>
        /// Gets or sets the height layer for this terrain.
        /// </summary>
        public Property<HeightLayer> HeightLayer
        {
            get
            {
                return Property.Get(Ref.Create(this, _HeightLayerRef), ref this._HeightLayer);
            }
            set
            {
                Property.Define(Ref.Create(this, _HeightLayerRef), ref this._HeightLayer, value);
            }
        }
    }

    /// <summary>
    /// A full static description of a terrain.
    /// </summary>
    public struct WorldPrototype
    {
        /// <summary>
        /// The ambient light for the world.
        /// </summary>
        public AmbientLight AmbientLight;

        /// <summary>
        /// The directional light for the world.
        /// </summary>
        public DirectionalLight DirectionalLight;

        /// <summary>
        /// Creates an instance of the world described by this prototype.
        /// </summary>
        public World Instantiate(Initializer Initializer)
        {
            return new World()
            {
                AmbientLight = this.AmbientLight,
                DirectionalLight = this.DirectionalLight
            };
        }
    }
}