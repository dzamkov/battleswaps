﻿using System;
using System.Collections.Generic;

using BattleSwaps.Geometry;

namespace BattleSwaps.Terrain
{
    /// <summary>
    /// Contains functions related to patches.
    /// </summary>
    public static class Patch
    {
        /// <summary>
        /// The height of a triangle in a patch.
        /// </summary>
        public static readonly double TriangleHeight = Math.Sqrt(3.0) / 2.0;

        /// <summary>
        /// Gets the bounds of a patch with the given data width and height.
        /// </summary>
        public static PatchBounds GetBounds(uint Width, uint Height)
        {
            return new PatchBounds(0, 2 * (int)Width - 3, 0, (int)Height - 1);
        }

        /// <summary>
        /// Gets the patch vector for a point stored at the given location in a patch.
        /// </summary>
        public static PatchVector GetPointAt(uint Y, uint X)
        {
            return new PatchVector((int)X * 2 - ((int)Y & 1), (int)Y);
        }
    }

    /// <summary>
    /// Assigns every point on a finite regular 2D grid to a value of the given type. For
    /// sampling/interpolation purposes, the grid is a tessellation of equilateral triangles, all of which pointing
    /// directly upward (-Y) or downward (+Y).
    /// </summary>
    public struct Patch<T>
    {
        public Patch(uint Width, uint Height)
        {
            this.Data = new T[Height, Width];
        }

        /// <summary>
        /// The point data for this patch. The vector difference between points in the same row is always (1, 0),
        /// while the difference between points in the same column is ((+/-)1/2, sqrt(3)/2). For example, the point
        /// [1, 0] has coordinates (-1/2, sqrt(3)/2) and the point [2, 1] has coordinates (1, sqrt(3)).
        /// </summary>
        public T[,] Data;

        /// <summary>
        /// Gets or sets the value of a point in this patch.
        /// </summary>
        public T this[PatchVector Point]
        {
            get
            {
                return this.Data[Point.Y, (Point.X + 1) / 2];
            }
            set
            {
                this.Data[Point.Y, (Point.X + 1) / 2] = value;
            }
        }

        /// <summary>
        /// Gets the number of points in a horizontal line on this patch.
        /// </summary>
        public uint Width
        {
            get
            {
                return (uint)this.Data.GetLength(1);
            }
        }

        /// <summary>
        /// Gets the number of vertical lines of points on this patch.
        /// </summary>
        public uint Height
        {
            get
            {
                return (uint)this.Data.GetLength(0);
            }
        }

        /// <summary>
        /// Replaces the values of all points in this patch with the given value.
        /// </summary>
        public void Fill(T Value)
        {
            for (uint y = 0; y < this.Height; y++)
                for (uint x = 0; x < this.Width; x++)
                    this.Data[y, x] = Value;
        }

        /// <summary>
        /// Gets the largest bounds for which all points are covered by this patch.
        /// </summary>
        public PatchBounds Bounds
        {
            get
            {
                return Patch.GetBounds(this.Width, this.Height);
            }
        }
    }

    /// <summary>
    /// Describes a vector between two points on a patch.
    /// </summary>
    public struct PatchVector
    {
        public int X;
        public int Y;
        public PatchVector(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }

        /// <summary>
        /// The zero vector.
        /// </summary>
        public static readonly PatchVector Zero = new PatchVector(0, 0);

        /// <summary>
        /// Gets the square of the real length of this vector.
        /// </summary>
        public double SquareLength
        {
            get
            {
                return ((Vector)this).SquareLength;
            }
        }

        /// <summary>
        /// Gets the real length of this vector.
        /// </summary>
        public double Length
        {
            get
            {
                return Math.Sqrt(this.SquareLength);
            }
        }

        /// <summary>
        /// Gets all vectors with a length not greater than the given value, returning them in no particular order.
        /// </summary>
        public static IEnumerable<PatchVector> GetSmall(double Length)
        {
            double sqrLen = Length * Length;
            int maxY = (int)Math.Floor(Length / Patch.TriangleHeight);
            for (int y = -maxY; y <= maxY; y++)
            {
                double halfWidth = Math.Sqrt(sqrLen - (Patch.TriangleHeight * Patch.TriangleHeight) * (y * y));
                int maxX = (int)Math.Floor(2.0 * halfWidth);
                int minX = -maxX;
                int adjustedMinX = minX + ((minX ^ y) & 1); // Ensure x always has the same parity as y
                for (int x = adjustedMinX; x <= maxX; x += 2)
                {
                    yield return new PatchVector(x, y);
                }
            }
        }

        public static implicit operator Vector(PatchVector Source)
        {
            return new Vector(0.5 * Source.X, Patch.TriangleHeight * Source.Y);
        }

        public static PatchVector operator +(PatchVector A, PatchVector B)
        {
            return new PatchVector(A.X + B.X, A.Y + B.Y);
        }

        public static PatchVector operator -(PatchVector A, PatchVector B)
        {
            return new PatchVector(A.X - B.X, A.Y - B.Y);
        }

        public static PatchVector operator *(int A, PatchVector B)
        {
            return new PatchVector(A * B.X, A * B.Y);
        }

        public static PatchVector operator *(PatchVector A, int B)
        {
            return new PatchVector(A.X * B, A.Y * B);
        }

        public override string ToString()
        {
            return String.Format("({0}, {1})", this.X, this.Y);
        }
    }

    /// <summary>
    /// Describes a rectangular area on a patch.
    /// </summary>
    public struct PatchBounds
    {
        public PatchVector Min;
        public PatchVector Max;
        public PatchBounds(PatchVector Min, PatchVector Max)
        {
            this.Min = Min;
            this.Max = Max;
        }

        public PatchBounds(int MinX, int MaxX, int MinY, int MaxY)
            : this(new PatchVector(MinX, MinY), new PatchVector(MaxX, MaxY))
        { }

        /// <summary>
        /// Gets the tighest patch bounds including all patch points within the given bounds.
        /// </summary>
        public static PatchBounds Tight(Bounds Bounds)
        {
            return new PatchBounds(
                new PatchVector(
                    (int)Math.Ceiling(Bounds.Min.X / 0.5),
                    (int)Math.Ceiling(Bounds.Min.Y / Patch.TriangleHeight)),
                new PatchVector(
                    (int)Math.Floor(Bounds.Max.X / 0.5),
                    (int)Math.Floor(Bounds.Max.Y / Patch.TriangleHeight)));
        }

        /// <summary>
        /// Indicates whether these bounds contain the given bound.
        /// </summary>
        public bool Contains(PatchVector Point)
        {
            return
                Point.X >= this.Min.X &&
                Point.Y >= this.Min.Y &&
                Point.X <= this.Max.X &&
                Point.Y <= this.Max.Y;
        }

        /// <summary>
        /// Gets all patch points contained within these bounds.
        /// </summary>
        public IEnumerable<PatchVector> ContainedPoints
        {
            get
            {
                for (int y = this.Min.Y; y <= this.Max.Y; y++)
                {
                    // Ensure x always has the same parity as y
                    int adjustedMinX = this.Min.X + ((this.Min.X ^ y) & 1);
                    for (int x = adjustedMinX; x <= this.Max.X; x += 2)
                    {
                        yield return new PatchVector(x, y);
                    }
                }
            }
        }

        public static implicit operator Bounds(PatchBounds Bounds)
        {
            return new Bounds(Bounds.Min, Bounds.Max);
        }

        public override string ToString()
        {
            return String.Format("([{0}, {1}], [{2}, {3}])", this.Min.X, this.Max.X, this.Min.Y, this.Max.Y);
        }
    }

    /// <summary>
    /// A patch vector associated with a weight.
    /// </summary>
    public struct WeightedPatchVector
    {
        public PatchVector Source;
        public double Weight;
        public WeightedPatchVector(PatchVector Source, double Weight)
        {
            this.Source = Source;
            this.Weight = Weight;
        }

        /// <summary>
        /// Gets a set of weighted patch vectors that make up a kernel of the given radius. All patch vectors with
        /// a length less than the given radius will be included, and their weights will fall off
        /// quadratically to 0 as their length approaches the radius. Additionally, the sum of all weights will be 1.
        /// </summary>
        public static WeightedPatchVector[] GetQuadraticKernel(double Radius)
        {
            List<WeightedPatchVector> list = new List<WeightedPatchVector>();
            double sqrRadius = Radius * Radius;
            double totalWeight = 0.0;
            foreach (var vec in PatchVector.GetSmall(Radius))
            {
                double weight = 1.0 - vec.SquareLength / sqrRadius;
                list.Add(new WeightedPatchVector(vec, weight));
                totalWeight += weight;
            }
            WeightedPatchVector[] kernel = list.ToArray();
            double iTotalWeight = 1.0 / totalWeight;
            for (int i = 0; i < kernel.Length; i++)
                kernel[i].Weight *= iTotalWeight;
            return kernel;
        }
    }
}
