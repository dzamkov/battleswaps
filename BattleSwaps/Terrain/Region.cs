﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Linq;

using BattleSwaps.Util;
using BattleSwaps.Geometry;

namespace BattleSwaps.Terrain
{
    /// <summary>
    /// Describes a region of a terrain.
    /// </summary>
    public sealed class Region
    {
        private Dictionary<Feature, Patch<double>> _ErrorPatches;
        public Region(Bounds Bounds,
            OrthoTransform PatchTransform, uint PatchWidth, uint PatchHeight,
            Dictionary<Ground, uint> GroundIndex, Patch<double>[] GroundPresence)
        {
            this._ErrorPatches = new Dictionary<Feature, Patch<double>>();
            this.Bounds = Bounds;
            this.PatchTransform = PatchTransform;
            this.PatchWidth = PatchWidth;
            this.PatchHeight = PatchHeight;
            this.GroundIndex = GroundIndex;
            this.PresencePatches = GroundPresence;
        }

        /// <summary>
        /// The bounds for which the base data for this region is valid.
        /// </summary>
        public readonly Bounds Bounds;

        /// <summary>
        /// The transform from the patch space (of all patches defined in this region) to real terrain space.
        /// </summary>
        public readonly OrthoTransform PatchTransform;

        /// <summary>
        /// The number of points on a horizontal line of a patch in this region.
        /// </summary>
        public readonly uint PatchWidth;

        /// <summary>
        /// The number of points on a vertical line of a patch in this region.
        /// </summary>
        public readonly uint PatchHeight;

        /// <summary>
        /// A read-only mapping from grounds to their storage index in this region.
        /// </summary>
        public readonly Dictionary<Ground, uint> GroundIndex;

        /// <summary>
        /// Stores the presence of ground types with corresponding indices. Note that at each point, the sum of
        /// the presence of all ground types is 1.
        /// </summary>
        public readonly Patch<double>[] PresencePatches;

        /// <summary>
        /// Creates a seed region with the given properties.
        /// </summary>
        public static Region CreateSeed(
            OrthoTransform PatchTransform, uint PatchWidth, uint PatchHeight,
            Ground[] Grounds, Random Random)
        {
            // Initialize patches
            Dictionary<Ground, uint> groundIndex = new Dictionary<Ground, uint>();
            Patch<double>[] groundPresence = new Patch<double>[Grounds.Length];
            for (uint i = 0; i < Grounds.Length; i++)
            {
                Patch<double> patch = new Patch<double>(PatchWidth, PatchHeight);
                groundPresence[i] = patch;
                groundIndex[Grounds[i]] = i;
            }

            // Randomize patch values so that the sum of all patches at a given point is 1
            for (uint y = 0; y < PatchWidth; y++)
            {
                for (uint x = 0; x < PatchHeight; x++)
                {
                    double rem = 1.0;
                    for (int i = 0; i < Grounds.Length; i++)
                    {
                        double value = rem;
                        double rand = Random.NextDouble();
                        for (int exp = Grounds.Length - i - 1; exp > 0; exp--) // TODO: Use exponentation by squaring
                            value *= rand;
                        rem -= value;
                        groundPresence[i].Data[x, y] = value;
                    }
                }
            }

            // Create region
            return new Region(
                PatchTransform * (Bounds)Patch.GetBounds(PatchWidth, PatchHeight),
                PatchTransform, PatchWidth, PatchHeight, groundIndex, groundPresence);
        }

        /// <summary>
        /// Gets a patch which provides the error for the given feature at points within this region. Note that
        /// the bounds for which this patch is valid may be smaller than the bounds for the region.
        /// </summary>
        public Patch<double> GetErrorPatch(Feature Feature)
        {
            lock (this._ErrorPatches)
            {
                Patch<double> res;
                if (!this._ErrorPatches.TryGetValue(Feature, out res))
                {
                    res = new Patch<double>(this.PatchWidth, this.PatchHeight);
                    Feature.ComputeError(this, PatchBounds.Tight(
                        this.PatchTransform.Inverse * this.Bounds.Pad(-Feature.Extent)), res);
                    this._ErrorPatches[Feature] = res;
                }
                return res;
            }
        }

        /// <summary>
        /// Gets a patch which provides the presence for the given ground type at points within this region. Presence
        /// is normalized such that the sum of the presence values for all ground types at a point is 1.
        /// </summary>
        public Patch<double> GetPresencePatch(Ground Ground)
        {
            return this.PresencePatches[this.GroundIndex[Ground]];
        }
    }

    /// <summary>
    /// Describes an optimization objective for a region.
    /// </summary>
    public struct Objective
    {
        public Objective(Bounds Bounds, Feature Feature)
        {
            this.Bounds = Bounds;
            this.Feature = Feature;
        }

        /// <summary>
        /// The bounds to optimize in.
        /// </summary>
        public Bounds Bounds;

        /// <summary>
        /// The feature to attempt to satisfy over the region.
        /// </summary>
        public Feature Feature;

        /// <summary>
        /// Gets the total error for this objective in the given region. This will be 0 when the objective is
        /// completely satisfied, and decrease as the region becomes less fit for the object.
        /// </summary>
        public double GetError(Region Region)
        {
            double totalError = 0.0;
            Patch<double> errorPatch = Region.GetErrorPatch(this.Feature);
            foreach (var point in PatchBounds.Tight(Region.PatchTransform.Inverse * this.Bounds).ContainedPoints)
                totalError += errorPatch[point];
            return totalError;
        }

        /// <summary>
        /// Creates an adjustment which attempts to optimize the given region with respect to this objective.
        /// </summary>
        public RegionAdjustment CreateAdjustment(Region Region)
        {
            PatchBounds patchBounds = PatchBounds.Tight(Region.PatchTransform.Inverse * this.Bounds);
            RegionAdjustment adjustment = RegionAdjustment.Create(Region, patchBounds);
            Patch<double> errorAdjustmentPatch = new Patch<double>(Region.PatchWidth, Region.PatchHeight);
            foreach (var point in patchBounds.ContainedPoints)
                errorAdjustmentPatch[point] = 1.0;
            adjustment.ErrorPatches.Add(this.Feature, errorAdjustmentPatch);
            return adjustment;
        }
    }

    /// <summary>
    /// An interface for adjusting a region.
    /// </summary>
    public struct RegionAdjustment
    {
        // TODO: Multithreading support

        /// <summary>
        /// The original region to be adjusted.
        /// </summary>
        public Region Base;

        /// <summary>
        /// The bounds over which adjustments are made.
        /// </summary>
        public PatchBounds PatchBounds;

        /// <summary>
        /// The pending adjustments to feature error that have not been applied yet.
        /// </summary>
        public Dictionary<Feature, Patch<double>> ErrorPatches;

        /// <summary>
        /// The adjustements to be made to the presence for given ground types.
        /// </summary>
        public Patch<double>[] PresencePatches;

        /// <summary>
        /// Creates a region adjustment interface for the given base region.
        /// </summary>
        public static RegionAdjustment Create(Region Base, PatchBounds PatchBounds)
        {
            Patch<double>[] presencePatches = new Patch<double>[Base.PresencePatches.Length];
            for (int i = 0; i < presencePatches.Length; i++)
                presencePatches[i] = new Patch<double>(Base.PatchWidth, Base.PatchHeight);
            return new RegionAdjustment
            {
                Base = Base,
                PatchBounds = PatchBounds,
                ErrorPatches = new Dictionary<Feature, Patch<double>>(),
                PresencePatches = presencePatches
            };
        }

        /// <summary>
        /// Gets an interface to adjust the error patch for the given feature. Adjustments may be made by adding
        /// a positive value to points on this patch.
        /// </summary>
        public Patch<double> LockErrorPatch(Feature Feature)
        {
            Patch<double> res;
            if (!this.ErrorPatches.TryGetValue(Feature, out res))
            {
                res = new Patch<double>(this.Base.PatchWidth, this.Base.PatchHeight);
                this.ErrorPatches[Feature] = res;
            }
            Monitor.Enter(res.Data);
            return res;
        }

        /// <summary>
        /// Unlocks a previously-locked error adjustment patch.
        /// </summary>
        public void UnlockErrorPatch(Patch<double> ErrorAdjustmentPatch)
        {

        }

        /// <summary>
        /// Gets an interface to adjust the presence patch for the given ground type. Adjustments may be made by
        /// adding a positive value to points on this patch.
        /// </summary>
        public Patch<double> LockPresencePatch(Ground Ground)
        {
            return this.PresencePatches[this.Base.GroundIndex[Ground]];
        }

        /// <summary>
        /// Unlocks a previously-locked error presence patch.
        /// </summary>
        public void UnlockPresencePatch(Patch<double> PresenceAdjustmentPatch)
        {

        }

        /// <summary>
        /// Adjusts the base region by the given multiple of this adjustment.
        /// </summary>
        public Region Apply(double Amount)
        {
            // Apply all pending adjustments
            while (this.ErrorPatches.Count > 0) // TODO: Level-based evaluation
            {
                var kvp = this.ErrorPatches.First();
                this.ErrorPatches.Remove(kvp.Key);
                kvp.Key.Adjust(this, this.PatchBounds, kvp.Value);
            }

            // Normalize presence adjustments
            double totalSquareAdjustment = 0.0;
            foreach (var point in this.PatchBounds.ContainedPoints)
            {
                double totalPresenceAdjustment = 0.0;
                for (int i = 0; i < this.PresencePatches.Length; i++)
                    totalPresenceAdjustment += this.PresencePatches[i][point];
                double sub = -totalPresenceAdjustment / this.PresencePatches.Length;
                for (int i = 0; i < this.PresencePatches.Length; i++)
                {
                    double presenceAdjustment = this.PresencePatches[i][point];
                    presenceAdjustment += sub;
                    this.PresencePatches[i][point] = presenceAdjustment;
                    totalSquareAdjustment += presenceAdjustment * presenceAdjustment;
                }
            }

            // Apply adjustments
            Debug.Assert(!double.IsNaN(totalSquareAdjustment));
            Patch<double>[] presencePatches = this.Base.PresencePatches;
            Patch<double>[] nPresencePatches = new Patch<double>[this.PresencePatches.Length];
            for (int i = 0; i < nPresencePatches.Length; i++)
                nPresencePatches[i] = new Patch<double>(this.Base.PatchWidth, this.Base.PatchHeight);
            foreach (var point in this.PatchBounds.ContainedPoints)
            {
                double totalPresence = 0.0;
                for (int i = 0; i < this.PresencePatches.Length; i++)
                {
                    double presence = presencePatches[i][point] +
                        this.PresencePatches[i][point] * (Amount / totalSquareAdjustment);
                    presence = Math.Max(0.0, Math.Min(1.0, presence));
                    nPresencePatches[i][point] = presence;
                    totalPresence += presence;
                }
                double iTotalPresence = 1.0 / totalPresence;
                for (int i = 0; i < this.PresencePatches.Length; i++)
                    this.PresencePatches[i][point] *= iTotalPresence;
            }
            return new Region(this.Base.PatchTransform * (Bounds)this.PatchBounds,
                this.Base.PatchTransform, this.Base.PatchWidth, this.Base.PatchHeight,
                this.Base.GroundIndex, nPresencePatches);
        }
    }
}
