﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Terrain
{
    /// <summary>
    /// Describes an ideal, primitive material that can cover a terrain at a given point. This defines the appearance
    /// and physical properties of the terrain at that point. When multiple ground types exist at the same point in a
    /// terrain, they are blended linearly based on their presence.
    /// </summary>
    public sealed class Ground : Feature
    {
        public Ground()
            : base(0.0)
        { }

        public static Ground Grass = new Ground();
        public static Ground Dirt = new Ground();

        /// <summary>
        /// A small value added to presence values for numerical stability.
        /// </summary>
        public const double Epsilon = 0.01;

        public override void ComputeError(Region Region, PatchBounds Bounds, Patch<double> Error)
        {
            Patch<double> presence = Region.GetPresencePatch(this);
            foreach (var point in Bounds.ContainedPoints)
                Error[point] = Math.Log(presence[point] + Epsilon);
        }

        public override void Adjust(RegionAdjustment Adjustment, PatchBounds Bounds, Patch<double> Weight)
        {
            Patch<double> presence = Adjustment.Base.GetPresencePatch(this);
            Patch<double> presenceAdjustment = Adjustment.LockPresencePatch(this);
            foreach (var point in Bounds.ContainedPoints)
                presenceAdjustment[point] += Weight[point] / (presence[point] + Epsilon);
            Adjustment.UnlockPresencePatch(presenceAdjustment);
        }
    }
}
