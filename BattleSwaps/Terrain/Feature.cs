﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Terrain
{
    /// <summary>
    /// Abstractly describes a type of terrain feature. A feature is "exhibited" at a point with a "power" between
    /// 0 and 1 depending on how well the terrain surronding that point satisfies the defining properties of the
    /// feature. Features can vary in scale and are not mutually exclusive. For example, a "forest" feature can
    /// coexist with a "dirt", both at a power of 1, since dirt could be a small-scale component of forest.
    /// </summary>
    /// <remarks>Features are translationally and rotationally invariant, but not necessarily scale
    /// invariant.</remarks>
    public abstract class Feature
    {
        public Feature(double Extent)
        {
            this.Extent = Extent;
        }

        /// <summary>
        /// The radius of the area about a point that must be inspected to determine how well the feature is exhibited
        /// at that point.
        /// </summary>
        public readonly double Extent;

        /// <summary>
        /// A feature that is always satisfied with power 1.
        /// </summary>
        public static readonly Feature Null = NullFeature.Instance;

        /// <summary>
        /// Computes the "error" of this feature at all points within the given bounds for the given region. Error
        /// is defined as <code>ln(power)</code>, where power is the value between 0 and 1 indicating how well
        /// the feature is exhibited at a point.
        /// </summary>
        /// <param name="Bounds">The bounds of error patch to write to. Note that points in the error patch correspond
        /// to points in region patches, and by writing within these bounds, the feature should not require reads
        /// that fall outside the region.</param>
        /// <param name="Error">The patch to write error values to.</param>
        public abstract void ComputeError(Region Region, PatchBounds Bounds, Patch<double> Error);

        /// <summary>
        /// Adjusts a region by adding some multiple of the gradient of the "error" of this feature at all points
        /// within the given bounds.
        /// </summary>
        public abstract void Adjust(RegionAdjustment Adjustment, PatchBounds Bounds, Patch<double> Weight);

        /// <summary>
        /// Constructs a feature which tests for this feature within the given radius.
        /// </summary>
        public Feature Over(double Radius)
        {
            Debug.Assert(Radius >= 0.0);
            if (this == Null) return Null;
            if (Radius == 0.0) return this;
            return new AreaFeature(this, Radius);
        }

        public static Feature operator |(Feature A, Feature B)
        {
            if (A == Null || B == null) return Null;
            return new OrFeature(A, B);
        }

        public static Feature operator +(Feature A, Feature B)
        {
            if (A == Null) return B;
            if (B == Null) return A;
            return new AndFeature(A, B);
        }
    }

    /// <summary>
    /// A feature which is always satisfied, regardless of terrain.
    /// </summary>
    public sealed class NullFeature : Feature
    {
        private NullFeature()
            : base(0.0)
        { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly NullFeature Instance = new NullFeature();

        public override void ComputeError(Region Region, PatchBounds Bounds, Patch<double> Error)
        {
            Error.Fill(0.0);
        }

        public override void Adjust(RegionAdjustment Adjustment, PatchBounds Bounds, Patch<double> Weight)
        {
            // Nothing to do
        }
    }

    /// <summary>
    /// A feature that is satisfied when either of two given features are satisfied.
    /// </summary>
    public sealed class OrFeature : Feature
    {
        public readonly Feature A;
        public readonly Feature B;
        public OrFeature(Feature A, Feature B)
            : base(Math.Max(A.Extent, B.Extent))
        {
            this.A = A;
            this.B = B;
        }

        public override void ComputeError(Region Region, PatchBounds Bounds, Patch<double> Error)
        {
            Patch<double> errorA = Region.GetErrorPatch(this.A);
            Patch<double> errorB = Region.GetErrorPatch(this.B);
            foreach (var point in Bounds.ContainedPoints)
                Error[point] = Math.Max(errorA[point], errorB[point]);
        }

        public override void Adjust(RegionAdjustment Adjustment, PatchBounds Bounds, Patch<double> Weight)
        {
            Patch<double> errorAdjustmentA = Adjustment.LockErrorPatch(this.A);
            Patch<double> errorAdjustmentB = Adjustment.LockErrorPatch(this.B);
            Patch<double> errorA = Adjustment.Base.GetErrorPatch(this.A);
            Patch<double> errorB = Adjustment.Base.GetErrorPatch(this.B);
            foreach (var point in Bounds.ContainedPoints)
            {
                // Adjust only the sub-feature that is closest to being satisfied
                if (errorA[point] > errorB[point])
                    errorAdjustmentA[point] += Weight[point];
                else
                    errorAdjustmentB[point] += Weight[point];
            }
            Adjustment.UnlockErrorPatch(errorAdjustmentA);
            Adjustment.UnlockErrorPatch(errorAdjustmentB);
        }
    }

    /// <summary>
    /// A feature that is satisfied when both of two given features are satisfied.
    /// </summary>
    public sealed class AndFeature : Feature
    {
        public readonly Feature A;
        public readonly Feature B;
        public AndFeature(Feature A, Feature B)
            : base(Math.Max(A.Extent, B.Extent))
        {
            this.A = A;
            this.B = B;
        }

        public override void ComputeError(Region Region, PatchBounds Bounds, Patch<double> Error)
        {
            Patch<double> errorA = Region.GetErrorPatch(this.A);
            Patch<double> errorB = Region.GetErrorPatch(this.B);
            foreach (var point in Bounds.ContainedPoints)
                Error[point] = errorA[point] + errorB[point];
        }

        public override void Adjust(RegionAdjustment Adjustment, PatchBounds Bounds, Patch<double> Weight)
        {
            Patch<double> errorAdjustmentA = Adjustment.LockErrorPatch(this.A);
            Patch<double> errorAdjustmentB = Adjustment.LockErrorPatch(this.B);
            Patch<double> errorA = Adjustment.Base.GetErrorPatch(this.A);
            Patch<double> errorB = Adjustment.Base.GetErrorPatch(this.B);
            foreach (var point in Bounds.ContainedPoints)
            {
                double pointWeight = Weight[point];
                errorAdjustmentA[point] += pointWeight;
                errorAdjustmentB[point] += pointWeight;
            }
            Adjustment.UnlockErrorPatch(errorAdjustmentA);
            Adjustment.UnlockErrorPatch(errorAdjustmentB);
        }
    }

    /// <summary>
    /// A feature that is satisfied when a given source feature is satisfied at all points within a given radius. The
    /// power of the feature is the falloff-weighted average of the power of the source feature at the points.
    /// </summary>
    public sealed class AreaFeature : Feature
    {
        public AreaFeature(Feature Source, double Radius)
            : base(Source.Extent + Radius)
        {
            this.Source = Source;
        }

        /// <summary>
        /// The source feature this feature tests for.
        /// </summary>
        public readonly Feature Source;

        /// <summary>
        /// Gets the radius of the area that this feature tests the source feature in.
        /// </summary>
        public double Radius
        {
            get
            {
                return this.Extent - this.Source.Extent;
            }
        }

        public override void ComputeError(Region Region, PatchBounds Bounds, Patch<double> Error)
        {
            WeightedPatchVector[] kernel = WeightedPatchVector.GetQuadraticKernel(
                this.Radius / Region.PatchTransform.Scale);
            Patch<double> sourceError = Region.GetErrorPatch(Source);
            foreach (PatchVector center in Bounds.ContainedPoints)
            {
                double centerError = 0.0;
                foreach (WeightedPatchVector tap in kernel)
                    centerError += sourceError[center + tap.Source] * tap.Weight;
                Error[center] = centerError;
            }
        }

        public override void Adjust(RegionAdjustment Adjustment, PatchBounds Bounds, Patch<double> Weight)
        {
            WeightedPatchVector[] kernel = WeightedPatchVector.GetQuadraticKernel(
                this.Radius / Adjustment.Base.PatchTransform.Scale);
            Patch<double> sourceErrorAdjustment = Adjustment.LockErrorPatch(this.Source);
            foreach (PatchVector center in Bounds.ContainedPoints)
            {
                double centerWeight = Weight[center];
                foreach (WeightedPatchVector tap in kernel)
                    sourceErrorAdjustment[center + tap.Source] += centerWeight * tap.Weight;
            }
            Adjustment.UnlockErrorPatch(sourceErrorAdjustment);
        }
    }
}
