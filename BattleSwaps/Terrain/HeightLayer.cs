﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

namespace BattleSwaps.Terrain
{
    /// <summary>
    /// A context for creating a shader function which evaluates some feature of a layer.
    /// </summary>
    public struct LayerShaderContext
    {
        /// <summary>
        /// A cursor where operations can be emitted to be executed before the shader function is evaluated. This may
        /// be used to set up uniforms.
        /// </summary>
        public Cursor Prepare;

        /// <summary>
        /// The builder for the final shader function for this context. The only parameter of this function should
        /// be the position to evaluate the layer at.
        /// </summary>
        public ShaderFuncBuilder Builder;

        /// <summary>
        /// Contains the shader expressions for height layers that have already been evaluated within this context.
        /// </summary>
        public Dictionary<HeightLayer, HeightShaderExpressions> HeightShaderExpressions;

        /// <summary>
        /// The types for the parameters for the final shader function produced by this context.
        /// </summary>
        public static ImmutableList<ShaderValueType> Parameters = ImmutableList.From(ShaderValueType.Vec2);

        /// <summary>
        /// The expression for the point to evaluate the layer at.
        /// </summary>
        public ShaderExpression Point
        {
            get
            {
                return this.Builder.GetParameter(0);
            }
        }

        /// <summary>
        /// Creates a new empty layer shader context.
        /// </summary>
        public static LayerShaderContext Create(Cursor Prepare)
        {
            return new LayerShaderContext
            {
                Prepare = Prepare,
                Builder = ShaderFuncBuilder.Create(Parameters),
                HeightShaderExpressions = new Dictionary<HeightLayer, HeightShaderExpressions>()
            };
        }

        /// <summary>
        /// Gives ownership of the given object to this context. The object is guranteed to remain alive as long
        /// as the shader function for this context may be executed.
        /// </summary>
        public void Take(IDisposable Object)
        {
            this.Prepare.Take(Object);
        }
    }

    /// <summary>
    /// A continuous, differentiable mapping from points in 2D space to height values that may vary over reactive
    /// moments.
    /// </summary>
    public abstract class HeightLayer
    {
        /// <summary>
        /// Samples this height layer at the given point.
        /// </summary>
        public abstract HeightSample Sample(Query Query, Vector Point);

        /// <summary>
        /// Creates expressions within a shader context which provide the height and gradient of this layer at a
        /// given point.
        /// </summary>
        protected abstract HeightShaderExpressions BuildShaderExpressions(LayerShaderContext Context);

        /// <summary>
        /// Gets expressions within a shader context which provide the height and gradient of this layer at a
        /// given point.
        /// </summary>
        public HeightShaderExpressions GetShaderExpressions(LayerShaderContext Context)
        {
            HeightShaderExpressions res;
            if (!Context.HeightShaderExpressions.TryGetValue(this, out res))
                Context.HeightShaderExpressions[this] = res = this.BuildShaderExpressions(Context);
            return res;
        }

        /// <summary>
        /// Constructs a height layer from a tiling heightmap image.
        /// </summary>
        public static HeightLayer FromImage(Graphics.Image Source, Transform Transform)
        {
            return new ImageHeightLayer(Source, Transform);
        }
    }

    /// <summary>
    /// A sample from a height layer at a given point.
    /// </summary>
    public struct HeightSample
    {
        public HeightSample(double Height, Vector Gradient)
        {
            this.Height = Height;
            this.Gradient = Gradient;
        }

        /// <summary>
        /// The height value for the height layer at this sample.
        /// </summary>
        public double Height;

        /// <summary>
        /// The gradient of the height layer at this sample.
        /// </summary>
        public Vector Gradient;
    }

    /// <summary>
    /// Contains shader expressions for the components of a height layer at an implied point.
    /// </summary>
    public struct HeightShaderExpressions
    {
        public HeightShaderExpressions(ShaderExpression Height, ShaderExpression Gradient)
        {
            Debug.Assert(Height.Type == ShaderValueType.Float);
            Debug.Assert(Gradient.Type == ShaderValueType.Vec2);
            this.Height = Height;
            this.Gradient = Gradient;
        }

        /// <summary>
        /// The height of the layer at the implied point. This expression has type
        /// <see cref="ShaderValueType.Float"/>.
        /// </summary>
        public ShaderExpression Height;

        /// <summary>
        /// The gradient of the layer at the implied point. This expression has type
        /// <see cref="ShaderValueType.Vec2"/>.
        /// </summary>
        public ShaderExpression Gradient;
    }

    /// <summary>
    /// A height layer which is produced by an axis-aligned tiled mapping of a heightmap image that is interpolated
    /// with a bicubic filter.
    /// </summary>
    public sealed class ImageHeightLayer : HeightLayer
    {
        public ImageHeightLayer(Graphics.Image Source, Transform Transform)
        {
            this.Source = Source;
            this.Transform = Transform;
        }

        /// <summary>
        /// The image this layer is based on.
        /// </summary>
        public readonly Graphics.Image Source;

        /// <summary>
        /// The transform from image space to world space, where the source image is tiled on every unit square.
        /// </summary>
        public readonly Transform Transform;

        public override HeightSample Sample(Query Query, Vector Point)
        {
            // TODO
            throw new NotImplementedException();
        }

        protected override HeightShaderExpressions BuildShaderExpressions(LayerShaderContext Context)
        {
            /* Usage<Texture> textureUsage = this.Source.ToTexture(TextureFormat.Depth, TextureSampling.Smooth).Use();
            Context.Take(textureUsage);
            ShaderExpression texture = Context.Builder.Load(Uniform.Texture2D(textureUsage)); */
            return new HeightShaderExpressions(0.1f, ShaderExpression.Vec2(0.0f, 0.0f));
        }
    }
}