﻿using System;
using System.Collections.Generic;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

using OpenTK.Input;

using BattleSwaps;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics;
using BattleSwaps.Graphics.UI;
using BattleSwaps.Graphics.Render;

namespace BattleSwaps
{
    using UI = Graphics.UI;

    /// <summary>
    /// A game window that hosts a widget in its own reactive system.
    /// </summary>
    public class WidgetGameWindow : GameWindow
    {
        private readonly Mouse<ImagePoint> _Mouse;
        private readonly Signal<ImageSize> _Size;
        private readonly Stimulus<ImageSize> _UpdateSize;
        private readonly Dictionary<Key, Stimulus<bool>> _UpdateKeyDown;
        private readonly Stimulus<ImagePoint> _UpdateMousePos;
        private readonly Stimulus<bool> _UpdateMouseLeftDown;
        private readonly Stimulus<bool> _UpdateMouseRightDown;

        private readonly ExplicitDynamic<Moment> _RenderMoment;
        private readonly Integrator _Integrator;
        private readonly Procedure _Procedure;
        private readonly Widget _Widget;
        private Action<Transaction> _Poll;
        public WidgetGameWindow(Integrator Integrator,
            Stimulus<Entry<Bag<Event>>> AddEventSource,
            WidgetPrototype Widget)
            : base(1024, 768, GraphicsMode.Default, "BattleSwaps Alpha")
        {
            this.WindowState = OpenTK.WindowState.Maximized;
            Graphics.Render.Context.IsThreadBound = true;

            // Prepare environment for widget
            Initializer initializer = Initializer.Create();
            var isKeyDown = new Dictionary<Key, Signal<bool>>();
            this._UpdateKeyDown = new Dictionary<Key, Stimulus<bool>>();
            foreach (Key key in Enum.GetValues(typeof(Key)))
            {
                Stimulus<bool> update;
                isKeyDown[key] = Signal.Create(initializer, false, out update);
                this._UpdateKeyDown[key] = update;
            }
            ImageSize initialSize = new ImageSize((uint)this.Width, (uint)this.Height);
            this._Size = Signal.Create(initializer, initialSize, out this._UpdateSize);
            WidgetEnvironment env = new WidgetEnvironment
            {
                Name = Name.Generate(),
                Domain = new _Domain(isKeyDown),
                Size = this._Size,
            };

            // Prepare mouse
            this._Mouse = new Mouse<ImagePoint>
            {
                Position = Signal.Create(initializer, ImagePoint.Zero, out this._UpdateMousePos),
                IsLeftDown = Signal.Create(initializer, false, out this._UpdateMouseLeftDown),
                IsRightDown = Signal.Create(initializer, false, out this._UpdateMouseRightDown)
            };

            // Prepare widget
            this._Integrator = Integrator;
            this._Widget = Widget.Instantiate(initializer, env);

            // Initialize
            this._Integrator.Apply(delegate(Transaction trans)
            {
                trans.Initializer.MergeFinish(initializer);

                // TODO: Remove event source when window is closed.
                AddEventSource(trans, new Entry<Bag<Event>>(this._Widget.PendingEvents.RuntimeValue, Latch.Open));
            });
            this._RenderMoment = new ExplicitDynamic<Moment>(this._Integrator.Head);

            // Setup rendering procedure
            this._Procedure = Procedure.Create();
            Cursor cursor = this._Procedure.End;
            Renderable renderable;
            cursor.Render(
                new Target(null, this._RenderMoment.Read(this._Size), DepthTarget.Disabled, BlendTarget.Standard),
                Clear.None, out renderable);
            this._Widget.Display.RuntimeValue.Instantiate(renderable, new Display.Context
            {
                Moment = this._RenderMoment,
                Offset = ImagePoint.Zero,
                ViewportSize = this._RenderMoment.Read(this._Size)
            });
        }

        /// <summary>
        /// Gets the widget this window is rendering.
        /// </summary>
        public Widget Widget
        {
            get
            {
                return this._Widget;
            }
        }

        /// <summary>
        /// Gets or sets the polling transaction applied every render by this window. This may read or write
        /// information from outside the reactive system.
        /// </summary>
        public Action<Transaction> Poll
        {
            get
            {
                return this._Poll;
            }
            set
            {
                this._Poll = value;
            }
        }

        /// <summary>
        /// A domain for a widget hosted in a game window.
        /// </summary>
        private class _Domain : Domain
        {
            public readonly Dictionary<Key, Signal<bool>> _IsKeyDown;
            public _Domain(Dictionary<Key, Signal<bool>> IsKeyDown)
            {
                this._IsKeyDown = IsKeyDown;
            }

            public override Util.Property<Signal<bool>> IsKeyDown(Key Key)
            {
                return this._IsKeyDown[Key];
            }
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            Stimulus<bool> update;
            if (this._UpdateKeyDown.TryGetValue(e.Key, out update))
            {
                this._Integrator.Apply(delegate(Transaction trans)
                {
                    update(trans, true);
                    this._Widget.OnKeyDown(trans, e.Key);
                });
            }
        }

        protected override void OnKeyUp(KeyboardKeyEventArgs e)
        {
            Stimulus<bool> update;
            if (this._UpdateKeyDown.TryGetValue(e.Key, out update))
            {
                this._Integrator.Apply(delegate(Transaction trans)
                {
                    update(trans, false);
                    this._Widget.OnKeyUp(trans, e.Key);
                });
            }
        }

        protected override void OnMouseDown(OpenTK.Input.MouseButtonEventArgs e)
        {
            if (e.Button == OpenTK.Input.MouseButton.Left)
            {
                this._Integrator.Apply(delegate(Transaction trans)
                {
                    this._UpdateMouseLeftDown(trans, true);
                    this._Widget.OnMouseDown(trans, UI.MouseButton.Left, this._Mouse);
                });
            }
            else if (e.Button == OpenTK.Input.MouseButton.Right)
            {
                this._Integrator.Apply(delegate(Transaction trans)
                {
                    this._UpdateMouseRightDown(trans, true);
                    this._Widget.OnMouseDown(trans, UI.MouseButton.Right, this._Mouse);
                });
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (e.Button == OpenTK.Input.MouseButton.Left)
                this._Integrator.Apply(this._UpdateMouseLeftDown, false);
            else if (e.Button == OpenTK.Input.MouseButton.Right)
                this._Integrator.Apply(this._UpdateMouseRightDown, false);
        }
        
        protected override void OnMouseMove(OpenTK.Input.MouseMoveEventArgs e)
        {
            this._Integrator.Apply(this._UpdateMousePos, e.Position);
        }

        protected override void OnMouseWheel(OpenTK.Input.MouseWheelEventArgs e)
        {
            this._Integrator.Apply(trans => this._Widget.OnMouseScroll(trans, this._Mouse, e.DeltaPrecise));
        }

        protected override void OnResize(EventArgs e)
        {
            this._Integrator.Apply(this._UpdateSize, new ImageSize((uint)this.Width, (uint)this.Height));
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            if (this._Poll != null)
                this._Integrator.Apply(this._Poll);
            if (this._Procedure.Routine != null && !this._Size.Get(this._Integrator.Current).IsZero)
            {
                this._RenderMoment.Current = this._Integrator.Current;
                this._Procedure.Execute();
                this.SwapBuffers();
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            ((IDisposable)this._Procedure).Dispose();
            base.OnClosed(e);
        }
    }
}