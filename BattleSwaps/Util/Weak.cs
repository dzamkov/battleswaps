﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Util
{
    /// <summary>
    /// A weak reference to an object of the given type.
    /// </summary>
    /// <remarks>This has equivalent functionality to the .Net 4.5 type WeakReference, but I don't want
    /// to incur a dependency just for a simple helper class.</remarks>
    public struct Weak<T>
        where T : class
    {
        public Weak(T Target)
        {
            this.Source = new WeakReference(Target);
        }

        /// <summary>
        /// This source weak reference for this wrapper.
        /// </summary>
        public WeakReference Source;

        /// <summary>
        /// A weak reference that doesn't have a target.
        /// </summary>
        public static readonly Weak<T> Null = new Weak<T>();

        /// <summary>
        /// Gets or sets the target of the weak reference. A value of null indicates that the target has been collected.
        /// </summary>
        public T Target
        {
            get
            {
                return this.Source == null ? null : (T)this.Source.Target;
            }
            set
            {
                if (this.Source == null)
                    this.Source = new WeakReference(value);
                else
                    this.Source.Target = value;
            }
        }

        /// <summary>
        /// Indicates whether the target of this weak reference has still not been collected.
        /// </summary>
        public bool IsAlive
        {
            get
            {
                return this.Source != null && this.Source.IsAlive;
            }
        }

        /// <summary>
        /// Indicates whether this weak reference is null, and thus never had a target.
        /// </summary>
        public bool IsNull
        {
            get
            {
                return this.Source == null;
            }
        }

        /// <summary>
        /// Tries getting the target for this weak reference, returning false if the object has been collected.
        /// </summary>
        public bool TryGetTarget(out T Target)
        {
            Target = this.Target;
            return Target != null;
        }
    }
}
