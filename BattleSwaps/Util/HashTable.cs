﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Util
{
    /// <summary>
    /// A low-level implementation of a hash table with items of the given type.
    /// </summary>
    /// <typeparam name="TItem">The type of an item or empty bin in the hash table. The default value for this
    /// type should be an empty bin.</typeparam>
    /// <typeparam name="TKey">The key type used to index items.</typeparam>
    public struct HashTable<TItem, TKey>
        where TItem : IHashItem<TKey>
    {
        /// <summary>
        /// The bins in this hash table.
        /// </summary>
        public TItem[] Bins;

        /// <summary>
        /// The number of non-empty items in this hash table.
        /// </summary>
        public int Count;

        /// <summary>
        /// Removes the item at the given index, returning the item, if any, that replaces it.
        /// </summary>
        private bool _RemoveTryGet(IEqualityComparer<TKey> Comparer, uint Index, out TItem Item, out TKey Key)
        {
            uint takeIndex;
            uint offset = 1;
            while (this._TryGet(Comparer, takeIndex = (Index + offset) % (uint)this.Bins.Length, out Item, out Key))
            {
                uint baseIndex = unchecked((uint)Comparer.GetHashCode(Key)) % (uint)this.Bins.Length;
                uint baseOffset = (takeIndex + (uint)this.Bins.Length - baseIndex) % (uint)this.Bins.Length;
                if (offset <= baseOffset)
                {
                    this.Bins[Index] = Item;
                    return true;
                }
                else
                {
                    offset++;
                }
            }
            return false;
        }

        /// <summary>
        /// Tries getting the first alive item for the linear probing sequence starting at the given index.
        /// </summary>
        private bool _TryGet(IEqualityComparer<TKey> Comparer, uint Index, out TItem Item, out TKey Key)
        {
            Item = this.Bins[Index];
            HashItemStatus status = Item.TryGetKey(out Key);
            if (status == HashItemStatus.Alive)
            {
                return true;
            }
            else if (status == HashItemStatus.Empty)
            {
                return false;
            }
            else
            {
                Debug.Assert(status == HashItemStatus.Dead);
                return this._RemoveTryGet(Comparer, Index, out Item, out Key);
            }
        }

        /// <summary>
        /// Tries getting one of the items with the given key in this hash table.
        /// </summary>
        /// <param name="Key">The key to search for. If successful, this will be updated to the (equivalent) key used
        /// by the returned item.</param>
        public bool TryGet(IEqualityComparer<TKey> Comparer, ref TKey Key, out TItem Item)
        {
            if (this.Count > 0)
            {
                uint index = unchecked((uint)Comparer.GetHashCode(Key)) % (uint)this.Bins.Length;
                TKey otherKey;
                while (this._TryGet(Comparer, index, out Item, out otherKey))
                {
                    if (Comparer.Equals(Key, otherKey))
                    {
                        Key = otherKey;
                        return true;
                    }
                    else
                    {
                        index = (index + 1) % (uint)this.Bins.Length;
                    }
                }
            }
            Item = default(TItem);
            return false;
        }

        /// <summary>
        /// Tries taking one of the items with the given key in this hash table. This will remove it from the
        /// hash table.
        /// </summary>
        /// <param name="Key">The key to search for. If successful, this will be updated to the (equivalent) key used
        /// by the returned item.</param>
        public bool TryTake(IEqualityComparer<TKey> Comparer, ref TKey Key, out TItem Item)
        {
            if (this.Count > 0)
            {
                uint index = unchecked((uint)Comparer.GetHashCode(Key)) % (uint)this.Bins.Length;
                TKey otherKey;
                while (this._TryGet(Comparer, index, out Item, out otherKey))
                {
                    if (Comparer.Equals(Key, otherKey))
                    {
                        TItem dummyItem;
                        this._RemoveTryGet(Comparer, index, out dummyItem, out otherKey);
                        Key = otherKey;
                        return true;
                    }
                    else
                    {
                        index = (index + 1) % (uint)this.Bins.Length;
                    }
                }
            }
            Item = default(TItem);
            return false;
        }

        /// <summary>
        /// Indicates whether an item with the given key exists in this hash table.
        /// </summary>
        public bool Contains(IEqualityComparer<TKey> Comparer, TKey Key)
        {
            TItem item;
            return this.TryGet(Comparer, ref Key, out item);
        }

        /// <summary>
        /// Adds an item to the given bin array.
        /// </summary>
        private static void _Add(IEqualityComparer<TKey> Comparer, TItem[] Bins, TItem Item, TKey Key)
        {
            uint index = unchecked((uint)Comparer.GetHashCode(Key)) % (uint)Bins.Length;
            while (true)
            {
                TKey otherKey;
                TItem cur = Bins[index];
                HashItemStatus status = cur.TryGetKey(out otherKey);
                if (status == HashItemStatus.Dead || status == HashItemStatus.Empty)
                {
                    Bins[index] = Item;
                    break;
                }
                else
                {
                    Debug.Assert(status == HashItemStatus.Alive);
                    index = (index + 1) % (uint)Bins.Length;
                }
            }
        }

        /// <summary>
        /// Recreates the bin array for this hash table. This will also clean up all dead items.
        /// </summary>
        public void Rehash(IEqualityComparer<TKey> Comparer, int Capacity)
        {
            TItem[] nBins = new TItem[Capacity];
            this.Count = 0;
            if (this.Bins != null)
            {
                for (int i = 0; i < this.Bins.Length; i++)
                {
                    TItem item;
                    TKey key;
                    if ((item = this.Bins[i]).TryGetKey(out key) == HashItemStatus.Alive)
                    {
                        _Add(Comparer, nBins, item, key);
                        this.Count++;
                    }
                }
            }
            this.Bins = nBins;
        }

        /// <summary>
        /// Adds an item to this hash table. This will not check if an item with the same key already exists.
        /// </summary>
        public void Add(IEqualityComparer<TKey> Comparer, TItem Item, TKey Key)
        {
            // Keep max load factor at 0.6
            if (this.Bins == null)
                this.Rehash(Comparer, 31);
            else if ((this.Count + 1) * 10 > this.Bins.Length * 6)
                this.Rehash(Comparer, this.Bins.Length * 2 + 1);

            // Add item
            _Add(Comparer, this.Bins, Item, Key);
            this.Count++;
        }

        /// <summary>
        /// Adds an item to this hash table. This will not check if an item with the same key already exists.
        /// </summary>
        public void Add(IEqualityComparer<TKey> Comparer, TItem Item)
        {
            TKey key;
            if (Item.TryGetKey(out key) == HashItemStatus.Alive)
                this.Add(Comparer, Item, key);
        }
    }

    /// <summary>
    /// An interface to an item, or empty bin, in a hash table.
    /// </summary>
    public interface IHashItem<TKey>
    {
        /// <summary>
        /// Tries getting the key for this item.
        /// </summary>
        HashItemStatus TryGetKey(out TKey Key);
    }

    /// <summary>
    /// Gives the status of an item or bin in a hash table.
    /// </summary>
    public enum HashItemStatus
    {
        /// <summary>
        /// The item is an empty bin, with no key or data.
        /// </summary>
        Empty,

        /// <summary>
        /// The item is not empty, but no longer has a key or data and should be removed. Items may change from
        /// being <see cref="Alive"/> to <see cref="Dead"/> at any time without being prompted by the hash table.
        /// </summary>
        Dead,

        /// <summary>
        /// The item is normal, with a key and useful data.
        /// </summary>
        Alive
    }
}
