﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace BattleSwaps.Util
{
    /// <summary>
    /// An immutable, unordered collection of items where the multiplicity of items is not tracked. The definition
    /// of item equality is defined by an implied <see cref="IEqualityComparer{T}"/>
    /// </summary>
    public struct ImmutableSet<T>
    {
        public ImmutableSet(params T[] Items)
        {
            this.Source = Items;
        }

        public ImmutableSet(HashSet<T> Items)
        {
            this.Source = Items;
        }

        /// <summary>
        /// The source array or hashset for this set. If this is an array, it may contain duplicate items.
        /// </summary>
        public IEnumerable<T> Source;

        /// <summary>
        /// The empty immutable set of this type.
        /// </summary>
        public static readonly ImmutableSet<T> Empty = new ImmutableSet<T>(ImmutableList<T>.Empty.Items);

        /// <summary>
        /// Indicates whether this set is empty.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this.Source == Empty.Source;
            }
        }

        /// <summary>
        /// Adds items to this set.
        /// </summary>
        public ImmutableSet<T> Cons(IEnumerable<T> Items)
        {
            if (this.Source is HashSet<T>)
            {
                HashSet<T> source = (HashSet<T>)this.Source;
                HashSet<T> nSource = new HashSet<T>(source.Comparer);
                nSource.UnionWith(source);
                nSource.UnionWith(Items);
                return new ImmutableSet<T>(nSource);
            }
            else
            {
                // TODO: Bypass list when both collections are arrays
                T[] source = (T[])this.Source;
                List<T> nSource = new List<T>(source.Length);
                nSource.AddRange(source);
                nSource.AddRange(Items);
                return new ImmutableSet<T>(nSource.ToArray());
            }
        }

        /// <summary>
        /// Adds items to this set.
        /// </summary>
        public ImmutableSet<T> Cons(params T[] Items)
        {
            return this.Cons((IEnumerable<T>)Items);
        }

        /// <summary>
        /// Gets the items in this set, in no particular order, with no repeats.
        /// </summary>
        /// <param name="Comparer">The implied comparer for this set.</param>
        public IEnumerable<T> GetItems(IEqualityComparer<T> Comparer)
        {
            if (this.Source is HashSet<T>)
            {
                HashSet<T> source = (HashSet<T>)this.Source;
                Debug.Assert(source.Comparer == Comparer);
                return source;
            }
            else
            {
                return this.Source.Distinct(Comparer);
            }
        }

        /// <summary>
        /// Gets the union of two immutable sets.
        /// </summary>
        public static ImmutableSet<T> operator |(ImmutableSet<T> A, ImmutableSet<T> B)
        {
            if (A.IsEmpty) return B;
            if (B.IsEmpty) return A;
            return A.Cons(B.Source);
        }
    }

    /// <summary>
    /// Contains functions related to immutable sets.
    /// </summary>
    public static class ImmutableSet
    {
        /// <summary>
        /// Creates an immutable set containing the given items.
        /// </summary>
        public static ImmutableSet<T> From<T>(params T[] Items)
        {
            if (Items.Length == 0) return ImmutableSet<T>.Empty;
            return new ImmutableSet<T>(Items);
        }
    }
}
