﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace BattleSwaps.Util
{
    /// <summary>
    /// A type with exactly one value.
    /// </summary>
    public struct Unit
    {
        /// <summary>
        /// The only value for this type.
        /// </summary>
        public static readonly Unit Value = new Unit();
    }

    /// <summary>
    /// A type with no values.
    /// </summary>
    public class Void
    {
        private Void() { }
    }

    /// <summary>
    /// Contains functions related to optional values.
    /// </summary>
    public static class Maybe
    {
        public static Maybe<T> Just<T>(T Value)
        {
            return Maybe<T>.Just(Value);
        }
    }

    /// <summary>
    /// The standard maybe type.
    /// </summary>
    public struct Maybe<T>
    {
        public bool IsJust;
        public T Value;
        public Maybe(bool IsJust, T Value)
        {
            this.IsJust = IsJust;
            this.Value = Value;
        }

        public bool IsNothing
        {
            get
            {
                return !this.IsJust;
            }
        }

        public T GetOrDefault(T Default)
        {
            return this.IsJust ? this.Value : Default;
        }

        public static Maybe<T> Just(T Value)
        {
            return new Maybe<T>(true, Value);
        }

        public static readonly Maybe<T> Nothing = new Maybe<T>(false, default(T));

        public static Maybe<T> Coalesce(Maybe<T> A, Maybe<T> B)
        {
            return A.IsNothing ? B : A;
        }
    }
    
    /// <summary>
    /// A static reference to a value. This is useful for performance reasons (and not much else).
    /// </summary>
    public class Static<T>
        where T : struct
    {
        public Static(T Value)
        {
            this.Value = Value;
        }

        /// <summary>
        /// The value of this static reference.
        /// </summary>
        public readonly T Value;

        public static implicit operator T(Static<T> Static)
        {
            return Static.Value;
        }

        public static implicit operator Static<T>(T Value)
        {
            return new Static<T>(Value);
        }
    }

    /// <summary>
    /// Contains extension functions related to IEnumerable
    /// </summary>
    public static class EnumerableEx
    {
        /// <summary>
        /// Merges two ordered lists such that they remain ordered.
        /// </summary>
        /// <remarks>Taken from 
        /// http://stackoverflow.com/questions/9807701/is-there-an-easy-way-to-merge-two-ordered-sequences-using-linq
        /// </remarks>
        public static IEnumerable<T> Merge<T>(
            this IEnumerable<T> First,
            IEnumerable<T> Second)
            where T : IComparable<T>
        {
            using (var firstEnumerator = First.GetEnumerator())
            using (var secondEnumerator = Second.GetEnumerator())
            {

                var elementsLeftInFirst = firstEnumerator.MoveNext();
                var elementsLeftInSecond = secondEnumerator.MoveNext();
                while (elementsLeftInFirst || elementsLeftInSecond)
                {
                    if (!elementsLeftInFirst)
                    {
                        do yield return secondEnumerator.Current;
                        while (secondEnumerator.MoveNext());
                        yield break;
                    }

                    if (!elementsLeftInSecond)
                    {
                        do yield return firstEnumerator.Current;
                        while (firstEnumerator.MoveNext());
                        yield break;
                    }

                    if (firstEnumerator.Current.CompareTo(secondEnumerator.Current) < 0)
                    {
                        yield return firstEnumerator.Current;
                        elementsLeftInFirst = firstEnumerator.MoveNext();
                    }
                    else
                    {
                        yield return secondEnumerator.Current;
                        elementsLeftInSecond = secondEnumerator.MoveNext();
                    }
                }
            }
        }
    }
}