﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Util
{
    /// <summary>
    /// A mapping from equivalence classes of objects of a given type (as defined by a given comparer) to the
    /// preferred representative of the class, which is weakly referenced. This is useful for ensuring that objects
    /// are reused when possible.
    /// </summary>
    public sealed class WeakInstanceTable<T>
        where T : class
    {
        private HashTable<_HashItem, T> _HashTable;
        public WeakInstanceTable(IEqualityComparer<T> Comparer)
        {
            this.Comparer = Comparer;
        }

        /// <summary>
        /// An item in the hash table for an instance table.
        /// </summary>
        private struct _HashItem : IHashItem<T>
        {
            public Weak<T> Instance;
            public _HashItem(Weak<T> Instance)
            {
                this.Instance = Instance;
            }

            HashItemStatus IHashItem<T>.TryGetKey(out T Key)
            {
                if (!this.Instance.IsNull)
                {
                    if (this.Instance.TryGetTarget(out Key))
                        return HashItemStatus.Alive;
                    else
                        return HashItemStatus.Dead;
                }
                Key = null;
                return HashItemStatus.Empty;
            }
        }

        /// <summary>
        /// The equality comparer used to define the equivalence classes used by this table.
        /// </summary>
        public readonly IEqualityComparer<T> Comparer;

        /// <summary>
        /// Gets the preferred representative for the equivalence class the given object is in. If no such
        /// representative has been defined yet, it will be set to the given object.
        /// </summary>
        public T Coalesce(T Prototype)
        {
            // TODO: No contention for simultaneous readers
            lock (this)
            {
                _HashItem item;
                T res = Prototype;
                if (!this._HashTable.TryGet(this.Comparer, ref res, out item))
                    this._HashTable.Add(Comparer, new _HashItem(new Weak<T>(res)));
                return res;
            }
        }
    }
}
