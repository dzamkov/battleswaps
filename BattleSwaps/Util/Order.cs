﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

namespace BattleSwaps.Util
{
    /// <summary>
    /// An element in an order-maintenance structure. This class is thread-safe.
    /// </summary>
    public sealed class Order
    {
        private Order(Order Base, _ListNode Node, ulong Label)
        {
            this._Base = Base;
            this._Node = Node;
            this._Label = Label;
        }

        private Order(_ListNode Node, ulong Label)
        {
            this._Base = this;
            this._Node = Node;
            this._Label = Label;
        }

        /// <summary>
        /// The "base" order for the list this order is in, as defined by the Dietz-Sleator algorithm.
        /// </summary>
        private readonly Order _Base;

        /// <summary>
        /// The list node corresponding to this order.
        /// </summary>
        /// <remarks>The seperation of nodes and orders is necessary for garbage collection. Otherwise, a usage
        /// of any order would prevent collection of all of them in the same list.</remarks>
        private volatile _ListNode _Node;

        /// <summary>
        /// The "label" of this order, as defined by the Dietz-Sleator algorithm.
        /// </summary>
        private ulong _Label;

        /// <summary>
        /// Identifies a node in a Dietz-Sleator circular linked list.
        /// </summary>
        /// <remarks>See https://www.cs.cmu.edu/~sleator/papers/maintaining-order.pdf for details</remarks>
        private sealed class _ListNode
        {
            /// <summary>
            /// The order this node belongs to. If this has been collected, the node can be removed.
            /// </summary>
            public Weak<Order> Order;

            /// <summary>
            /// The next node in the list.
            /// </summary>
            public _ListNode Next;
        }

        /// <summary>
        /// Creates a new order which is unrelated to all existing orders.
        /// </summary>
        public static Order Create()
        {
            // Created circular linked list with two nodes
            _ListNode bNode = new _ListNode();
            _ListNode resNode = new _ListNode();
            bNode.Next = resNode;
            resNode.Next = bNode;

            // Create base order
            Order b = new Order(bNode, 0);
            bNode.Order = new Weak<Order>(b);

            // Create result order
            Order res = new Order(b, resNode, ulong.MaxValue / 2);
            resNode.Order = new Weak<Order>(res);

            // Don't return until we are sure the list structure is ready
            Thread.MemoryBarrier();
            return res;
        }

        /// <summary>
        /// Acquires a lock on the node associated with the given order.
        /// </summary>
        private static _ListNode _Lock(Order Order)
        {
        retry:
            _ListNode node = Order._Node;
            Monitor.Enter(node);
            if (Order._Node != node)
            {
                Monitor.Exit(node);
                goto retry;
            }
            return node;
        }

        /// <summary>
        /// Given that the node for the given start order is locked, balances the labels of the next few orders
        /// such that the gap between the given order and the next order (returned) is at least 2. Ends with a lock
        /// on both the given and returned order node.
        /// </summary>
        private static void _Balance(Order Start, out Order Next)
        {
            ulong i = 1;
            ulong startLabel = Start._Label;
            List<Order> toBalance = null;

            // Traverse list until the gap is over i * i (as discussed in Sleator paper)
            _ListNode cur = Start._Node;
            ulong maxGap;
            while (true)
            {
                _ListNode next = cur.Next;
                Monitor.Enter(next);
                Order nextOrder;
                if (next.Order.TryGetTarget(out nextOrder))
                {
                    Debug.Assert(nextOrder._Node == next);
                    ulong gap = unchecked(nextOrder._Label - startLabel);

                    // Check stop condition
                    if (gap > i * i)
                    {
                        if (toBalance == null)
                        {
                            // No balancing to be done (keep lock on next)
                            Next = nextOrder;
                            return;
                        }
                        else
                        {
                            maxGap = gap;
                            Monitor.Exit(next);
                            break;
                        }
                    }
                    else
                    {
                        if (toBalance == null)
                            toBalance = new List<Order>();
                        toBalance.Add(nextOrder);
                        cur = next;
                        i += 1;
                    }
                }
                else
                {
                    // This node is garbage, remove from list
                    cur.Next = next.Next;
                    Monitor.Exit(next);
                }
            }

            // Balance starting from the end so as not to disrupt label relationships in the process (all labels
            // should increase).
            ulong remGap = maxGap;
            for (int k = toBalance.Count - 1; k >= 0; k--)
            {
                Order order = toBalance[k];
                ulong diff = remGap / (ulong)(k + 2);
                remGap -= diff;
                ulong nLabel = startLabel + remGap;
                Debug.Assert(unchecked(nLabel - startLabel > order._Label - startLabel));
                Debug.Assert(unchecked(nLabel - startLabel) < maxGap);
                order._Label = nLabel;
                if (k != 0) Monitor.Exit(order._Node);
            }
            Next = toBalance[0];
        }

        /// <summary>
        /// Creates a new order immediately after this one.
        /// </summary>
        public Order After()
        {
            Order next;
            _ListNode cur = _Lock(this);
            _Balance(this, out next);
            _ListNode between = new _ListNode();
            cur.Next = between;
            between.Next = next._Node;
            Debug.Assert(unchecked(next._Label - this._Label) >= 2);
            Order res = new Order(this._Base, between, unchecked(this._Label + (next._Label - this._Label) / 2));
            between.Order = new Weak<Order>(res);
            Monitor.Exit(cur);
            Monitor.Exit(next._Node);
            return res;
        }

        /// <summary>
        /// Creates a new order immediately before this one.
        /// </summary>
        public Order Before()
        {
            Order next;
            _ListNode prev = _Lock(this);
            _Balance(this, out next);

            // Create a new node for this order after the current node.
            _ListNode between = new _ListNode();
            Monitor.Enter(between);
            prev.Next = between;
            between.Next = next._Node;
            this._Node = between;
            between.Order = new Weak<Order>(this);

            // Create a node for previous position of this node
            Order res = new Order(this._Base, prev, this._Label);
            prev.Order = new Weak<Order>(res);

            // Move this node forward
            Debug.Assert(unchecked(next._Label - this._Label) >= 2);
            this._Label += (next._Label - this._Label) / 2;

            Monitor.Exit(prev);
            Monitor.Exit(between);
            Monitor.Exit(next._Node);
            return res;
        }

        public override string ToString()
        {
            return String.Format("0x{0:x16}", (this._Label - this._Base._Label));
        }

        public static bool operator <(Order A, Order B)
        {
            // TODO: Verify thread safety
            if (A._Base != B._Base)
                return false;
            ulong baseLabel = A._Base._Label;
            return unchecked(A._Label - baseLabel) < unchecked(B._Label - baseLabel);
        }

        public static bool operator >(Order A, Order B)
        {
            // TODO: Verify thread safety
            if (A._Base != B._Base)
                return false;
            ulong baseLabel = A._Base._Label;
            return unchecked(A._Label - baseLabel) > unchecked(B._Label - baseLabel);
        }

        public static bool operator <=(Order A, Order B)
        {
            return A == B || A < B;
        }

        public static bool operator >=(Order A, Order B)
        {
            return A == B || A > B;
        }
    }

    /// <summary>
    /// An element in a tree that supports boolean queries, but not traversal. Alternatively, this can be
    /// thought of as an element in a partial order that forms a directed tree. This class is thread-safe.
    /// </summary>
    public struct TreeOrder
    {
        public TreeOrder(Order Pre, Order Post)
        {
            this.Pre = Pre;
            this.Post = Post;
        }

        /// <summary>
        /// Identifies this node's place in a pre-order traversal of the forest.
        /// </summary>
        public Order Pre;

        /// <summary>
        /// Identifies this node's place in a post-order traversal of the forest.
        /// </summary>
        public Order Post;

        /// <summary>
        /// Creates a new root node unrelated to all existing nodes.
        /// </summary>
        public static TreeOrder Create()
        {
            Order pre = Order.Create();
            Order post = pre.After();
            return new TreeOrder(pre, post);
        }

        /// <summary>
        /// Creates a new node whose parent is this node. This corresponds to a call to <see cref="Order.After"/> on a
        /// regular order.
        /// </summary>
        public TreeOrder Extend()
        {
            Order pre = this.Post.Before();
            Order post = pre.After();
            return new TreeOrder(pre, post);
        }

        /// <summary>
        /// Creates a new node between this node and its current parent. This corresponds to a call to
        /// <see cref="Order.Before"/> on a regular order.
        /// </summary>
        public TreeOrder Expound()
        {
            // Lock to prevent the nightmare scenario where two simultaneous calls to expound create two
            // tree-orders with overlapping but non-enveloping pre/post intervals.
            lock (this.Pre)
            {
                return new TreeOrder(this.Pre.Before(), this.Post.After());
            }
        }

        /// <summary>
        /// Determines whether the given node is a strict descendant of this node.
        /// </summary>
        public bool IsDescendant(TreeOrder Other)
        {
            return this.Pre < Other.Pre && this.Post > Other.Post;
        }

        public override string ToString()
        {
            return string.Format("[{0}, {1}]", this.Pre.ToString(), this.Post.ToString());
        }

        public static bool operator ==(TreeOrder A, TreeOrder B)
        {
            if (A.Pre == B.Pre)
            {
                Debug.Assert(A.Post == B.Post);
                return true;
            }
            return false;
        }

        public static bool operator !=(TreeOrder A, TreeOrder B)
        {
            return !(A == B);
        }

        public static bool operator <(TreeOrder A, TreeOrder B)
        {
            return A.IsDescendant(B);
        }

        public static bool operator >(TreeOrder A, TreeOrder B)
        {
            return B < A;
        }

        public static bool operator <=(TreeOrder A, TreeOrder B)
        {
            return A == B || A < B;
        }

        public static bool operator >=(TreeOrder A, TreeOrder B)
        {
            return A == B || A > B;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TreeOrder))
                return false;
            return this == (TreeOrder)obj;
        }

        public override int GetHashCode()
        {
            return this.Pre.GetHashCode();
        }
    }
}
