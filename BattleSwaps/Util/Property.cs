﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Util
{
    /// <summary>
    /// Contains functions related to properties.
    /// </summary>
    public static class Property
    {
        /// <summary>
        /// Contains the set of property-supporting data for properties introduced on a specific thread.
        /// </summary>
        private class _ThreadStore
        {
            public _ThreadStore()
            {
                this.Undefined = new HashSet<RefId>();
                this.Deferred = new Queue<Func<bool>>();
            }

            /// <summary>
            /// The set of all memory locations that have unfilled or undefined values.
            /// </summary>
            public HashSet<RefId> Undefined;

            /// <summary>
            /// The set of deferred definitions that are yet to be applied because their dependencies aren't
            /// satisfied.
            /// </summary>
            public Queue<Func<bool>> Deferred;
        }

        /// <summary>
        /// The thread store for properties introduced on this thread.
        /// </summary>
        [ThreadStatic]
        private static _ThreadStore _Store = null;

        /// <summary>
        /// The set of all thread stores.
        /// </summary>
        private static List<_ThreadStore> _AllStores = new List<_ThreadStore>();

        /// <summary>
        /// If true, indicates that there are no non-empty thread stores, which implies that all properties are
        /// ready to be read.
        /// </summary>
        private static bool _InRuntime = false;

        /// <summary>
        /// Introduces a new memory location in the undefined state.
        /// </summary>
        public static void Introduce<T>(Ref<T> Ref)
        {
            if (_Store == null)
            {
                _Store = new _ThreadStore();
                _Store.Undefined.Add(Ref.GetId());
                lock (_AllStores)
                {
                    _AllStores.Add(_Store);
                }
                _InRuntime = false;
            }
            else
            {
                lock (_Store)
                {
                    _Store.Undefined.Add(Ref.GetId());
                }
            }
        }

        /// <summary>
        /// Gets the thread store that introduced the property with the given memory location, or returns null if
        /// the property has been defined.
        /// </summary>
        private static _ThreadStore _GetSource(RefId Id)
        {
            // Check current thread store first, as this is most likely
            if (_Store != null)
            {
                lock (_Store)
                {
                    if (_Store.Undefined.Contains(Id))
                        return _Store;
                }
            }

            // Check all thread stores
            lock (_AllStores)
            {
                foreach (var store in _AllStores)
                {
                    lock (store)
                    {
                        if (store.Undefined.Contains(Id))
                            return store;
                    }
                }
            }

            // Not found
            return null;
        }

        /// <summary>
        /// Gets the value of a memory location as a property, even if it is undefined or unfilled.
        /// </summary>
        /// <param name="Store">An optional reference to the memory location in question, for performance
        /// reasons.</param>
        public static Property<T> Get<T>(Ref<T> Ref, ref T Store)
        {
            if (_InRuntime)
                return Store;

            // Find source for property
            RefId id = Ref.GetId();
            _ThreadStore source = _GetSource(id);
            if (source == null)
                return Store;

            // Create property
            return new Property<T>(delegate(out T value)
            {
                if (_InRuntime)
                {
                    value = Ref.Value;
                    return true;
                }
                lock (source)
                {
                    if (!source.Undefined.Contains(id))
                    {
                        value = Ref.Value;
                        return true;
                    }
                }
                value = default(T);
                return false;
            });
        }

        /// <summary>
        /// Gets the value of a memory location as a property, even if it is undefined or unfilled.
        /// </summary>
        public static Property<T> Get<T>(Ref<T> Ref)
        {
            Property<T> res = default(Property<T>);
            Ref.Use((ref T store) => res = Get<T>(Ref, ref store));
            return res;
        }

        /// <summary>
        /// Defines the value for a memory location, possibly using a property that can not yet be evaluated. This
        /// must be called on the same thread that introduced the property.
        /// </summary>
        /// <param name="Store">An optional reference to the memory location in question, for performance
        /// reasons.</param>
        public static void Define<T>(Ref<T> Ref, ref T Store, Property<T> Property)
        {
            if (_Store == null)
                throw new Exception("Attempted redefinition");

            bool hasValue = Property.TryGetValue(out Store);
            RefId id = Ref.GetId();
            lock (_Store)
            {
                if (hasValue)
                {
                    if (!_Store.Undefined.Remove(id))
                        throw new Exception("Attempted redefinition");
                }
                else
                {
                    if (!_Store.Undefined.Contains(id))
                        throw new Exception("Attempted redefinition");

                    // Create deferred definition
                    _ThreadStore curStore = _Store;
                    curStore.Deferred.Enqueue(delegate
                    {
                        T nValue;
                        if (Property.TryGetValue(out nValue))
                        {
                            Ref.Value = nValue;
                            if (!curStore.Undefined.Remove(id))
                                throw new Exception("Attempted redefinition");
                            return true;
                        }
                        return false;
                    });
                }
            }
        }

        /// <summary>
        /// Defines the value for a memory location, possibly using a property that can not yet be evaluated. This
        /// must be called on the same thread that introduced the property.
        /// </summary>
        public static void Define<T>(Ref<T> Ref, Property<T> Property)
        {
            Ref.Use((ref T store) => Define(Ref, ref store, Property));
        }

        /// <summary>
        /// Queues an action to be called when the value for the given property becomes available. This can be used
        /// to set up relationships between the objects contained in several properties before the properties are
        /// defined.
        /// </summary>
        public static void Defer<T>(Property<T> Property, Action<T> Action)
        {
            T curValue;
            if (Property.TryGetValue(out curValue))
            {
                Action(curValue);
            }
            else
            {
                Func<bool> deferred = delegate
                {
                    T value;
                    if (Property.TryGetValue(out value))
                    {
                        Action(value);
                        return true;
                    }
                    return false;
                };
                if (_Store == null)
                {
                    _Store = new _ThreadStore();
                    _Store.Deferred.Enqueue(deferred);
                    lock (_AllStores)
                    {
                        _AllStores.Add(_Store);
                    }
                    _InRuntime = false;
                }
                else
                {
                    lock (_Store)
                    {
                        _Store.Deferred.Enqueue(deferred);
                    }
                }
            }
        }

        /// <summary>
        /// Applies as many property definitions as possible, assuming <see cref="_AllStores"/> is locked.
        /// </summary>
        private static void _FlushLocked()
        {
            int? removeStoreIndex = null;
            bool allEmpty = true;
            for (int i = 0; i < _AllStores.Count; i++)
            {
                _ThreadStore store = _AllStores[i];
                lock (store)
                {
                    int stallCounter = store.Deferred.Count;

                    // Apply deferred definitions.
                    while (stallCounter > 0 && store.Deferred.Count > 0)
                    {
                        Func<bool> deferred = store.Deferred.Dequeue();
                        if (deferred())
                        {
                            stallCounter = store.Deferred.Count;
                        }
                        else
                        {
                            // Retry later
                            store.Deferred.Enqueue(deferred);
                            stallCounter--;
                        }
                    }

                    // Check if empty
                    if (store.Undefined.Count == 0)
                    {
                        if (store == _Store)
                            removeStoreIndex = i;
                    }
                    else
                    {
                        allEmpty = false;
                    }
                }
            }

            // Remove current store if empty
            if (removeStoreIndex.HasValue)
            {
                int i = removeStoreIndex.Value;
                int j = _AllStores.Count - 1;
                _AllStores[i] = _AllStores[j];
                _AllStores.RemoveAt(j);
                _Store = null;
            }

            // If there are no empty stores left, activate _InRuntime shortcut
            _InRuntime = allEmpty;
        }

        /// <summary>
        /// Applies as many property definitions as possible, allowing quicker access when they are read.
        /// </summary>
        public static void Flush()
        {
            lock (_AllStores)
            {
                _FlushLocked();
            }
        }

        /// <summary>
        /// Gets the value of a property, assuming it has been determined.
        /// </summary>
        internal static T _GetRuntimeValue<T>(ref Property<T> Property)
        {
            T value;
            if (!Property.TryGetValue(out value))
            {
                // Apply property definitions
                _FlushLocked();

                // Retry
                if (!Property.TryGetValue(out value))
                    throw new Exception("Property value was not determined");
            }
            return value;
        }

        /// <summary>
        /// Constructs a property based on the given evaluator.
        /// </summary>
        public static Property<T> From<T>(PropertyEvaluator<T> Evaluator)
        {
            return new Property<T>(Evaluator);
        }
    }

    /// <summary>
    /// Identifies a value of the given type.
    /// </summary>
    public struct Property<T>
    {
        public Property(PropertyEvaluator<T> Evaluator)
        {
            this.Evaluator = Evaluator;
            this.CacheValue = default(T);
        }

        /// <summary>
        /// When not null, a function that will compute the value of this property, provided that all of its
        /// dependencies have been satisfied.
        /// </summary>
        public PropertyEvaluator<T> Evaluator;
        
        /// <summary>
        /// When <see cref="Compute"/> is null, the value of this property.
        /// </summary>
        public T CacheValue;

        /// <summary>
        /// Tries getting the value of this property, returning false if it is not yet determined.
        /// </summary>
        public bool TryGetValue(out T Value)
        {
            if (this.Evaluator == null)
            {
                Value = this.CacheValue;
                return true;
            }
            else if (this.Evaluator(out Value))
            {
                this.CacheValue = Value;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the value of this property, assuming that it has been completely determined at this point.
        /// </summary>
        public T RuntimeValue
        {
            get
            {
                return Property._GetRuntimeValue(ref this);
            }
        }

        /// <summary>
        /// Applies a mapping function to this property.
        /// </summary>
        public Property<TN> Map<TN>(Func<T, TN> Func)
        {
            if (this.Evaluator == null)
            {
                return Func(this.CacheValue);
            }
            else
            {
                PropertyEvaluator<T> evaluator = this.Evaluator;
                return new Property<TN>(delegate(out TN value)
                {
                    T source;
                    if (evaluator(out source))
                    {
                        value = Func(source);
                        return true;
                    }
                    value = default(TN);
                    return false;
                });
            }
        }

        public static implicit operator Property<T>(T Value)
        {
            return new Property<T>
            {
                Evaluator = null,
                CacheValue = Value
            };
        }
    }

    /// <summary>
    /// A function which tries evaluating a property, returning false if the value of the property is not
    /// fully defined yet.
    /// </summary>
    public delegate bool PropertyEvaluator<T>(out T Value);
}
