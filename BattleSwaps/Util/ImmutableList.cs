﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using BattleSwaps.Data;

namespace BattleSwaps.Util
{
    /// <summary>
    /// An immutable ordered collection of items.
    /// </summary>
    public struct ImmutableList<T> : IEnumerable<T>
    {
        [DefaultSerializer]
        public static readonly Serializer<ImmutableList<T>> DefaultSerializer =
            Data.Serializer.Struct<ImmutableList<T>>();
        public ImmutableList(params T[] Items)
        {
            this.Items = Items;
        }

        /// <summary>
        /// The items in this list, in order. This array may not be null and should not be modified.
        /// </summary>
        public T[] Items;

        /// <summary>
        /// The empty immutable list of this type.
        /// </summary>
        public static readonly ImmutableList<T> Empty = new ImmutableList<T>(new T[0]);

        /// <summary>
        /// Gets the number of items in this list.
        /// </summary>
        public uint Count
        {
            get
            {
                return (uint)this.Items.Length;
            }
        }

        /// <summary>
        /// Gets or sets the item at the given index in this list.
        /// </summary>
        public T this[uint Index]
        {
            get
            {
                return this.Items[Index];
            }
            set
            {
                this.Items[Index] = value;
            }
        }

        /// <summary>
        /// Adds an item to this list.
        /// </summary>
        public ImmutableList<T> Cons(T Item)
        {
            T[] nItems = new T[this.Items.Length + 1];
            Array.Copy(this.Items, 0, nItems, 0, this.Items.Length);
            nItems[this.Items.Length] = Item;
            return new ImmutableList<T>(nItems);
        }

        /// <summary>
        /// Adds items to this list.
        /// </summary>
        public ImmutableList<T> Cons(params T[] Items)
        {
            T[] nItems = new T[this.Items.Length + Items.Length];
            Array.Copy(this.Items, 0, nItems, 0, this.Items.Length);
            Array.Copy(Items, 0, nItems, this.Items.Length, Items.Length);
            return new ImmutableList<T>(nItems);
        }

        /// <summary>
        /// Gets a segment of this list.
        /// </summary>
        public ImmutableList<T> Sublist(uint Start, uint Count)
        {
            Debug.Assert(Start + Count <= this.Count);
            T[] nItems = new T[Count];
            Array.Copy(this.Items, Start, nItems, 0, Count);
            return new ImmutableList<T>(nItems);
        }

        /// <summary>
        /// Concatenates two immutable lists.
        /// </summary>
        public static ImmutableList<T> operator +(ImmutableList<T> A, ImmutableList<T> B)
        {
            if (A.Count == 0) return B;
            if (B.Count == 0) return A;
            return A.Cons(B.Items);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return ((IEnumerable<T>)this.Items).GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.Items.GetEnumerator();
        }
    }

    /// <summary>
    /// Contains functions related to immutable lists.
    /// </summary>
    public static class ImmutableList
    {
        /// <summary>
        /// Creates an immutable list from the given items.
        /// </summary>
        public static ImmutableList<T> From<T>(params T[] Items)
        {
            if (Items.Length == 0) return ImmutableList<T>.Empty;
            return new ImmutableList<T>(Items);
        }
    }
}
