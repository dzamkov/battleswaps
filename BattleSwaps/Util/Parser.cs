﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace BattleSwaps.Util
{
    /// <summary>
    /// Identifies a point in a string from which data can be parsed.
    /// </summary>
    public struct Parser
    {
        public Parser(string String, int Position)
        {
            this.String = String;
            this.Position = Position;
        }

        /// <summary>
        /// The string to be parsed.
        /// </summary>
        public string String;

        /// <summary>
        /// The position of the parser within the string.
        /// </summary>
        public int Position;

        /// <summary>
        /// Creates a parser for the given string.
        /// </summary>
        public static Parser Create(string String)
        {
            return new Parser(String, 0);
        }

        /// <summary>
        /// Indicates whether this parser has reached the end of its source string.
        /// </summary>
        public bool AtEOF
        {
            get
            {
                return this.Position == this.String.Length;
            }
        }

        /// <summary>
        /// Tries parsing the given character.
        /// </summary>
        public bool TryAccept(char Char)
        {
            if (this.Position < this.String.Length && this.String[this.Position] == Char)
            {
                this.Position++;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Tries parsing the given string.
        /// </summary>
        public bool TryAccept(string String)
        {
            if (this.Position + String.Length > this.String.Length)
                return false;
            for (int pos = 0; pos < String.Length; pos++)
                if (this.String[this.Position + pos] != String[pos])
                    return false;
            this.Position += String.Length;
            return true;
        }

        /// <summary>
        /// Tries greedily parsing a non-empty sequence of white space characters.
        /// </summary>
        public bool TryAcceptWhiteSpace()
        {
            if (this.Position < this.String.Length && char.IsWhiteSpace(this.String[this.Position]))
            {
                this.Position++;
                this.AcceptWhiteSpace();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Tries parsing non-empty white space or an EOF.
        /// </summary>
        public bool TryAcceptItemTerminator()
        {
            return this.AtEOF || this.TryAcceptWhiteSpace();
        }

        /// <summary>
        /// Tries parsing the given item, followed by white space or an EOF.
        /// </summary>
        public bool TryAcceptItem(string String)
        {
            Parser alt = this;
            if (alt.TryAccept(String) && alt.TryAcceptItemTerminator())
            {
                this = alt;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Indicates whether a character is a letter or underscore.
        /// </summary>
        private static bool _IsLetterOrUnderscore(char Char)
        {
            return char.IsLetter(Char) || Char == '_';
        }

        /// <summary>
        /// Indicates whether a character is a valid identifier character.
        /// </summary>
        private static bool _IsIdentifier(char Char)
        {
            return char.IsLetterOrDigit(Char) || Char == '_';
        }

        /// <summary>
        /// Tries parsing a non-empty sequence of characters that is a valid identifier in C#.
        /// </summary>
        public bool TryAcceptIdentifier(out string Identifier)
        {
            int cur = this.Position;
            if (cur < this.String.Length && _IsLetterOrUnderscore(this.String[cur]))
            {
                cur++;
                while (cur < this.String.Length && _IsIdentifier(this.String[cur]))
                    cur++;
                Identifier = this.String.Substring(this.Position, cur - this.Position);
                this.Position = cur;
                return true;
            }
            Identifier = default(string);
            return false;
        }

        /// <summary>
        /// Tries parsing an unsigned integral value.
        /// </summary>
        public bool TryAcceptIntegral(out ulong Value)
        {
            Value = 0;
            int cur = this.Position;
            if (cur < this.String.Length)
            {
                char ch = this.String[cur];
                if (char.IsDigit(ch) && ch != '0')
                {
                    Value = (ulong)ch - (ulong)'0';
                    cur++;
                    while (cur < this.String.Length && char.IsDigit(ch = this.String[cur]))
                    {
                        Value *= 10;
                        Value += (ulong)ch - (ulong)'0';
                        cur++;
                    }
                    this.Position = cur;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Tries parsing either a sequence of characters with no interior white space, or a quoted string.
        /// </summary>
        public bool TryAcceptString(out string String)
        {
            int cur = this.Position;
            if (cur < this.String.Length)
            {
                char first = this.String[cur];
                if (first == '"')
                {
                    cur++;
                    bool inEscape = false;
                    List<char> quote = new List<char>();
                    while (cur < this.String.Length)
                    {
                        char ch = this.String[cur];
                        cur++;
                        if (inEscape)
                        {
                            if (ch == '"')
                                quote.Add('\"');
                            else if (ch == '\\')
                                quote.Add('\\');
                            else if (ch == 'n')
                                quote.Add('\n');
                            else if (ch == 'r')
                                quote.Add('\r');
                            else if (ch == 't')
                                quote.Add('\t');
                            inEscape = false;
                        }
                        else if (ch == '\\')
                        {
                            inEscape = true;
                        }
                        else if (ch == '"')
                        {
                            String = new string(quote.ToArray());
                            this.Position = cur;
                            return true;
                        }
                        else
                        {
                            quote.Add(ch);
                        }
                    }
                }
                else if (!char.IsWhiteSpace(first))
                {
                    cur++;
                    while (cur < this.String.Length && !char.IsWhiteSpace(this.String[cur]))
                        cur++;
                    String = this.String.Substring(this.Position, cur - this.Position);
                    this.Position = cur;
                    return true;
                }
            }
            String = default(string);
            return false;
        }

        /// <summary>
        /// Tries parsing a non-empty sequence of characters with no interior white space, or a quoted string,
        /// followed by white space or an EOF.
        /// </summary>
        public bool TryAcceptStringItem(out string String)
        {
            Parser alt = this;
            if (alt.TryAcceptString(out String) && alt.TryAcceptItemTerminator())
            {
                this = alt;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Greedily parses as much white space as possible.
        /// </summary>
        public void AcceptWhiteSpace()
        {
            while (this.Position < this.String.Length && char.IsWhiteSpace(this.String[this.Position]))
                this.Position++;
        }

        /// <summary>
        /// Parses the given character.
        /// </summary>
        public void Expect(char Char)
        {
            if (!this.TryAccept(Char))
                throw new ParserException(this, "\'" + Char + "\'");
        }

        /// <summary>
        /// Parses the given string.
        /// </summary>
        public void Expect(string String)
        {
            if (!this.TryAccept(String))
                throw new ParserException(this, "\"" + String + "\"");
        }

        /// <summary>
        /// Greedily parses a non-empty sequence of white space characters.
        /// </summary>
        public void ExpectWhiteSpace()
        {
            if (!this.TryAcceptWhiteSpace())
                throw new ParserException(this, "WhiteSpace");
        }

        /// <summary>
        /// Parses an identifier.
        /// </summary>
        public string ExpectIdentifier()
        {
            string identifier;
            if (!this.TryAcceptIdentifier(out identifier))
                throw new ParserException(this, "Identifier");
            return identifier;
        }

        /// <summary>
        /// Parses either a sequence of characters with no interior white space, or a quoted string.
        /// </summary>
        public string ExpectString()
        {
            string str;
            if (!this.TryAcceptString(out str))
                throw new ParserException(this, "String");
            return str;
        }

        /// <summary>
        /// Parses an unsigned integer.
        /// </summary>
        public ulong ExpectIntegral()
        {
            ulong value;
            if (!this.TryAcceptIntegral(out value))
                throw new ParserException(this, "Integral");
            return value;
        }

        /// <summary>
        /// Parses either a sequence of characters with no interior white space, or a quoted string, followed by
        /// non-empty white space or an EOF.
        /// </summary>
        public string ExpectStringItem()
        {
            string str;
            if (!this.TryAcceptStringItem(out str))
                throw new ParserException(this, "String");
            return str;
        }

        /// <summary>
        /// Parses a double, followed by white space or an EOF.
        /// </summary>
        public double ExpectDoubleItem()
        {
            return double.Parse(this.ExpectStringItem(), CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Expects the parser to be at the EOF.
        /// </summary>
        public void ExpectEOF()
        {
            if (!this.AtEOF)
                throw new ParserException(this, "EOF");
        }
    }

    /// <summary>
    /// An exception that is thrown when an expected token is not able to be parsed.
    /// </summary>
    public sealed class ParserException : Exception
    {
        public ParserException(Parser Parser, string Expected)
        {
            this.Parser = Parser;
            this.Expected = Expected;
        }

        /// <summary>
        /// The parser that was being read.
        /// </summary>
        public readonly Parser Parser;

        /// <summary>
        /// The type of token that was expected.
        /// </summary>
        public readonly string Expected;
    }
}
