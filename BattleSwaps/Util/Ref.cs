﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Util
{
    /// <summary>
    /// An action which uses a reference of the given type.
    /// </summary>
    public delegate void RefAction<T>(ref T Ref);
   
    /// <summary>
    /// A reference to a field of an object.
    /// </summary>
    public delegate void InteriorRef<T>(object Object, RefAction<T> Action);

    /// <summary>
    /// Identifies, but does not allow access to, a memory location.
    /// </summary>
    public struct RefId : IEquatable<RefId>
    {
        public object Object;
        public Delegate InteriorRef;
        public RefId(object Object, Delegate InteriorRef)
        {
            this.Object = Object;
            this.InteriorRef = InteriorRef;
        }

        public static bool operator ==(RefId A, RefId B)
        {
            return object.ReferenceEquals(A.Object, B.Object) && A.InteriorRef == B.InteriorRef;
        }

        public static bool operator !=(RefId A, RefId B)
        {
            return !(A == B);
        }

        public bool Equals(RefId Other)
        {
            return this == Other;
        }

        public override bool Equals(object Other)
        {
            if (!(Other is RefId))
                return false;
            return this == (RefId)Other;
        }

        public override int GetHashCode()
        {
            return this.Object.GetHashCode() ^ this.InteriorRef.GetHashCode();
        }
    }

    /// <summary>
    /// Contains functions related to references.
    /// </summary>
    public static class Ref
    {
        /// <summary>
        /// Constructs a reference to a field of an object.
        /// </summary>
        public static Ref<T> Create<T>(object Object, InteriorRef<T> InteriorRef)
        {
            return new Ref<T>(Object, InteriorRef);
        }
    }

    /// <summary>
    /// A persistent reference to a memory location of the given type, possibly a field of an object.
    /// </summary>
    public struct Ref<T> : IEquatable<Ref<T>>
    {
        public Ref(object Object, InteriorRef<T> InteriorRef)
        {
            this.Object = Object;
            this.InteriorRef = InteriorRef;
        }

        /// <summary>
        /// The object this reference belongs to, or null if the reference is global.
        /// </summary>
        public object Object;

        /// <summary>
        /// A reference to the field of the object. If this reference is global, then the object given will
        /// be ignored.
        /// </summary>
        public InteriorRef<T> InteriorRef;

        /// <summary>
        /// Gets or sets the value at the memory location associated with this reference.
        /// </summary>
        public T Value
        {
            get
            {
                T res = default(T);
                this.Use((ref T value) => res = value);
                return res;
            }
            set
            {
                this.Use((ref T loc) => loc = value);
            }
        }

        /// <summary>
        /// Applies this reference to the given action.
        /// </summary>
        public void Use(RefAction<T> Action)
        {
            this.InteriorRef(this.Object, Action);
        }

        /// <summary>
        /// Gets an identifier for this reference.
        /// </summary>
        public RefId GetId()
        {
            return new RefId(this.Object, this.InteriorRef);
        }

        public static bool operator ==(Ref<T> A, Ref<T> B)
        {
            return object.ReferenceEquals(A.Object, B.Object) && A.InteriorRef == B.InteriorRef;
        }

        public static bool operator !=(Ref<T> A, Ref<T> B)
        {
            return !(A == B);
        }

        public bool Equals(Ref<T> Other)
        {
            return this == Other;
        }

        public override bool Equals(object Other)
        {
            if (!(Other is Ref<T>))
                return false;
            return this == (Ref<T>)Other;
        }

        public override int GetHashCode()
        {
            return this.Object.GetHashCode() ^ this.InteriorRef.GetHashCode();
        }
    }
}
