﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

using BattleSwaps.Util;
using BattleSwaps.Data;

using OpenTK.Graphics.OpenGL;

namespace BattleSwaps.Graphics.Render
{
    /// <summary>
    /// Describes a custom function which may be used in shader code.
    /// </summary>
    public struct ShaderFunc
    {
        /// <summary>
        /// The paths for the source files that must be imported to use this shader function.
        /// </summary>
        public ImmutableSet<RelativePath> Import;

        /// <summary>
        /// The uniforms provided for use in the body of this shader function.
        /// </summary>
        public ImmutableList<Uniform> Uniforms;

        /// <summary>
        /// The types for the parameters for this shader function.
        /// </summary>
        public ImmutableList<ShaderValueType> Parameters;

        /// <summary>
        /// The return type for this shader function.
        /// </summary>
        public ShaderValueType Return;

        /// <summary>
        /// The GLSL code for the body of this shader function.
        /// </summary>
        public string Body;

        /// <summary>
        /// Gets the name of the uniform at the given index, for use in the body of the shader function.
        /// </summary>
        public static string GetUniformName(uint Index)
        {
            return "u" + Index.ToString();
        }

        /// <summary>
        /// Gets the name of the parameter at the given index, for use in the body of the shader function.
        /// </summary>
        public static string GetParameterName(uint Index)
        {
            return "p" + Index.ToString();
        }

        /// <summary>
        /// Instantiates this shader function, writing it out to the given string builder.
        /// </summary>
        /// <param name="Name">The name of the shader function.</param>
        /// <param name="Uniforms">The set of uniform bindings that must be made to use the function. If there
        /// are any existing bindings passed in, they will remain.</param>
        public void Instantiate(string Name, StringBuilder Source, ref ImmutableSet<UniformBinding> Uniforms)
        {
            // Update bindings
            UniformBinding[] bindings = new UniformBinding[this.Uniforms.Count];
            for (uint i = 0; i < this.Uniforms.Count; i++)
                bindings[i] = this.Uniforms[i].BindTo(string.Format("{0}_u{1}", Name, i));

            // Write source
            for (uint i = 0; i < this.Uniforms.Count; i++)
                Source.AppendFormat("uniform {0} {1};\n", this.Uniforms[i].ValueType, bindings[i].Name);
            Source.AppendFormat("{0} {1}(", this.Return.Source, Name);
            if (this.Parameters.Count > 0)
            {
                Source.AppendFormat("{0} {1}", this.Parameters[0].Source, GetParameterName(0));
                for (uint i = 1; i < this.Parameters.Count; i++)
                    Source.AppendFormat(", {0} {1}", this.Parameters[i].Source, GetParameterName(i));
            }
            Source.Append(") {\n");
            for (uint i = 0; i < this.Uniforms.Count; i++)
                Source.AppendFormat("\t{0} {1} = {2};\n",
                    this.Uniforms[i].ValueType.Source,
                    GetUniformName(i),
                    bindings[i].Name);
            Source.Append(this.Body);
            Source.Append("} \n");
        }
    }

    /// <summary>
    /// A searchable index of shader functions that may be imported into a shader.
    /// </summary>
    public sealed class ShaderFuncImporter
    {
        public ShaderFuncImporter()
        {
            this.Uniforms = ImmutableSet<UniformBinding>.Empty;
            this.Unimported = new Dictionary<string, ShaderFunc>();
        }

        /// <summary>
        /// The uniform bindings that must be in effect in order for the shader functions that have been imported
        /// so far to work.
        /// </summary>
        public ImmutableSet<UniformBinding> Uniforms;

        /// <summary>
        /// The set of shader functions that have yet to be imported.
        /// </summary>
        public readonly Dictionary<string, ShaderFunc> Unimported;

        /// <summary>
        /// Tries importing a function from this resolver into a shader's source, returning false if no function
        /// with the given signature exists in this resolver.
        /// </summary>
        public bool TryImport(string Name, ShaderValueType[] Parameters, ShaderValueType Return, StringBuilder Source)
        {
            ShaderFunc func;
            if (this.Unimported.TryGetValue(Name, out func))
            {
                if (func.Return != Return)
                    goto noMatch;
                if (func.Parameters.Count != Parameters.Length)
                    goto noMatch;
                for (uint i = 0; i < func.Parameters.Count; i++)
                    if (func.Parameters[i] != Parameters[i])
                        goto noMatch;
                func.Instantiate(Name, Source, ref this.Uniforms);
                this.Unimported.Remove(Name);
                return true;
            noMatch: { }
            }
            return false;
        }
    }

    /// <summary>
    /// Identifies the type of a value in a shader.
    /// </summary>
    public struct ShaderValueType
    {
        public static readonly ShaderValueType Bool = new ShaderValueType("bool");
        public static readonly ShaderValueType Int = new ShaderValueType("int");
        public static readonly ShaderValueType UInt = new ShaderValueType("uint");
        public static readonly ShaderValueType Float = new ShaderValueType("float");
        public static readonly ShaderValueType Double = new ShaderValueType("double");
        public static readonly ShaderValueType Vec2 = new ShaderValueType("vec2");
        public static readonly ShaderValueType Vec3 = new ShaderValueType("vec3");
        public static readonly ShaderValueType Vec4 = new ShaderValueType("vec4");
        public static readonly ShaderValueType IVec2 = new ShaderValueType("ivec2");
        public static readonly ShaderValueType Mat2 = new ShaderValueType("mat2");
        public static readonly ShaderValueType Mat2x3 = new ShaderValueType("mat2x3");
        public static readonly ShaderValueType Mat2x4 = new ShaderValueType("mat2x4");
        public static readonly ShaderValueType Mat3 = new ShaderValueType("mat3");
        public static readonly ShaderValueType Mat3x2 = new ShaderValueType("mat3x2");
        public static readonly ShaderValueType Mat3x4 = new ShaderValueType("mat3x4");
        public static readonly ShaderValueType Mat4 = new ShaderValueType("mat4");
        public static readonly ShaderValueType Mat4x2 = new ShaderValueType("mat4x2");
        public static readonly ShaderValueType Mat4x3 = new ShaderValueType("mat4x3");
        public static readonly ShaderValueType Sampler2D = new ShaderValueType("sampler2D");
        public ShaderValueType(string Source)
        {
            this.Source = Source;
        }

        /// <summary>
        /// The GLSL name for this type. 
        /// </summary>
        public string Source;

        /// <summary>
        /// Gets the sampler type for a texture bound to the given target.
        /// </summary>
        public static ShaderValueType Sampler(TextureTarget Target)
        {
            if (Target == TextureTarget.Texture2D)
                return Sampler2D;
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the shader value type with the given GLSL source representation.
        /// </summary>
        public static ShaderValueType FromGLSL(string Source)
        {
            return new ShaderValueType(Source);
        }

        public static bool operator ==(ShaderValueType A, ShaderValueType B)
        {
            return A.Source == B.Source;
        }

        public static bool operator !=(ShaderValueType A, ShaderValueType B)
        {
            return A.Source != B.Source;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ShaderValueType))
                return false;
            return this == (ShaderValueType)obj;
        }

        public override int GetHashCode()
        {
            return this.Source.GetHashCode();
        }

        public override string ToString()
        {
            return this.Source;
        }
    }

    /// <summary>
    /// Describes an expression for a value in a shader function, which may reference variables.
    /// </summary>
    public struct ShaderExpression
    {
        public ShaderExpression(ImmutableSet<RelativePath> Import, ShaderValueType Type, string Source)
        {
            this.Import = Import;
            this.Type = Type;
            this.Source = Source;
        }

        /// <summary>
        /// The paths for the source files that must be imported prior to using this expression.
        /// </summary>
        public ImmutableSet<RelativePath> Import;

        /// <summary>
        /// The type of this expression.
        /// </summary>
        public ShaderValueType Type;

        /// <summary>
        /// The GLSL source for this expression.
        /// </summary>
        public string Source;

        /// <summary>
        /// Constructs a vector from its two components.
        /// </summary>
        public static ShaderExpression Vec2(ShaderExpression A, ShaderExpression B)
        {
            Debug.Assert(A.Type == ShaderValueType.Float);
            Debug.Assert(A.Type == ShaderValueType.Float);
            return new ShaderExpression(
                A.Import | B.Import,
                ShaderValueType.Vec2,
                string.Format("vec2({0}, {1})", A.Source, B.Source));
        }

        public static implicit operator ShaderExpression(float Source)
        {
            return new ShaderExpression(
                ImmutableSet<RelativePath>.Empty,
                ShaderValueType.Float,
                Source.ToString("R", CultureInfo.InvariantCulture));
        }
    }

    /// <summary>
    /// A helper class for constructing shader functions.
    /// </summary>
    public struct ShaderFuncBuilder
    {
        /// <summary>
        /// The types of the parameters for the resulting shader function.
        /// </summary>
        public ImmutableList<ShaderValueType> Parameters;

        /// <summary>
        /// The paths for the source files that need to be imported to use the resulting function.
        /// </summary>
        public HashSet<RelativePath> Import;

        /// <summary>
        /// The uniforms referenced by this shader function.
        /// </summary>
        public List<Uniform> Uniforms;

        /// <summary>
        /// The builder for the body of the shader function.
        /// </summary>
        public StringBuilder Body;

        /// <summary>
        /// Creates a new shader function builder for a shader function with the given parameter types.
        /// </summary>
        public static ShaderFuncBuilder Create(ImmutableList<ShaderValueType> Parameters)
        {
            return new ShaderFuncBuilder
            {
                Parameters = Parameters,
                Import = new HashSet<RelativePath>(RelativePath.Serializer.Comparer),
                Uniforms = new List<Uniform>(),
                Body = new StringBuilder()
            };
        }

        /// <summary>
        /// Gets the expression for the function parameter with the given index.s
        /// </summary>
        public ShaderExpression GetParameter(uint Index)
        {
            return new ShaderExpression(
                ImmutableSet<RelativePath>.Empty, this.Parameters[Index],
                ShaderFunc.GetParameterName(Index));
        }

        /// <summary>
        /// Gets an expression for the given uniform.
        /// </summary>
        public ShaderExpression Load(Uniform Uniform)
        {
            uint index = (uint)this.Uniforms.Count;
            this.Uniforms.Add(Uniform);
            return new ShaderExpression(
                ImmutableSet<RelativePath>.Empty, Uniform.ValueType,
                ShaderFunc.GetUniformName(index));
        }

        /// <summary>
        /// Defines the return value of the shader function and finishes its construction.
        /// </summary>
        public ShaderFunc Return(ShaderExpression Expression)
        {
            this.Import.UnionWith(Expression.Import.Source);
            this.Body.AppendFormat("\treturn {0};\n", Expression.Source);
            return new ShaderFunc
            {
                Import = new ImmutableSet<RelativePath>(this.Import),
                Uniforms = new ImmutableList<Uniform>(this.Uniforms.ToArray()),
                Parameters = this.Parameters,
                Return = Expression.Type,
                Body = this.Body.ToString()
            };
        }
    }
}