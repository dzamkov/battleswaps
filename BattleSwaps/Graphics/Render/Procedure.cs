﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

using BattleSwaps.Reactive;

namespace BattleSwaps.Graphics.Render
{
    /// <summary>
    /// A dynamic OpenGL procedure that performs operations which render a visual effects or modify the context state.
    /// </summary>
    public struct Procedure : IDisposable
    {
        public Procedure(Routine Routine)
        {
            this.Routine = Routine;
        }

        /// <summary>
        /// The routine containing all operations for the procedure.
        /// </summary>
        public Routine Routine;

        /// <summary>
        /// Gets the cursor at the end of this procedure.
        /// </summary>
        public Cursor End
        {
            get
            {
                return new Cursor(this.Routine);
            }
        }

        /// <summary>
        /// Gets the list of all steps in this procedure.
        /// </summary>
        public IEnumerable<Operation> Steps
        {
            get
            {
                return this.Routine.Steps;
            }
        }

        /// <summary>
        /// Gets the array of all steps in this procedure.
        /// </summary>
        public Operation[] StepsArray
        {
            get
            {
                return new List<Operation>(this.Steps).ToArray();
            }
        }

        /// <summary>
        /// Executes this procedure. This requires the current thread to be GL-enabled.
        /// </summary>
        public void Execute()
        {
            this.Routine.Execute();
        }

        /// <summary>
        /// Creates a new empty procedure.
        /// </summary>
        public static Procedure Create()
        {
            return new Procedure(new Routine());
        }

        /// <summary>
        /// Disposes of all objects owned by the procedure.
        /// </summary>
        public void Dispose()
        {
            if (this.Routine != null)
                this.Routine.Dispose();
            this.Routine = null;
        }
    }

    /// <summary>
    /// Identifies a contiguous set of operations within a procedure that can be removed as a group.
    /// </summary>
    public sealed class Routine : IDisposable
    {
        public Routine()
        {
            this._Owned = new List<IDisposable>();
            this._Steps = new List<Operation>();
            this._Last = this;
        }

        /// <summary>
        /// The set of objects owned by this routine. When the routine is disposed, so are these objects.
        /// </summary>
        private List<IDisposable> _Owned;

        /// <summary>
        /// The steps to execute upon entering this routine.
        /// </summary>
        private List<Operation> _Steps;

        /// <summary>
        /// The routine that has this routine set as 'Next'.
        /// </summary>
        private Routine _Previous;

        /// <summary>
        /// The routine to execute immediately after executing the steps for this routine. This may either be the
        /// first child of this routine, or the next sibling.
        /// </summary>
        private Routine _Next;

        /// <summary>
        /// The immediate parent of this routine.
        /// </summary>
        private Routine _Parent;

        /// <summary>
        /// The last descendant routine in this routine, or a routine whose 'Last' 
        /// ty is this descendant.
        /// </summary>
        private Routine _Last;

        /// <summary>
        /// Gets the last descendant routine in this routine, or this routine if it has no children.
        /// </summary>
        private Routine Last
        {
            get
            {
                Routine cur = this;
                while (cur != cur._Last) cur = cur._Last;
                return cur;
            }
        }

        /// <summary>
        /// Gets the cursor at the end of this routine, but also just inside it such that all emitted operations will
        /// be a part of the routine.
        /// </summary>
        public Cursor End
        {
            get
            {
                return new Cursor(this);
            }
        }

        
        /// <summary>
        /// Gives ownership of the given object to this routine.
        /// </summary>
        public void Take(IDisposable Object)
        {
            this._Owned.Add(Object);
        }

        /// <summary>
        /// Gives ownership of the given objects to this routine.
        /// </summary>
        public void Take(IEnumerable<IDisposable> Object)
        {
            this._Owned.AddRange(Object);
        }

        /// <summary>
        /// Emits an operation after all of the current contents of this routine and returns the routine that has
        /// the new operation as its last step.
        /// </summary>
        public Routine EmitAfter(Operation Operation)
        {
            if (this._Last == this)
            {
                this._Steps.Add(Operation);
                return this;
            }
            else
            {
                Routine child = this.Expand();
                return child.EmitAfter(Operation);
            }
        }

        /// <summary>
        /// Adds a new child routine for this routine after all of its current contents.
        /// </summary>
        public Routine Expand()
        {
            Routine child = new Routine();
            Routine last = this.Last;
            if (last._Next != null)
            {
                child._Next = last._Next;
                child._Next._Previous = child;
            }
            last._Next = child;
            child._Previous = last;
            child._Parent = this;
            return this._Last = child;
        }

        /// <summary>
        /// Gets the list of all steps in this routine.
        /// </summary>
        public IEnumerable<Operation> Steps
        {
            get
            {
                Routine afterLast = this.Last._Next;
                Routine cur = this;
                while (cur != afterLast)
                {
                    foreach (var step in cur._Steps)
                        yield return step;
                    cur = cur._Next;
                }
            }
        }

        /// <summary>
        /// Executes this routine. This requires the current thread to be GL-enabled.
        /// </summary>
        public void Execute()
        {
            Routine afterLast = this.Last._Next;
            Routine cur = this;
            while (cur != afterLast)
            {
                // Note: it's possible for the set of steps to be modified during execution
                for (int i = 0; i < cur._Steps.Count; i++)
                {
                    Operation step = cur._Steps[i];
                    step.Execute();

#if DEBUG
                    ErrorCode ec = GL.GetError();
                    if (ec != ErrorCode.NoError)
                        throw new GLException(ec);
#endif
                }
                cur = cur._Next;
            }
        }

        /// <summary>
        /// Indicates whether this routine has been disposed.
        /// </summary>
        public bool IsDisposed
        {
            get
            {
                return this._Steps == null;
            }
        }

        /// <summary>
        /// Removes this routine from the procedure it is in and disposes all of its owned objects.
        /// </summary>
        public void Dispose()
        {
            if (this._Steps != null)
            {
                // Dispose all routines between this and its last child.
                Routine afterLast = this.Last._Next;
                if (this._Previous != null) this._Previous._Next = afterLast;
                if (afterLast != null) afterLast._Previous = this._Previous;
                if (this._Parent != null && this._Parent._Last == this)
                {
                    Debug.Assert(this._Previous == this._Parent || this._Previous._Parent == this._Parent);
                    this._Parent._Last = this._Previous;
                }
                Routine cur = this;
                while (cur != afterLast)
                {
                    foreach (var owned in cur._Owned)
                        owned.Dispose();
                    cur._Owned = null;
                    cur._Steps = null;
                    cur = cur._Next;
                }
            }
        }
    }

    /// <summary>
    /// Identifies a place in a procedure where new operations and routines can be added.
    /// </summary>
    public struct Cursor
    {
        public Cursor(Routine Routine)
        {
            this.Routine = Routine;
        }

        /// <summary>
        /// The routine that defines this cursor. The cursor is always at the end of this routine, but also just
        /// inside it.
        /// </summary>
        public Routine Routine;

        /// <summary>
        /// Gives ownership of the given object to the routine the cursor is in. This gurantees that the
        /// object will be alive for as long as operations at this cursor may be executed.
        /// </summary>
        public void Take(IDisposable Object)
        {
            this.Routine.Take(Object);
        }

        /// <summary>
        /// Gives ownership of the given objects to the routine the cursor is in. This gurantees that the
        /// objects will be alive for as long as operations at this cursor may be executed.
        /// </summary>
        public void Take(IEnumerable<IDisposable> Object)
        {
            this.Routine.Take(Object);
        }

        /// <summary>
        /// Emits an operation immediately before this cursor.
        /// </summary>
        public void Emit(Operation Operation)
        {
            this.Routine = this.Routine.EmitAfter(Operation);
        }

        /// <summary>
        /// Creates a new routine immediately before this cursor.
        /// </summary>
        public Routine Expand()
        {
            return this.Routine.Expand();
        }
    }

    /// <summary>
    /// An exception that is thrown when there is a GL error caused by an operation.
    /// </summary>
    public class GLException : Exception
    {
        public readonly ErrorCode ErrorCode;
        public GLException(ErrorCode ErrorCode)
        {
            this.ErrorCode = ErrorCode;
        }
    }

    /// <summary>
    /// A primitive operation in a procedure.
    /// </summary>
    public struct Operation
    {
        public Operation(Action Execute)
        {
            Debug.Assert(Execute != null);
            this.Execute = Execute;
#if DEBUG
            this.Explaination = null;
#endif
        }

        /// <summary>
        /// The action performed by this operation.
        /// </summary>
        public Action Execute;

#if DEBUG
        /// <summary>
        /// Explains what this operation does.
        /// </summary>
        public OperationExplanation? Explaination;
#endif

        /// <summary>
        /// Provides an explaination for what this operation does.
        /// </summary>
        [Conditional("DEBUG")]
        public void Explain(OperationExplanation Explaination)
        {
#if DEBUG
            this.Explaination = Explaination;
#endif
        }

        /// <summary>
        /// Provides an explaination for what this operation does.
        /// </summary>
        [Conditional("DEBUG")]
        public void Explain(string Name, params object[] Params)
        {
            this.Explain(new OperationExplanation
            {
                Name = Name,
                Params = Params
            });
        }

        /// <summary>
        /// An operation which enables a capability.
        /// </summary>
        public static Operation Enable(EnableCap Cap)
        {
            Operation op = new Operation(() => GL.Enable(Cap));
            op.Explain("Enable", Cap);
            return op;
        }

        /// <summary>
        /// An operation which disables a capability.
        /// </summary>
        public static Operation Disable(EnableCap Cap)
        {
            Operation op = new Operation(() => GL.Disable(Cap));
            op.Explain("Disable", Cap);
            return op;
        }

        /// <summary>
        /// An operation which sets the cull face mode.
        /// </summary>
        public static Operation CullFace(CullFaceMode Mode)
        {
            Operation op = new Operation(() => GL.CullFace(Mode));
            op.Explain("CullFace", Mode);
            return op;
        }

        /// <summary>
        /// An operation which sets the depth mask.
        /// </summary>
        public static Operation DepthMask(bool WriteEnabled)
        {
            Operation op = new Operation(() => GL.DepthMask(WriteEnabled));
            op.Explain("DepthMask", WriteEnabled);
            return op;
        }

        /// <summary>
        /// An operation which sets the depth function.
        /// </summary>
        public static Operation DepthFunc(DepthFunction DepthFunc)
        {
            Operation op = new Operation(() => GL.DepthFunc(DepthFunc));
            op.Explain("DepthFunc", DepthFunc);
            return op;
        }

        /// <summary>
        /// An operation which sets the blend function.
        /// </summary>
        public static Operation BlendFunc(BlendingFactorSrc Src, BlendingFactorDest Dest)
        {
            Operation op = new Operation(() => GL.BlendFunc(Src, Dest));
            op.Explain("BlendFunc", Src, Dest);
            return op;
        }

        /// <summary>
        /// An operation which sets the active texture unit.
        /// </summary>
        public static Operation ActiveTexture(TextureUnit Unit)
        {
            Operation op = new Operation(() => GL.ActiveTexture(Unit));
            op.Explain("ActiveTexture", Unit);
            return op;
        }

        /// <summary>
        /// An operation which binds a texture.
        /// </summary>
        public static Operation Bind(TextureTarget Target, Texture Texture)
        {
            Operation op = new Operation(() => Texture.Bind(Target));
            op.Explain("Bind", Target, Texture);
            return op;
        }

        /// <summary>
        /// An operation which binds a framebuffer.
        /// </summary>
        public static Operation Bind(FramebufferTarget Target, Framebuffer? Framebuffer)
        {
            Operation op = new Operation(() => Render.Framebuffer.Bind(Target, Framebuffer));
            op.Explain("Bind", Target, Framebuffer);
            return op;
        }

        /// <summary>
        /// An operation which binds a mesh.
        /// </summary>
        public static Operation Bind(Mesh Mesh)
        {
            Operation op = new Operation(() => Mesh.Bind());
            op.Explain("Bind", Mesh);
            return op;
        }

        /// <summary>
        /// An operation which binds a program.
        /// </summary>
        public static Operation Use(Program Program)
        {
            Operation op = new Operation(() => Render.Program.Use(Program));
            op.Explain("Use", Program);
            return op;
        }

        /// <summary>
        /// An operation which sets a uniform value.
        /// </summary>
        public static Operation Uniform<T>(Program Program, string Name, Dynamic<T> Source, Action<int, T> Set)
        {
            int loc = Program.GetUniformLocation(Name);
            Operation op = new Operation(() => Set(loc, Source.Current));
            op.Explain("Uniform", Program, Name, Source);
            return op;
        }

        /// <summary>
        /// An operation which sets the viewport.
        /// </summary>
        public static Operation Viewport(Dynamic<Geometry.ImageBounds> Viewport)
        {
            Operation op = new Operation(delegate
            {
                Geometry.ImageBounds current = Viewport.Current;
                GL.Viewport(
                    (int)current.Min.X, (int)current.Min.Y,
                    (int)current.Width, (int)current.Height);
            });
            op.Explain("Viewport", Viewport);
            return op;
        }

        /// <summary>
        /// An operation which sets the clear color.
        /// </summary>
        public static Operation ClearColor(Dynamic<Paint> Paint)
        {
            Operation op = new Operation(delegate
            {
                GL.ClearColor(Paint.Current);
            });
            op.Explain("ClearColor", Paint);
            return op;
        }

        /// <summary>
        /// An operation which sets the clear depth.
        /// </summary>
        public static Operation ClearDepth(Dynamic<double> Depth)
        {
            Operation op = new Operation(delegate
            {
                GL.ClearDepth(Depth.Current);
            });
            op.Explain("ClearDepth", Depth);
            return op;
        }

        /// <summary>
        /// An operation which clears the render target.
        /// </summary>
        public static Operation Clear(ClearBufferMask Mask)
        {
            Operation op = new Operation(delegate
            {
                GL.Clear(Mask);
            });
            op.Explain("Clear", Mask);
            return op;
        }

        public override string ToString()
        {
#if DEBUG
            if (this.Explaination != null)
                return this.Explaination.Value.ToString();
#endif
            return this.Execute.Method.Name;
        }
    }

    /// <summary>
    /// Explains an operation in a human- and debugger- friendly way.
    /// </summary>
    public struct OperationExplanation
    {
        /// <summary>
        /// The name of the operation.
        /// </summary>
        public string Name;

        /// <summary>
        /// The parameters for the operation.
        /// </summary>
        public object[] Params;

        /// <summary>
        /// Gets the first parameter for this operation.
        /// </summary>
        public object Param1
        {
            get
            {
                return this.Params[0];
            }
        }

        /// <summary>
        /// Gets the second parameter for this operation.
        /// </summary>
        public object Param2
        {
            get
            {
                return this.Params[1];
            }
        }

        /// <summary>
        /// Gets the third parameter for this operation.
        /// </summary>
        public object Param3
        {
            get
            {
                return this.Params[2];
            }
        }

        public override string ToString()
        {
            string str = this.Name;
            foreach (object param in this.Params)
            {
                if (param != null)
                    str += " " + param.ToString();
                else
                    str += " null";
            }
            return str;
        }
    }
}