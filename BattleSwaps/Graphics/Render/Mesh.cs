﻿using System;
using System.Collections.Generic;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

using BattleSwaps.Data;

namespace BattleSwaps.Graphics.Render
{
    /// <summary>
    /// Identifies a vertex array object and associated vertex buffers stored in a graphics context. Manages ownership
    /// of all objects and buffers.
    /// </summary>
    public class Mesh : IDisposable
    {
        private List<uint> _BufferIds;
        private bool _HasIndices;
        private int _Length;
        private Mesh(uint Id)
        {
            this._BufferIds = new List<uint>();
            this.Id = Id;
        }

        /// <summary>
        /// Identifies the vertex array object for this mesh.
        /// </summary>
        public readonly uint Id;

        /// <summary>
        /// Binds this mesh.
        /// </summary>
        public void Bind()
        {
            GL.BindVertexArray(this.Id);
        }

        /// <summary>
        /// Draws this mesh, assuming that it is currently bound.
        /// </summary>
        public void Draw(PrimitiveType Type)
        {
            if (this._HasIndices)
                GL.DrawElements(Type, this._Length, DrawElementsType.UnsignedInt, 0);
            else
                GL.DrawArrays(Type, 0, this._Length);
        }

        /// <summary>
        /// Creates a mesh from the given mesh data.
        /// </summary>
        public static Mesh CreateFromData(MeshData Data)
        {
            uint vao = (uint)GL.GenVertexArray();
            Mesh mesh = new Mesh(vao);
            GL.BindVertexArray(vao);

            // Array buffer
            uint arrayVbo = (uint)GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, arrayVbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)Data.ArraySize, IntPtr.Zero, BufferUsageHint.StaticDraw);
            int offset = 0;
            for (int i = 0; i < Data.Attributes.Length; i++)
            {
                MeshAttributeData attrData = Data.Attributes[i];
                mesh._Length = attrData.Length;
                GL.EnableVertexAttribArray(i);
                attrData.BufferSubData((IntPtr)offset);
                attrData.VertexAttribPointer(i, (IntPtr)offset);
                offset += attrData.Size;
            }
            mesh._BufferIds.Add(arrayVbo);

            if (Data.Indices != null)
            {
                // Element array buffer
                mesh._HasIndices = true;
                mesh._Length = Data.Indices.Length;
                uint elementVbo = (uint)GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, elementVbo);
                GL.BufferData(BufferTarget.ElementArrayBuffer,
                    (IntPtr)(sizeof(uint) * Data.Indices.Length),
                    Data.Indices, BufferUsageHint.StaticDraw);
                mesh._BufferIds.Add(elementVbo);
            }

            return mesh;
        }

        /// <summary>
        /// Creates a mesh from the given mesh data.
        /// </summary>
        public static Mesh CreateFromData(uint[] Indices, params MeshAttributeData[] Attributes)
        {
            return CreateFromData(new MeshData(Attributes, Indices));
        }

        /// <summary>
        /// Creates a mesh from the given mesh data.
        /// </summary>
        public static Mesh CreateFromData(params MeshAttributeData[] Attributes)
        {
            return CreateFromData(new MeshData(Attributes));
        }

        /// <summary>
        /// A resource for a statically-defined mesh.
        /// </summary>
        public sealed class StaticResource : Resource<Render.Mesh>
        {
            public readonly MeshData Data;
            public StaticResource(MeshData Data)
            {
                this.Data = Data;
            }

            protected override void Load()
            {
                Context.Invoke(delegate
                {
                    this.Provide(CreateFromData(this.Data));
                });
            }
        }

        /// <summary>
        /// A mesh resource for the unit quad.
        /// </summary>
        [Tag(typeof(Resource<Mesh>), (uint)949373537)]
        public static readonly Resource<Mesh> UnitQuad = new StaticResource(MeshData.UnitQuad);

        /// <summary>
        /// Deletes this mesh and associated buffers.
        /// </summary>
        public void Dispose()
        {
            GL.DeleteVertexArray(this.Id);
            foreach (uint id in this._BufferIds)
                GL.DeleteBuffer(id);
        }

        public override string ToString()
        {
            return string.Format("Mesh({0})", this.Id.ToString());
        }
    }

    /// <summary>
    /// Describes the data in a mesh.
    /// </summary>
    public struct MeshData
    {
        public MeshData(MeshAttributeData[] Attributes, uint[] Indices)
        {
            this.Attributes = Attributes;
            this.Indices = Indices;
        }

        public MeshData(MeshAttributeData[] Attributes)
        {
            this.Attributes = Attributes;
            this.Indices = null;
        }

        /// <summary>
        /// Contains the attribute data corresponding to each attribute index.
        /// </summary>
        public MeshAttributeData[] Attributes;

        /// <summary>
        /// The index data for this mesh, if it exists.
        /// </summary>
        public uint[] Indices;

        /// <summary>
        /// Gets the size of the array buffer for this data.
        /// </summary>
        public int ArraySize
        {
            get
            {
                int size = 0;
                foreach (MeshAttributeData attr in this.Attributes)
                    size += attr.Size;
                return size;
            }
        }

        /// <summary>
        /// Appends a quad to list of indices.
        /// </summary>
        public static void OutputQuadIndices(List<uint> Indices, uint Base)
        {
            Indices.Add(Base);
            Indices.Add(Base + 1);
            Indices.Add(Base + 2);
            Indices.Add(Base + 2);
            Indices.Add(Base + 3);
            Indices.Add(Base);
        }

        /// <summary>
        /// Appends a quad to a list of vertices.
        /// </summary>
        public static void OutputQuadVertices(List<Vector2> Vertices, Geometry.Bounds Bounds)
        {
            Vertices.Add(new Vector2((float)Bounds.Min.X, (float)Bounds.Min.Y));
            Vertices.Add(new Vector2((float)Bounds.Max.X, (float)Bounds.Min.Y));
            Vertices.Add(new Vector2((float)Bounds.Max.X, (float)Bounds.Max.Y));
            Vertices.Add(new Vector2((float)Bounds.Min.X, (float)Bounds.Max.Y));
        }

        /// <summary>
        /// Mesh data for a 2D quad occupying ([0, 1], [0, 1]) with only a single attribute.
        /// </summary>
        public static readonly MeshData UnitQuad = new MeshData
        {
            Indices = new uint[] { 0, 1, 2, 2, 3, 0 },
            Attributes = new[] { 
                MeshAttributeData.FromArray(
                    new Vector2(0.0f, 0.0f),
                    new Vector2(1.0f, 0.0f),
                    new Vector2(1.0f, 1.0f),
                    new Vector2(0.0f, 1.0f))
            }
        };
    }

    /// <summary>
    /// Describes the data in a mesh for a single attribute.
    /// </summary>
    public abstract class MeshAttributeData
    {
        public MeshAttributeData(int Length, int Components, VertexAttribPointerType ComponentType, int ElementSize)
        {
            this.Length = Length;
            this.Components = Components;
            this.ComponentType = ComponentType;
            this.ElementSize = ElementSize;
        }

        /// <summary>
        /// The number of elements in this dataset.
        /// </summary>
        public readonly int Length;

        /// <summary>
        /// The number of components per element.
        /// </summary>
        public readonly int Components;

        /// <summary>
        /// The type of components for this attribute.
        /// </summary>
        public readonly VertexAttribPointerType ComponentType;

        /// <summary>
        /// The size, in bytes, of an element of the associated attribute type.
        /// </summary>
        public readonly int ElementSize;

        /// <summary>
        /// Gets the total size of this attribute data.
        /// </summary>
        public int Size
        {
            get
            {
                return this.Length * this.ElementSize;
            }
        }

        /// <summary>
        /// Writes the vertex data to the currently-bound array buffer.
        /// </summary>
        public abstract void BufferSubData(IntPtr Offset);

        /// <summary>
        /// Sets up the vertex attribute pointer for this data.
        /// </summary>
        public void VertexAttribPointer(int Index, IntPtr Offset)
        {
            VertexAttribIntegerType integerType;
            if (this.ComponentType == VertexAttribPointerType.Int)
            {
                integerType = VertexAttribIntegerType.Int;
                goto integer;
            }
            GL.VertexAttribPointer(Index, this.Components, this.ComponentType, false, this.ElementSize, Offset);
            return;

        integer:
            GL.VertexAttribIPointer(Index, this.Components, integerType, this.ElementSize, Offset);
            return;
        }

        /// <summary>
        /// Creates mesh attribute data from an array of integers.
        /// </summary>
        public static MeshAttributeData FromArray(params int[] Data)
        {
            return new MeshAttributeData<int>(1, VertexAttribPointerType.Int, sizeof(int), Data);
        }

        /// <summary>
        /// Creates mesh attribute data from an array of float vectors.
        /// </summary>
        public static MeshAttributeData FromArray(params float[] Data)
        {
            return new MeshAttributeData<float>(1, VertexAttribPointerType.Float, sizeof(float), Data);
        }

        /// <summary>
        /// Creates mesh attribute data from an array of float vectors.
        /// </summary>
        public static MeshAttributeData FromArray(params Vector2[] Data)
        {
            return new MeshAttributeData<Vector2>(2, VertexAttribPointerType.Float, Vector2.SizeInBytes, Data);
        }

        /// <summary>
        /// Creates mesh attribute data from an array of float vectors.
        /// </summary>
        public static MeshAttributeData FromArray(params Vector3[] Data)
        {
            return new MeshAttributeData<Vector3>(3, VertexAttribPointerType.Float, Vector3.SizeInBytes, Data);
        }
    }

    /// <summary>
    /// Describes the data in a mesh for a single attribute of a known type.
    /// </summary>
    public sealed class MeshAttributeData<T> : MeshAttributeData
        where T : struct
    {
        public MeshAttributeData(int Components, VertexAttribPointerType ComponentType, int ElementSize, T[] Data)
            : base(Data.Length, Components, ComponentType, ElementSize)
        {
            this.Data = Data;
        }

        /// <summary>
        /// The data array for this attribute.
        /// </summary>
        public readonly T[] Data;

        public override void BufferSubData(IntPtr Offset)
        {
            GL.BufferSubData<T>(BufferTarget.ArrayBuffer, Offset, new IntPtr(this.Size), this.Data);
        }
    }
}