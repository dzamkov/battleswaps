﻿using System;
using System.Collections.Generic;

using BattleSwaps.Reactive;

namespace BattleSwaps.Graphics.Render
{
    /// <summary>
    /// The non-parametric base class for dynamic values.
    /// </summary>
    public abstract class Dynamic
    {
        /// <summary>
        /// Constructs a dynamic value from the given static value.
        /// </summary>
        public static Dynamic<T> Static<T>(T Value)
        {
            return new StaticDynamic<T>(Value);
        }

        /// <summary>
        /// Constructs a dynamic value based on the given lookup function.
        /// </summary>
        public static Dynamic<T> Lookup<T>(Func<T> Func)
        {
            return new LookupDynamic<T>(Func);
        }

        /// <summary>
        /// Constructs a dynamic value that reads from the given signal.
        /// </summary>
        public static Dynamic<T> FromSignal<T>(Dynamic<Moment> Moment, Signal<T> Signal)
        {
            Moment staticMoment;
            if (Moment.TryGetStatic(out staticMoment))
                return Signal.Get(staticMoment);
            T staticValue;
            if (Signal.TryGetStatic(out staticValue))
                return Static<T>(staticValue);
            return new SignalDynamic<T>(Moment, Signal);
        }

        /// <summary>
        /// Applies a mapping function to two dynamic values.
        /// </summary>
        public static Dynamic<TN> Lift<TA, TB, TN>(Dynamic<TA> SourceA, Dynamic<TB> SourceB, Func<TA, TB, TN> Func)
        {
            TA valueA;
            TB valueB;
            if (SourceA.TryGetStatic(out valueA) && SourceB.TryGetStatic(out valueB))
                return Static(Func(valueA, valueB));
            return new Lift2Dynamic<TA, TB, TN>(SourceA, SourceB, Func);
        }

        /// <summary>
        /// Applies a mapping function to three dynamic values.
        /// </summary>
        public static Dynamic<TN> Lift<TA, TB, TC, TN>(
            Dynamic<TA> SourceA, Dynamic<TB> SourceB, Dynamic<TC> SourceC,
            Func<TA, TB, TC, TN> Func)
        {
            TA valueA;
            TB valueB;
            TC valueC;
            if (SourceA.TryGetStatic(out valueA) &&
                SourceB.TryGetStatic(out valueB) &&
                SourceC.TryGetStatic(out valueC))
                return Static(Func(valueA, valueB, valueC));
            return new Lift3Dynamic<TA, TB, TC, TN>(SourceA, SourceB, SourceC, Func);
        }
    }

    /// <summary>
    /// Contains extension functions related to dynamic values.
    /// </summary>
    public static class DynamicEx
    {
        /// <summary>
        /// Gets the value of a signal at this moment.
        /// </summary>
        public static Dynamic<T> Read<T>(this Dynamic<Moment> Moment, Signal<T> Signal)
        {
            return Dynamic.FromSignal(Moment, Signal);
        }

        /// <summary>
        /// Wraps the given switchable disposable object as an individual disposable object.
        /// </summary>
        public static IDisposable ToDisposable<T>(this Dynamic<T> Source)
            where T : IDisposable
        {
            return new DynamicDisposable<T>(Source);
        }
    }

    /// <summary>
    /// Provides a dynamic value for use by operations in a procedure. Unlike signals, dynamic values are only
    /// ever accessed from a single thread and their value is not persistent. Additionally, equality between
    /// dynamic values is defined to be logical, so that repeating the same operations on the same dynamic value will
    /// yield dynamic values that are considered equal.
    /// </summary>
    public abstract class Dynamic<T> : Dynamic
    {
        /// <summary>
        /// Gets the current value of this dynamic value.
        /// </summary>
        public T Current
        {
            get
            {
                return this.GetValue();
            }
        }

        /// <summary>
        /// Gets the current value of this dynamic value.
        /// </summary>
        public abstract T GetValue();

        /// <summary>
        /// Tries getting the value of this dynamic value, if it is static.
        /// </summary>
        public bool TryGetStatic(out T Value)
        {
            StaticDynamic<T> staticThis = this as StaticDynamic<T>;
            if (!object.ReferenceEquals(staticThis, null))
            {
                Value = staticThis.Value;
                return true;
            }
            Value = default(T);
            return false;
        }

        /// <summary>
        /// Applies a mapping function to this dynamic value.
        /// </summary>
        public Dynamic<TN> Map<TN>(Func<T, TN> Func)
        {
            T staticValue;
            if (this.TryGetStatic(out staticValue))
                return Static(Func(staticValue));
            return new MapDynamic<T, TN>(this, Func);
        }

        public static implicit operator Dynamic<T>(T Value)
        {
            return Dynamic.Static(Value);
        }

        public static bool operator ==(Dynamic<T> A, Dynamic<T> B)
        {
            if (object.ReferenceEquals(A, null)) return object.ReferenceEquals(B, null);
            if (object.ReferenceEquals(B, null)) return false;
            return A.Equals(B);
        }

        public static bool operator !=(Dynamic<T> A, Dynamic<T> B)
        {
            return !(A == B);
        }

        public override string ToString()
        {
            return String.Format("<{0}>", this.Current);
        }

        public override bool Equals(object obj)
        {
            return object.ReferenceEquals(this, obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    /// <summary>
    /// A static value as a dynamic value.
    /// </summary>
    public sealed class StaticDynamic<T> : Dynamic<T>
    {
        public StaticDynamic(T Value)
        {
            this.Value = Value;
        }

        /// <summary>
        /// The value of this dynamic value.
        /// </summary>
        public readonly T Value;

        public override T GetValue()
        {
            return this.Value;
        }

        public static bool operator ==(StaticDynamic<T> A, StaticDynamic<T> B)
        {
            return A.Value.Equals(B.Value);
        }

        public static bool operator !=(StaticDynamic<T> A, StaticDynamic<T> B)
        {
            return !(A == B);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is StaticDynamic<T>))
                return false;
            return this == (StaticDynamic<T>)obj;
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }
    }

    /// <summary>
    /// A dynamic value taken from a signal.
    /// </summary>
    public sealed class SignalDynamic<T> : Dynamic<T>
    {
        public SignalDynamic(Dynamic<Moment> Moment, Signal<T> Source)
        {
            this.Moment = Moment;
            this.Source = Source;
        }

        /// <summary>
        /// The moment to read from.
        /// </summary>
        public readonly Dynamic<Moment> Moment;

        /// <summary>
        /// The source moment for this dynamic value.
        /// </summary>
        public readonly Signal<T> Source;

        public override T GetValue()
        {
            return this.Source.Get(this.Moment.Current);
        }

        public static bool operator ==(SignalDynamic<T> A, SignalDynamic<T> B)
        {
            return A.Source == B.Source && A.Moment == B.Moment;
        }

        public static bool operator !=(SignalDynamic<T> A, SignalDynamic<T> B)
        {
            return !(A == B);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is SignalDynamic<T>))
                return false;
            return this == (SignalDynamic<T>)obj;
        }

        public override int GetHashCode()
        {
            return this.Moment.GetHashCode() ^ this.Source.GetHashCode();
        }
    }

    /// <summary>
    /// A dynamic value based on a lookup function.
    /// </summary>
    public sealed class LookupDynamic<T> : Dynamic<T>
    {
        public LookupDynamic(Func<T> Func)
        {
            this.Func = Func;
        }

        /// <summary>
        /// The lookup function for this value.
        /// </summary>
        public readonly Func<T> Func;

        public override T GetValue()
        {
            return this.Func();
        }

        public static bool operator ==(LookupDynamic<T> A, LookupDynamic<T> B)
        {
            return A.Func == B.Func;
        }

        public static bool operator !=(LookupDynamic<T> A, LookupDynamic<T> B)
        {
            return !(A == B);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is LookupDynamic<T>))
                return false;
            return this == (LookupDynamic<T>)obj;
        }

        public override int GetHashCode()
        {
            return this.Func.GetHashCode();
        }
    }

    /// <summary>
    /// Applies a mapping function to a dynamic value.
    /// </summary>
    public sealed class MapDynamic<T, TN> : Dynamic<TN>
    {
        public MapDynamic(Dynamic<T> Source, Func<T, TN> Func)
        {
            this.Source = Source;
            this.Func = Func;
        }

        /// <summary>
        /// The source dynamic value to be mapped.
        /// </summary>
        public readonly Dynamic<T> Source;

        /// <summary>
        /// The mapping function applied by this dynamic value.
        /// </summary>
        public readonly Func<T, TN> Func;

        public override TN GetValue()
        {
            return this.Func(this.Source.Current);
        }

        public static bool operator ==(MapDynamic<T, TN> A, MapDynamic<T, TN> B)
        {
            return A.Source == B.Source && A.Func == B.Func;
        }

        public static bool operator !=(MapDynamic<T, TN> A, MapDynamic<T, TN> B)
        {
            return !(A == B);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is MapDynamic<T, TN>))
                return false;
            return this == (MapDynamic<T, TN>)obj;
        }

        public override int GetHashCode()
        {
            return this.Source.GetHashCode() ^ this.Func.GetHashCode();
        }
    }

    /// <summary>
    /// Applies a mapping function to two dynamic values.
    /// </summary>
    public sealed class Lift2Dynamic<TA, TB, TN> : Dynamic<TN>
    {
        public Lift2Dynamic(Dynamic<TA> SourceA, Dynamic<TB> SourceB, Func<TA, TB, TN> Func)
        {
            this.SourceA = SourceA;
            this.SourceB = SourceB;
            this.Func = Func;
        }

        /// <summary>
        /// The first source dynamic value to be mapped.
        /// </summary>
        public readonly Dynamic<TA> SourceA;

        /// <summary>
        /// The second source dynamic value to be mapped.
        /// </summary>
        public readonly Dynamic<TB> SourceB;

        /// <summary>
        /// The mapping function applied by this dynamic value.
        /// </summary>
        public readonly Func<TA, TB, TN> Func;

        public override TN GetValue()
        {
            return this.Func(this.SourceA.Current, this.SourceB.Current);
        }

        public static bool operator ==(Lift2Dynamic<TA, TB, TN> A, Lift2Dynamic<TA, TB, TN> B)
        {
            return A.SourceA == B.SourceA && A.SourceB == B.SourceB && A.Func == B.Func;
        }

        public static bool operator !=(Lift2Dynamic<TA, TB, TN> A, Lift2Dynamic<TA, TB, TN> B)
        {
            return !(A == B);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Lift2Dynamic<TA, TB, TN>))
                return false;
            return this == (Lift2Dynamic<TA, TB, TN>)obj;
        }

        public override int GetHashCode()
        {
            return this.SourceA.GetHashCode() ^ (this.SourceB.GetHashCode() << 1) ^ this.Func.GetHashCode();
        }
    }

    /// <summary>
    /// Applies a mapping function to three dynamic values.
    /// </summary>
    public sealed class Lift3Dynamic<TA, TB, TC, TN> : Dynamic<TN>
    {
        public Lift3Dynamic(Dynamic<TA> SourceA, Dynamic<TB> SourceB, Dynamic<TC> SourceC, Func<TA, TB, TC, TN> Func)
        {
            this.SourceA = SourceA;
            this.SourceB = SourceB;
            this.SourceC = SourceC;
            this.Func = Func;
        }

        /// <summary>
        /// The first source dynamic value to be mapped.
        /// </summary>
        public readonly Dynamic<TA> SourceA;

        /// <summary>
        /// The second source dynamic value to be mapped.
        /// </summary>
        public readonly Dynamic<TB> SourceB;

        /// <summary>
        /// The third source dynamic value to be mapped.
        /// </summary>
        public readonly Dynamic<TC> SourceC;

        /// <summary>
        /// The mapping function applied by this dynamic value.
        /// </summary>
        public readonly Func<TA, TB, TC, TN> Func;

        public override TN GetValue()
        {
            return this.Func(this.SourceA.Current, this.SourceB.Current, this.SourceC.Current);
        }

        public static bool operator ==(Lift3Dynamic<TA, TB, TC, TN> A, Lift3Dynamic<TA, TB, TC, TN> B)
        {
            return A.SourceA == B.SourceA && A.SourceB == B.SourceB && A.SourceC == B.SourceC && A.Func == B.Func;
        }

        public static bool operator !=(Lift3Dynamic<TA, TB, TC, TN> A, Lift3Dynamic<TA, TB, TC, TN> B)
        {
            return !(A == B);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Lift3Dynamic<TA, TB, TC, TN>))
                return false;
            return this == (Lift3Dynamic<TA, TB, TC, TN>)obj;
        }

        public override int GetHashCode()
        {
            return this.SourceA.GetHashCode() ^
                (this.SourceB.GetHashCode() << 1) ^
                (this.SourceC.GetHashCode() << 2) ^
                this.Func.GetHashCode();
        }
    }

    /// <summary>
    /// A dynamic value that is explicitly updated.
    /// </summary>
    public sealed class ExplicitDynamic<T> : Dynamic<T>
    {
        public ExplicitDynamic(T Initial)
        {
            this.Current = Initial;
        }

        /// <summary>
        /// The current value of this dynamic value.
        /// </summary>
        public new T Current;

        public override T GetValue()
        {
            return this.Current;
        }
    }

    /// <summary>
    /// Wraps a dynamic value as a disposable object. This is useful when there is a need to maintain ownership
    /// of an object that can be swapped.
    /// </summary>
    public sealed class DynamicDisposable<T> : IDisposable
        where T : IDisposable
    {
        public DynamicDisposable(Dynamic<T> Source)
        {
            this.Source = Source;
        }

        /// <summary>
        /// The dynamic value to be disposed.
        /// </summary>
        public readonly Dynamic<T> Source;

        /// <summary>
        /// Disposes the current value of the underlying dynamic value.
        /// </summary>
        public void Dispose()
        {
            this.Source.Current.Dispose();
        }
    }
}
