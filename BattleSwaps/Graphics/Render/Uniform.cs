﻿using System;
using System.Collections.Generic;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;

namespace BattleSwaps.Graphics.Render
{
    /// <summary>
    /// Describes a possible uniform value.
    /// </summary>
    public abstract class Uniform
    {
        public Uniform(ShaderValueType ValueType)
        {
            this.ValueType = ValueType;
        }

        /// <summary>
        /// Gets the type of this uniform as seen by the shader.
        /// </summary>
        public readonly ShaderValueType ValueType;

        /// <summary>
        /// Emits a sequence of operations to set this uniform value.
        /// </summary>
        public abstract void Apply(Cursor Cursor, Program Program, string Name, ref TextureUnit NextFreeUnit);

        /// <summary>
        /// Gets a uniform for a static property value.
        /// </summary>
        public static Uniform Static<T>(UniformType<T> Type, T Value)
        {
            return new DynamicUniform<T>(Type, Value);
        }

        /// <summary>
        /// Gets a uniform for a dynamic value.
        /// </summary>
        public static Uniform FromDynamic<T>(UniformType<T> Type, Dynamic<T> Source)
        {
            return new DynamicUniform<T>(Type, Source);
        }

        /// <summary>
        /// Gets a uniform for a texture binding.
        /// </summary>
        public static Uniform Texture(TextureBinding Binding)
        {
            return new VolatileTextureUniform(Binding);
        }

        /// <summary>
        /// Gets a uniform for a texture binding.
        /// </summary>
        public static Uniform Texture(TextureTarget Target, Texture Texture)
        {
            return new VolatileTextureUniform(new TextureBinding(Target, Texture));
        }

        /// <summary>
        /// Gets a uniform for a texture binding.
        /// </summary>
        public static Uniform Texture(TextureTarget Target, Usage<Texture> Texture)
        {
            return new ResourceTextureUniform(Target, Texture);
        }

        /// <summary>
        /// Gets a uniform for a texture binding.
        /// </summary>
        public static Uniform Texture2D(Texture Texture)
        {
            return Uniform.Texture(TextureTarget.Texture2D, Texture);
        }

        /// <summary>
        /// Gets a uniform for a texture binding.
        /// </summary>
        public static Uniform Texture2D(Usage<Texture> Texture)
        {
            return Uniform.Texture(TextureTarget.Texture2D, Texture);
        }

        /// <summary>
        /// Gets a uniform for an int value.
        /// </summary>
        public static Uniform Int(Dynamic<int> Value)
        {
            return Uniform.FromDynamic(UniformType.Int, Value);
        }

        /// <summary>
        /// Gets a uniform for a float value.
        /// </summary>
        public static Uniform Float(Dynamic<float> Value)
        {
            return Uniform.FromDynamic(UniformType.Float, Value);
        }

        /// <summary>
        /// Gets a uniform for a float value.
        /// </summary>
        public static Uniform Float(Dynamic<double> Value)
        {
            return Uniform.Float(Value.Map(v => (float)v));
        }

        /// <summary>
        /// Gets a uniform for a float value.
        /// </summary>
        public static Uniform Float(float Value)
        {
            return Uniform.Float((Dynamic<float>)Value);
        }

        /// <summary>
        /// Gets a uniform for a vector value.
        /// </summary>
        public static Uniform Vec2(Dynamic<Vector2> Value)
        {
            return Uniform.FromDynamic(UniformType.Vec2, Value);
        }

        /// <summary>
        /// Gets a uniform for a vector value.
        /// </summary>
        public static Uniform Vec2(Dynamic<Geometry.Vector> Value)
        {
            return Uniform.Vec2(Value.Map(v => (Vector2)v));
        }

        /// <summary>
        /// Gets a uniform for a vector value.
        /// </summary>
        public static Uniform Vec2(Dynamic<Geometry.HeightTransform> Value)
        {
            return Uniform.Vec2(Value.Map(v => (Vector2)v));
        }

        /// <summary>
        /// Gets a uniform for an integer vector value.
        /// </summary>
        public static Uniform IVec2(Dynamic<Vector2i> Value)
        {
            return Uniform.FromDynamic(UniformType.IVec2, Value);
        }

        /// <summary>
        /// Gets a uniform for an integer vector value.
        /// </summary>
        public static Uniform IVec2(Dynamic<Geometry.ImagePoint> Value)
        {
            return Uniform.IVec2(Value.Map(v => new Vector2i((int)v.X, (int)v.Y)));
        }

        /// <summary>
        /// Gets a uniform for an integer vector value.
        /// </summary>
        public static Uniform IVec2(Dynamic<Geometry.ImageSize> Value)
        {
            return Uniform.IVec2(Value.Map(v => new Vector2i((int)v.Width, (int)v.Height)));
        }

        /// <summary>
        /// Gets a uniform for a vector value.
        /// </summary>
        public static Uniform Vec3(Dynamic<Vector3> Value)
        {
            return Uniform.FromDynamic(UniformType.Vec3, Value);
        }

        /// <summary>
        /// Gets a uniform for a vector value.
        /// </summary>
        public static Uniform Vec3(Dynamic<Geometry.DetailVector> Value)
        {
            return Uniform.Vec3(Value.Map(v => (Vector3)v));
        }

        /// <summary>
        /// Gets a uniform for a vector value.
        /// </summary>
        public static Uniform Vec3(Dynamic<Color> Value)
        {
            return Uniform.Vec3(Value.Map(c => (Vector3)c));
        }

        /// <summary>
        /// Gets a uniform for a vector value.
        /// </summary>
        public static Uniform Vec3(Dynamic<Scene.Radiance> Value)
        {
            return Uniform.Vec3(Value.Map(c => (Vector3)c));
        }

        /// <summary>
        /// Gets a uniform for a vector value.
        /// </summary>
        public static Uniform Vec4(Dynamic<Vector4> Value)
        {
            return Uniform.FromDynamic(UniformType.Vec4, Value);
        }

        /// <summary>
        /// Gets a uniform for a vector value.
        /// </summary>
        public static Uniform Vec4(Dynamic<Paint> Value)
        {
            return Uniform.Vec4(Value.Map(p => (Vector4)p));
        }

        /// <summary>
        /// Gets a uniform for a matrix value.
        /// </summary>
        public static Uniform Mat3(Dynamic<Matrix3> Value)
        {
            return Uniform.FromDynamic(UniformType.Mat3, Value);
        }

        /// <summary>
        /// Gets a uniform for a matrix value.
        /// </summary>
        public static Uniform Mat3(Dynamic<Geometry.Transform> Value)
        {
            return Uniform.Mat3(Value.Map(t => (Matrix3)t));
        }

        /// <summary>
        /// Gets a uniform for a matrix value.
        /// </summary>
        public static Uniform Mat3x2(Dynamic<Matrix3x2> Value)
        {
            return Uniform.FromDynamic(UniformType.Mat3x2, Value);
        }

        /// <summary>
        /// Gets a uniform for a matrix value.
        /// </summary>
        public static Uniform Mat3x2(Dynamic<Geometry.Transform> Value)
        {
            return Uniform.Mat3x2(Value.Map(t => (Matrix3x2)t));
        }

        /// <summary>
        /// Gets a uniform for a matrix value.
        /// </summary>
        public static Uniform Mat3x2(Dynamic<Geometry.OrthoTransform> Value)
        {
            return Uniform.Mat3x2(Value.Map(t => (Matrix3x2)t));
        }

        /// <summary>
        /// Gets a uniform for a matrix value.
        /// </summary>
        public static Uniform Mat4x3(Dynamic<Matrix4x3> Value)
        {
            return Uniform.FromDynamic(UniformType.Mat4x3, Value);
        }

        /// <summary>
        /// Gets a uniform for a matrix value.
        /// </summary>
        public static Uniform Mat4x3(Dynamic<Geometry.UprightTransform> Value)
        {
            return Uniform.Mat4x3(Value.Map(t => (Matrix4x3)t));
        }

        /// <summary>
        /// Gets a uniform for a matrix value.
        /// </summary>
        public static Uniform Mat4x3(Dynamic<Geometry.DetailTransform> Value)
        {
            return Uniform.Mat4x3(Value.Map(t => (Matrix4x3)t));
        }

        /// <summary>
        /// Gets a uniform for a matrix value.
        /// </summary>
        public static Uniform Mat4(Dynamic<Matrix4> Value)
        {
            return Uniform.FromDynamic(UniformType.Mat4, Value);
        }

        /// <summary>
        /// Creates a binding with this uniform as the value.
        /// </summary>
        public UniformBinding BindTo(string Name)
        {
            return new UniformBinding(Name, this);
        }

        public static bool operator ==(Uniform A, Uniform B)
        {
            return object.ReferenceEquals(A, null) ? object.ReferenceEquals(B, null) : A.Equals(B);
        }

        public static bool operator !=(Uniform A, Uniform B)
        {
            return !(A == B);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    /// <summary>
    /// A mapping of a named uniform location to a value.
    /// </summary>
    public struct UniformBinding
    {
        public UniformBinding(string Name, Uniform Value)
        {
            this.Name = Name;
            this.Value = Value;
        }

        /// <summary>
        /// The name of the uniform to bind.
        /// </summary>
        public string Name;

        /// <summary>
        /// The value to bind the uniform to.
        /// </summary>
        public Uniform Value;

        public void Apply(Cursor Cursor, Program Program, ref TextureUnit NextFreeUnit)
        {
            this.Value.Apply(Cursor, Program, this.Name, ref NextFreeUnit);
        }

        public override string ToString()
        {
            return string.Format("{0} : {1}", this.Name, this.Value);
        }
    }

    /// <summary>
    /// Contains a variety of common uniform types.
    /// </summary>
    public static class UniformType
    {
        public static UniformType<int> Int = new UniformType<int>(
            ShaderValueType.Int, (loc, val) => GL.Uniform1(loc, val));
        public static UniformType<uint> UInt = new UniformType<uint>(
            ShaderValueType.UInt, (loc, val) => GL.Uniform1(loc, val));
        public static UniformType<float> Float = new UniformType<float>(
            ShaderValueType.Float, (loc, val) => GL.Uniform1(loc, val));
        public static UniformType<double> Double = new UniformType<double>(
            ShaderValueType.Double, (loc, val) => GL.Uniform1(loc, val));
        public static UniformType<Vector2> Vec2 = new UniformType<Vector2>(
            ShaderValueType.Vec2, (loc, val) => GL.Uniform2(loc, val));
        public static UniformType<Vector3> Vec3 = new UniformType<Vector3>(
            ShaderValueType.Vec3, (loc, val) => GL.Uniform3(loc, val));
        public static UniformType<Vector4> Vec4 = new UniformType<Vector4>(
            ShaderValueType.Vec4, (loc, val) => GL.Uniform4(loc, val));
        public static UniformType<Vector2i> IVec2 = new UniformType<Vector2i>(
            ShaderValueType.IVec2, (loc, val) => GL.Uniform2(loc, val.X, val.Y));
        public static UniformType<Matrix2> Mat2 = new UniformType<Matrix2>(
            ShaderValueType.Mat2, (loc, val) => GL.UniformMatrix2(loc, false, ref val));
        public static UniformType<Matrix2x3> Mat2x3 = new UniformType<Matrix2x3>(
            ShaderValueType.Mat2x3, (loc, val) => GL.UniformMatrix2x3(loc, false, ref val));
        public static UniformType<Matrix2x4> Mat2x4 = new UniformType<Matrix2x4>(
            ShaderValueType.Mat2x4, (loc, val) => GL.UniformMatrix2x4(loc, false, ref val));
        public static UniformType<Matrix3> Mat3 = new UniformType<Matrix3>(
            ShaderValueType.Mat3, (loc, val) => GL.UniformMatrix3(loc, false, ref val));
        public static UniformType<Matrix3x2> Mat3x2 = new UniformType<Matrix3x2>(
            ShaderValueType.Mat3x2, (loc, val) => GL.UniformMatrix3x2(loc, false, ref val));
        public static UniformType<Matrix3x4> Mat3x4 = new UniformType<Matrix3x4>(
            ShaderValueType.Mat3x4, (loc, val) => GL.UniformMatrix3x4(loc, false, ref val));
        public static UniformType<Matrix4> Mat4 = new UniformType<Matrix4>(
            ShaderValueType.Mat4, (loc, val) => GL.UniformMatrix4(loc, false, ref val));
        public static UniformType<Matrix4x2> Mat4x2 = new UniformType<Matrix4x2>(
            ShaderValueType.Mat4x2, (loc, val) => GL.UniformMatrix4x2(loc, false, ref val));
        public static UniformType<Matrix4x3> Mat4x3 = new UniformType<Matrix4x3>(
            ShaderValueType.Mat4x3, (loc, val) => GL.UniformMatrix4x3(loc, false, ref val));
    }

    /// <summary>
    /// Describes a type of uniform variable with an application representation of the given type.
    /// </summary>
    public struct UniformType<T>
    {
        public UniformType(ShaderValueType ValueType, Action<int, T> Set)
        {
            this.ValueType = ValueType;
            this.Set = Set;
        }

        /// <summary>
        /// The type of this uniform as seen by the shader.
        /// </summary>
        public ShaderValueType ValueType;

        /// <summary>
        /// An action which sets the uniform value at the given program location to the given value using this
        /// uniform type.
        /// </summary>
        public Action<int, T> Set;
    }

    /// <summary>
    /// A uniform that gets its value from a dynamic source.
    /// </summary>
    public sealed class DynamicUniform<T> : Uniform
    {
        public DynamicUniform(UniformType<T> Type, Dynamic<T> Source)
            : base(Type.ValueType)
        {
            this.Set = Type.Set;
            this.Source = Source;
        }

        /// <summary>
        /// An action which sets the uniform value at the given program location to the given value.
        /// </summary>
        public readonly Action<int, T> Set;

        /// <summary>
        /// The source giving the value for this uniform.
        /// </summary>
        public readonly Dynamic<T> Source;

        /// <summary>
        /// Gets an operation to apply this uniform to the given program.
        /// </summary>
        public Operation GetOperation(Program Program, string Name)
        {
            return Operation.Uniform(Program, Name, this.Source, this.Set);
        }

        public override void Apply(Cursor Cursor, Program Program, string Name, ref TextureUnit NextFreeUnit)
        {
            Cursor.Emit(this.GetOperation(Program, Name));
        }

        public override bool Equals(object obj)
        {
            DynamicUniform<T> other = obj as DynamicUniform<T>;
            if (other != null)
                return this.Source == other.Source;
            return false;
        }

        public override int GetHashCode()
        {
            return this.Source.GetHashCode();
        }

        public override string ToString()
        {
            return this.Source.ToString();
        }
    }

    /// <summary>
    /// A uniform for a texture.
    /// </summary>
    public abstract class TextureUniform : Uniform
    {
        public TextureUniform(TextureTarget Target)
            : base(ShaderValueType.Sampler(Target))
        { }

        /// <summary>
        /// Gets the texture binding to be given by this uniform.
        /// </summary>
        public abstract TextureBinding Binding { get; }

        public sealed override void Apply(Cursor Cursor, Program Program, string Name, ref TextureUnit NextFreeUnit)
        {
            int val = (int)NextFreeUnit - (int)TextureUnit.Texture0;
            Cursor.Emit(Operation.ActiveTexture(NextFreeUnit));
            Cursor.Emit(this.Binding.Apply);
            Cursor.Emit(Operation.Uniform<int>(Program, Name, val, UniformType.Int.Set));
            NextFreeUnit = (TextureUnit)((int)NextFreeUnit + 1);
        }

        public override string ToString()
        {
            return this.Binding.ToString();
        }
    }

    /// <summary>
    /// A uniform for a static texture loaded from a resource.
    /// </summary>
    public sealed class ResourceTextureUniform : TextureUniform
    {
        public ResourceTextureUniform(TextureTarget Target, Usage<Texture> Texture)
            : base(Target)
        {
            this.Target = Target;
            this.Texture = Texture;
        }

        /// <summary>
        /// The texture target for the texture given by this uniform.
        /// </summary>
        public readonly TextureTarget Target;

        /// <summary>
        /// The usage for the texture given by this uniform.
        /// </summary>
        public readonly new Usage<Texture> Texture;

        public override TextureBinding Binding
        {
            get
            {
                return new TextureBinding(this.Target, this.Texture.Wait());
            }
        }

        public override bool Equals(object obj)
        {
            ResourceTextureUniform other = obj as ResourceTextureUniform;
            if (other != null)
                return this.Texture.Resource == other.Texture.Resource && this.Target == other.Target;
            return false;
        }

        public override int GetHashCode()
        {
            return this.Texture.Resource.GetHashCode() ^ this.Target.GetHashCode();
        }
    }

    /// <summary>
    /// A uniform for a texture which may change over time, identified by its ID.
    /// </summary>
    public sealed class VolatileTextureUniform : TextureUniform
    {
        private readonly TextureBinding _Binding;
        public VolatileTextureUniform(TextureBinding Binding)
            : base(Binding.Target)
        {
            this._Binding = Binding;
        }

        public override TextureBinding Binding
        {
            get
            {
                return this._Binding;
            }
        }

        public override bool Equals(object obj)
        {
            VolatileTextureUniform textureOther = obj as VolatileTextureUniform;
            if (textureOther != null)
                return this._Binding == textureOther._Binding;
            return false;
        }

        public override int GetHashCode()
        {
            return this.Binding.GetHashCode();
        }
    }

    /// <summary>
    /// A vector of two integers.
    /// </summary>
    public struct Vector2i
    {
        public int X;
        public int Y;
        public Vector2i(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", this.X, this.Y);
        }
    }
}
