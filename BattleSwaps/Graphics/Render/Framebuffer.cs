﻿using System;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Graphics.Render
{
    /// <summary>
    /// Identifies a framebuffer in a graphics context. Framebuffers do not maintain ownership of attachments.
    /// </summary>
    public struct Framebuffer : IDisposable
    {
        public Framebuffer(uint Id)
        {
            this.Id = Id;
        }

        /// <summary>
        /// The identifier for the framebuffer.
        /// </summary>
        public uint Id;

        /// <summary>
        /// Binds this framebuffer.
        /// </summary>
        public void Bind(FramebufferTarget Target)
        {
            GL.BindFramebuffer(Target, this.Id);
        }

        /// <summary>
        /// Binds this framebuffer.
        /// </summary>
        public void Bind()
        {
            this.Bind(FramebufferTarget.Framebuffer);
        }

        /// <summary>
        /// Creates a new framebuffer with no attachments.
        /// </summary>
        public static Framebuffer Create()
        {
            return new Framebuffer((uint)GL.GenFramebuffer());
        }

        /// <summary>
        /// Unbinds any bound framebuffers.
        /// </summary>
        public static void Clear(FramebufferTarget Target)
        {
            GL.BindFramebuffer(Target, 0);
        }

        /// <summary>
        /// Binds or clears a framebuffer.
        /// </summary>
        public static void Bind(FramebufferTarget Target, Framebuffer? Framebuffer)
        {
            if (Framebuffer.HasValue)
                Framebuffer.Value.Bind();
            else
                Clear(Target);
        }

        /// <summary>
        /// Binds or clears a framebuffer.
        /// </summary>
        public static void Bind(Framebuffer? Framebuffer)
        {
            Bind(FramebufferTarget.Framebuffer, Framebuffer);
        }

        /// <summary>
        /// Determines whether the currently-bound framebuffer is complete.
        /// </summary>
        public static bool IsComplete(FramebufferTarget Target)
        {
            FramebufferErrorCode ec = GL.CheckFramebufferStatus(Target);
            return ec == FramebufferErrorCode.FramebufferComplete;
        }

        /// <summary>
        /// Attaches a 2D non-mipmapped texture to the currently-bound framebuffer.
        /// </summary>
        public static void Attach2D(FramebufferTarget Target, FramebufferAttachment Attachment, Texture Texture)
        {
            GL.FramebufferTexture2D(Target, Attachment, TextureTarget.Texture2D, Texture.Id, 0);
        }

        /// <summary>
        /// Disables the draw and read buffers for a framebuffer. This is required for depth-only framebuffers.
        /// </summary>
        public static void DisableColor()
        {
            GL.DrawBuffer(DrawBufferMode.None);
            GL.ReadBuffer(ReadBufferMode.None);
        }

        /// <summary>
        /// Sets the drawbuffers for a framebuffer.
        /// </summary>
        public static void DrawBuffers(params DrawBuffersEnum[] Buffers)
        {
            GL.DrawBuffers(Buffers.Length, ref Buffers[0]);
        }

        /// <summary>
        /// Deletes the framebuffer.
        /// </summary>
        public void Dispose()
        {
            GL.DeleteFramebuffer(this.Id);
        }

        public static bool operator ==(Framebuffer A, Framebuffer B)
        {
            return A.Id == B.Id;
        }

        public static bool operator !=(Framebuffer A, Framebuffer B)
        {
            return A.Id != B.Id;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Framebuffer))
                return false;
            return this == (Framebuffer)obj;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Framebuffer({0})", this.Id.ToString());
        }
    }

    /// <summary>
    /// Describes how a render target treats depth information.
    /// </summary>
    public struct DepthTarget
    {
        public DepthTarget(bool WriteEnabled, DepthFunction DepthFunction)
        {
            this.WriteEnabled = WriteEnabled;
            this.DepthFunction = DepthFunction;
        }

        /// <summary>
        /// Indicates whether writing to the depth buffer is enabled.
        /// </summary>
        public bool WriteEnabled;

        /// <summary>
        /// The depth function used to compare drawn depths to the contents of the depth buffer.
        /// </summary>
        public DepthFunction DepthFunction;

        /// <summary>
        /// Indicates whether any of the functionality of the depth buffer is enabled.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return this.WriteEnabled || this.DepthFunction != DepthFunction.Always;
            }
        }

        /// <summary>
        /// A depth target setting which disables writing and accepts all fragments regardless of depth.
        /// </summary>
        public static readonly DepthTarget Disabled = new DepthTarget(false, DepthFunction.Always);

        /// <summary>
        /// Emits a sequences of operations to apply these settings to the current context.
        /// </summary>
        public void Apply(Cursor Cursor)
        {
            if (this.IsEnabled)
            {
                Cursor.Emit(Operation.Enable(EnableCap.DepthTest));
                Cursor.Emit(Operation.DepthMask(this.WriteEnabled));
                Cursor.Emit(Operation.DepthFunc(this.DepthFunction));
            }
            else
            {
                Cursor.Emit(Operation.Disable(EnableCap.DepthTest));
            }
        }
    }

    /// <summary>
    /// Describes how a render target handles transparency.
    /// </summary>
    public struct BlendTarget
    {
        public BlendTarget(BlendingFactorSrc Src, BlendingFactorDest Dest)
        {
            this.Src = Src;
            this.Dest = Dest;
        }

        /// <summary>
        /// The blending factor for drawn objects.
        /// </summary>
        public BlendingFactorSrc Src;

        /// <summary>
        /// The blending factor for the target.
        /// </summary>
        public BlendingFactorDest Dest;

        /// <summary>
        /// Indicates whether any blending functionality is enabled.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return this.Src != BlendingFactorSrc.One || this.Dest != BlendingFactorDest.Zero;
            }
        }

        /// <summary>
        /// A blend target where all drawn objects are opaque.
        /// </summary>
        public static readonly BlendTarget Disabled = new BlendTarget(BlendingFactorSrc.One, BlendingFactorDest.Zero);

        /// <summary>
        /// The standard blend target which uses alpha for blending.
        /// </summary>
        public static readonly BlendTarget Standard = new BlendTarget(
            BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

        /// <summary>
        /// A blend target which adds non-alpha components and uses alpha as a transmittance value.
        /// </summary>
        public static readonly BlendTarget Cumulative = new BlendTarget(
            BlendingFactorSrc.One, BlendingFactorDest.SrcAlpha);

        /// <summary>
        /// Emits a sequences of operations to apply these settings to the current context.
        /// </summary>
        public void Apply(Cursor Cursor)
        {
            if (this.IsEnabled)
            {
                Cursor.Emit(Operation.Enable(EnableCap.Blend));
                Cursor.Emit(Operation.BlendFunc(this.Src, this.Dest));
            }
            else
            {
                Cursor.Emit(Operation.Disable(EnableCap.Blend));
            }
        }
    }

    /// <summary>
    /// Describes a rectangular target that can be drawn to, and the policies for doing so.
    /// </summary>
    public struct Target
    {
        public Target(Framebuffer? Framebuffer, Dynamic<ImageBounds> Viewport, DepthTarget Depth, BlendTarget Blend)
        {
            this.Framebuffer = Framebuffer;
            this.Viewport = Viewport;
            this.Depth = Depth;
            this.Blend = Blend;
        }

        public Target(Framebuffer? Framebuffer, Dynamic<ImageSize> Size, DepthTarget Depth, BlendTarget Blend)
        {
            this.Framebuffer = Framebuffer;
            this.Viewport = Size.Map(s => new ImageBounds(s));
            this.Depth = Depth;
            this.Blend = Blend;
        }

        /// <summary>
        /// The framebuffer to render to.
        /// </summary>
        public Framebuffer? Framebuffer;

        /// <summary>
        /// The viewport rectangle to use, specified using the OpenGL convention where Y coordinates go upward.
        /// </summary>
        public Dynamic<ImageBounds> Viewport;
        
        /// <summary>
        /// Gets the size of this target.
        /// </summary>
        public Dynamic<ImageSize> Size
        {
            get
            {
                return this.Viewport.Map(b => b.Size);
            }
        }

        /// <summary>
        /// Determines how this render target treats depth information.
        /// </summary>
        public DepthTarget Depth;

        /// <summary>
        /// Determines how this render target handles transparency.
        /// </summary>
        public BlendTarget Blend;

        /// <summary>
        /// Emits a sequences of operations to bind and apply the settings for this target.
        /// </summary>
        public void Apply(Cursor Cursor)
        {
            Cursor.Emit(Operation.Bind(FramebufferTarget.Framebuffer, this.Framebuffer));
            Cursor.Emit(Operation.Viewport(this.Viewport));
            this.Depth.Apply(Cursor);
            this.Blend.Apply(Cursor);
        }

        /// <summary>
        /// Gets a rectangular region inside this target.
        /// </summary>
        public Target Region(Dynamic<ImageBounds> Bounds)
        {
            return new Target(this.Framebuffer,
                Dynamic.Lift(this.Viewport, Bounds,
                    (v, b) => new ImageBounds(v.Min.X + b.Min.X, v.Max.Y - b.Max.Y, b.Size)),
                this.Depth, this.Blend);
        }
    }

    /// <summary>
    /// Describes how a render target is cleared before use.
    /// </summary>
    public struct Clear
    {
        public Clear(Dynamic<Paint> Paint, Dynamic<double> Depth)
        {
            this.Paint = Paint;
            this.Depth = Depth;
        }

        public Clear(Dynamic<Paint> Paint)
        {
            this.Paint = Paint;
            this.Depth = null;
        }

        public Clear(Dynamic<double> Depth)
        {
            this.Paint = null;
            this.Depth = Depth;
        }

        /// <summary>
        /// The paint to clear the color of the render target to, or null if not applicable.
        /// </summary>
        public Dynamic<Paint> Paint;

        /// <summary>
        /// The relative depth (between 0.0 and 1.0) to clear the depth buffer to, or null if not applicable.
        /// </summary>
        public Dynamic<double> Depth;

        /// <summary>
        /// Does no clearing.
        /// </summary>
        public static readonly Clear None = new Clear(null, null);

        /// <summary>
        /// Clears the color buffer to be transparent.
        /// </summary>
        public static readonly Clear Transparent = new Clear(Graphics.Paint.Transparent);

        /// <summary>
        /// Emits a sequences of operations to perform this clearing.
        /// </summary>
        public void Apply(Cursor Cursor)
        {
            ClearBufferMask mask = ClearBufferMask.None;
            if (this.Paint != null)
            {
                mask |= ClearBufferMask.ColorBufferBit;
                Cursor.Emit(Operation.ClearColor(this.Paint));
            }
            if (this.Depth != null)
            {
                mask |= ClearBufferMask.DepthBufferBit;
                Cursor.Emit(Operation.ClearDepth(this.Depth));
            }
            if (mask != ClearBufferMask.None)
            {
                Cursor.Emit(Operation.Clear(mask));
            }
        }
    }

    /// <summary>
    /// Contains framebuffer-related extensions.
    /// </summary>
    public static class FramebufferEx
    {
        /// <summary>
        /// Emits a set of operations to render to the given target.
        /// </summary>
        public static void Render(
            this Cursor Cursor,
            Target Target, Clear Clear,
            out Renderable Renderable)
        {
            Routine prepare = Cursor.Expand();
            Routine render = Cursor.Expand();
            Cursor proc = render.End;
            Target.Apply(proc);
            Clear.Apply(proc);
            Renderable = new SequentialRenderable(prepare.End, proc);
        }

        /// <summary>
        /// Emits a set of operations to render to a texture.
        /// </summary>
        public static Texture RenderToTexture(
            this Cursor Cursor,
            TextureFormat Format, DepthTarget Depth,
            BlendTarget Blend, Clear Clear,
            Dynamic<ImageSize> TargetSize,
            out Renderable Renderable,
            out Dynamic<ImageSize> TextureSize,
            out Dynamic<ImageBounds> ContentBounds)
        {
            // Create texture
            Texture texture = Texture.Create();
            texture.Bind(TextureTarget.Texture2D);
            Texture.SetSampling(TextureTarget.Texture2D, TextureSampling.Pixel);
            Cursor.Take(texture);

            // Create framebuffer
            Framebuffer framebuffer = Framebuffer.Create();
            framebuffer.Bind(FramebufferTarget.Framebuffer);
            Framebuffer.Attach2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, texture);
            Cursor.Take(framebuffer);

            // Add step to maintain texture size
            Cursor.MaintainTextureSize(texture, TextureFormat.Paint, TargetSize, out TextureSize);

            // Define target
            ContentBounds = Dynamic.Lift(
                TargetSize, TextureSize,
                (v, t) => new ImageBounds(t).Embed(v));
            Target fullTarget = new Target(framebuffer,
                TextureSize.Map(t => new ImageBounds(0, 0, t)),
                Depth, Blend);
            Target contentTarget = fullTarget.Region(ContentBounds);
            Cursor.Render(contentTarget, Clear, out Renderable);
            return texture;
        }
    }
}