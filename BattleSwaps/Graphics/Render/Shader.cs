﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using OpenTK.Graphics.OpenGL;

using BattleSwaps.Util;
using BattleSwaps.Data;

namespace BattleSwaps.Graphics.Render
{
    /// <summary>
    /// Identifies a shader stored in a graphics context.
    /// </summary>
    public struct Shader : IDisposable
    {
        public Shader(uint Id)
        {
            this.Id = Id;
        }

        /// <summary>
        /// The identifier for the shader.
        /// </summary>
        public uint Id;

        /// <summary>
        /// Provides the source for a shader. This will throw an exception if the source is invalid.
        /// </summary>
        public void Compile(string Source)
        {
            GL.ShaderSource((int)this.Id, Source);
            GL.CompileShader((int)this.Id);

            int status;
            GL.GetShader(this.Id, ShaderParameter.CompileStatus, out status);
            string info = GL.GetShaderInfoLog((int)this.Id);
            if (status != 1) throw new ShaderCompileException(info);
        }

        /// <summary>
        /// Creates a shader with no source.
        /// </summary>
        public static Shader Create(ShaderType Type)
        {
            return new Shader((uint)GL.CreateShader(Type));
        }

        /// <summary>
        /// Creates a shader with the given source.
        /// </summary>
        public static Shader CreateFromSource(ShaderType Type, string Source)
        {
            Shader shader = Create(Type);
            shader.Compile(Source);
            return shader;
        }

        /// <summary>
        /// Deletes the shader.
        /// </summary>
        public void Dispose()
        {
            GL.DeleteShader(this.Id);
        }

        public override string ToString()
        {
            return string.Format("Shader({0})", this.Id.ToString());
        }
    }

    /// <summary>
    /// An exception that is thrown when a shader fails to compile.
    /// </summary>
    public sealed class ShaderCompileException : Exception
    {
        public readonly string Log;
        public ShaderCompileException(string Log)
        {
            this.Log = Log;
        }
    }

    /// <summary>
    /// Describes a shader macro.
    /// </summary>
    public struct Macro
    {
        [DefaultSerializer]
        public static readonly Serializer<Macro> Serializer =
            Data.Serializer.Struct<Macro>();
        public Macro(string Identifier, string Definition)
        {
            this.Identifier = Identifier;
            this.Definition = Definition;
        }

        /// <summary>
        /// The identifier part of the macro. This may include parameters for parametric macros.
        /// </summary>
        public string Identifier;

        /// <summary>
        /// The definition part of the macro.
        /// </summary>
        public string Definition;

        /// <summary>
        /// Gets the full line that defines this macro.
        /// </summary>
        public string Line
        {
            get
            {
                return string.Format("#define {0} {1}", this.Identifier, this.Definition);
            }
        }

        /// <summary>
        /// Constructs a conditional macro for the given identifier.
        /// </summary>
        public static Macro Conditional(string Identifier)
        {
            return new Macro(Identifier, "1");
        }
    }

    /// <summary>
    /// A mapping of a named program attribute to a generic vertex attribute.
    /// </summary>
    public struct AttributeBinding
    {
        [DefaultSerializer]
        public static readonly Serializer<AttributeBinding> Serializer =
            Data.Serializer.Struct<AttributeBinding>();
        public AttributeBinding(string Name, uint Index)
        {
            this.Name = Name;
            this.Index = Index;
        }

        /// <summary>
        /// The name of the attribute to bind.
        /// </summary>
        public string Name;

        /// <summary>
        /// The index of the generic vertex attribute to bind to.
        /// </summary>
        public uint Index;
    }

    /// <summary>
    /// Describes the parameters for loading a shader program from a combined source.
    /// </summary>
    public struct ProgramOptions
    {
        [DefaultSerializer]
        public static readonly Serializer<ProgramOptions> Serializer =
            Data.Serializer.Struct<ProgramOptions>();

        /// <summary>
        /// The preprocessor macros defined for the program.
        /// </summary>
        public ImmutableSet<Macro> Macros;

        /// <summary>
        /// The attribute bindings for the program.
        /// </summary>
        public ImmutableSet<AttributeBinding> Bindings;
    }

    /// <summary>
    /// Identifies a shader program stored in a graphics context and also takes ownership of attached shaders.
    /// </summary>
    public class Program : IDisposable
    {
        private List<Shader> _Attached;
        private Program(uint Id)
        {
            this._Attached = new List<Shader>();
            this.Id = Id;
        }

        /// <summary>
        /// The identifier for the program.
        /// </summary>
        public readonly uint Id;

        /// <summary>
        /// Attaches a shader to this program, taking ownership of it.
        /// </summary>
        public void Attach(Shader Shader)
        {
            GL.AttachShader(this.Id, Shader.Id);
            this._Attached.Add(Shader);
        }

        /// <summary>
        /// Links the program, preparing it for use. This will throw an exception if the program
        /// is not set up correctly.
        /// </summary>
        public void Link()
        {
            GL.LinkProgram(this.Id);

            int status;
            GL.GetProgram(this.Id, GetProgramParameterName.LinkStatus, out status);
            if (status != 1)
            {
                string log = GL.GetProgramInfoLog((int)this.Id);
                throw new ProgramLinkException(log);
            }
        }

        /// <summary>
        /// Binds this program to the graphics context.
        /// </summary>
        public void Use()
        {
            GL.UseProgram(this.Id);
        }

        /// <summary>
        /// Gets the location of the uniform with the given name for this linked program.
        /// </summary>
        public int GetUniformLocation(string Name)
        {
            return GL.GetUniformLocation(this.Id, Name);
        }

        /// <summary>
        /// Binds a named attribute of this program to the given generic vertex attribute index.
        /// </summary>
        public void BindAttribute(string Name, uint Index)
        {
            GL.BindAttribLocation(this.Id, Index, Name);
        }

        /// <summary>
        /// Binds a named attribute of this program to the given generic vertex attribute index.
        /// </summary>
        public void BindAttribute(AttributeBinding Binding)
        {
            this.BindAttribute(Binding.Name, Binding.Index);
        }

        /// <summary>
        /// Creates a new unlinked program with no attached shaders.
        /// </summary>
        public static Program Create()
        {
            return new Program((uint)GL.CreateProgram());
        }

        /// <summary>
        /// Writes the source from the given reader to the given string builder.
        /// </summary>
        /// <param name="Imported">The current set of source files that were imported.</param>
        /// <param name="FuncImporter">A resolver for external shader functions.</param>
        /// <param name="Version">The version string for the source, with newline.</param>
        private static void _WriteSource(
            HashSet<AbsolutePath> Imported,
            ShaderFuncImporter FuncImporter,
            AbsolutePath ResourcePath,
            TextReader Reader, StringBuilder Source,
            ref string Version)
        {
            // Transfer source from reader, line by line
            string line;
            while ((line = Reader.ReadLine()) != null)
            {
                // Check for directives
                Parser parser = Parser.Create(line);
                if (parser.TryAccept('#'))
                {
                    if (parser.TryAcceptItem("version"))
                    {
                        // Read version string
                        Version = line + "\n";
                        continue;
                    }
                    else if (parser.TryAcceptItem("pragma"))
                    {
                        if (parser.TryAcceptItem("include"))
                        {
                            _IncludeSource(Imported, FuncImporter,
                                ResourcePath / parser.ExpectStringItem(),
                                Source);
                            continue;
                        }
                        else if (parser.TryAcceptItem("import"))
                        {
                            _ImportSource(Imported, FuncImporter,
                                ResourcePath / parser.ExpectStringItem(),
                                Source);
                            continue;
                        }
                        else if (parser.TryAcceptItem("extern"))
                        {
                            // TODO: Better GLSL type parsing
                            ShaderValueType returnType = ShaderValueType.FromGLSL(parser.ExpectIdentifier());
                            parser.ExpectWhiteSpace();
                            string name = parser.ExpectIdentifier();
                            parser.Expect('(');
                            parser.AcceptWhiteSpace();
                            List<ShaderValueType> parameterTypes = new List<ShaderValueType>();
                            string ident;
                            while (parser.TryAcceptIdentifier(out ident))
                            {
                                parameterTypes.Add(ShaderValueType.FromGLSL(ident));
                                parser.AcceptWhiteSpace();
                                if (!parser.TryAccept(','))
                                    break;
                                parser.AcceptWhiteSpace();
                            }
                            parser.Expect(')');
                            parser.AcceptWhiteSpace();
                            parser.ExpectEOF();

                            // Import shader function
                            if (!FuncImporter.TryImport(name, parameterTypes.ToArray(), returnType, Source))
                                throw new Exception("No match for external function");
                            continue;
                        }
                    }
                }

                // Add line to source
                Source.Append(line);
                Source.Append('\n');
            }
        }

        /// <summary>
        /// Includes the source from the given reader to the given string builder.
        /// </summary>
        /// <param name="Imported">The current set of source files that were imported.</param>
        /// <param name="FuncImporter">A resolver for external shader functions.</param>
        private static void _IncludeSource(
            HashSet<AbsolutePath> Imported,
            ShaderFuncImporter FuncImporter,
            AbsolutePath Path, StringBuilder Source)
        {
            using (var usage = Blob.Load(Path).Use())
            {
                Blob included = usage.Wait();
                included.Use(null, reader =>
                {
                    string dummyVersion = "";
                    _WriteSource(
                        Imported, FuncImporter,
                        Path / RelativePath.GoUp,
                        reader, Source, ref dummyVersion);
                });
            }
        }

        /// <summary>
        /// Imports the source from the given reader to the given string builder.
        /// </summary>
        /// <param name="Imported">The current set of source files that were imported.</param>
        /// <param name="FuncImporter">A resolver for external shader functions.</param>
        private static void _ImportSource(
            HashSet<AbsolutePath> Imported,
            ShaderFuncImporter FuncImporter,
            AbsolutePath Path, StringBuilder Source)
        {
            if (Imported.Add(Path)) _IncludeSource(Imported, FuncImporter, Path, Source);
        }

        /// <summary>
        /// Creates a new linked program from the combined vertex/fragment source in the given text.
        /// </summary>
        public static Program CreateFromText(
            AbsolutePath ResourcePath, TextReader Reader,
            ShaderFuncImporter FuncImporter,
            ImmutableSet<Macro> Macros,
            ImmutableSet<AttributeBinding> Attributes)
        {
            // Start building new actual source based on given source.
            StringBuilder source = new StringBuilder();
            string version = "";

            // Write macros
            foreach (Macro macro in Macros.Source)
            {
                source.Append(macro.Line);
                source.Append('\n');
            }

            // Write source
            HashSet<AbsolutePath> imported = new HashSet<AbsolutePath>(AbsolutePath.Serializer.Comparer);
            _WriteSource(imported, FuncImporter, ResourcePath, Reader, source, ref version);
            string sourceString = source.ToString();

            // Vertex shader
            string vertexSource = (version + "#define VERTEX 1\n") + sourceString;
            Shader vertex = Shader.CreateFromSource(ShaderType.VertexShader, vertexSource);

            // Fragment shader
            string fragmentSource = (version + "#define FRAGMENT 1\n") + sourceString;
            Shader fragment = Shader.CreateFromSource(ShaderType.FragmentShader, fragmentSource);

            Program program = Create();
            program.Attach(vertex);
            program.Attach(fragment);
            foreach (var binding in Attributes.Source)
                program.BindAttribute(binding);
            program.Link();
            return program;
        }

        /// <summary>
        /// Loads a shader program from a blob, which will be interpreted as GLSL source.
        /// </summary>
        public static Program CreateFromBlob(
            AbsolutePath ResourcePath, Blob Blob,
            ShaderFuncImporter FuncResolver,
            ImmutableSet<Macro> Macros,
            ImmutableSet<AttributeBinding> Attributes)
        {
            Program prog = default(Program);
            Blob.Use(null, reader => prog = CreateFromText(ResourcePath, reader, FuncResolver, Macros, Attributes));
            return prog;
        }

        /// <summary>
        /// Loads a shader program from a path, which will be loaded and interpreted as GLSL source.
        /// </summary>
        public static Program LoadImmediate(
            AbsolutePath SourcePath,
            ShaderFuncImporter FuncImporter,
            ImmutableSet<Macro> Macros,
            ImmutableSet<AttributeBinding> Attributes)
        {
            using (Usage<Blob> blobUsage = Blob.Load(SourcePath).Use())
            {
                return CreateFromBlob(
                    SourcePath / RelativePath.GoUp, blobUsage.Wait(),
                    FuncImporter, Macros, Attributes);
            }
        }

        /// <summary>
        /// A resource for a program loaded from a blob.
        /// </summary>
        [Tag(typeof(Resource<Program>), (uint)3624329264)]
        public sealed class LoadResource : Resource<Program>
        {
            [DefaultSerializer]
            public static readonly Serializer<LoadResource> Serializer =
                Data.Serializer.Immutable<LoadResource>();
            public readonly AbsolutePath ResourcePath;
            public readonly Resource<Blob> Source;
            public readonly ImmutableSet<Macro> Macros;
            public readonly ImmutableSet<AttributeBinding> Attributes;
            private LoadResource(
                AbsolutePath ResourcePath, Resource<Blob> Source,
                ImmutableSet<Macro> Macros, ImmutableSet<AttributeBinding> Attributes)
            {
                this.ResourcePath = ResourcePath;
                this.Source = Source;
                this.Macros = Macros;
                this.Attributes = Attributes;
            }

            public static LoadResource Get(
                AbsolutePath ResourcePath, Resource<Blob> Source,
                ImmutableSet<Macro> Macros, ImmutableSet<AttributeBinding> Attributes)
            {
                return (LoadResource)Resource.Coalesce(new LoadResource(ResourcePath, Source, Macros, Attributes));
            }

            protected override void Load()
            {
                var sourceUsage = this.Source.Use();
                Context.Invoke(delegate
                {
                    this.Provide(CreateFromBlob(
                        this.ResourcePath, sourceUsage.Wait(), null,
                        this.Macros, this.Attributes));
                    sourceUsage.Dispose();
                });
            }
        }

        /// <summary>
        /// Loads a program resource from an external source.
        /// </summary>
        public static Resource<Program> Load(
            AbsolutePath Path,
            ImmutableSet<Macro> Macros, 
            ImmutableSet<AttributeBinding> Attributes)
        {
            return LoadResource.Get(Path / RelativePath.GoUp, Blob.Load(Path), Macros, Attributes);
        }

        /// <summary>
        /// Sets the graphics context to use the given program. If null is given, the programmable
        /// pipeline will be disabled.
        /// </summary>
        public static void Use(Program Program)
        {
            if (Program != null)
                Program.Use();
            else
                Clear();
        }

        /// <summary>
        /// Disables the programmable pipeline.
        /// </summary>
        public static void Clear()
        {
            GL.UseProgram(0);
        }

        /// <summary>
        /// Detaches all shaders from this program and deletes both the shaders and the program.
        /// </summary>
        public void Dispose()
        {
            foreach (Shader shader in this._Attached)
            {
                GL.DetachShader(this.Id, shader.Id);
                shader.Dispose();
            }
            GL.DeleteProgram(this.Id);
        }

        public override string ToString()
        {
            return string.Format("Program({0})", this.Id.ToString());
        }
    }

    /// <summary>
    /// An exception that is thrown when a program fails to link.
    /// </summary>
    public sealed class ProgramLinkException : Exception
    {
        public readonly string Log;
        public ProgramLinkException(string Log)
        {
            this.Log = Log;
        }
    }
}
