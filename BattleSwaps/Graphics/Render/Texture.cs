﻿using System;
using System.Drawing;
using System.Diagnostics;
using Imaging = System.Drawing.Imaging;

using OpenTK.Graphics.OpenGL;

using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Data;

namespace BattleSwaps.Graphics.Render
{
    /// <summary>
    /// Identifies a texture stored in a graphics context.
    /// </summary>
    public struct Texture : IDisposable
    {
        public Texture(uint Id)
        {
            this.Id = Id;
        }

        /// <summary>
        /// The identifier for the texture.
        /// </summary>
        public uint Id;

        /// <summary>
        /// Creates a texture with no associated image data.
        /// </summary>
        public static Texture Create()
        {
            uint id = (uint)GL.GenTexture();
            return new Texture(id);
        }

        /// <summary>
        /// Creates and binds a 2D texture containing image data from the given bitmap.
        /// </summary>
        public static Texture CreateFromBitmap(TextureFormat Format, Bitmap Bitmap)
        {
            Texture texture = Create();
            texture.Bind(TextureTarget.Texture2D);
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, Bitmap.Width, Bitmap.Height);
            if (Format.BitmapFormat.HasValue)
            {
                // Copy bitmap data directly
                var bd = Bitmap.LockBits(rect,
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    Format.BitmapFormat.Value);
                GL.TexImage2D(TextureTarget.Texture2D, 0,
                    Format.PixelInternalFormat,
                    bd.Width, bd.Height, 0,
                    Format.PixelFormat, Format.PixelType, bd.Scan0);
                Bitmap.UnlockBits(bd);
            }
            else
            {
                // Manual read
                _ReadBitmapData(Format, Bitmap, data =>
                {
                    GL.TexImage2D(TextureTarget.Texture2D, 0,
                        Format.PixelInternalFormat,
                        Bitmap.Width, Bitmap.Height, 0,
                        Format.PixelFormat, Format.PixelType, data);
                });
            }
            return texture;
        }

        /// <summary>
        /// Read color data from the given bitmap into the given texture format.
        /// </summary>
        private static unsafe void _ReadBitmapData(TextureFormat Format, Bitmap Bitmap, Action<IntPtr> WithData)
        {
            // Depth texture
            if (Format.PixelFormat == PixelFormat.DepthComponent && Format.PixelType == PixelType.UnsignedShort)
            {
                var bd = Bitmap.LockBits(
                    new System.Drawing.Rectangle(0, 0, Bitmap.Width, Bitmap.Height),
                    Imaging.ImageLockMode.ReadOnly,
                    Imaging.PixelFormat.Format24bppRgb);
                ushort[] result = new ushort[bd.Width * bd.Height];
                fixed (ushort* resultPtr = result)
                {
                    ushort* resultPixel = resultPtr;
                    for (int y = 0; y < bd.Height; y++)
                    {
                        byte* sourcePixel = (byte*)bd.Scan0.ToPointer() + y * bd.Stride;
                        for (int x = 0; x < bd.Width; x++)
                        {
                            byte r = sourcePixel[2];
                            *resultPixel = (ushort)(r * 256);

                            sourcePixel += 3;
                            resultPixel++;
                        }
                    }
                    WithData((IntPtr)resultPtr);
                }
                Bitmap.UnlockBits(bd);
                return;
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Sets the wrap mode for the currently-bound texture.
        /// </summary>
        public static void SetWrapMode(TextureTarget Target, TextureWrapMode Horizontal, TextureWrapMode Vertical)
        {
            GL.TexParameter(Target, TextureParameterName.TextureWrapS, (int)Horizontal);
            GL.TexParameter(Target, TextureParameterName.TextureWrapT, (int)Vertical);
        }

        /// <summary>
        /// Sets the filter mode for the currently-bound texture.
        /// </summary>
        public static void SetFilterMode(TextureTarget Target, TextureMinFilter Min, TextureMagFilter Mag)
        {
            GL.TexParameter(Target, TextureParameterName.TextureMinFilter, (int)Min);
            GL.TexParameter(Target, TextureParameterName.TextureMagFilter, (int)Mag);
        }

        /// <summary>
        /// Creates a mipmap for the currently-bound texture.
        /// </summary>
        public static void GenerateMipmap(GenerateMipmapTarget Target)
        {
            GL.Ext.GenerateMipmap(Target);
        }

        /// <summary>
        /// Sets the sampling mode for the currently-bound texture. Note that if the sampling mode needs a mipmap,
        /// one should be generated before using the texture.
        /// </summary>
        public static void SetSampling(TextureTarget Target, TextureSampling Sampling)
        {
            SetWrapMode(Target, Sampling.WrapHorizontal, Sampling.WrapVertical);
            SetFilterMode(Target, Sampling.Min, Sampling.Mag);
        }

        /// <summary>
        /// Allocates data for the currently-bound 2D texture.
        /// </summary>
        public static void AllocateData2D(TextureFormat Format, ImageSize Size)
        {
            GL.TexImage2D(TextureTarget.Texture2D, 0,
                Format.PixelInternalFormat, (int)Size.Width, (int)Size.Height, 0,
                Format.PixelFormat, Format.PixelType, IntPtr.Zero);
        }

        /// <summary>
        /// Binds this texture.
        /// </summary>
        public void Bind(TextureTarget Target)
        {
            GL.BindTexture(Target, this.Id);
        }

        /// <summary>
        /// Binds this as a 2D texture.
        /// </summary>
        public void Bind2D()
        {
            this.Bind(TextureTarget.Texture2D);
        }

        /// <summary>
        /// Deletes the texture.
        /// </summary>
        public void Dispose()
        {
            GL.DeleteTexture(this.Id);
        }

        public static bool operator ==(Texture A, Texture B)
        {
            return A.Id == B.Id;
        }

        public static bool operator !=(Texture A, Texture B)
        {
            return A.Id != B.Id;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Texture))
                return false;
            return this == (Texture)obj;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Texture({0})", this.Id.ToString());
        }
    }

    /// <summary>
    /// Describes the pixel format of a texture.
    /// </summary>
    public struct TextureFormat
    {
        [DefaultSerializer]
        public static readonly Serializer<TextureFormat> Serializer =
            Data.Serializer.Struct<TextureFormat>();

        public PixelInternalFormat PixelInternalFormat;
        public PixelFormat PixelFormat;
        public PixelType PixelType;
        public Imaging.PixelFormat? BitmapFormat;

        /// <summary>
        /// The recommended texture format for paints.
        /// </summary>
        public static readonly TextureFormat Paint = new TextureFormat
        {
            PixelInternalFormat = PixelInternalFormat.Rgba,
            PixelFormat = PixelFormat.Bgra,
            PixelType = PixelType.UnsignedByte,
            BitmapFormat = Imaging.PixelFormat.Format32bppArgb
        };

        /// <summary>
        /// The recommended texture format for radiance values.
        /// </summary>
        public static readonly TextureFormat Radiance = new TextureFormat
        {
            PixelInternalFormat = PixelInternalFormat.Rgba32f,
            PixelFormat = PixelFormat.Bgra,
            PixelType = PixelType.Float
        };

        /// <summary>
        /// The recommended texture format for colors.
        /// </summary>
        public static readonly TextureFormat Color = new TextureFormat
        {
            PixelInternalFormat = PixelInternalFormat.Rgb,
            PixelFormat = PixelFormat.Bgr,
            PixelType = PixelType.UnsignedByte,
            BitmapFormat = Imaging.PixelFormat.Format24bppRgb
        };

        /// <summary>
        /// The recommended texture format for depthmaps.
        /// </summary>
        public static readonly TextureFormat Depth = new TextureFormat
        {
            PixelInternalFormat = PixelInternalFormat.DepthComponent16,
            PixelFormat = PixelFormat.DepthComponent,
            PixelType = PixelType.UnsignedShort
        };
    }

    /// <summary>
    /// Describes how a texture is sampled.
    /// </summary>
    public struct TextureSampling
    {
        [DefaultSerializer]
        public static readonly Serializer<TextureSampling> Serializer =
            Data.Serializer.Struct<TextureSampling>();

        public TextureWrapMode WrapHorizontal;
        public TextureWrapMode WrapVertical;
        public TextureMinFilter Min;
        public TextureMagFilter Mag;

        /// <summary>
        /// Indicates whether this sampling mode needs a mipmap.
        /// </summary>
        public bool NeedsMipmap
        {
            get
            {
                return
                    this.Min == TextureMinFilter.LinearMipmapLinear ||
                    this.Min == TextureMinFilter.LinearMipmapNearest ||
                    this.Min == TextureMinFilter.NearestMipmapLinear ||
                    this.Min == TextureMinFilter.NearestMipmapNearest;
            }
        }

        /// <summary>
        /// The recommended sampling method for texture that should appear smoothly at different zoom levels.
        /// </summary>
        public static readonly TextureSampling Smooth = new TextureSampling
        {
            WrapHorizontal = TextureWrapMode.Repeat,
            WrapVertical = TextureWrapMode.Repeat,
            Min = TextureMinFilter.LinearMipmapLinear,
            Mag = TextureMagFilter.Linear
        };

        /// <summary>
        /// The recommended sampling method for texture that should appear smoothly at different zoom levels.
        /// </summary>
        public static readonly TextureSampling SmoothNoWrap = new TextureSampling
        {
            WrapHorizontal = TextureWrapMode.Clamp,
            WrapVertical = TextureWrapMode.Clamp,
            Min = TextureMinFilter.LinearMipmapLinear,
            Mag = TextureMagFilter.Linear
        };

        /// <summary>
        /// The recommended sampling method for texture that should appear smoothly at different zoom levels,
        /// but for some reason can not be mipmapped.
        /// </summary>
        public static readonly TextureSampling SmoothNoWrapNoMipmap = new TextureSampling
        {
            WrapHorizontal = TextureWrapMode.Clamp,
            WrapVertical = TextureWrapMode.Clamp,
            Min = TextureMinFilter.Linear,
            Mag = TextureMagFilter.Linear
        };

        /// <summary>
        /// The recommended sampling method for a texture that is accessed by pixel.
        /// </summary>
        public static readonly TextureSampling Pixel = new TextureSampling
        {
            WrapHorizontal = TextureWrapMode.Clamp,
            WrapVertical = TextureWrapMode.Clamp,
            Min = TextureMinFilter.Nearest,
            Mag = TextureMagFilter.Nearest
        };
    }
    
    /// <summary>
    /// A strategy for resizing an automatically-sized texture.
    /// </summary>
    public struct ResizeStrategy
    {
        public ResizeStrategy(Func<ImageSize, ImageSize, ImageSize?> GetNewSize)
        {
            this.GetNewSize = GetNewSize;
        }

        /// <summary>
        /// Given the current texture size, and the required texture size, returns the new size of the
        /// texture, or null if the texture should not be resized.
        /// </summary>
        public Func<ImageSize, ImageSize, ImageSize?> GetNewSize;

        /// <summary>
        /// A resize strategy that keeps the texture at exactly the required size. This is best when
        /// resize requests are infrequent.
        /// </summary>
        public static readonly ResizeStrategy Tight = new ResizeStrategy(
            (curSize, newSize) => curSize != newSize ? (ImageSize?)newSize : null);
    }

    /// <summary>
    /// Describes the binding for a texture target.
    /// </summary>
    public struct TextureBinding
    {
        public TextureBinding(TextureTarget Target, Texture Texture)
        {
            this.Target = Target;
            this.Texture = Texture;
        }

        /// <summary>
        /// The texture target used for binding.
        /// </summary>
        public TextureTarget Target;

        /// <summary>
        /// The texture bound to the target.
        /// </summary>
        public Texture Texture;

        /// <summary>
        /// The operation to apply this binding.
        /// </summary>
        public Operation Apply
        {
            get
            {
                return Operation.Bind(this.Target, this.Texture);
            }
        }

        public static bool operator ==(TextureBinding A, TextureBinding B)
        {
            return A.Target == B.Target && A.Texture == B.Texture;
        }

        public static bool operator !=(TextureBinding A, TextureBinding B)
        {
            return A.Target != B.Target || A.Texture != B.Texture;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TextureBinding))
                return false;
            return this == (TextureBinding)obj;
        }

        public override int GetHashCode()
        {
            return this.Target.GetHashCode() ^ this.Texture.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("{0} : {1}", this.Target, this.Texture);
        }
    }

    /// <summary>
    /// Contains extension functions related to textures.
    /// </summary>
    public static class TextureEx
    {
        [DefaultSerializer]
        public static readonly Serializer<PixelInternalFormat> PixelInternalFormatSerializer =
            new EnumSerializer<PixelInternalFormat, int>(Int32Serializer.Instance);

        [DefaultSerializer]
        public static readonly Serializer<PixelFormat> PixelFormatSerializer =
            new EnumSerializer<PixelFormat, int>(Int32Serializer.Instance);

        [DefaultSerializer]
        public static readonly Serializer<PixelType> PixelTypeSerializer =
            new EnumSerializer<PixelType, int>(Int32Serializer.Instance);

        [DefaultSerializer]
        public static readonly Serializer<Imaging.PixelFormat> ImagingPixelFormatSerializer =
            new EnumSerializer<Imaging.PixelFormat, int>(Int32Serializer.Instance);

        [DefaultSerializer]
        public static readonly Serializer<TextureWrapMode> TextureWrapModeSerializer =
            new EnumSerializer<TextureWrapMode, int>(Int32Serializer.Instance);

        [DefaultSerializer]
        public static readonly Serializer<TextureMinFilter> TextureMinFilterSerializer =
            new EnumSerializer<TextureMinFilter, int>(Int32Serializer.Instance);

        [DefaultSerializer]
        public static readonly Serializer<TextureMagFilter> TextureMagFilterSerializer =
            new EnumSerializer<TextureMagFilter, int>(Int32Serializer.Instance);

        /// <summary>
        /// Gets the name of a texture constructed from an image
        /// </summary>
        private static Func<Name, Name> _ImageTextureName =
            BattleSwaps.Name.GetDecorator(new Name(16170399605020717810));

        /// <summary>
        /// A resource for a texture loaded from an image.
        /// </summary>
        [Tag(typeof(Resource<Texture>), (uint)3204524560)]
        public sealed class ImageResource : Resource<Texture>
        {
            [DefaultSerializer]
            public static readonly Serializer<ImageResource> Serializer =
                Data.Serializer.Immutable<ImageResource>();
            public readonly Image Image;
            public readonly TextureFormat Format;
            public readonly TextureSampling Sampling;
            private ImageResource(Image Image, TextureFormat Format, TextureSampling Sampling)
            {
                this.Image = Image;
                this.Format = Format;
                this.Sampling = Sampling;
            }

            public static ImageResource Get(Image Image, TextureFormat Format, TextureSampling Sampling)
            {
                return (ImageResource)Resource.Coalesce(new ImageResource(Image, Format, Sampling));
            }

            protected override void Load()
            {
                var sourceUsage = this.Image.Source.Use();
                Context.Invoke(delegate
                {
                    Texture texture = Texture.CreateFromBitmap(this.Format, sourceUsage.Wait());
                    sourceUsage.Dispose();

                    if (this.Sampling.NeedsMipmap) Texture.GenerateMipmap(GenerateMipmapTarget.Texture2D);
                    Texture.SetSampling(TextureTarget.Texture2D, this.Sampling);
                    this.Provide(texture);
                });
            }
        }

        /// <summary>
        /// Constructs a texture resource for the given image using the specified format and sampling method.
        /// </summary>
        public static Resource<Texture> ToTexture(this Image Image, TextureFormat Format, TextureSampling Sampling)
        {
            return ImageResource.Get(Image, Format, Sampling);
        }

        /// <summary>
        /// Emits operations to make the given 2D texture be automatically-sized, updating its size (if needed) at
        /// this point in the procedure. The texture will have at least the given minimum size. The actual size will
        /// be given by the returned dynamic value.
        /// </summary>
        public static void MaintainTextureSize(
            this Cursor Cursor, Texture Texture,
            TextureFormat Format, ResizeStrategy Strategy,
            Dynamic<ImageSize> MinSize, out Dynamic<ImageSize> Size)
        {
            ExplicitDynamic<ImageSize> size = new ExplicitDynamic<ImageSize>(ImageSize.Zero);
            Size = size;
            Operation op = new Operation(delegate
            {
                ImageSize current = size.Current;
                ImageSize required = MinSize.Current;
                ImageSize? next = Strategy.GetNewSize(current, required);
                if (next.HasValue)
                {
                    current = next.Value;
                    Texture.Bind2D();
                    Texture.AllocateData2D(Format, current);
                    size.Current = current;
                }
            });
            op.Explain("MaintainTextureSize", Texture, MinSize);
            Cursor.Emit(op);
        }

        /// <summary>
        /// Emits operations to make the given 2D texture be automatically-sized, updating its size (if needed) at
        /// this point in the procedure. The texture will have at least the given minimum size. The actual size will
        /// be given by the returned dynamic value.
        /// </summary>
        public static void MaintainTextureSize(
            this Cursor Cursor, Texture Texture, TextureFormat Format,
            Dynamic<ImageSize> MinSize, out Dynamic<ImageSize> Size)
        {
            MaintainTextureSize(Cursor, Texture, Format, ResizeStrategy.Tight, MinSize, out Size);
        }


        /// <summary>
        /// Emits operations to make the given 2D texture to be automatically-sized, updating its size (if needed) at
        /// this point in the procedure. The texture will have exactly the given size afterwards.
        /// </summary>
        public static void MaintainTextureSize(
            this Cursor Cursor, Texture Texture, TextureFormat Format,
            Dynamic<ImageSize> Size)
        {
            MaintainTextureSize(Cursor, Texture, Format, ResizeStrategy.Tight, Size, out Size);
        }

        /// <summary>
        /// Draws the given texture over the entire render target. This is intended to be used for debugging.
        /// </summary>
        public static void DebugDrawTexture(this Renderable Renderable, Texture Texture)
        {
            var prog = Render.Program.Load("Shader/DebugDrawTexture.glsl",
                Util.ImmutableSet<Macro>.Empty,
                Util.ImmutableSet.From(new AttributeBinding("pos", 0)));
            Renderable.Add(new Primitive
            {
                IsUnitQuad = true,
                Program = prog,
                Uniforms = Util.ImmutableSet.From(Uniform.Texture2D(Texture).BindTo("texture")),
                Resources = Util.ImmutableSet<IDisposable>.Empty
            });
        }
    }
}