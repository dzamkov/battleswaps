﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Graphics.Render
{
    /// <summary>
    /// Contains functions related to the program OpenGL context.
    /// </summary>
    public static class Context
    {
        /// <summary>
        /// Indicates whether the program OpenGL context is bound on this thread.
        /// </summary>
        [ThreadStatic]
        public static bool IsThreadBound;

        /// <summary>
        /// Invokes an action on a thread with the program OpenGL context bound to it. The action may be invoked
        /// some time after this returns.
        /// </summary>
        public static void Invoke(Action Action)
        {
            if (IsThreadBound)
                Action();
            else
                throw new NotImplementedException(); // TODO
        }
    }
}