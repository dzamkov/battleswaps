﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;

using OpenTK.Graphics.OpenGL;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;

namespace BattleSwaps.Graphics.Render
{
    /// <summary>
    /// An interface which allows the introduction of renderable effects into a procedure. The framebuffer target,
    /// viewport, format, blend mode, depth settings and other pertinent context information is
    /// fixed within the renderable.
    /// </summary>
    public abstract class Renderable
    {
        public Renderable(Cursor Prepare)
        {
            this.Prepare = Prepare;
        }

        /// <summary>
        /// A cursor where operations can be emitted to prepare resources needed for rendered effects. This is
        /// guranteed to be executed before all primitives added to the renderable, and there is no requirement for
        /// operations at this cursor to maintain render-target-related state.
        /// </summary>
        public readonly Cursor Prepare;

        /// <summary>
        /// Creates a new renderable that adds to this renderable. If draw order matters in the render target, all
        /// effects added to the returned renderable will be drawn after all existing effects in the renderable, but
        /// before all effects that are added to this renderable after this call returns.
        /// </summary>
        public abstract Renderable Expand();

        /// <summary>
        /// Adds a primitive to be drawn by this renderable. This returns an object that can be disposed to
        /// remove the primitive and dispose its resources. If draw order matters in the render target, the added
        /// primitive will be drawn after all existing effects in the renderable.
        /// </summary>
        public abstract IDisposable Add(Primitive Primitive);
        
        /// <summary>
        /// Adds a bag of objects to be drawn giving the reactive state to access them from and an action to build
        /// effects for them. The order the objects will be drawn in is undefined, but they will all be drawn after
        /// all existing effects in the renderable.
        /// </summary>
        public void Loop<T>(Dynamic<Moment> Moment, Bag<T> Objects, Action<T, Renderable> Build)
        {
            this.Loop(Moment.Map(m => m.State), Objects, Build);
        }

        /// <summary>
        /// Adds a bag of objects to be drawn giving the reactive state to access them from and an action to build
        /// effects for them. The order the objects will be drawn in is undefined, but they will all be drawn after
        /// all existing effects in the renderable.
        /// </summary>
        public void Loop<T>(Dynamic<State> State, Bag<T> Objects, Action<T, Renderable> Build)
        {
            Renderable loopRenderable = this.Expand();
            Dictionary<Latch, List<IDisposable>> removables = new Dictionary<Latch, List<IDisposable>>();

            // Adds an item to be rendered in the loop.
            Action<Entry<T>> add = delegate(Entry<T> entry)
            {
                if (!entry.Remove.IsAlwaysOpen)
                {
                    // If the item can be removed, track the objects that need to be disposed to remove it
                    List<IDisposable> itemRemovables;
                    if (!removables.TryGetValue(entry.Remove, out itemRemovables))
                        removables[entry.Remove] = itemRemovables = new List<IDisposable>();
                    Build(entry.Value, new GroupRenderable(loopRenderable, itemRemovables));
                }
                else
                {
                    // If the item is never removed, add it directly to the loop renderable
                    Build(entry.Value, loopRenderable);
                }
            };

            // Add initial set of items
            State lastState = State.Current;
            OrLatchBuilder invalidateBuilder = OrLatchBuilder.Create();
            Objects.Iterate(lastState, add, invalidateBuilder);
            Latch invalidate = invalidateBuilder.Finish();

            // Add procedure to maintain loop items
            if (removables.Count > 0 || !invalidate.IsAlwaysOpen)
            {
                Operation maintain = new Operation(delegate
                {
                    State curState = State.Current;

                    // TODO: Use lifetimes instead of latches to allow branch switching
                    if (!(lastState.Tag <= curState.Tag))
                        throw new NotImplementedException();

                    // Check if any old items expired
                    List<KeyValuePair<Latch, List<IDisposable>>> toRemove = null;
                    foreach (var kvp in removables)
                    {
                        if (kvp.Key.IsClosed(curState))
                        {
                            if (toRemove == null)
                                toRemove = new List<KeyValuePair<Latch, List<IDisposable>>>();
                            toRemove.Add(kvp);
                        }
                    }
                    if (toRemove != null)
                    {
                        foreach (var kvp in toRemove)
                        {
                            foreach (var disposable in kvp.Value)
                                disposable.Dispose();
                            removables.Remove(kvp.Key);
                        }
                    }

                    // Check for new items
                    if (invalidate.IsClosed(curState))
                    {
                        invalidateBuilder = OrLatchBuilder.Create();
                        Objects.IterateDelta(lastState, curState, add, invalidateBuilder);
                        invalidate = invalidateBuilder.Finish();
                        lastState = curState;
                    }
                });
                maintain.Explain("MaintainLoop", State, Objects);
                this.Prepare.Emit(maintain);
            }
        }
    }
    
    /// <summary>
    /// Identifies the face-culling operation applied to a primitive.
    /// </summary>
    public enum Culling
    {
        /// <summary>
        /// No triangles are culled.
        /// </summary>
        None,

        /// <summary>
        /// Back-facing triangles are culled.
        /// </summary>
        Back,

        /// <summary>
        /// Front-facing triangles are culled.
        /// </summary>
        Front
    }

    /// <summary>
    /// Describes a renderable primitive.
    /// </summary>
    public struct Primitive
    {
        /// <summary>
        /// The program used to draw this primitive.
        /// </summary>
        public ResourceRef<Program> Program;
        
        /// <summary>
        /// The mesh used to draw this primitive.
        /// </summary>
        public ResourceRef<Mesh> Mesh;
        
        /// <summary>
        /// The type of this primitive.
        /// </summary>
        public PrimitiveType Type;

        /// <summary>
        /// The culling applied to this primitive.
        /// </summary>
        public Culling Culling;

        /// <summary>
        /// The uniforms to render the primitive with.
        /// </summary>
        public ImmutableSet<UniformBinding> Uniforms;

        /// <summary>
        /// The resources needed to render this primitive. These will be disposed when the primitive stops being
        /// rendered.
        /// </summary>
        public ImmutableSet<IDisposable> Resources;

        /// <summary>
        /// Indicates whether this primitive is based on the unit quad.
        /// </summary>
        public bool IsUnitQuad
        {
            get
            {
                return this.Mesh.Resource == Render.Mesh.UnitQuad && this.Type == PrimitiveType.Triangles;
            }
            set
            {
                Debug.Assert(value);
                this.Mesh = Render.Mesh.UnitQuad;
                this.Type = PrimitiveType.Triangles;
            }
        }
    }

    /// <summary>
    /// A concrete renderable which maintains the order that effects are added.
    /// </summary>
    public sealed class SequentialRenderable : Renderable
    {
        private Cursor _Render;
        public SequentialRenderable(Cursor Prepare, Cursor Render)
            : base(Prepare)
        {
            this._Render = Render;
        }

        public override Renderable Expand()
        {
            return new SequentialRenderable(this.Prepare, this._Render.Expand().End);
        }

        public override IDisposable Add(Primitive Primitive)
        {
            Routine routine = this._Render.Expand();
            Cursor proc = routine.End;
            Program program = Primitive.Program.Object;
            if (Primitive.Program.Resource != null)
            {
                var programUsage = Primitive.Program.Resource.Use();
                proc.Take(programUsage);
                program = programUsage.Wait();
            }
            Mesh mesh = Primitive.Mesh.Object;
            if (Primitive.Mesh.Resource != null)
            {
                var meshUsage = Primitive.Mesh.Resource.Use();
                proc.Take(meshUsage);
                mesh = meshUsage.Wait();
            }
            proc.Take(Primitive.Resources.Source);
            if (Primitive.Culling == Culling.None)
            {
                proc.Emit(Operation.Disable(EnableCap.CullFace));
            }
            else if (Primitive.Culling == Culling.Back)
            {
                proc.Emit(Operation.Enable(EnableCap.CullFace));
                proc.Emit(Operation.CullFace(CullFaceMode.Back));
            }
            else if (Primitive.Culling == Culling.Front)
            {
                proc.Emit(Operation.Enable(EnableCap.CullFace));
                proc.Emit(Operation.CullFace(CullFaceMode.Front));
            }
            proc.Emit(Operation.Use(program));
            proc.Emit(Operation.Bind(mesh));
            TextureUnit nextFreeUnit = TextureUnit.Texture0;
            foreach (var uniform in Primitive.Uniforms.Source)
                uniform.Apply(proc, program, ref nextFreeUnit);
            PrimitiveType type = Primitive.Type;
            Operation draw = new Operation(() => mesh.Draw(type));
            draw.Explain("Draw", type);
            proc.Emit(draw);
            return routine;
        }
    }

    /// <summary>
    /// A renderable which adds to a source renderable, but tracks which primitives are added to it, allowing
    /// them to be removed as a group.
    /// </summary>
    public sealed class GroupRenderable : Renderable
    {
        public GroupRenderable(Renderable Source, List<IDisposable> Objects)
            : base(Source.Prepare)
        {
            this.Source = Source;
            this.Objects = Objects;
        }

        /// <summary>
        /// The source renderable this renderable writes to.
        /// </summary>
        public readonly Renderable Source;

        /// <summary>
        /// The disposable objects for the primitives added to this renderable.
        /// </summary>
        public readonly List<IDisposable> Objects;

        public override Renderable Expand()
        {
            Renderable nSource = this.Source.Expand();
            if (nSource != this.Source)
                return new GroupRenderable(nSource, this.Objects);
            else
                return this;
        }

        public override IDisposable Add(Primitive Primitive)
        {
            IDisposable obj = this.Source.Add(Primitive);
            this.Objects.Add(obj);
            return obj;
        }
    }
}
