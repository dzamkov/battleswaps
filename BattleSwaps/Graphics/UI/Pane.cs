﻿using System;
using System.Collections.Generic;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

namespace BattleSwaps.Graphics.UI
{
    /// <summary>
    /// A display for a UI pane made up of a highlight color and a background texture.
    /// </summary>
    public sealed class PaneDisplay : Display
    {
        public PaneDisplay(Image Map, Image Texture, Signal<ImageSize> Size)
        {
            this.Map = Map;
            this.Texture = Texture;
            this.Size = Size;
        }

        /// <summary>
        /// The image that decides where highlights and textures are displayed. In the likely scenario that this
        /// is smaller than the pane, it will be stretched along its midline. Red pixels will be mapped to highlights.
        /// Green pixels will be mapped to texture. Black will remain black and transparent will remain transparent.
        /// </summary>
        public readonly Image Map;

        /// <summary>
        /// The texture image for this pane. This will be tiled to accommodate large panes.
        /// </summary>
        public readonly Image Texture;

        /// <summary>
        /// The size of the pane when drawn.
        /// </summary>
        public readonly Signal<ImageSize> Size;

        /// <summary>
        /// The resource for the shader used to render this display.
        /// </summary>
        public static Resource<Render.Program> Program =
            Render.Program.Load("Shader/Display/Pane.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(new AttributeBinding("pos", 0)));

        public override void Instantiate(Renderable Renderable, Display.Context Context)
        {
            var windowTexture = this.Map.ToTexture(TextureFormat.Paint, TextureSampling.Pixel).Use();
            var backTexture = this.Texture.ToTexture(TextureFormat.Paint, TextureSampling.Pixel).Use();
            Renderable.Add(new Primitive
            {
                IsUnitQuad = true,
                Program = Program,
                Uniforms = ImmutableSet.From(
                    Uniform.Texture2D(windowTexture).BindTo("windowTexture"),
                    Uniform.Texture2D(backTexture).BindTo("backTexture"),
                    Uniform.IVec2(Context.Moment.Read(this.Size)).BindTo("size"),
                    Uniform.IVec2(Context.Offset).BindTo("viewOffset"),
                    Uniform.IVec2(Context.ViewportSize).BindTo("viewSize")),
               Resources = ImmutableSet.From<IDisposable>(windowTexture, backTexture)
            });
        }
    }
}
