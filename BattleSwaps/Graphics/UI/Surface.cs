﻿using System;
using System.Collections.Generic;

using BattleSwaps.Reactive;

namespace BattleSwaps.Graphics.UI
{
    /// <summary>
    /// An interactive user interface in an unbounded continuous 2D space.
    /// </summary>
    public abstract class Surface : Element
    {

    }
}
