﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Graphics.UI
{
    /// <summary>
    /// An interactive user interface in an unbounded discrete 2D space.
    /// </summary>
    public abstract class Window : Element
    {
        /// <summary>
        /// Gets the display used to draw the window.
        /// </summary>
        public abstract Property<Display> Display { get; }
    }
}
