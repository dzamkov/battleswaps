﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Reactive;

namespace BattleSwaps.Graphics.UI
{
    /// <summary>
    /// A generic UI element.
    /// </summary>
    public abstract class Element
    {
        private Name _Name;
        private Domain _Domain;
        private static readonly InteriorRef<Name> _NameRef =
            (obj, act) => act(ref ((Element)obj)._Name);
        private static readonly InteriorRef<Domain> _DomainRef =
            (obj, act) => act(ref ((Element)obj)._Domain);
        public Element()
        {
            Property.Introduce(Ref.Create(this, _NameRef));
            Property.Introduce(Ref.Create(this, _DomainRef));
        }

        /// <summary>
        /// Gets or sets the name of this element for tie-breaking purposes.
        /// </summary>
        public Property<Name> Name
        {
            get
            {
                return Property.Get(Ref.Create(this, _NameRef), ref this._Name);
            }
            set
            {
                Property.Define(Ref.Create(this, _NameRef), ref this._Name, value);
            }
        }

        /// <summary>
        /// Gets or sets the domain this element is in.
        /// </summary>
        public Property<Domain> Domain
        {
            get
            {
                return Property.Get(Ref.Create(this, _DomainRef), ref this._Domain);
            }
            set
            {
                Property.Define(Ref.Create(this, _DomainRef), ref this._Domain, value);
            }
        }

        /// <summary>
        /// Gets the event source for this UI element.
        /// </summary>
        public virtual Property<Bag<Event>> PendingEvents
        {
            get
            {
                return Bag<Event>.Empty;
            }
        }
    }

    /// <summary>
    /// Contains the non-spatial information all UI elements can access.
    /// </summary>
    public abstract class Domain
    {
        /// <summary>
        /// Indicates whether the given key is being held down.
        /// </summary>
        public abstract Property<Signal<bool>> IsKeyDown(OpenTK.Input.Key Key);
    }

    /// <summary>
    /// Identifies a mouse button.
    /// </summary>
    public enum MouseButton
    {
        Left,
        Right
    }

    /// <summary>
    /// An interface for getting input from a mouse.
    /// </summary>
    public struct Mouse<TLoc>
    {
        /// <summary>
        /// The position of the mouse.
        /// </summary>
        public Signal<TLoc> Position;

        /// <summary>
        /// Indicates whether the left mouse button is down.
        /// </summary>
        public Signal<bool> IsLeftDown;

        /// <summary>
        /// Indicates whether the right mouse button is down.
        /// </summary>
        public Signal<bool> IsRightDown;

        /// <summary>
        /// Indicates whether the given mouse button is down.
        /// </summary>
        public Signal<bool> IsButtonDown(MouseButton Button)
        {
            if (Button == MouseButton.Left) return this.IsLeftDown;
            else return this.IsRightDown;
        }
    }
}
