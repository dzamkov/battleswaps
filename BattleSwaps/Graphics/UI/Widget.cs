﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Graphics.UI
{
    /// <summary>
    /// An interactive user interface in a bounded discrete 2D space. Unlike windows, widgets have their size and
    /// location determined by their environment.
    /// </summary>
    public abstract class Widget : Element
    {
        private Signal<ImageSize> _Size;
        private static readonly InteriorRef<Signal<ImageSize>> _SizeRef =
            (obj, act) => act(ref ((Widget)obj)._Size);
        public Widget()
        {
            Property.Introduce(Ref.Create(this, _SizeRef));
        }

        /// <summary>
        /// Gets or sets the size of the widget.
        /// </summary>
        public Property<Signal<ImageSize>> Size
        {
            get
            {
                return Property.Get(Ref.Create(this, _SizeRef), ref this._Size);
            }
            set
            {
                Property.Define(Ref.Create(this, _SizeRef), ref this._Size, value);
            }
        }

        /// <summary>
        /// Sets the environment for this widget. This may be used in place of setting the global environment and
        /// size directly.
        /// </summary>
        public WidgetEnvironment Environment
        {
            set
            {
                this.Name = value.Name;
                this.Domain = value.Domain;
                this.Size = value.Size;
            }
        }

        /// <summary>
        /// Gets the display used to draw the widget.
        /// </summary>
        public abstract Property<Display> Display { get; }

        /// <summary>
        /// Responds to a key down event.
        /// </summary>
        public virtual void OnKeyDown(Transaction Transaction, OpenTK.Input.Key Key)
        {

        }

        /// <summary>
        /// Responds to a key up event.
        /// </summary>
        public virtual void OnKeyUp(Transaction Transaction, OpenTK.Input.Key Key)
        {

        }

        /// <summary>
        /// Responds to a mouse scroll event.
        /// </summary>
        public virtual void OnMouseScroll(Transaction Transaction, Mouse<ImagePoint> Mouse, double Amount)
        {

        }

        /// <summary>
        /// Responds to a mouse down event.
        /// </summary>
        public virtual void OnMouseDown(Transaction Transaction, MouseButton Button, Mouse<ImagePoint> Mouse)
        {

        }
    }

    /// <summary>
    /// Describes the environment for a widget.
    /// </summary>
    public struct WidgetEnvironment
    {
        /// <summary>
        /// The name of the widget.
        /// </summary>
        public Name Name;

        /// <summary>
        /// The domain for the widget.
        /// </summary>
        public Domain Domain;

        /// <summary>
        /// The size of the widget.
        /// </summary>
        public Property<Signal<ImageSize>> Size;
    }

    /// <summary>
    /// A static description of a widget at a particular reactive state.
    /// </summary>
    public struct WidgetPrototype
    {
        public WidgetPrototype(Func<Initializer, Widget> Source)
        {
            this.Source = Source;
        }

        /// <summary>
        /// The instantiation function that defines this prototype.
        /// </summary>
        public Func<Initializer, Widget> Source;

        /// <summary>
        /// Creates an instance of the widget described by this prototype. The only missing information that
        /// needs to be supplied before the entity can be put to use is its environment.
        /// </summary>
        public Widget Instantiate(Initializer Initializer)
        {
            return this.Source(Initializer);
        }

        /// <summary>
        /// Creates an instance of the widget described by this prototype. The only missing information that
        /// needs to be supplied before the entity can be put to use is its environment.
        /// </summary>
        public Widget Instantiate(Initializer Initializer, WidgetEnvironment Environment)
        {
            Widget widget = this.Instantiate(Initializer);
            widget.Environment = Environment;
            return widget;
        }
    }
}
