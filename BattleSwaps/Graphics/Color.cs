﻿using System;
using System.Diagnostics;

using BattleSwaps.Data;

using OpenTK;

namespace BattleSwaps.Graphics
{
    /// <summary>
    /// Identifies a color (no alpha).
    /// </summary>
    public struct Color
    {
        public double R;
        public double G;
        public double B;

        [DefaultSerializer]
        public static readonly Serializer<Color> Serializer = Data.Serializer.Struct<Color>();
        public Color(double R, double G, double B)
        {
            this.R = R;
            this.G = G;
            this.B = B;
        }

        public static readonly Color White = new Color(1.0, 1.0, 1.0);

        public static readonly Color Black = new Color(0.0, 0.0, 0.0);

        public static implicit operator System.Drawing.Color(Color Color)
        {
            return System.Drawing.Color.FromArgb(
                (int)Math.Min(255.0, Color.R * 256.0),
                (int)Math.Min(255.0, Color.G * 256.0),
                (int)Math.Min(255.0, Color.B * 256.0));
        }

        public static implicit operator Vector3(Color Color)
        {
            return new Vector3(
                (float)Color.R,
                (float)Color.G,
                (float)Color.B);
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}, {2})", this.R, this.G, this.B);
        }
    }

    /// <summary>
    /// Identifies a color with transparency information.
    /// </summary>
    public struct Paint
    {
        public Color Color;
        public double A;

        [DefaultSerializer]
        public static readonly Serializer<Paint> Serializer = Data.Serializer.Struct<Paint>();
        public Paint(Color Color, double A)
        {
            this.Color = Color;
            this.A = A;
        }

        public Paint(double R, double G, double B, double A)
        {
            this.Color = new Color(R, G, B);
            this.A = A;
        }

        /// <summary>
        /// A completely transparent paint.
        /// </summary>
        public static readonly Paint Transparent = new Paint(0.0, 0.0, 0.0, 0.0);

        /// <summary>
        /// Constructs a completely opaque paint with the given color.
        /// </summary>
        public static Paint Opaque(Color Color)
        {
            return new Paint(Color, 1.0);
        }

        /// <summary>
        /// Constructs a completely opaque paint with the given color.
        /// </summary>
        public static Paint Opaque(double R, double G, double B)
        {
            return new Paint(R, G, B, 1.0);
        }

        public static implicit operator Paint(Color Color)
        {
            return new Paint(Color, 1.0);
        }

        public static implicit operator System.Drawing.Color(Paint Paint)
        {
            return System.Drawing.Color.FromArgb(
                (int)Math.Min(255.0, Paint.A * 256.0),
                (int)Math.Min(255.0, Paint.Color.R * 256.0),
                (int)Math.Min(255.0, Paint.Color.G * 256.0),
                (int)Math.Min(255.0, Paint.Color.B * 256.0));
        }

        public static implicit operator Vector4(Paint Paint)
        {
            return new Vector4(
                (float)Paint.Color.R,
                (float)Paint.Color.G,
                (float)Paint.Color.B,
                (float)Paint.A);
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}, {2}, {3})", this.Color.R, this.Color.G, this.Color.B, this.A);
        }
    }

    /// <summary>
    /// A linear transform that maps paints to other paints.
    /// </summary>
    public struct PaintTransform
    {
        public Paint Offset;
        public Paint R;
        public Paint G;
        public Paint B;
        public Paint A;

        [DefaultSerializer]
        public static readonly Serializer<PaintTransform> Serializer = Data.Serializer.Struct<PaintTransform>();
        public PaintTransform(Paint Offset, Paint R, Paint G, Paint B, Paint A)
        {
            this.Offset = Offset;
            this.R = R;
            this.G = G;
            this.B = B;
            this.A = A;
        }

        public PaintTransform(Paint R, Paint G, Paint B)
            : this(Paint.Transparent, R, G, B, Paint.Transparent)
        { }

        /// <summary>
        /// Constructs a paint transform which scales and offsets colors to be within the given range.
        /// </summary>
        public static PaintTransform Between(Color Min, Color Max)
        {
            return new PaintTransform(
                new Paint(Min, 0.0),
                new Paint(Max.R - Min.R, 0.0, 0.0, 0.0),
                new Paint(0.0, Max.G - Min.G, 0.0, 0.0),
                new Paint(0.0, 0.0, Max.B - Min.B, 0.0),
                new Paint(0.0, 0.0, 0.0, 1.0));
        }

        /// <summary>
        /// Constructs a paint transform which scales and offsets colors to be within the given range.
        /// </summary>
        public static PaintTransform Between(double Min, double Max)
        {
            return Between(
                new Color(Min, Min, Min),
                new Color(Max, Max, Max));
        }

        public static implicit operator System.Drawing.Imaging.ColorMatrix(PaintTransform Transform)
        {
            var mat = new System.Drawing.Imaging.ColorMatrix();
            mat.Matrix00 = (float)Transform.R.Color.R;
            mat.Matrix01 = (float)Transform.R.Color.G;
            mat.Matrix02 = (float)Transform.R.Color.B;
            mat.Matrix03 = (float)Transform.R.A;
            mat.Matrix10 = (float)Transform.G.Color.R;
            mat.Matrix11 = (float)Transform.G.Color.G;
            mat.Matrix12 = (float)Transform.G.Color.B;
            mat.Matrix13 = (float)Transform.G.A;
            mat.Matrix20 = (float)Transform.B.Color.R;
            mat.Matrix21 = (float)Transform.B.Color.G;
            mat.Matrix22 = (float)Transform.B.Color.B;
            mat.Matrix23 = (float)Transform.B.A;
            mat.Matrix30 = (float)Transform.A.Color.R;
            mat.Matrix31 = (float)Transform.A.Color.G;
            mat.Matrix32 = (float)Transform.A.Color.B;
            mat.Matrix33 = (float)Transform.A.A;
            mat.Matrix40 = (float)Transform.Offset.Color.R;
            mat.Matrix41 = (float)Transform.Offset.Color.G;
            mat.Matrix42 = (float)Transform.Offset.Color.B;
            mat.Matrix43 = (float)Transform.Offset.A;
            return mat;
        }

        public static explicit operator Matrix4(PaintTransform Transform)
        {
            return new Matrix4(Transform.R, Transform.G, Transform.B, Transform.A);
        }
    }
}
