﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Size = System.Drawing.Size;
using SystemGraphics = System.Drawing.Graphics;

using BattleSwaps.Geometry;

namespace BattleSwaps.Graphics
{
    /// <summary>
    /// Describes a font for drawing text. Fonts incude information such as the appearance, weight and spacing
    /// of glyphs. They do not include color information, nor absolute size.
    /// </summary>
    public abstract class Font
    {
        /// <summary>
        /// Creates a new atlas for this font, containing glyphs for at least the given characters which have the
        /// given size.
        /// </summary>
        /// <param name="EmHeight">The height, in pixels, of the em-box of the font. This is always at least as
        /// height as the tallest glyph.</param>
        public abstract FontAtlas MakeAtlas(IEnumerable<char> Characters, float EmHeight);

        /// <summary>
        /// Constructs a font from an installed system font.
        /// </summary>
        public static Font FromSystem(string Family)
        {
            return new SystemFont(new FontFamily(Family), FontStyle.Regular);
        }
    }

    /// <summary>
    /// An atlas image containing glyphs for a font, along with associated layout and spacing information. All
    /// coordinates within a font atlas are in pixel space.
    /// </summary>
    public sealed class FontAtlas : IDisposable
    {
        public FontAtlas(Bitmap Bitmap, Dictionary<char, Glyph> Glyphs)
        {
            this.Bitmap = Bitmap;
            this.Glyphs = Glyphs;
        }

        /// <summary>
        /// The bitmap containing all the glyphs for this font. The glyphs will be white on a black background, with
        /// possible red and blue for subpixel anti-aliasing.
        /// </summary>
        public readonly Bitmap Bitmap;

        /// <summary>
        /// A mapping of characters to their associated glyph in the font.
        /// </summary>
        public readonly Dictionary<char, Glyph> Glyphs;

        /// <summary>
        /// Describes a glyph in a font.
        /// </summary>
        public struct Glyph
        {
            /// <summary>
            /// The bounds of the glyph in the font atlas.
            /// </summary>
            public ImageBounds Bounds;

            /// <summary>
            /// The vertical offset of the glyph from the top of the text.
            /// </summary>
            public uint Ascent;

            /// <summary>
            /// The layout width of this glyph. This is how much space it takes up.
            /// </summary>
            public uint Width;

            /// <summary>
            /// The horizontal offset of the glyph within its layout box.
            /// </summary>
            public uint Offset;
        }

        /// <summary>
        /// A quad produced by rendering a glyph with a font atlas.
        /// </summary>
        public struct Quad
        {
            /// <summary>
            /// The bounds of source image for this quad in the bitmap.
            /// </summary>
            public ImageBounds Source;

            /// <summary>
            /// The offset of this quad in view space.
            /// </summary>
            public ImagePoint Offset;

            /// <summary>
            /// The bounds of this quad in view space.
            /// </summary>
            public ImageBounds Dest
            {
                get
                {
                    return new ImageBounds(this.Offset, this.Source.Size);
                }
            }
        }

        /// <summary>
        /// Gets the width of the given text when drawn using this font.
        /// </summary>
        public uint GetWidth(string Text)
        {
            uint width = 0;
            foreach (var ch in Text)
            {
                if (ch != ' ')
                {
                    Glyph glyph = this.Glyphs[ch];
                    width += glyph.Width;
                }
            }
            return width;
        }

        /// <summary>
        /// Typesets the given text starting at the origin, returning quads for all glyphs in the text.
        /// </summary>
        public IEnumerable<Quad> Typeset(string Text)
        {
            // TODO: alignment, break on words
            uint x = 0;
            foreach (var ch in Text)
            {
                if (ch != ' ')
                {
                    Glyph glyph = this.Glyphs[ch];
                    yield return new Quad
                    {
                        Source = glyph.Bounds,
                        Offset = new ImagePoint(x + glyph.Offset, glyph.Ascent)
                    };
                    x += glyph.Width;
                }
            }
        }

        /// <summary>
        /// An alternative representation of glyphs used for construction.
        /// </summary>
        public struct SourceGlyph
        {
            /// <summary>
            /// The character this glyph is for.
            /// </summary>
            public char Character;

            /// <summary>
            /// The bitmap containing the glyph.
            /// </summary>
            public Bitmap Bitmap;

            /// <summary>
            /// The tightest bounds on the bitmap that fully contains the glyph.
            /// </summary>
            public ImageBounds Bounds;

            /// <summary>
            /// The vertical offset of the glyph from the top of the text.
            /// </summary>
            public uint Ascent;

            /// <summary>
            /// The layout width of this glyph. This is how much space it takes up.
            /// </summary>
            public uint Width;

            /// <summary>
            /// The horizontal offset of the glyph within its layout box.
            /// </summary>
            public uint Offset;
        }

        /// <summary>
        /// Creates a source glyph for a character.
        /// </summary>
        private static SourceGlyph _CreateSourceGlyph(
            System.Drawing.Graphics MeasureGraphics,
            System.Drawing.Font Font,
            StringFormat Format,
            char Character)
        {
            // Initial measurements before drawing
            string str = Character.ToString();
            SizeF sizef = MeasureGraphics.MeasureString(str, Font, new PointF(0.0f, 0.0f), Format);
            Size size = new Size((int)Math.Ceiling(sizef.Width), (int)Math.Ceiling(sizef.Height));
            Format.SetMeasurableCharacterRanges(new[] { new CharacterRange(0, 1) });
            Region[] rgs = MeasureGraphics.MeasureCharacterRanges(str, Font,
                new RectangleF(0.0f, 0.0f, sizef.Width, sizef.Height), Format);
            RectangleF crg = rgs[0].GetBounds(MeasureGraphics);

            // Draw character
            Bitmap bitmap = new Bitmap(size.Width, size.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            using (var g = SystemGraphics.FromImage(bitmap))
            {
                g.Clear(System.Drawing.Color.Black);
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                g.DrawString(str, Font, Brushes.White, 0.0f, 0.0f, Format);
            }

            // Character bounds
            ImageBounds bounds = _GetBounds(bitmap);

            // Output glyph
            return new SourceGlyph
            {
                Character = Character,
                Bitmap = bitmap,
                Bounds = bounds,
                Ascent = bounds.Min.Y,
                Width = (uint)crg.Width,
                Offset = (uint)(bounds.Min.X - crg.X)
            };
        }

        /// <summary>
        /// Measures the bounds of the non-empty region in a 24bbp bitmap.
        /// </summary>
        private static unsafe ImageBounds _GetBounds(Bitmap Bitmap)
        {
            System.Drawing.Imaging.BitmapData bd = Bitmap.LockBits(
                    new System.Drawing.Rectangle(0, 0, Bitmap.Width, Bitmap.Height),
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            byte* ptr = (Byte*)bd.Scan0.ToPointer();

            int minX, maxX, minY, maxY;
            int w = bd.Width;
            int h = bd.Height;
            int s = bd.Stride;

            // Left to right scan
            for (minX = 0; minX < w; minX++)
            {
                for (int y = 0; y < h; y++)
                {
                    if (ptr[(minX * 3) + y * s] > 0)
                    {
                        goto ltr_end;
                    }
                }
            }
        ltr_end:

            // Right to left scan
            for (maxX = w - 1; maxX > minX; maxX--)
            {
                for (int y = 0; y < h; y++)
                {
                    if (ptr[(maxX * 3) + y * s] > 0)
                    {
                        goto rtl_end;
                    }
                }
            }
        rtl_end:

            // Top to bottom scan
            for (minY = 0; minY < h; minY++)
            {
                for (int x = 0; x < w; x++)
                {
                    if (ptr[(x * 3) + minY * s] > 0)
                    {
                        goto ttb_end;
                    }
                }
            }
        ttb_end:

            // Bottom to top scan
            for (maxY = h - 1; maxY > minY; maxY--)
            {
                for (int x = 0; x < w; x++)
                {
                    if (ptr[(x * 3) + maxY * s] > 0)
                    {
                        goto btt_end;
                    }
                }
            }
        btt_end:

            Bitmap.UnlockBits(bd);
            return new ImageBounds((uint)minX, (uint)maxX, (uint)minY, (uint)maxY);
        }

        /// <summary>
        /// Tries constructing a font containing all of the given source glyphs in an image of the given size.
        /// Returns false if they can't fit.
        /// </summary>
        public static bool TryFromSourceGlyphs(
            IEnumerable<SourceGlyph> Glyphs,
            ImageSize Size, out FontAtlas Font)
        {
            Bitmap atlas = new Bitmap(
                (int)Size.Width, (int)Size.Height,
                System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Dictionary<char, Glyph> glyphMap = new Dictionary<char, Glyph>();

            // Sort glyphs by height for increased packing efficency
            Glyphs = Glyphs.OrderBy(glyph => glyph.Bounds.Height);

            using (SystemGraphics g = SystemGraphics.FromImage(atlas))
            {
                // Begin placing glyphs in rows on the bitmap
                uint curY = 0;
                uint nextY = 0;
                uint nextX = 0;
                foreach (SourceGlyph gly in Glyphs)
                {
                    uint glyX = nextX;
                    uint glyY = curY;

                    nextX = glyX + gly.Bounds.Width;
                    nextY = Math.Max(glyY + gly.Bounds.Height, nextY);
                    if (nextX >= atlas.Width)
                    {
                        glyX = 0;
                        curY = glyY = nextY;
                        nextX = gly.Bounds.Width;
                        nextY = glyY + gly.Bounds.Height;
                    }
                    if (nextY >= atlas.Height)
                    {
                        // Nope :(
                        Font = null;
                        return false;
                    }

                    g.SetClip(new System.Drawing.Rectangle(
                        (int)glyX, (int)glyY,
                        (int)gly.Bounds.Width, (int)gly.Bounds.Height));
                    g.DrawImageUnscaled(gly.Bitmap, (int)(glyX - gly.Bounds.Min.X), (int)(glyY - gly.Bounds.Min.Y));
                    glyphMap.Add(gly.Character, new Glyph
                    {
                        Bounds = new ImageBounds(glyX, glyY, gly.Bounds.Size),
                        Ascent = gly.Ascent,
                        Width = gly.Width,
                        Offset = gly.Offset
                    });
                }
            }

            // Finalize font.
            Font = new FontAtlas(atlas, glyphMap);
            return true;
        }

        /// <summary>
        /// Constructs a font containing all of the given source glyphs.
        /// </summary>
        public static FontAtlas FromSourceGlyphs(IEnumerable<SourceGlyph> Glyphs)
        {
            uint size = 128;
            while (true)
            {
                FontAtlas font;
                if (TryFromSourceGlyphs(Glyphs, new ImageSize(size, size), out font))
                    return font;
                size *= 2;
            }
        }

        /// <summary>
        /// Constructs a font from an installed system font using GDI+.
        /// </summary>
        public static FontAtlas FromSystemFont(IEnumerable<char> Characters, System.Drawing.Font Font)
        {
            using (StringFormat sf = new StringFormat())
            {
                sf.Trimming = StringTrimming.None;
                sf.FormatFlags |= StringFormatFlags.MeasureTrailingSpaces;
                sf.FormatFlags |= StringFormatFlags.FitBlackBox;
                List<SourceGlyph> glyphs = new List<SourceGlyph>();
                using (Bitmap dummy = new Bitmap(1, 1))
                {
                    using (SystemGraphics measureGraphics = SystemGraphics.FromImage(dummy))
                    {
                        foreach (char ch in Characters)
                            glyphs.Add(_CreateSourceGlyph(measureGraphics, Font, sf, ch));
                    }
                }
                
                FontAtlas font = FromSourceGlyphs(glyphs);
                foreach (var glyph in glyphs)
                    glyph.Bitmap.Dispose();
                return font;
            }
        }

        /// <summary>
        /// The set of all printable ASCII characters.
        /// </summary>
        public static IEnumerable<char> ASCII
        {
            get
            {
                for (short t = 33; t <= 126; t++)
                {
                    yield return (char)t;
                }
            }
        }

        /// <summary>
        /// Constructs a font from an installed system font using GDI+.
        /// </summary>
        public static FontAtlas FromSystemFont(System.Drawing.Font Font)
        {
            return FromSystemFont(ASCII, Font);
        }

        /// <summary>
        /// Disposes the bitmap associated with this font atlas.
        /// </summary>
        public void Dispose()
        {
            this.Bitmap.Dispose();
        }
    }

    /// <summary>
    /// A font that is constructed from an installed system font.
    /// </summary>
    public sealed class SystemFont : Font
    {
        public SystemFont(FontFamily Family, FontStyle Style)
        {
            this.Family = Family;
            this.Style = Style;
        }

        /// <summary>
        /// The font family defining the general appearance of glyphs.
        /// </summary>
        public readonly FontFamily Family;

        /// <summary>
        /// The style of the font.
        /// </summary>
        public readonly FontStyle Style;

        public override FontAtlas MakeAtlas(IEnumerable<char> Characters, float EmHeight)
        {
            using (var font = new System.Drawing.Font(this.Family, EmHeight, this.Style, GraphicsUnit.Pixel))
            {
                return FontAtlas.FromSystemFont(Characters, font);
            }
        }
    }
}
