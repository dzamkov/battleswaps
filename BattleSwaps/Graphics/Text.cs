﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace BattleSwaps.Graphics
{
    /// <summary>
    /// A display for text.
    /// </summary>
    public class TextDisplay : Display
    {
        public TextDisplay(Font Font, float EmHeight, Paint Paint, string Text)
        {
            this.Font = Font;
            this.EmHeight = EmHeight;
            this.Paint = Paint;
            this.Text = Text;
        }

        /// <summary>
        /// The font to use for text.
        /// </summary>
        public readonly Font Font;

        /// <summary>
        /// The em-height of the text.
        /// </summary>
        public readonly float EmHeight;

        /// <summary>
        /// The paint used to display the text.
        /// </summary>
        public readonly Paint Paint;

        /// <summary>
        /// The text to be displayed.
        /// </summary>
        public readonly new string Text;

        /// <summary>
        /// The resource for the shader used to render this display.
        /// </summary>
        public static Resource<Render.Program> Program =
            Render.Program.Load("Shader/Display/Text.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(
                    new AttributeBinding("uv", 0),
                    new AttributeBinding("pos", 1)));

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            using (FontAtlas atlas = this.Font.MakeAtlas(FontAtlas.ASCII, EmHeight))
            {
                // TODO: Share atlas texture if possible
                var texture = Texture.CreateFromBitmap(TextureFormat.Color, atlas.Bitmap);
                Texture.SetSampling(TextureTarget.Texture2D, TextureSampling.Pixel);

                // Create mesh
                List<uint> inds = new List<uint>();
                List<Vector2> uv = new List<Vector2>();
                List<Vector2> pos = new List<Vector2>();
                ImageBounds atlasBounds = new ImageBounds(atlas.Bitmap.Size);
                foreach (FontAtlas.Quad quad in atlas.Typeset(this.Text))
                {
                    MeshData.OutputQuadIndices(inds, (uint)pos.Count);
                    MeshData.OutputQuadVertices(uv, atlasBounds.Relative(quad.Source));
                    MeshData.OutputQuadVertices(pos, quad.Dest);
                }
                Render.Mesh mesh = Render.Mesh.CreateFromData(inds.ToArray(),
                    MeshAttributeData.FromArray(uv.ToArray()),
                    MeshAttributeData.FromArray(pos.ToArray()));

                // Draw
                Renderable.Add(new Primitive
                {
                    Program = Program,
                    Mesh = mesh,
                    Type = PrimitiveType.Triangles,
                    Uniforms = ImmutableSet.From(
                         Uniform.Texture2D(texture).BindTo("atlasTexture"),
                        Uniform.IVec2(Context.ViewportSize).BindTo("viewSize"),
                        Uniform.IVec2(Context.Offset).BindTo("viewOffset"),
                        Uniform.Vec4(this.Paint).BindTo("paint")),
                    Resources = ImmutableSet.From<IDisposable>(texture, mesh)
                });
            }
        }
    }
}
