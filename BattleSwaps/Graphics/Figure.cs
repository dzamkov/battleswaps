﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Diagnostics;
using System.Linq;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace BattleSwaps.Graphics
{
    /// <summary>
    /// Describes a dynamic visual object in an unbounded continuous 2D space. Figures may be view-dependent,
    /// changing appearance depending on the projection to view space.
    /// </summary>
    public abstract class Figure
    {
        /// <summary>
        /// Describes a context in which a figure can be drawn.
        /// </summary>
        public struct Context
        {
            /// <summary>
            /// Provides the current reactive moment to read from.
            /// </summary>
            public Dynamic<Moment> Moment;

            /// <summary>
            /// Provides the transform from figure space to normalized device space.
            /// </summary>
            public Dynamic<Transform> FigureToDevice;

            /// <summary>
            /// Gives the size of the viewport.
            /// </summary>
            public Dynamic<ImageSize> ViewportSize;
        }

        /// <summary>
        /// Adds the effects needed to render this figure to the given renderable.
        /// </summary>
        public abstract void Instantiate(Renderable Renderable, Context Context);

        /// <summary>
        /// Indicates whether this is the empty figure.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this == Empty;
            }
        }

        /// <summary>
        /// A figure that is completely transparent.
        /// </summary>
        public static readonly Figure Empty = new UnionFigure(Bag.Empty<Figure>());

        /// <summary>
        /// Constructs a figure that displays a static image.
        /// </summary>
        public static Figure FromImage(Image Image, Bounds Bounds, Signal<PaintTransform?> Transform)
        {
            return new ImageFigure(Image, Bounds, Transform);
        }

        /// <summary>
        /// Constructs a figure that displays a static image.
        /// </summary>
        public static Figure FromImage(Image Image, Bounds Bounds)
        {
            return new ImageFigure(Image, Bounds, Signal.Static<PaintTransform?>(null));
        }

        /// <summary>
        /// Constructs a figure that displays a static image.
        /// </summary>
        public static Figure FromImage(Image Image, Signal<PaintTransform?> Transform)
        {
            return FromImage(Image, Bounds.Unit, Transform);
        }

        /// <summary>
        /// Constructs a figure that displays a static image.
        /// </summary>
        public static Figure FromImage(Image Image)
        {
            return FromImage(Image, Signal.Static<PaintTransform?>(null));
        }

        /// <summary>
        /// Constructs a figure that displays a static image.
        /// </summary>
        public static Figure FromImage(Image Image, PaintTransform Transform)
        {
            return FromImage(Image, Signal.Static<PaintTransform?>(Transform));
        }

        /// <summary>
        /// Overlays a list of figures on top of each other.
        /// </summary>
        public static Figure Overlay(IEnumerable<Figure> Figures)
        {
            List<Figure> sources = new List<Figure>();
            foreach (Figure figure in Figures)
            {
                if (figure == Empty)
                    continue;

                OverlayFigure overlayFigure = figure as OverlayFigure;
                if (overlayFigure != null)
                {
                    sources.AddRange(overlayFigure.Sources);
                    continue;
                }

                sources.Add(figure);
            }
            if (sources.Count == 0)
                return Empty;
            else if (sources.Count == 1)
                return sources[0];
            else return new OverlayFigure(sources.ToArray());
        }

        /// <summary>
        /// Overlays an array of figures on top of each other.
        /// </summary>
        public static Figure Overlay(params Figure[] Figures)
        {
            return Overlay((IEnumerable<Figure>)Figures);
        }

        /// <summary>
        /// Constructs a figure that overlays all of the given figures in an undefined order. This should be used for
        /// figures that don't overlap, or where overlap resolution doesn't matter.
        /// </summary>
        public static Figure Union(Bag<Figure> Figures)
        {
            if (Figures.IsEmpty)
                return Empty;
            return new UnionFigure(Figures);
        }

        /// <summary>
        /// Constructs a figure in one reactive system (the "external" system) that takes from a figure in another
        /// reactive system (the "internal" system) based on a bridge.
        /// </summary>
        public static Figure InverseApplyBridge(Bridge Bridge, Figure Inner)
        {
            if (Inner.IsEmpty)
                return Empty;
            return new BridgeFigure(Bridge, Inner);
        }

        public static Figure operator *(Signal<Transform> Transform, Figure Figure)
        {
            if (Figure.IsEmpty)
                return Empty;
            return new TransformFigure(Figure, Transform);
        }

        public static Figure operator *(Transform Transform, Figure Figure)
        {
            return Signal.Static(Transform) * Figure;
        }

        public static Figure operator +(Figure Below, Figure Above)
        {
            return Overlay(Below, Above);
        }

        public static Figure operator |(Figure A, Figure B)
        {
            if (A.IsEmpty) return B;
            if (B.IsEmpty) return A;

            UnionFigure aUnion = A as UnionFigure;
            UnionFigure bUnion = B as UnionFigure;
            if (aUnion != null && bUnion != null)
                return new UnionFigure(aUnion.Sources + bUnion.Sources);
            else if (aUnion != null)
                return new UnionFigure(aUnion.Sources.Cons(B));
            else if (bUnion != null)
                return new UnionFigure(bUnion.Sources.Cons(A));
            else
                return new UnionFigure(Bag.Static(A, B));
        }
    }

    /// <summary>
    /// Contains extension functions related to figures.
    /// </summary>
    public static class FigureEx
    {
        /// <summary>
        /// Constructs a figure in one reactive system (the "external" system) that takes from a figure in another
        /// reactive system (the "internal" system) based on a bridge.
        /// </summary>
        public static Figure InverseApply(this Bridge Bridge, Figure Inner)
        {
            return Figure.InverseApplyBridge(Bridge, Inner);
        }
    }

    /// <summary>
    /// A figure that overlays several source figures in an undefined order.
    /// </summary>
    public sealed class UnionFigure : Figure
    {
        public UnionFigure(Bag<Figure> Sources)
        {
            this.Sources = Sources;
        }

        /// <summary>
        /// The source figures for this figure.
        /// </summary>
        public readonly Bag<Figure> Sources;

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            Renderable.Loop(Context.Moment, this.Sources, (f, r) => f.Instantiate(r, Context));
        }
    }

    /// <summary>
    /// A figure that draws a image.
    /// </summary>
    public sealed class ImageFigure : Figure
    {
        public ImageFigure(Image Image, Bounds Bounds, Signal<PaintTransform?> PaintTransform)
        {
            this.Image = Image;
            this.Bounds = Bounds;
            this.PaintTransform = PaintTransform;
        }

        /// <summary>
        /// The image this figure draws.
        /// </summary>
        public readonly Image Image;

        /// <summary>
        /// The portion of the image to draw, where the image occupies the bounds ([0, 1], [0, 1]).
        /// If this is larger than the image, the image will be tiled.
        /// </summary>
        public readonly Bounds Bounds;

        /// <summary>
        /// The paint transform applied to the image.
        /// </summary>
        public readonly Signal<PaintTransform?> PaintTransform;

        /// <summary>
        /// The resource for the shader program used by this figure.
        /// </summary>
        public static readonly Resource<Render.Program> Program =
            Render.Program.Load("Shader/ImageFigure.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(new AttributeBinding("pos", 0)));

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            var texture = this.Image.ToTexture(TextureFormat.Paint, TextureSampling.SmoothNoWrap).Use();
            Transform uvTransform = Geometry.Transform.Between(Bounds.Unit, this.Bounds);
            Dynamic<Transform> posTransform = Context.FigureToDevice.Map(t => t * uvTransform);
            Dynamic<Matrix4> colorTransform = Dynamic.FromSignal(Context.Moment, this.PaintTransform)
                .Map(t => t.HasValue ? (Matrix4)t.Value : Matrix4.Identity);
            Dynamic<Vector4> colorOffset = Dynamic.FromSignal(Context.Moment, this.PaintTransform)
                .Map(t => t.HasValue ? (Vector4)t.Value.Offset : Vector4.Zero);

            Renderable.Add(new Primitive
            {
                IsUnitQuad = true,
                Program = Program,
                Uniforms = ImmutableSet.From(
                    Uniform.Mat3(uvTransform).BindTo("uvTransform"),
                    Uniform.Mat3(posTransform).BindTo("posTransform"),
                    Uniform.Texture2D(texture).BindTo("texture"),
                    Uniform.Mat4(colorTransform).BindTo("colorTransform"),
                    Uniform.Vec4(colorOffset).BindTo("colorOffset")),
                Resources = ImmutableSet.From<IDisposable>(texture)
            });
        }
    }

    /// <summary>
    /// A figure that applies a transform to a source figure.
    /// </summary>
    public sealed class TransformFigure : Figure
    {
        public TransformFigure(Figure Source, Signal<Transform> Transform)
        {
            this.Source = Source;
            this.Transform = Transform;
        }

        /// <summary>
        /// The source figure to transform.
        /// </summary>
        public readonly Figure Source;

        /// <summary>
        /// The transform applied by this figure.
        /// </summary>
        public readonly Signal<Transform> Transform;

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            Context.FigureToDevice = Dynamic.Lift(
                Context.FigureToDevice, Context.Moment.Read(this.Transform),
                (a, b) => a * b);
            this.Source.Instantiate(Renderable, Context);
        }
    }

    /// <summary>
    /// A figure that overlays several source figures.
    /// </summary>
    public sealed class OverlayFigure : Figure
    {
        public OverlayFigure(Figure[] Sources)
        {
            this.Sources = Sources;
        }

        /// <summary>
        /// The source figures, in draw order.
        /// </summary>
        public readonly Figure[] Sources;

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            foreach (var source in this.Sources)
                source.Instantiate(Renderable, Context);
        }
    }

    /// <summary>
    /// A figure which displays a figure from a different reactive system.
    /// </summary>
    public sealed class BridgeFigure : Figure
    {
        public BridgeFigure(Bridge Bridge, Figure Inner)
        {
            this.Bridge = Bridge;
            this.Inner = Inner;
        }

        /// <summary>
        /// The bridge between the externally-visible reactive system and the internal system.
        /// </summary>
        public readonly Bridge Bridge;

        /// <summary>
        /// The source figure in the internal system to be displayed.
        /// </summary>
        public readonly Figure Inner;

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            Context.Moment = Context.Moment.Map(e => this.Bridge.Apply(e.State));
            this.Inner.Instantiate(Renderable, Context);
        }
    }
}
