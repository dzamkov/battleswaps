﻿using System;
using System.Collections.Generic;

using OpenTK;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

namespace BattleSwaps.Graphics
{
    /// <summary>
    /// A figure that displays a selection marker around a rectangle.
    /// </summary>
    public sealed class MarkerFigure : Figure
    {
        public MarkerFigure(Signal<Paint> Paint, Signal<double> Size, Signal<Rectangle> Rectangle)
        {
            this.Paint = Paint;
            this.Size = Size;
            this.Rectangle = Rectangle;
        }

        /// <summary>
        /// The paint used to display the marker.
        /// </summary>
        public Signal<Paint> Paint;

        /// <summary>
        /// The size of the ends of the marker.
        /// </summary>
        public Signal<double> Size;

        /// <summary>
        /// The rectangle that this marker is around.
        /// </summary>
        public Signal<Rectangle> Rectangle;

        /// <summary>
        /// The resource for the shader program used by this marker.
        /// </summary>
        public static readonly Resource<Render.Program> Program =
            Render.Program.Load("Shader/Figure/Marker.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(
                    new AttributeBinding("pos", 0),
                    new AttributeBinding("dir", 1)));

        /// <summary>
        /// A mesh resource for a marker.
        /// </summary>
        [Tag(typeof(Resource<Render.Mesh>), (uint)790896077)]
        public static readonly Resource<Render.Mesh> Mesh = new Render.Mesh.StaticResource(new MeshData
            {
                Indices = new uint[] { 0, 1, 1, 2, 3, 4, 4, 5, 6, 7, 7, 8, 9, 10, 10, 11 },
                Attributes = new MeshAttributeData[]
                {
                    MeshAttributeData.FromArray(
                        new Vector2(0.0f, 0.0f),
                        new Vector2(0.0f, 0.0f),
                        new Vector2(0.0f, 0.0f),
                        new Vector2(1.0f, 0.0f),
                        new Vector2(1.0f, 0.0f),
                        new Vector2(1.0f, 0.0f),
                        new Vector2(1.0f, 1.0f),
                        new Vector2(1.0f, 1.0f),
                        new Vector2(1.0f, 1.0f),
                        new Vector2(0.0f, 1.0f),
                        new Vector2(0.0f, 1.0f),
                        new Vector2(0.0f, 1.0f)),
                    MeshAttributeData.FromArray(
                        new Vector2(1.0f, 0.0f),
                        new Vector2(0.0f, 0.0f),
                        new Vector2(0.0f, 1.0f),
                        new Vector2(0.0f, 1.0f),
                        new Vector2(0.0f, 0.0f),
                        new Vector2(-1.0f, 0.0f),
                        new Vector2(-1.0f, 0.0f),
                        new Vector2(0.0f, 0.0f),
                        new Vector2(0.0f, -1.0f),
                        new Vector2(0.0f, -1.0f),
                        new Vector2(0.0f, 0.0f),
                        new Vector2(1.0f, 0.0f))
                }
            });

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            Renderable.Add(new Primitive
            {
                Program = Program,
                Mesh = Mesh,
                Type = OpenTK.Graphics.OpenGL.PrimitiveType.Lines,
                Uniforms = ImmutableSet.From(
                    Uniform.Mat3(Context.FigureToDevice).BindTo("figureToDevice"),
                    Uniform.Mat3(Context.Moment.Read(this.Rectangle.Map(r => r.Transform))).BindTo("transform"),
                    Uniform.Float(Context.Moment.Read(this.Size)).BindTo("size"),
                    Uniform.Vec4(Context.Moment.Read(this.Paint)).BindTo("color")),
                Resources = ImmutableSet<IDisposable>.Empty
            });
        }
    }
}
