﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

namespace BattleSwaps.Graphics
{
    /// <summary>
    /// Describes a dynamic visual object in an unbounded discrete 2D space. This is similar to a figure, but displays
    /// are not view-dependent and use integer coordinates corresponding directly to pixels.
    /// </summary>
    public abstract class Display
    {
        /// <summary>
        /// Describes a context in which a display can be drawn.
        /// </summary>
        public struct Context
        {
            /// <summary>
            /// Provides the current reactive moment to read from.
            /// </summary>
            public Dynamic<Moment> Moment;

            /// <summary>
            /// Gives the size of the viewport.
            /// </summary>
            public Dynamic<ImageSize> ViewportSize;

            /// <summary>
            /// Gives the offset of the display in the viewport.
            /// </summary>
            public Dynamic<ImagePoint> Offset;
        }

        /// <summary>
        /// Adds the effects needed to render this display to the given renderable.
        /// </summary>
        public abstract void Instantiate(Renderable Renderable, Context Context);

        /// <summary>
        /// Indicates whether this is the empty display.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this == Empty;
            }
        }

        /// <summary>
        /// A display that is completely transparent.
        /// </summary>
        public static readonly Display Empty = new UnionDisplay(Bag.Empty<Display>());

        /// <summary>
        /// Creates a display for a figure.
        /// </summary>
        public static Display FromFigure(Figure Figure)
        {
            return new FigureDisplay(Figure);
        }

        /// <summary>
        /// Creates a display for the given text.
        /// </summary>
        public static Display Text(Font Font, float EmHeight, Paint Paint, string Text)
        {
            return new TextDisplay(Font, EmHeight, Paint, Text);
        }

        /// <summary>
        /// Translates this display down and/or right.
        /// </summary>
        public Display Translate(Signal<ImagePoint> Offset)
        {
            return new TranslateDisplay(this, Offset);
        }

        /// <summary>
        /// Translates this display down and/or right.
        /// </summary>
        public Display Translate(uint X, uint Y)
        {
            if (this.IsEmpty) return Empty;
            return Translate(new ImagePoint(X, Y));
        }

        /// <summary>
        /// Constructs a display that overlays all of the given displays in an undefined order. This should be used for
        /// display that don't overlap, or where overlap resolution doesn't matter.
        /// </summary>
        public static Display Union(Bag<Display> Displays)
        {
            if (Displays.IsEmpty) return Empty;
            return new UnionDisplay(Displays);
        }

        /// <summary>
        /// Applies a dither effect to this display.
        /// </summary>
        public Display Dither(int Levels)
        {
            return new DitherDisplay(this, Levels);
        }

        public static Display operator +(Display Below, Display Above)
        {
            if (Below.IsEmpty) return Above;
            if (Above.IsEmpty) return Below;
            return new OverlayImage(Below, Above);
        }

        public static Display operator |(Display A, Display B)
        {
            if (A.IsEmpty) return B;
            if (B.IsEmpty) return A;

            UnionDisplay aUnion = A as UnionDisplay;
            UnionDisplay bUnion = B as UnionDisplay;
            if (aUnion != null && bUnion != null)
                return new UnionDisplay(aUnion.Sources + bUnion.Sources);
            else if (aUnion != null)
                return new UnionDisplay(aUnion.Sources.Cons(B));
            else if (bUnion != null)
                return new UnionDisplay(bUnion.Sources.Cons(A));
            else
                return new UnionDisplay(Bag.Static(A, B));
        }
    }

    /// <summary>
    /// A figure that draws several source figures in an undefined order.
    /// </summary>
    public sealed class UnionDisplay : Display
    {
        public UnionDisplay(Bag<Display> Sources)
        {
            this.Sources = Sources;
        }

        /// <summary>
        /// The source figures for this figure.
        /// </summary>
        public readonly Bag<Display> Sources;

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            Renderable.Loop(Context.Moment, this.Sources, (f, r) => f.Instantiate(r, Context));
        }
    }

    /// <summary>
    /// A display which translates a source display down and/or right.
    /// </summary>
    public sealed class TranslateDisplay : Display
    {
        public TranslateDisplay(Display Source, Signal<ImagePoint> Offset)
        {
            this.Source = Source;
            this.Offset = Offset;
        }

        /// <summary>
        /// The display to be translated.
        /// </summary>
        public readonly Display Source;

        /// <summary>
        /// The offset of the source display within this display.
        /// </summary>
        public readonly Signal<ImagePoint> Offset;

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            Context.Offset = Dynamic.Lift(Context.Moment.Read(this.Offset), Context.Offset, (a, b) => a + b);
            this.Source.Instantiate(Renderable, Context);
        }
    }

    /// <summary>
    /// A display which overlays one display atop another, respecting transparency information.
    /// </summary>
    public sealed class OverlayImage : Display
    {
        public OverlayImage(Display Below, Display Above)
        {
            this.Below = Below;
            this.Above = Above;
        }

        /// <summary>
        /// The display to be drawn first.
        /// </summary>
        public Display Below;

        /// <summary>
        /// The display to be drawn second.
        /// </summary>
        public Display Above;

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            this.Below.Instantiate(Renderable, Context);
            this.Above.Instantiate(Renderable, Context);
        }
    }
    
    /// <summary>
    /// A display that draws a figure using a direct mapping from pixel coordinates to corresponding points on the
    /// figure.
    /// </summary>
    public sealed class FigureDisplay : Display
    {
        public FigureDisplay(Figure Figure)
        {
            this.Figure = Figure;
        }

        /// <summary>
        /// The figure being drawn.
        /// </summary>
        public readonly Figure Figure;

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            this.Figure.Instantiate(Renderable, new Figure.Context
            {
                Moment = Context.Moment,
                FigureToDevice = Dynamic.Lift(Context.Offset, Context.ViewportSize,
                    (o, s) => Transform.Between(new ImageBounds(o, s), Geometry.Bounds.Device, false, true)),
                ViewportSize = Context.ViewportSize
            });
        }
    }

    /// <summary>
    /// A display that applies a dithering post-processing effect to a source display.
    /// </summary>
    public sealed class DitherDisplay : Display
    {
        public DitherDisplay(Display Source, int Levels)
        {
            this.Source = Source;
            this.Levels = Levels;
        }

        /// <summary>
        /// The source display to apply the effect to.
        /// </summary>
        public readonly Display Source;

        /// <summary>
        /// The number of color levels to dither to. The less this value, the lower quality the appearance.
        /// </summary>
        public readonly int Levels;

        /// <summary>
        /// The resource for the shader program used by this display.
        /// </summary>
        public static readonly Resource<Render.Program> Program =
            Render.Program.Load("Shader/Dither.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(new AttributeBinding("pos", 0)));

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            Routine preparation = Renderable.Prepare.Expand();
            Cursor prepare = preparation.End;

            Dynamic<ImageSize> textureSize;
            Dynamic<ImageBounds> contentBounds;
            Renderable innerRenderable;
            Texture texture = prepare.RenderToTexture(
                TextureFormat.Paint, DepthTarget.Disabled, BlendTarget.Standard, Clear.Transparent,
                Context.ViewportSize, out innerRenderable,
                out textureSize, out contentBounds);
            this.Source.Instantiate(innerRenderable, Context);

            Dynamic<Transform> uvTransform = Dynamic.Lift(
                textureSize, contentBounds,
                (s, b) => Geometry.Transform.Between(b.Relative(new ImageBounds(s)), Bounds.Unit));

            Renderable.Add(new Primitive
            {
                IsUnitQuad = true,
                Program = Program,
                Uniforms = ImmutableSet.From(
                    Uniform.Mat3(uvTransform).BindTo("uvTransform"),
                    Uniform.Texture2D(texture).BindTo("source"),
                    Uniform.Int(this.Levels).BindTo("levels")),
                Resources = ImmutableSet.From<IDisposable>(preparation)
            });
        }
    }
}
