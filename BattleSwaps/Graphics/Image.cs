﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;
using System.IO;

using BattleSwaps.Data;
using BattleSwaps.Geometry;

namespace BattleSwaps.Graphics
{
    /// <summary>
    /// Describes a static 2D array of pixels.
    /// </summary>
    public struct Image
    {
        [DefaultSerializer]
        public static readonly Serializer<Image> Serializer =
            Data.Serializer.Struct<Image>();
        public Image(Resource<Bitmap> Source)
        {
            this.Source = Source;
        }

        /// <summary>
        /// The bitmap resource that describes this image.
        /// </summary>
        public Resource<Bitmap> Source;

        /// <summary>
        /// A resource for a bitmap loaded from a blob.
        /// </summary>
        [Tag(typeof(Resource<Bitmap>), (uint)1358263808)]
        public sealed class LoadResource : Resource<Bitmap>
        {
            [DefaultSerializer]
            public static readonly Serializer<LoadResource> Serializer =
                Data.Serializer.Immutable<LoadResource>();
            public readonly Resource<Blob> Source;
            private LoadResource(Resource<Blob> Source)
            {
                this.Source = Source;
            }

            public static LoadResource Get(Resource<Blob> Source)
            {
                return (LoadResource)Resource.Coalesce(new LoadResource(Source));
            }

            protected override void Load()
            {
                using (var sourceUsage = this.Source.Use())
                {
                    sourceUsage.Wait().Use(stream => this.Provide(new Bitmap(stream)));
                }
            }
        }

        /// <summary>
        /// Loads an image from the external source at the given path.
        /// </summary>
        public static Image Load(AbsolutePath Path)
        {
            return new Image(LoadResource.Get(Blob.Load(Path)));
        }

        /// <summary>
        /// A resource for a transformed bitmap.
        /// </summary>
        [Tag(typeof(Resource<Bitmap>), (uint)1423771708)]
        public sealed class TransformResource : Resource<Bitmap>
        {
            [DefaultSerializer]
            public static readonly Serializer<TransformResource> Serializer =
                Data.Serializer.Immutable<TransformResource>();
            public readonly Resource<Bitmap> Source;
            public readonly PaintTransform Transform;
            private TransformResource(Resource<Bitmap> Source, PaintTransform Transform)
            {
                this.Source = Source;
                this.Transform = Transform;
            }

            public static TransformResource Get(Resource<Bitmap> Source, PaintTransform Transform)
            {
                return (TransformResource)Resource.Coalesce(new TransformResource(Source, Transform));
            }

            protected override void Load()
            {
                using (var sourceUsage = this.Source.Use())
                {
                    var sourceBitmap = sourceUsage.Wait();
                    Bitmap resultBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);
                    using (var g = System.Drawing.Graphics.FromImage(resultBitmap))
                    {
                        using (ImageAttributes attr = new ImageAttributes())
                        {
                            attr.SetColorMatrix(this.Transform);
                            g.DrawImage(sourceBitmap,
                                new System.Drawing.Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height),
                                0, 0, sourceBitmap.Width, sourceBitmap.Height,
                                GraphicsUnit.Pixel, attr);
                        }
                    }
                    this.Provide(resultBitmap);
                }
            }
        }

        /// <summary>
        /// Applies a paint transform to this image.
        /// </summary>
        public Image Transform(PaintTransform Transform)
        {
            return new Image(TransformResource.Get(this.Source, Transform));
        }

        /// <summary>
        /// A resource for an overlayed bitmap.
        /// </summary>
        [Tag(typeof(Resource<Bitmap>), (uint)1797506267)]
        public sealed class OverlayResource : Resource<Bitmap>
        {
            [DefaultSerializer]
            public static readonly Serializer<OverlayResource> Serializer =
                Data.Serializer.Immutable<OverlayResource>();
            public readonly Resource<Bitmap> Below;
            public readonly Resource<Bitmap> Above;
            private OverlayResource(Resource<Bitmap> Below, Resource<Bitmap> Above)
            {
                this.Below = Below;
                this.Above = Above;
            }

            public static OverlayResource Get(Resource<Bitmap> Below, Resource<Bitmap> Above)
            {
                return (OverlayResource)Resource.Coalesce(new OverlayResource(Below, Above));
            }

            protected override void Load()
            {
                using (var belowSourceUsage = this.Below.Use())
                using (var aboveSourceUsage = this.Above.Use())
                {
                    var belowBitmap = belowSourceUsage.Wait();
                    var aboveBitmap = aboveSourceUsage.Wait();
                    Debug.Assert(belowBitmap.Size == aboveBitmap.Size);
                    Bitmap resultBitmap = new Bitmap(belowBitmap.Width, belowBitmap.Height);
                    using (var g = System.Drawing.Graphics.FromImage(resultBitmap))
                    {
                        g.DrawImage(belowBitmap, 0, 0, resultBitmap.Width, resultBitmap.Height);
                        g.DrawImage(aboveBitmap, 0, 0, resultBitmap.Width, resultBitmap.Height);
                    }
                    this.Provide(resultBitmap);
                }
            }
        }

        /// <summary>
        /// Overlays one image on top of another, respecting transparency information. The images must be the
        /// same size.
        /// </summary>
        public static Image Overlay(Image Below, Image Above)
        {
            return new Image(OverlayResource.Get(Below.Source, Above.Source));
        }

        /// <summary>
        /// A resource for a composed bitmap.
        /// </summary>
        [Tag(typeof(Resource<Bitmap>), (uint)2450153477)]
        public sealed class ComposeResource : Resource<Bitmap>
        {
            [DefaultSerializer]
            public static readonly Serializer<ComposeResource> Serializer =
                Data.Serializer.Immutable<ComposeResource>();
            public readonly Resource<Bitmap> Map;
            public readonly Resource<Bitmap> Texture;
            private ComposeResource(Resource<Bitmap> Map, Resource<Bitmap> Texture)
            {
                this.Map = Map;
                this.Texture = Texture;
            }

            public static ComposeResource Get(Resource<Bitmap> Map, Resource<Bitmap> Texture)
            {
                return (ComposeResource)Resource.Coalesce(new ComposeResource(Map, Texture));
            }

            protected unsafe override void Load()
            {
                using (var mapSourceUsage = this.Map.Use())
                using (var textureSourceUsage = this.Texture.Use())
                {
                    var mapBitmap = mapSourceUsage.Wait();
                    var textureBitmap = textureSourceUsage.Wait();
                    Debug.Assert(textureBitmap.Width >= 256);
                    Debug.Assert(textureBitmap.Height >= 256);
                    Bitmap resultBitmap = new Bitmap(mapBitmap.Width, mapBitmap.Height);
                    var mapData = mapBitmap.LockBits(
                        new System.Drawing.Rectangle(0, 0, mapBitmap.Width, mapBitmap.Height),
                        ImageLockMode.ReadOnly,
                        PixelFormat.Format32bppArgb);

                    var textureData = textureBitmap.LockBits(
                        new System.Drawing.Rectangle(0, 0, textureBitmap.Width, textureBitmap.Height),
                        ImageLockMode.ReadOnly,
                        PixelFormat.Format32bppArgb);

                    var resultData = resultBitmap.LockBits(
                        new System.Drawing.Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height),
                        ImageLockMode.WriteOnly,
                        PixelFormat.Format32bppArgb);

                    for (int y = 0; y < mapBitmap.Height; y++)
                    {
                        byte* mapPixel = (byte*)mapData.Scan0.ToPointer() + y * mapData.Stride;
                        byte* resultPixel = (byte*)resultData.Scan0.ToPointer() + y * resultData.Stride;
                        for (int x = 0; x < mapBitmap.Width; x++)
                        {
                            byte a = mapPixel[3];
                            byte mx = mapPixel[2];
                            byte my = mapPixel[1];

                            int color = *(int*)(
                                (byte*)textureData.Scan0.ToPointer() +
                                my * textureData.Stride + mx * 4);
                            *(int*)resultPixel = color;

                            // Multiply alpha
                            resultPixel[3] = (byte)((int)resultPixel[3] * a / 255);

                            mapPixel += 4;
                            resultPixel += 4;
                        }
                    }

                    mapBitmap.UnlockBits(mapData);
                    textureBitmap.UnlockBits(textureData);
                    resultBitmap.UnlockBits(resultData);
                    this.Provide(resultBitmap);
                }
            }
        }

        /// <summary>
        /// Composes two images by using the (r, g) values of the pixels in the given map image to select
        /// corresponding pixels by (x, y) coordinate from the given texture image.
        /// </summary>
        public static Image Compose(Image Map, Image Texture)
        {
            return new Image(ComposeResource.Get(Map.Source, Texture.Source));
        }

        /// <summary>
        /// A resource for a clipped bitmap.
        /// </summary>
        [Tag(typeof(Resource<Bitmap>), (uint)3141001411)]
        public sealed class ClipResource : Resource<Bitmap>
        {
            [DefaultSerializer]
            public static readonly Serializer<ClipResource> Serializer =
                Data.Serializer.Immutable<ClipResource>();
            public readonly Resource<Bitmap> Source;
            public readonly ImageBounds Bounds;
            private ClipResource(Resource<Bitmap> Source, ImageBounds Bounds)
            {
                this.Source = Source;
                this.Bounds = Bounds;
            }

            public static ClipResource Get(Resource<Bitmap> Source, ImageBounds Bounds)
            {
                return (ClipResource)Resource.Coalesce(new ClipResource(Source, Bounds));
            }

            protected unsafe override void Load()
            {
                using (var sourceUsage = this.Source.Use())
                {
                    var sourceBitmap = sourceUsage.Wait();
                    Bitmap resultBitmap = new Bitmap((int)this.Bounds.Width, (int)this.Bounds.Height);
                    using (var g = System.Drawing.Graphics.FromImage(resultBitmap))
                    {
                        g.DrawImage(sourceBitmap,
                            -(int)this.Bounds.Min.X, -(int)this.Bounds.Min.Y,
                            sourceBitmap.Width, sourceBitmap.Height);
                    }
                    this.Provide(resultBitmap);
                }
            }
        }

        /// <summary>
        /// Gets a clipped portion of this image.
        /// </summary>
        public Image Clip(ImageBounds Bounds)
        {
            return new Image(ClipResource.Get(this.Source, Bounds));
        }

        public static Image operator +(Image Below, Image Above)
        {
            return Overlay(Below, Above);
        }

        public static Image operator *(PaintTransform Transform, Image Image)
        {
            return Image.Transform(Transform);
        }

        public static Image operator *(Image Map, Image Texture)
        {
            return Compose(Map, Texture);
        }
    }
}
