﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Geometry;
using BattleSwaps.Terrain;
using BattleSwaps.Graphics.Render;

namespace BattleSwaps.Graphics.Scene
{
    /// <summary>
    /// A solid which renders a region of terrain.
    /// </summary>
    public sealed class TerrainRegionSolid : Solid
    {
        public TerrainRegionSolid(Region Region)
        {
            this.Region = Region;
        }

        /// <summary>
        /// The region to be rendered.
        /// </summary>
        public readonly Region Region;

        /// <summary>
        /// Creates the mesh for this solid.
        /// </summary>
        private Render.Mesh _CreateMesh()
        {
            uint patchWidth = this.Region.PatchWidth;
            uint patchHeight = this.Region.PatchHeight;
            Patch<double> grassPresence = this.Region.GetPresencePatch(Ground.Grass);

            // Create points
            OpenTK.Vector2[] pos = new OpenTK.Vector2[patchHeight * patchWidth];
            float[] grass = new float[patchHeight * patchWidth];
            for (uint y = 0; y < patchHeight; y++)
            {
                for (uint x = 0; x < patchWidth; x++)
                {
                    uint i = y * patchWidth + x;
                    pos[i] = (OpenTK.Vector2)(Vector)Patch.GetPointAt(y, x);
                    grass[i] = (float)grassPresence.Data[y, x];
                }
            }

            // Create indices
            uint lineCount = patchHeight - 1;
            uint lineIndCount = 2 * patchWidth + 1;
            uint[] inds = new uint[1 + lineIndCount * lineCount];
            inds[0] = patchWidth - 1; // Dummy index to reverse all triangles.
            for (uint line = 0; line < lineCount; line += 2)
            {
                for (uint pair = 0; pair < patchWidth; pair++)
                {
                    inds[line * lineIndCount + 2 * pair + 1] = line * patchWidth + patchWidth - pair - 1;
                    inds[line * lineIndCount + 2 * pair + 2] = (line + 1) * patchWidth + patchWidth - pair - 1;
                }
                inds[line * lineIndCount + 2 * patchWidth + 1] = (line + 1) * patchWidth;
            }
            for (uint line = 1; line < lineCount; line += 2)
            {
                for (uint pair = 0; pair < patchWidth; pair++)
                {
                    inds[line * lineIndCount + 2 * pair + 1] = line * patchWidth + pair;
                    inds[line * lineIndCount + 2 * pair + 2] = (line + 1) * patchWidth + pair;
                }
                inds[line * lineIndCount + 2 * patchWidth + 1] = (line + 1) * patchWidth + patchWidth - 1;
            }

            // Create mesh
            return Render.Mesh.CreateFromData(inds,
                Render.MeshAttributeData.FromArray(pos),
                Render.MeshAttributeData.FromArray(grass));
        }

        /// <summary>
        /// The resource for the shader used to visually render a terrain patch.
        /// </summary>
        public static Resource<Render.Program> VisualProgram =
            Render.Program.Load("Shader/Scene/TerrainSolidVisual.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(
                    new AttributeBinding("pos", 0),
                    new AttributeBinding("grass", 1)));

        /// <summary>
        /// The texture used for terrain.
        /// </summary>
        public static readonly Image Texture = Image.Load("Background.png");

        /// <summary>
        /// Creates a patch solid for testing
        /// </summary>
        public static TerrainRegionSolid Test() // TODO: Remove
        {
            Feature feature = Ground.Grass.Over(2.0) | Ground.Dirt;
            Region region = Region.CreateSeed(
                new OrthoTransform(new Vector(0.0, 0.0), 0.4), 128, 128,
                new Ground[] { Ground.Grass, Ground.Dirt },
                new Random(1));
            for (int i = 0; i < 3; i++)
            {
                Objective objective = new Objective(region.Bounds.Pad(-feature.Extent), feature);
                double error = objective.GetError(region);
                RegionAdjustment adjustment = objective.CreateAdjustment(region);
                region = adjustment.Apply(-error * 1.5);
            }
            return new TerrainRegionSolid(region);
        }

        public override void InstantiateVisual(Renderable Renderable, Solid.VisualContext Context)
        {
            Dynamic<OrthoTransform> modelToProjScene = this.Region.PatchTransform;
            Render.Mesh mesh = this._CreateMesh();
            Usage<Texture> groundTexture = Texture.ToTexture(
                TextureFormat.Color, TextureSampling.Smooth).Use();
            Renderable.Add(new Primitive
            {
                Program = VisualProgram,
                Mesh = mesh,
                Type = OpenTK.Graphics.OpenGL.PrimitiveType.TriangleStrip,
                Culling = Culling.Back,
                Uniforms = Context.Uniforms.Cons(
                    Uniform.Mat3x2(modelToProjScene).BindTo("modelToProjScene"),
                    Uniform.Texture2D(groundTexture).BindTo("groundTexture")),
                Resources = ImmutableSet.From<IDisposable>(mesh, groundTexture)
            });
        }

        public override void InstantiateShadow(Renderable Renderable, ShadowContext Context)
        {
            // TODO
        }
    }
}
