﻿using System;
using System.Collections.Generic;

using BattleSwaps.Data;
using BattleSwaps.Graphics.Render;

namespace BattleSwaps.Graphics.Scene
{
    /// <summary>
    /// Describes a material for a solid surface in a scene.
    /// </summary>
    public struct Material
    {
        [DefaultSerializer]
        public static readonly Serializer<Material> Serializer = Data.Serializer.Struct<Material>();
        public Material(MaterialMap Diffuse, Color Specular, double Shininess)
        {
            this.Visible = true;
            this.Diffuse = Diffuse;
            this.Specular = Specular;
            this.Shininess = Shininess;
        }

        /// <summary>
        /// The default material to use when no other is specified.
        /// </summary>
        public static readonly Material Default = new Material
        {
            Visible = true,
            Diffuse = Color.White
        };

        /// <summary>
        /// A material that is invisible.
        /// </summary>
        public static readonly Material Invisible = new Material
        {
            Visible = false
        };

        /// <summary>
        /// Indicates whether the material is visible.
        /// </summary>
        public bool Visible;

        /// <summary>
        /// The diffuse map for the material.
        /// </summary>
        public MaterialMap Diffuse;

        /// <summary>
        /// The specular reflection of the material.
        /// </summary>
        public Color Specular;

        /// <summary>
        /// The specular exponent of the material.
        /// </summary>
        public double Shininess;

        /// <summary>
        /// Indicates whether this model needs to be applied to a UV-mapped model.
        /// </summary>
        public bool NeedsUV
        {
            get
            {
                return this.Diffuse.Texture != null;
            }
        }
    }

    /// <summary>
    /// Describes a mapping from 2D space to colors.
    /// </summary>
    public struct MaterialMap
    {
        [DefaultSerializer]
        public static readonly Serializer<MaterialMap> Serializer =
            Data.Serializer.Struct<MaterialMap>();
        public MaterialMap(Color Base, Image? Texture)
        {
            this.Base = Base;
            this.Texture = Texture;
        }

        /// <summary>
        /// The base color for the map. If a texture isn't present, this will be the color at all points in the map.
        /// </summary>
        public Color Base;

        /// <summary>
        /// The texture for the map. This will be multiplied by the base color.
        /// </summary>
        public Image? Texture;

        public static implicit operator MaterialMap(Color Base)
        {
            return new MaterialMap(Base, null);
        }

        public static implicit operator MaterialMap(Image Texture)
        {
            return new MaterialMap(Color.White, Texture);
        }
    }
}
