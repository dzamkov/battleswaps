﻿using System;
using System.Collections.Generic;

using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Graphics.Scene
{
    /// <summary>
    /// Describes a spatial effect or object which alters the appearance of a scene within a local area.
    /// </summary>
    public abstract class Content
    {
        /// <summary>
        /// Indicates whether this is empty content.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this == Empty;
            }
        }

        /// <summary>
        /// Content which makes no alteration to any scene or object it is applied to.
        /// </summary>
        public static readonly Content Empty = new UnionContent(Bag.Empty<Content>());

        /// <summary>
        /// Gets the solid component of this content.
        /// </summary>
        /// <param name="Transform">The transform to apply to the resulting solid.</param>
        public abstract Solid GetSolidComponent(Signal<DetailTransform> Transform);

        /// <summary>
        /// Gets the volumetric components of this content.
        /// </summary>
        public abstract Bag<Volume> GetVolumeComponents();

        /// <summary>
        /// Constructs content in one reactive system (the "external" system) that takes from this content, which
        /// is in another reactive system (the "internal" system), based on a bridge.
        /// </summary>
        public abstract Content InverseApplyBridge(Bridge Bridge);

        /// <summary>
        /// Gets the union of a bag of content.
        /// </summary>
        public static Content Union(Bag<Content> Sources)
        {
            if (Sources.IsEmpty) return Empty;
            return new UnionContent(Sources);
        }

        /// <summary>
        /// Constructs content that is identical to this content up until the given latch is closed, at
        /// which point the content becomes empty.
        /// </summary>
        public Content DissolveAt(Latch Dissolve)
        {
            if (this.IsEmpty) return Empty;
            UnionContent union = this as UnionContent;
            if (union != null) return new UnionContent(union.Sources.DissolveAt(Dissolve));
            return new UnionContent(Bag.Singleton(this).DissolveAt(Dissolve));
        }

        /// <summary>
        /// Gets a latch such that this content appears empty at all states where the latch is closed.
        /// </summary>
        public Latch Dissolve
        {
            get
            {
                UnionContent union = this as UnionContent;
                if (union != null) return union.Sources.Dissolve;
                return Latch.Open;
            }
        }

        public static Content operator +(Content A, Content B)
        {
            if (A.IsEmpty) return B;
            if (B.IsEmpty) return A;

            UnionContent aUnion = A as UnionContent;
            UnionContent bUnion = B as UnionContent;
            if (aUnion != null && bUnion != null)
                return new UnionContent(aUnion.Sources + bUnion.Sources);
            else if (aUnion != null)
                return new UnionContent(aUnion.Sources.Cons(B));
            else if (bUnion != null)
                return new UnionContent(bUnion.Sources.Cons(A));
            else
                return new UnionContent(Bag.Static(A, B));
        }

        public static Content operator *(Signal<DetailTransform> Transform, Content Content)
        {
            if (Content.IsEmpty) return Empty;
            return new TransformContent(Content, Transform);
        }

        public static Content operator *(Signal<Transform> Transform, Content Content)
        {
            return Transform.Map(t => (DetailTransform)t) * Content;
        }
    }

    /// <summary>
    /// Content defined as the union of a bag of source content.
    /// </summary>
    public sealed class UnionContent : Content
    {
        public UnionContent(Bag<Content> Sources)
        {
            this.Sources = Sources;
        }

        /// <summary>
        /// The source content for this union.
        /// </summary>
        public readonly Bag<Content> Sources;

        public override Solid GetSolidComponent(Signal<DetailTransform> Transform)
        {
            return Solid.Union(this.Sources.Map(c => c.GetSolidComponent(Transform)));
        }

        public override Bag<Volume> GetVolumeComponents()
        {
            return Bag.Union(this.Sources.Map(c => c.GetVolumeComponents()));
        }

        public override Content InverseApplyBridge(Bridge Bridge)
        {
            return Content.Union(Bridge.InverseApply(this.Sources).Map(c => c.InverseApplyBridge(Bridge)));
        }
    }

    /// <summary>
    /// Applies a transform to source content.
    /// </summary>
    public sealed class TransformContent : Content
    {
        public TransformContent(Content Source, Signal<DetailTransform> Transform)
        {
            this.Source = Source;
            this.Transform = Transform;
        }

        /// <summary>
        /// The source content to be transformed.
        /// </summary>
        public readonly Content Source;

        /// <summary>
        /// The transform to apply.
        /// </summary>
        public readonly Signal<DetailTransform> Transform;

        public override Solid GetSolidComponent(Signal<DetailTransform> Transform)
        {
            return this.Source.GetSolidComponent(Signal.Lift(Transform, this.Transform, (a, b) => a * b));
        }

        public override Bag<Volume> GetVolumeComponents()
        {
            return this.Source.GetVolumeComponents().Map(v => this.Transform * v);
        }

        public override Content InverseApplyBridge(Bridge Bridge)
        {
            return Bridge.InverseApply(this.Transform) * this.Source.InverseApplyBridge(Bridge);
        }
    }
}