﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

namespace BattleSwaps.Graphics.Scene
{
    /// <summary>
    /// Contains the resources needed to render a group of a model using a uniform material.
    /// </summary>
    public struct ModelMesh : IDisposable
    {
        /// <summary>
        /// The mesh for this group.
        /// </summary>
        public Render.Mesh Mesh;

        /// <summary>
        /// Indicates whether this group is visible. If not, it may still contain important height data.
        /// </summary>
        public bool Visible;

        /// <summary>
        /// The diffuse color for the group.
        /// </summary>
        public Vector3 Diffuse;

        /// <summary>
        /// The diffuse texture for the group, if applicable.
        /// </summary>
        public Usage<Texture> DiffuseTexture;

        /// <summary>
        /// The specular color for the group.
        /// </summary>
        public Vector3 Specular;

        /// <summary>
        /// The specular exponent of the group.
        /// </summary>
        public float Shininess;

        /// <summary>
        /// Identifies a model mesh given a model.
        /// </summary>
        public struct Identifier
        {
            [DefaultSerializer]
            public static readonly Serializer<Identifier> Serializer = Data.Serializer.Struct<Identifier>();
            public Identifier(string[] ObjectNames, string[] MaterialNames, Material Material)
            {
                this.ObjectNames = ObjectNames;
                this.MaterialNames = MaterialNames;
                this.Material = Material;
            }

            // TODO: Equality

            /// <summary>
            /// The named objects to include in the mesh.
            /// </summary>
            public string[] ObjectNames;

            /// <summary>
            /// The named materials to include in the mesh.
            /// </summary>
            public string[] MaterialNames;

            /// <summary>
            /// The material to apply to the mesh.
            /// </summary>
            public Material Material;
        }

        /// <summary>
        /// Creates an instance of the mesh with the given identifier in the given model.
        /// </summary>
        private static Render.Mesh _CreateMeshWithoutUV(Identifier Identifier, Model.Data Data)
        {
            List<uint> inds = new List<uint>();
            List<Vector3> pos = new List<Vector3>();
            Dictionary<uint, uint> map = new Dictionary<uint, uint>();
            foreach (var objectName in Identifier.ObjectNames)
            {
                Model.Object obj;
                if (Data.Objects.TryGetValue(objectName, out obj))
                {
                    foreach (var materialName in Identifier.MaterialNames)
                    {
                        Model.Group group;
                        if (obj.Groups.TryGetValue(materialName, out group))
                        {
                            for (int i = 0; i < group.Indices.Length; i++)
                            {
                                uint ind;
                                uint from = group.Indices[i].Position;
                                if (!map.TryGetValue(from, out ind))
                                {
                                    ind = (uint)pos.Count;
                                    map[from] = ind;
                                    pos.Add(Data.Pos[from]);
                                }
                                inds.Add(ind);
                            }
                        }
                    }
                }
            }
            return Render.Mesh.CreateFromData(inds.ToArray(), MeshAttributeData.FromArray(pos.ToArray()));
        }

        /// <summary>
        /// Creates an instance of the mesh with the given identifier in the given model.
        /// </summary>
        private static Render.Mesh _CreateMeshWithUV(Identifier Identifier, Model.Data Data)
        {
            List<uint> inds = new List<uint>();
            List<Vector3> pos = new List<Vector3>();
            List<Vector2> uv = new List<Vector2>();
            Dictionary<Model.Index, uint> map = new Dictionary<Model.Index, uint>();
            foreach (var objectName in Identifier.ObjectNames)
            {
                Model.Object obj;
                if (Data.Objects.TryGetValue(objectName, out obj))
                {
                    foreach (var materialName in Identifier.MaterialNames)
                    {
                        Model.Group group;
                        if (obj.Groups.TryGetValue(materialName, out group))
                        {
                            for (int i = 0; i < group.Indices.Length; i++)
                            {
                                uint ind;
                                Model.Index from = group.Indices[i];
                                if (!map.TryGetValue(from, out ind))
                                {
                                    ind = (uint)pos.Count;
                                    map[from] = ind;
                                    pos.Add(Data.Pos[from.Position]);
                                    uv.Add(Data.UV[from.UV]);
                                }
                                inds.Add(ind);
                            }
                        }
                    }
                }
            }
            return Render.Mesh.CreateFromData(inds.ToArray(),
                MeshAttributeData.FromArray(pos.ToArray()),
                MeshAttributeData.FromArray(uv.ToArray()));
        }

        /// <summary>
        /// Creates an instance of the mesh with the given identifier in the given model.
        /// </summary>
        private static ModelMesh _Create(Identifier Identifier, Model.Data Data)
        {
            Material mat = Identifier.Material;
            return new ModelMesh
            {
                Mesh = mat.NeedsUV ?
                    _CreateMeshWithUV(Identifier, Data) :
                    _CreateMeshWithoutUV(Identifier, Data),
                Visible = mat.Visible,
                Diffuse = mat.Diffuse.Base,
                DiffuseTexture = mat.Diffuse.Texture != null ?
                    mat.Diffuse.Texture.Value.ToTexture(TextureFormat.Color, TextureSampling.Smooth).Use() :
                    Usage<Texture>.Invalid,
                Specular = mat.Specular,
                Shininess = (float)mat.Shininess,
            };
        }

        /// <summary>
        /// A resource for a <see cref="ModelMesh"/> loaded from a <see cref="Model"/>.
        /// </summary>
        [Tag(typeof(Resource<ModelMesh>), (uint)840342325)]
        public sealed class LoadResource : Resource<ModelMesh>
        {
            [DefaultSerializer]
            public static readonly Serializer<LoadResource> Serializer =
                Data.Serializer.Immutable<LoadResource>();
            public readonly Resource<Model.Data> Source;
            public readonly Identifier Identifier;
            private LoadResource(Resource<Model.Data> Source, Identifier Identifier)
            {
                this.Source = Source;
                this.Identifier = Identifier;
            }

            public static LoadResource Get(Resource<Model.Data> Source, Identifier Identifier)
            {
                return (LoadResource)Resource.Coalesce(new LoadResource(Source, Identifier));
            }

            protected override void Load()
            {
                var sourceUsage = this.Source.Use();
                Context.Invoke(delegate
                {
                    this.Provide(_Create(this.Identifier, sourceUsage.Wait()));
                    sourceUsage.Dispose();
                });
            }
        }

        /// <summary>
        /// Gets the resource for the mesh with the given identifier in the given model.
        /// </summary>
        public static Resource<ModelMesh> Load(Model Model, Identifier Identifier)
        {
            return LoadResource.Get(Model.Source, Identifier);
        }

        /// <summary>
        /// Loads all meshes that are needed to render a model.
        /// </summary>
        public static IEnumerable<Resource<ModelMesh>> LoadAll(Model Model)
        {
            using (var usage = Model.Source.Use())
            {
                Model.Data data = usage.Wait();
                string[] objectNames =
                    Model.FilterObjects.IsJust ?
                    Model.FilterObjects.Value :
                    data.Objects.Keys.ToArray();

                // TODO: Check for duplicate materials
                foreach (var kvp in data.Materials)
                {
                    Material mat = kvp.Value;
                    if (Model.Substitutions != null)
                    {
                        foreach (var subs in Model.Substitutions)
                            if (kvp.Key == subs.Key)
                                mat = subs.Value;
                    }
                    yield return ModelMesh.Load(Model, new ModelMesh.Identifier(
                        objectNames, new string[] { kvp.Key }, mat));
                }
            }
        }

        /// <summary>
        /// Gets the uniform bindings associated with this renderable.
        /// </summary>
        public ImmutableSet<UniformBinding> Uniforms
        {
            get
            {
                ImmutableSet<UniformBinding> bindings = ImmutableSet.From(
                    Uniform.Vec3(this.Diffuse).BindTo("diffuse"),
                    Uniform.Vec3(this.Specular).BindTo("specular"),
                    Uniform.Float(this.Shininess).BindTo("shininess"));
                if (this.DiffuseTexture.IsValid)
                    bindings = bindings.Cons(
                        Uniform.Texture2D(this.DiffuseTexture).BindTo("diffuseTexture"));
                return bindings;
            }
        }

        /// <summary>
        /// Deletes all resources for this renderable.
        /// </summary>
        public void Dispose()
        {
            this.Mesh.Dispose();
            this.DiffuseTexture.Dispose();
        }

        /// <summary>
        /// The resource for the shader used to visually render a model.
        /// </summary>
        public static Resource<Render.Program> VisualProgram =
            Render.Program.Load("Shader/Scene/ModelSolidVisual.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(new AttributeBinding("pos", 0)));

        /// <summary>
        /// The resource for the shader used to visually render a model when textures are being used.
        /// </summary>
        public static Resource<Render.Program> VisualTextureProgram =
            Render.Program.Load("Shader/Scene/ModelSolidVisual.glsl",
                ImmutableSet.From(Macro.Conditional("TEXTURE")),
                ImmutableSet.From(
                    new AttributeBinding("pos", 0),
                    new AttributeBinding("uv", 1)));

        /// <summary>
        /// The resource for the shader used to render the shadow for a model.
        /// </summary>
        public static Resource<Render.Program> ShadowProgram =
            Render.Program.Load("Shader/Scene/ModelSolidShadow.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(new AttributeBinding("pos", 0)));

        /// <summary>
        /// Adds the effects needed to visually render a mesh to the given renderable.
        /// </summary>
        public static void InstantiateVisual(
            Usage<ModelMesh> Mesh, Renderable Renderable,
            Solid.VisualContext Context, Dynamic<DetailTransform> ModelToScene)
        {
            Renderable.Add(new Primitive
            {
                Program = Mesh.Wait().DiffuseTexture.IsValid ? VisualTextureProgram : VisualProgram,
                Mesh = Mesh.Wait().Mesh,
                Type = PrimitiveType.Triangles,
                Culling = Culling.Back,
                Uniforms = (Context.Uniforms | Mesh.Wait().Uniforms).Cons(
                    Uniform.Mat4x3(ModelToScene).BindTo("modelToScene")),
                Resources = ImmutableSet.From<IDisposable>(Mesh)
            });
        }

        /// <summary>
        /// Adds the effects needed to render the shadow information for a mesh to the given renderable.
        /// </summary>
        public static void InstantiateShadow(
            Usage<ModelMesh> Mesh, Renderable Renderable,
            Solid.ShadowContext Context, Dynamic<DetailTransform> ModelToScene)
        {
            Renderable.Add(new Primitive
            {
                Program = ShadowProgram,
                Mesh = Mesh.Wait().Mesh,
                Type = PrimitiveType.Triangles,
                Culling = Culling.Front,
                Uniforms = Context.Uniforms.Cons(Uniform.Mat4x3(ModelToScene).BindTo("modelToScene")),
                Resources = ImmutableSet.From<IDisposable>(Mesh)
            });
        }
    }

    /// <summary>
    /// Content consisting entirely of a <see cref="ModelSolid"/>.
    /// </summary>
    public sealed class ModelContent : Content
    {
        public ModelContent(Model Model)
        {
            this.Model = Model;
        }

        /// <summary>
        /// The model for this content.
        /// </summary>
        public readonly Model Model;

        public override Solid GetSolidComponent(Signal<DetailTransform> Transform)
        {
            return new ModelSolid(this.Model, Transform);
        }

        public override Bag<Volume> GetVolumeComponents()
        {
            return Bag<Volume>.Empty;
        }

        public override Content InverseApplyBridge(Bridge Bridge)
        {
            return this;
        }
    }

    /// <summary>
    /// A renderable solid that is defined by a model.
    /// </summary>
    public sealed class ModelSolid : Solid
    {
        private Resource<ModelMesh>[] _Meshes;
        public ModelSolid(Model Model, Signal<DetailTransform> ModelToScene)
        {
            this.Model = Model;
            this.ModelToScene = ModelToScene;
        }
        private ModelSolid(Model Model, Resource<ModelMesh>[] Meshes, Signal<DetailTransform> ModelToScene)
        {
            this.Model = Model;
            this._Meshes = Meshes;
            this.ModelToScene = ModelToScene;
        }

        /// <summary>
        /// The model for this solid.
        /// </summary>
        public readonly Model Model;

        /// <summary>
        /// The transform applied to get from model space to scene space.
        /// </summary>
        public readonly Signal<DetailTransform> ModelToScene;

        /// <summary>
        /// Gets the meshes needed to render this solid.
        /// </summary>
        public Resource<ModelMesh>[] Meshes
        {
            get
            {
                Resource<ModelMesh>[] value = this._Meshes;
                if (value == null)
                {
                    value = ModelMesh.LoadAll(this.Model).ToArray();
                    value = Interlocked.CompareExchange(ref this._Meshes, value, null) ?? value;
                }
                return value;
            }
        }

        public override void InstantiateVisual(Renderable Renderable, VisualContext Context)
        {
            foreach (var mesh in this.Meshes)
                ModelMesh.InstantiateVisual(mesh.Use(), Renderable, Context, Context.Moment.Read(this.ModelToScene));
        }

        public override void InstantiateShadow(Renderable Renderable, ShadowContext Context)
        {
            foreach (var mesh in this.Meshes)
                ModelMesh.InstantiateShadow(mesh.Use(), Renderable, Context, Context.Moment.Read(this.ModelToScene));
        }
    }
}