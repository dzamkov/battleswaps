﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

namespace BattleSwaps.Graphics.Scene
{
    /// <summary>
    /// Describes opaque hard-edged contents of a scene.
    /// </summary>
    public abstract class Solid
    {
        /// <summary>
        /// The context information needed to visually render a solid.
        /// </summary>
        public struct VisualContext
        {
            /// <summary>
            /// Provides the current reactive moment to read from.
            /// </summary>
            public Dynamic<Moment> Moment;

            /// <summary>
            /// The transform used to convert from scene heights to relative heights as they should be written to
            /// the depth buffer.
            /// </summary>
            public Dynamic<HeightTransform> SceneToRelativeHeight;

            /// <summary>
            /// The projection from scene space to normalized device space.
            /// </summary>
            public Dynamic<Projection> SceneToDevice;

            /// <summary>
            /// Gets the uniform bindings for the information given in this structure.
            /// </summary>
            public ImmutableSet<UniformBinding> Uniforms
            {
                get
                {
                    Dynamic<Projection> sceneToDevice = this.SceneToDevice;
                    return ImmutableSet.From(
                        Uniform.Vec2(this.SceneToRelativeHeight).BindTo("sceneToRelativeHeight"),
                        Uniform.Mat3x2(sceneToDevice.Map(p => p.Fixed.Lateral)).BindTo("sceneToDeviceLateral"),
                        Uniform.Float(sceneToDevice.Map(p => p.InverseHeight)).BindTo("sceneToDeviceInverseHeight"),
                        Uniform.Vec2(sceneToDevice.Map(p => p.Offset)).BindTo("sceneToDeviceOffset"));
                }
            }
        }

        /// <summary>
        /// The context information needed to render the shadows for a solid.
        /// </summary>
        public struct ShadowContext
        {
            /// <summary>
            /// Provides the current reactive moment to read from.
            /// </summary>
            public Dynamic<Moment> Moment;

            /// <summary>
            /// The transform from scene space to normalized device space on the shadow map.
            /// </summary>
            public Dynamic<OpenTK.Matrix4> SceneToShadow;

            /// <summary>
            /// Gets the uniform bindings for the information given in this structure.
            /// </summary>
            public ImmutableSet<UniformBinding> Uniforms
            {
                get
                {
                    return ImmutableSet.From(Uniform.Mat4(this.SceneToShadow).BindTo("sceneToShadow"));
                }
            }
        }

        /// <summary>
        /// Adds the effects needed to visually render this solid to a deferred-shaded buffer.
        /// </summary>
        public abstract void InstantiateVisual(Renderable Renderable, VisualContext Context);

        /// <summary>
        /// Adds the effects needed to render shadow information for this solid to a deferred-shaded buffer.
        /// </summary>
        public abstract void InstantiateShadow(Renderable Renderable, ShadowContext Context);

        /// <summary>
        /// Indicates whether this solid is empty.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this == Empty;
            }
        }

        /// <summary>
        /// The empty solid.
        /// </summary>
        public static readonly Solid Empty = new UnionSolid(Bag.Empty<Solid>());

        /// <summary>
        /// Gets the union of a bag of solids.
        /// </summary>
        public static Solid Union(Bag<Solid> Sources)
        {
            if (Sources.IsEmpty) return Empty;
            return new UnionSolid(Sources);
        }

        public static Solid operator +(Solid A, Solid B)
        {
            if (A.IsEmpty) return B;
            if (B.IsEmpty) return A;

            UnionSolid aUnion = A as UnionSolid;
            UnionSolid bUnion = B as UnionSolid;
            if (aUnion != null && bUnion != null)
                return new UnionSolid(aUnion.Sources + bUnion.Sources);
            else if (aUnion != null)
                return new UnionSolid(aUnion.Sources.Cons(B));
            else if (bUnion != null)
                return new UnionSolid(bUnion.Sources.Cons(A));
            else
                return new UnionSolid(Bag.Static(A, B));
        }
    }

    /// <summary>
    /// A solid defined as the composition of several solids.
    /// </summary>
    public sealed class UnionSolid : Solid
    {
        public UnionSolid(Bag<Solid> Sources)
        {
            this.Sources = Sources;
        }

        /// <summary>
        /// The source solids for this union.
        /// </summary>
        public readonly Bag<Solid> Sources;

        public override void InstantiateVisual(Renderable Renderable, VisualContext Context)
        {
            Renderable.Loop(Context.Moment, this.Sources, (s, r) => s.InstantiateVisual(r, Context));
        }

        public override void InstantiateShadow(Renderable Renderable, ShadowContext Context)
        {
            Renderable.Loop(Context.Moment, this.Sources, (s, r) => s.InstantiateShadow(r, Context));
        }
    }
}
