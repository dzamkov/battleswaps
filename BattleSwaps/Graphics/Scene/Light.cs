﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace BattleSwaps.Graphics.Scene
{
    /// <summary>
    /// Describes the color-dependent radiance of a light. This measures how "bright" a light is, along with what
    /// color it appears as.
    /// </summary>
    public struct Radiance
    {
        public double R;
        public double G;
        public double B;
        public Radiance(double R, double G, double B)
        {
            this.R = R;
            this.G = G;
            this.B = B;
        }

        /// <summary>
        /// A radiance representing no light.
        /// </summary>
        public static readonly Radiance Zero = new Radiance(0.0, 0.0, 0.0);

        public static Radiance operator +(Radiance A, Radiance B)
        {
            return new Radiance(A.R + B.R, A.G + B.G, A.B + B.B);
        }

        public static Radiance operator *(Radiance A, Color B)
        {
            return new Radiance(A.R * B.R, A.G * B.G, A.B * B.B);
        }

        public static Radiance operator *(Color A, Radiance B)
        {
            return new Radiance(A.R * B.R, A.G * B.G, A.B * B.B);
        }

        public static Radiance operator *(Radiance Color, double Amount)
        {
            return new Radiance(Color.R * Amount, Color.G * Amount, Color.B * Amount);
        }

        public static Radiance operator *(double Amount, Radiance Color)
        {
            return new Radiance(Color.R * Amount, Color.G * Amount, Color.B * Amount);
        }

        public static implicit operator OpenTK.Vector3(Radiance Color)
        {
            return new OpenTK.Vector3(
                (float)Color.R,
                (float)Color.G,
                (float)Color.B);
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}, {2})", this.R, this.G, this.B);
        }
    }

    /// <summary>
    /// Describes an ambient light source. This type of source models light coming uniformly from the sky above the
    /// horizon, with no coherent direction.
    /// </summary>
    public struct AmbientLight
    {
        public AmbientLight(Signal<Radiance> Radiance)
        {
            this.Radiance = Radiance;
        }

        /// <summary>
        /// The radiance of the light source. This is the same at all points in space and in all directions that
        /// come from the sky.
        /// </summary>
        public Signal<Radiance> Radiance;

        /// <summary>
        /// An ambient light that has no effect on the scene.
        /// </summary>
        public static readonly AmbientLight Zero = new AmbientLight(Scene.Radiance.Zero);

        /// <summary>
        /// Gets the union of a bag of ambient lights.
        /// </summary>
        public static AmbientLight Union(Bag<AmbientLight> Sources)
        {
            return new AmbientLight(Sources.MapReduce(a => a.Radiance, Scene.Radiance.Zero, (a, b) =>
                Signal.Lift(a, b, (c, d) => c + d)).Switch());
        }

        /// <summary>
        /// Constructs an ambient light in one reactive system (the "external" system) that takes from a light in
        /// another reactive system (the "internal" system) based on a bridge.
        /// </summary>
        public static AmbientLight InverseApplyBridge(Bridge Bridge, AmbientLight Inner)
        {
            return new AmbientLight(Bridge.InverseApply(Inner.Radiance));
        }

        /// <summary>
        /// The context information needed to render an ambient light.
        /// </summary>
        public struct Context
        {
            /// <summary>
            /// Provides the current reactive moment to read from.
            /// </summary>
            public Dynamic<Moment> Moment;

            /// <summary>
            /// The diffuse texture from the deferred-shaded buffer.
            /// </summary>
            public Texture DiffuseTexture;
        }

        /// <summary>
        /// The resource for the shader used to render ambient lights.
        /// </summary>
        public static Resource<Render.Program> Program =
            Render.Program.Load("Shader/Scene/AmbientLight.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(new AttributeBinding("pos", 0)));

        /// <summary>
        /// Adds the effects needed to render this ambient light to a light texture to the given renderable.
        /// </summary>
        public void Instantiate(Renderable Renderable, Context Context)
        {
            Renderable.Add(new Primitive
            {
                IsUnitQuad = true,
                Program = Program,
                Uniforms = ImmutableSet.From(
                    Uniform.Vec3(Context.Moment.Read(this.Radiance)).BindTo("lightRadiance"),
                    Uniform.Texture2D(Context.DiffuseTexture).BindTo("diffuseTexture")),
                Resources = ImmutableSet<IDisposable>.Empty
            });
        }

        public static AmbientLight operator +(AmbientLight A, AmbientLight B)
        {
            return new AmbientLight(Signal.Lift(A.Radiance, B.Radiance, (a, b) => a + b));
        }
    }

    /// <summary>
    /// Describes a directional light source. This type of source models light coming from some upward-facing
    /// direction, with uniform intensity over the entire 2D plane.
    /// </summary>
    public struct DirectionalLight
    {
        public DirectionalLight(Signal<Radiance> Radiance, Signal<Vector> Direction)
        {
            this.Radiance = Radiance;
            this.Direction = Direction;
        }

        /// <summary>
        /// The radiance of the light source. This is the same at all points in space, but only applies in one
        /// direction.
        /// </summary>
        public Signal<Radiance> Radiance; // TODO: Wrong measure, should be radiosity

        /// <summary>
        /// The direction in scene space the light appears to come from. This vector should give the offset of the
        /// light for every height unit travelled upwards.
        /// </summary>
        public Signal<Vector> Direction;

        /// <summary>
        /// Constructs a directional light in one reactive system (the "external" system) that takes from a light in
        /// another reactive system (the "internal" system) based on a bridge.
        /// </summary>
        public static DirectionalLight InverseApplyBridge(Bridge Bridge, DirectionalLight Inner)
        {
            return new DirectionalLight(Bridge.InverseApply(Inner.Radiance), Bridge.InverseApply(Inner.Direction));
        }

        /// <summary>
        /// The context information needed to render a directional light.
        /// </summary>
        public struct Context
        {
            /// <summary>
            /// Provides the current reactive moment to read from.
            /// </summary>
            public Dynamic<Moment> Moment;

            /// <summary>
            /// The solid component in the scene present to cast shadows.
            /// </summary>
            public Solid Solid;

            /// <summary>
            /// The depth texture from the deferred-shaded buffer.
            /// </summary>
            public Texture DepthTexture;

            /// <summary>
            /// The diffuse texture from the deferred-shaded buffer.
            /// </summary>
            public Texture DiffuseTexture;

            /// <summary>
            /// The specular texture from the deferred-shaded buffer.
            /// </summary>
            public Texture SpecularTexture;

            /// <summary>
            /// The range of heights visible in the scene.
            /// </summary>
            public Dynamic<HeightRange> HeightRange;

            /// <summary>
            /// The transform used to convert from scene heights to relative heights as they are written in the depth
            /// texture.
            /// </summary>
            public Dynamic<HeightTransform> SceneToRelativeHeight;

            /// <summary>
            /// The projection from scene space to normalized device space.
            /// </summary>
            public Dynamic<Projection> SceneToDevice;
        }

        /// <summary>
        /// The resource for the shader used to render directional lights.
        /// </summary>
        public static Resource<Render.Program> Program =
            Render.Program.Load("Shader/Scene/DirectionalLight.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(new AttributeBinding("pos", 0)));

        /// <summary>
        /// Gets a projection matrix for a shadow map for a light with the given direction that includes all points in
        /// the given rectangle and height range.
        /// </summary>
        public static Matrix4 GetSceneToShadow(
            DetailVector LightDirection,
            Rectangle Rectangle, HeightRange HeightRange)
        {
            DetailVector center = Rectangle.Center.WithHeight((HeightRange.Min + HeightRange.Max) / 2.0);
            DetailVector down = -LightDirection;
            DetailVector x = DetailVector.Cross(down, new DetailVector(1.0, 0.0, 0.0)).Normal;
            DetailVector y = DetailVector.Cross(down, x);
            double downScale = 0.0;
            double xScale = 0.0;
            double yScale = 0.0;
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    DetailVector testPoint = Rectangle.Transform
                        .ApplyPoint(new Vector(i, j))
                        .WithHeight(HeightRange.Min);
                    DetailVector testDif = testPoint - center;
                    double downDot = DetailVector.Dot(down, testDif);
                    double xDot = DetailVector.Dot(x, testDif);
                    double yDot = DetailVector.Dot(y, testDif);
                    downScale = Math.Max(downScale, Math.Abs(downDot));
                    xScale = Math.Max(xScale, Math.Abs(xDot));
                    yScale = Math.Max(yScale, Math.Abs(yDot));
                }
            }
            down *= downScale;
            x *= xScale;
            y *= yScale;
            Matrix4 basis = new Matrix4(
                (float)x.X, (float)x.Y, (float)x.Z, 0.0f,
                (float)y.X, (float)y.Y, (float)y.Z, 0.0f,
                (float)down.X, (float)down.Y, (float)down.Z, 0.0f,
                (float)center.X, (float)center.Y, (float)center.Z, 1.0f);
            return basis.Inverted();
        }

        /// <summary>
        /// Adds the effects needed to render this ambient light to a light texture to the given renderable.
        /// </summary>
        public void Instantiate(Renderable Renderable, Context Context)
        {
            // Shadow pass
            Routine preparation = Renderable.Prepare.Expand();
            Cursor prepare = preparation.End;
            Dynamic<DetailVector> lightDirection =
                Context.Moment.Read(this.Direction)
                    .Map(d => new DetailVector(d, 1.0).Normal);
            ImageSize shadowSize = new ImageSize(1024, 1024);
            Texture shadowTexture = Texture.Create();
            shadowTexture.Bind(TextureTarget.Texture2D);
            Texture.SetSampling(TextureTarget.Texture2D, TextureSampling.Pixel);
            Texture.AllocateData2D(TextureFormat.Depth, shadowSize);
            Framebuffer shadowFramebuffer = Framebuffer.Create();
            shadowFramebuffer.Bind(FramebufferTarget.Framebuffer);
            Framebuffer.Attach2D(FramebufferTarget.Framebuffer,
                FramebufferAttachment.DepthAttachment, shadowTexture);
            prepare.Take(shadowTexture);
            prepare.Take(shadowFramebuffer);

            Solid.ShadowContext solidShadowContext = new Solid.ShadowContext
            {
                Moment = Context.Moment,
                SceneToShadow = Dynamic.Lift(lightDirection, Context.SceneToDevice, Context.HeightRange,
                    (a, b, c) => DirectionalLight.GetSceneToShadow(a, b.AtHeight(0.0).Inverse * Rectangle.Device, c))
            };

            Renderable shadowRenderable;
            prepare.Render(
                new Target(shadowFramebuffer, shadowSize,
                    new DepthTarget(true, DepthFunction.Less),
                    BlendTarget.Disabled),
                new Clear(1.0),
                out shadowRenderable);
            Context.Solid.InstantiateShadow(shadowRenderable, solidShadowContext);

            // Final render
            Dynamic<HeightTransform> relativeToSceneHeight =
                Context.SceneToRelativeHeight.Map(h => h.Inverse);
            Dynamic<Transform> deviceToSceneLateral = Context.SceneToDevice.Map(p => p.Fixed.Lateral.Inverse);
            Dynamic<double> sceneToDeviceInverseHeight = Context.SceneToDevice.Map(p => p.InverseHeight);
            Dynamic<Vector> sceneToDeviceOffset = Context.SceneToDevice.Map(p => p.Offset);
            Renderable.Add(new Primitive
            {
                IsUnitQuad = true,
                Program = Program,
                Uniforms = ImmutableSet.From(
                    Uniform.Vec2(relativeToSceneHeight).BindTo("relativeToSceneHeight"),
                    Uniform.Mat3x2(deviceToSceneLateral).BindTo("deviceToSceneLateral"),
                    Uniform.Float(sceneToDeviceInverseHeight).BindTo("sceneToDeviceInverseHeight"),
                    Uniform.Vec2(sceneToDeviceOffset).BindTo("sceneToDeviceOffset"),
                    Uniform.Vec3(Context.Moment.Read(this.Radiance)).BindTo("lightRadiance"),
                    Uniform.Vec3(lightDirection).BindTo("lightDirection"),
                    Uniform.Mat4(solidShadowContext.SceneToShadow).BindTo("sceneToShadow"),
                    Uniform.Texture2D(shadowTexture).BindTo("shadowTexture"),
                    Uniform.Texture2D(Context.DepthTexture).BindTo("depthTexture"),
                    Uniform.Texture2D(Context.DiffuseTexture).BindTo("diffuseTexture"),
                    Uniform.Texture2D(Context.SpecularTexture).BindTo("specularTexture")),
                Resources = ImmutableSet.From<IDisposable>(preparation)
            });
        }

        public static DirectionalLight operator *(Signal<UprightTransform> Transform, DirectionalLight Light)
        {
            return new DirectionalLight(Light.Radiance,
                Signal.Lift(Transform, Light.Direction,
                    (t, d) => t.Lateral.ApplyVector(d / t.Height.Scale)));
        }
    }
}
