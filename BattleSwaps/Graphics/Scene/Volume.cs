﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

namespace BattleSwaps.Graphics.Scene
{
    /// <summary>
    /// Describes translucent contents of a scene.
    /// </summary>
    public abstract class Volume
    {
        /// <summary>
        /// The context information needed to render a volume.
        /// </summary>
        public struct Context
        {
            /// <summary>
            /// Provides the current reactive moment to read from.
            /// </summary>
            public Dynamic<Moment> Moment;

            /// <summary>
            /// The transform used to convert from object coordinates to scene coordinates.
            /// </summary>
            public Dynamic<DetailTransform> ObjectToScene;

            /// <summary>
            /// The transform used to convert from scene heights to relative heights as they should be written to
            /// the depth buffer.
            /// </summary>
            public Dynamic<HeightTransform> SceneToRelativeHeight;

            /// <summary>
            /// The projection from scene space to normalized device space.
            /// </summary>
            public Dynamic<Projection> SceneToDevice;

            /// <summary>
            /// The texture that gives the depth of the scene.
            /// </summary>
            public Texture DepthTexture;

            /// <summary>
            /// The transform from normalized device space to the space of the depth texture. This can be used
            /// to get the depth at any point in device space.
            /// </summary>
            public Dynamic<Transform> DeviceToDepth;

            /// <summary>
            /// The color and intensity of ambient light.
            /// </summary>
            public Dynamic<Radiance> AmbientColor;

            /// <summary>
            /// The color and intensity of the light source.
            /// </summary>
            public Dynamic<Radiance> LightColor;

            /// <summary>
            /// The direction the light is coming from, in scene space.
            /// </summary>
            public Dynamic<Vector> LightDirection;

            /// <summary>
            /// Gets the uniform bindings for the information given in this structure.
            /// </summary>
            public ImmutableList<UniformBinding> Uniforms
            {
                get
                {
                    Dynamic<Projection> sceneToDevice = this.SceneToDevice;
                    return new ImmutableList<UniformBinding>(
                        Uniform.Mat4x3(this.ObjectToScene).BindTo("objectToScene"),
                        Uniform.Vec2(this.SceneToRelativeHeight).BindTo("sceneToRelativeHeight"),
                        Uniform.Mat3x2(sceneToDevice.Map(p => p.Fixed.Lateral)).BindTo("sceneToDeviceLateral"),
                        Uniform.Float(sceneToDevice.Map(p => p.InverseHeight)).BindTo("sceneToDeviceInverseHeight"),
                        Uniform.Vec2(sceneToDevice.Map(p => p.Offset)).BindTo("sceneToDeviceOffset"),
                        Uniform.Mat3x2(this.DeviceToDepth).BindTo("deviceToDepth"),
                        Uniform.Texture2D(this.DepthTexture).BindTo("depthTexture"),
                        Uniform.Vec3(this.AmbientColor).BindTo("ambientColor"),
                        Uniform.Vec3(this.LightColor).BindTo("lightColor"),
                        Uniform.Vec2(this.LightDirection).BindTo("lightDirection"));
                }
            }
        }

        /// <summary>
        /// Adds the effects needed to render this volume to the given renderable.
        /// </summary>
        public abstract void Instantiate(Renderable Renderable, Context Context);

        /// <summary>
        /// Constructs a volume in one reactive system (the "external" system) that takes from a volume in
        /// another reactive system (the "internal" system) based on a bridge.
        /// </summary>
        public static Volume InverseApplyBridge(Bridge Bridge, Volume Inner)
        {
            return new BridgeVolume(Bridge, Inner);
        }

        public static Volume operator *(Signal<DetailTransform> Transform, Volume Volume)
        {
            return new TransformVolume(Volume, Transform);
        }

        public static Volume operator *(Signal<Transform> Transform, Volume Volume)
        {
            return Transform.Map(t => (DetailTransform)t) * Volume;
        }
    }

    /// <summary>
    /// Applies a transform to a source volume.
    /// </summary>
    public sealed class TransformVolume : Volume
    {
        public TransformVolume(Volume Source, Signal<DetailTransform> Transform)
        {
            this.Source = Source;
            this.Transform = Transform;
        }

        /// <summary>
        /// The source volume to be transformed.
        /// </summary>
        public readonly Volume Source;

        /// <summary>
        /// The transform to apply.
        /// </summary>
        public readonly Signal<DetailTransform> Transform;

        /// <summary>
        /// Updates a context to reflect this transform being applied.
        /// </summary>
        private void _Decorate(ref Context Context)
        {
            Context.ObjectToScene = Dynamic.Lift<DetailTransform, DetailTransform, DetailTransform>(
                Context.ObjectToScene, Context.Moment.Read(this.Transform),
                (o, t) => o * t);
        }

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            this._Decorate(ref Context);
            this.Source.Instantiate(Renderable, Context);
        }
    }


    /// <summary>
    /// A volume which acts as a volume from a different reactive system.
    /// </summary>
    public sealed class BridgeVolume : Volume
    {
        public BridgeVolume(Bridge Bridge, Volume Inner)
        {
            this.Bridge = Bridge;
            this.Inner = Inner;
        }

        /// <summary>
        /// The bridge between the externally-visible reactive system and the internal system.
        /// </summary>
        public readonly Bridge Bridge;

        /// <summary>
        /// The source volume in the internal system.
        /// </summary>
        public readonly Volume Inner;

        public override void Instantiate(Renderable Renderable, Context Context)
        {
            Context.Moment = Context.Moment.Map(e => this.Bridge.Apply(e.State));
            this.Inner.Instantiate(Renderable, Context);
        }
    }
}