﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Render;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace BattleSwaps.Graphics.Scene
{
    /// <summary>
    /// Fully describes the visual contents of a scene.
    /// </summary>
    public struct Composition
    {
        /// <summary>
        /// Describes the ambient light source for this composition.
        /// </summary>
        public AmbientLight AmbientLight;

        /// <summary>
        /// Describes the directional light source for this composition.
        /// </summary>
        public DirectionalLight DirectionalLight;

        /// <summary>
        /// The range of possible heights for objects in the scene.
        /// </summary>
        public HeightRange HeightRange;

        /// <summary>
        /// Describes the visual contents of this composition.
        /// </summary>
        public Content Content;

        /// <summary>
        /// Indicates whether this structure contains a valid composition.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this.Content != null;
            }
        }

        /// <summary>
        /// Gets a figure which displays this composition using the given projection.
        /// </summary>
        public Figure GetFigure(Signal<Projection> Projection)
        {
            return new CompositionFigure(
                this.AmbientLight, this.DirectionalLight, this.HeightRange,
                this.Content.GetSolidComponent(DetailTransform.Identity),
                this.Content.GetVolumeComponents(),
                Projection);
        }

        /// <summary>
        /// Constructs a composition in one reactive system (the "external" system) that takes from a composition in
        /// another reactive system (the "internal" system) based on a bridge.
        /// </summary>
        public static Composition InverseApplyBridge(Bridge Bridge, Composition Inner)
        {
            return new Composition
            {
                AmbientLight = AmbientLight.InverseApplyBridge(Bridge, Inner.AmbientLight),
                DirectionalLight = DirectionalLight.InverseApplyBridge(Bridge, Inner.DirectionalLight),
                HeightRange = Inner.HeightRange,
                Content = Inner.Content.InverseApplyBridge(Bridge)
            };
        }
    }

    /// <summary>
    /// A figure which renders a composition.
    /// </summary>
    public sealed class CompositionFigure : Figure
    {
        public CompositionFigure(
            AmbientLight AmbientLight, DirectionalLight DirectionalLight,
            HeightRange HeightRange, Solid Solid, Bag<Volume> Volumes,
            Signal<Projection> Projection)
        {
            this.AmbientLight = AmbientLight;
            this.DirectionalLight = DirectionalLight;
            this.HeightRange = HeightRange;
            this.Solid = Solid;
            this.Volumes = Volumes;
            this.Projection = Projection;
        }

        /// <summary>
        /// The ambient light source for the scene.
        /// </summary>
        public readonly AmbientLight AmbientLight;

        /// <summary>
        /// The directional light source for the scene.
        /// </summary>
        public readonly DirectionalLight DirectionalLight;

        /// <summary>
        /// The range of possible heights for objects in the scene.
        /// </summary>
        public readonly HeightRange HeightRange;

        /// <summary>
        /// The solid component of the content for this scene.
        /// </summary>
        public readonly Solid Solid;

        /// <summary>
        /// The volume components of the content for this scene.
        /// </summary>
        public readonly Bag<Volume> Volumes;

        /// <summary>
        /// The projection used to convert from scene coordinates to figure coordinates.
        /// </summary>
        public readonly Signal<Projection> Projection;

        /// <summary>
        /// The resource for the shader used to render directional lights for this figure.
        /// </summary>
        public static Resource<Render.Program> DirectionalLightProgram =
            Render.Program.Load("Shader/Scene/DirectionalLight.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(new AttributeBinding("pos", 0)));

        /// <summary>
        /// The resource for the shader used to render HDR for this figure.
        /// </summary>
        public static Resource<Render.Program> HDRProgram =
            Render.Program.Load("Shader/Scene/HDR.glsl",
                ImmutableSet<Macro>.Empty,
                ImmutableSet.From(new AttributeBinding("pos", 0)));

        public override void Instantiate(Renderable Renderable, Figure.Context Context)
        {
            Dynamic<ImageSize> viewportSize = Context.ViewportSize;
            Dynamic<HeightRange> heightRange = this.HeightRange;
            Dynamic<UprightTransform> fullTransform = Context.FigureToDevice.Map<UprightTransform>(t => t);

            // Create preparation routine
            Routine preparation = Renderable.Prepare.Expand();
            Cursor prepare = preparation.End;

            // Create depth texture
            Texture depthTexture = Texture.Create();
            depthTexture.Bind(TextureTarget.Texture2D);
            Texture.SetSampling(TextureTarget.Texture2D, TextureSampling.Pixel);
            prepare.Take(depthTexture);
            prepare.MaintainTextureSize(depthTexture, TextureFormat.Depth, viewportSize);

            // Create diffuse texture
            Texture diffuseTexture = Texture.Create();
            diffuseTexture.Bind(TextureTarget.Texture2D);
            Texture.SetSampling(TextureTarget.Texture2D, TextureSampling.Pixel);
            prepare.Take(diffuseTexture);
            prepare.MaintainTextureSize(diffuseTexture, TextureFormat.Color, viewportSize);

            // Create specular texture
            Texture specularTexture = Texture.Create();
            specularTexture.Bind(TextureTarget.Texture2D);
            Texture.SetSampling(TextureTarget.Texture2D, TextureSampling.Pixel);
            prepare.Take(specularTexture);
            prepare.MaintainTextureSize(specularTexture, TextureFormat.Paint, viewportSize);

            // Create pre-hdr texture
            Texture lightTexture = Texture.Create();
            lightTexture.Bind(TextureTarget.Texture2D);
            Texture.SetSampling(TextureTarget.Texture2D, TextureSampling.Pixel);
            prepare.Take(lightTexture);
            prepare.MaintainTextureSize(lightTexture, TextureFormat.Radiance, viewportSize);

            // Geometry pass for solids
            var sceneToDevice = Dynamic.Lift(
                Context.FigureToDevice, Context.Moment.Read(this.Projection),
                (a, b) => a * b);
            Solid.VisualContext solidVisualContext = new Solid.VisualContext
            {
                Moment = Context.Moment,
                SceneToDevice = sceneToDevice,
                SceneToRelativeHeight = HeightTransform.Between(this.HeightRange, HeightRange.Unit)
            };

            Framebuffer solidFramebuffer = Framebuffer.Create();
            solidFramebuffer.Bind(FramebufferTarget.Framebuffer);
            Framebuffer.Attach2D(FramebufferTarget.Framebuffer,
                FramebufferAttachment.DepthAttachment, depthTexture);
            Framebuffer.Attach2D(FramebufferTarget.Framebuffer,
                FramebufferAttachment.ColorAttachment0, diffuseTexture);
            Framebuffer.Attach2D(FramebufferTarget.Framebuffer,
                FramebufferAttachment.ColorAttachment1, specularTexture);
            Framebuffer.DrawBuffers(DrawBuffersEnum.ColorAttachment0, DrawBuffersEnum.ColorAttachment1);
            prepare.Take(solidFramebuffer);

            Renderable solidRenderable;
            prepare.Render(
                new Target(solidFramebuffer, viewportSize,
                    new DepthTarget(true, DepthFunction.Less),
                    BlendTarget.Disabled),
                new Clear(new Paint(0.0, 0.0, 0.0, 0.0), 1.0),
                out solidRenderable);
            this.Solid.InstantiateVisual(solidRenderable, solidVisualContext);
            TerrainRegionSolid.Test().InstantiateVisual(solidRenderable, solidVisualContext);

            // Lighting pass
            Framebuffer lightFramebuffer = Framebuffer.Create();
            lightFramebuffer.Bind(FramebufferTarget.Framebuffer);
            Framebuffer.Attach2D(FramebufferTarget.Framebuffer,
                FramebufferAttachment.ColorAttachment0, lightTexture);
            prepare.Take(lightFramebuffer);

            Renderable lightRenderable;
            prepare.Render(
                new Target(lightFramebuffer, viewportSize,
                    DepthTarget.Disabled,
                    BlendTarget.Cumulative),
                new Clear(new Paint(0.0, 0.0, 0.0, 0.0)),
                out lightRenderable);

            // Do ambient lighting
            this.AmbientLight.Instantiate(lightRenderable, new AmbientLight.Context
            {
                Moment = Context.Moment,
                DiffuseTexture = diffuseTexture
            });

            // Do directional lighting
            this.DirectionalLight.Instantiate(lightRenderable, new DirectionalLight.Context
            {
                Moment = Context.Moment,
                Solid = this.Solid,
                DepthTexture = depthTexture,
                DiffuseTexture = diffuseTexture,
                SpecularTexture = specularTexture,
                HeightRange = heightRange,
                SceneToRelativeHeight = solidVisualContext.SceneToRelativeHeight,
                SceneToDevice = sceneToDevice
            });

            // Final render
            Renderable.Add(new Primitive
            {
                IsUnitQuad = true,
                Program = HDRProgram,
                Uniforms = ImmutableSet.From(Uniform.Texture2D(lightTexture).BindTo("lightTexture")),
                Resources = ImmutableSet.From<IDisposable>(preparation)
            });
        }
    }
}