﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics;
using BattleSwaps.Graphics.Scene;
using BattleSwaps.Game;

namespace BattleSwaps
{
    using Dynamics = BattleSwaps.Physics.Dynamics;
    using Asset = BattleSwaps.Game.Asset;

    public static class Program
    {
        /// <summary>
        /// Creates the test world and simulation for the application.
        /// </summary>
        private static Simulation _CreateSimulation(out Intel.Faction Faction)
        {
            // Build content
            MaterialMap pattern1 = new Color(0.1, 0.1, 0.3);
            MaterialMap pattern2 = new Color(0.3, 0.1, 0.1);
            MaterialMap pattern3 = new Color(0.1, 0.3, 0.0);
            MaterialMap pattern4 = Image.Load("Pattern/Camo.png").Transform(PaintTransform.Between(0.0, 0.9));
            MaterialMap pattern5 = Image.Load("Pattern/Zebra.png").Transform(PaintTransform.Between(0.0, 0.7));
            MaterialMap pattern6 = Image.Load("Pattern/Carbon.png");

            NameRef<Intel.Faction> factionRef = NameRef<Intel.Faction>.Generate();
            NameRef<Intel.Capture> driveCapture = NameRef<Intel.Capture>.Generate();
            NameRef<Intel.Unit> movingUnitRef = NameRef<Intel.Unit>.Generate();
            WorldPrototype prototype = new WorldPrototype
            {
                Gravity = 9.81,
                Surface = Dynamics.Surface.Default,
                Terrain = new Terrain.WorldPrototype
                {
                    AmbientLight = new AmbientLight(new Radiance(0.4, 0.4, 0.4)),
                    DirectionalLight = new DirectionalLight(
                        new Radiance(0.9, 0.8, 0.7),
                        new Vector(-0.7, -0.3))
                },
                Factions = new Intel.FactionPrototype[]
                {
                    new Intel.FactionPrototype
                    {
                        NameRef = factionRef,
                        State = new Intel.FactionState
                        {
                            Behavior = Intel.Behavior.Empty + Intel.Behavior.Define(
                                Intel.DriveController.CaptureParameter,
                                driveCapture).For(Intel.Group.Singleton(movingUnitRef)),
                            Captures = new Intel.CapturePrototype[]
                            {
                                new Intel.CapturePrototype<Intel.DriveBeat>
                                {
                                    NameRef = driveCapture,
                                    Parameter = Intel.DriveController.CaptureParameter,
                                    Beat = new Intel.DriveBeat(0.5, 1.0),
                                }
                            }
                        }
                    }
                },
                Entities = new EntityPrototype[]
                {
                    new Asset.TruckState
                    {
                        Controller = new Intel.ControllerState(factionRef),
                        Body = Dynamics.FreeBodyState.Static(new Vector(10.0, 12.0), Math.PI / 2.0),
                        IsSlipping = false,
                        Attachment = new Asset.CannonState
                        {
                            Controller = new Intel.ControllerState(factionRef),
                            Body = Dynamics.TurretBodyState.Static(Math.PI / 2.0),
                            LastFiring = Asset.Firing.None
                        }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern1)
                    }.ToPrototype(movingUnitRef, pattern1),

                    new Asset.TruckState
                    {
                        Controller = new Intel.ControllerState(factionRef),
                        Body = Dynamics.FreeBodyState.Static(new Vector(15.0, 12.0), Math.PI / 2.0),
                        IsSlipping = false,
                        Attachment = new Asset.CannonState
                        {
                            Controller = new Intel.ControllerState(factionRef),
                            Body = Dynamics.TurretBodyState.Static(Math.PI / 2.0),
                            LastFiring = Asset.Firing.None
                        }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern2)
                    }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern2),

                    new Asset.TruckState
                    {
                        Controller = new Intel.ControllerState(factionRef),
                        Body = Dynamics.FreeBodyState.Static(new Vector(20.0, 12.0), Math.PI / 2.0),
                        IsSlipping = false,
                        Attachment = new Asset.CannonState
                        {
                            Controller = new Intel.ControllerState(factionRef),
                            Body = Dynamics.TurretBodyState.Static(Math.PI / 2.0),
                            LastFiring = Asset.Firing.None
                        }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern3)
                    }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern3),

                    new Asset.TruckState
                    {
                        Controller = new Intel.ControllerState(factionRef),
                        Body = Dynamics.FreeBodyState.Static(new Vector(10.0, 18.0), -Math.PI / 2.0),
                        IsSlipping = false,
                        Attachment = new Asset.CannonState
                        {
                            Controller = new Intel.ControllerState(factionRef),
                            Body = Dynamics.TurretBodyState.Static(-Math.PI / 2.0),
                            LastFiring = Asset.Firing.None
                        }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern4)
                    }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern4),

                    new Asset.TruckState
                    {
                        Controller = new Intel.ControllerState(factionRef),
                        Body = Dynamics.FreeBodyState.Static(new Vector(15.0, 18.0), -Math.PI / 2.0),
                        IsSlipping = false,
                        Attachment = new Asset.CannonState
                        {
                            Controller = new Intel.ControllerState(factionRef),
                            Body = Dynamics.TurretBodyState.Static(-Math.PI / 2.0),
                            LastFiring = Asset.Firing.None
                        }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern5)
                    }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern5),

                    new Asset.TruckState
                    {
                        Controller = new Intel.ControllerState(factionRef),
                        Body = Dynamics.FreeBodyState.Static(new Vector(20.0, 18.0), -Math.PI / 2.0),
                        IsSlipping = false,
                        Attachment = new Asset.CannonState
                        {
                            Controller = new Intel.ControllerState(factionRef),
                            Body = Dynamics.TurretBodyState.Static(-Math.PI / 2.0),
                            LastFiring = Asset.Firing.None
                        }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern6)
                    }.ToPrototype(NameRef<Intel.Unit>.Generate(), pattern6)
                }
            };

            // Create simulation
            Simulation sim = new Simulation(prototype);
            Faction = factionRef.Get(sim.Head.State);
            return sim;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Directory.SetCurrentDirectory("Resources");

            // Set up trace listener to break on assertion failure
            Debug.Listeners.Clear();
            Debug.Listeners.Add(new _BreakTraceListener());

            // Create simulation and interfaces
            Intel.Faction faction;
            Initializer initializer = Initializer.Create();
            Simulation sim = _CreateSimulation(out faction);
            Observer obs = sim.CreateObserver(initializer, Time.FromSeconds(0.2));

            // Create integrator for UI reactive system
            Stimulus<Entry<Bag<Event>>> addEventSource;
            Bag<Event> events = Bag.Union(Bag.CreateEmpty<Bag<Event>>(initializer, out addEventSource));
            Integrator integrator = new Integrator(16, events, initializer.FinishInitialize());

            // Create interface
            Intel.CommandInterface inteface = new Intel.CommandInterface(
                new Intel.BridgeWorld(obs.Bridge, sim.World.Intel.RuntimeValue),
                new Intel.BridgeFaction(obs.Bridge, faction),
                obs.GetRelay(integrator.CreateOutput<Perturbation>(sim.Perturb), faction.NameRef));

            // Create window
            WidgetGameWindow window = new WidgetGameWindow(integrator, addEventSource,
                new Graphics.UI.WidgetPrototype(delegate(Initializer initializer2)
            {
                ViewState viewState = new ViewState
                {
                    Camera = new CameraViewState
                    {
                        Value = new Camera(new Vector(15.0, 15.0), 3.5) { ZoomVelocity = -1.0 },
                        LastUpdate = Time.Zero,
                    },
                    Mouse = default(Graphics.UI.Mouse<ImagePoint>),
                    Selection = Intel.StaticGroup.Empty
                };
                return (Graphics.UI.Widget)new Game.View(initializer2, viewState)
                {
                    Interface = (Intel.Interface)inteface
                };
            }));
            window.Poll = (Action<Transaction>)obs.Poll;

            // Run
            sim.Clock = new SignalClock(integrator, obs.InputTime);
            integrator.Clock = new RealClock(DateTime.Now);
            window.VSync = OpenTK.VSyncMode.On;
            window.Run();
        }

        private class _BreakTraceListener : TraceListener
        {
            public override void Fail(string message, string detailMessage)
            {
                Debugger.Break();
            }

            public override void Write(string message)
            {
                throw new NotImplementedException();
            }

            public override void WriteLine(string message)
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Calls the given action for all assemblies that are part of the program, including ones that are loaded
        /// later on. The action will only ever be called on one thread at a time.
        /// </summary>
        public static void AssemblyWatch(Action<System.Reflection.Assembly> Action)
        {
            // TODO
            Action(System.Reflection.Assembly.GetAssembly(typeof(Program)));
        }
    }
}
