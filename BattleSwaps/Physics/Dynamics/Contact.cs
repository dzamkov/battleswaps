﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Physics.Dynamics
{
    /// <summary>
    /// An actor describing a physical contact point between a free rigid body and the surface below it.
    /// </summary>
    public abstract class Contact : IActor<ContactState>
    {
        private readonly ExplicitSignal<bool> _IsSlipping;
        private Signal<FrameTransform> _LocalToWorld;
        private Signal<Surface> _Surface;
        private Signal<double> _Load;
        private Vector _Offset;
        private double _BaseStaticMu;
        private double _BaseKineticMu;
        private Signal<Vector> _Position;
        private static readonly InteriorRef<Signal<FrameTransform>> _LocalToWorldRef =
            (obj, act) => act(ref ((Contact)obj)._LocalToWorld);
        private static readonly InteriorRef<Signal<Surface>> _SurfaceRef =
            (obj, act) => act(ref ((Contact)obj)._Surface);
        private static readonly InteriorRef<Signal<double>> _LoadRef =
            (obj, act) => act(ref ((Contact)obj)._Load);
        private static readonly InteriorRef<Vector> _OffsetRef =
            (obj, act) => act(ref ((Contact)obj)._Offset);
        private static readonly InteriorRef<double> _BaseStaticMuRef =
            (obj, act) => act(ref ((Contact)obj)._BaseStaticMu);
        private static readonly InteriorRef<double> _BaseKineticMuRef =
            (obj, act) => act(ref ((Contact)obj)._BaseKineticMu);
        public Contact(Initializer Initializer, ContactState State)
        {
            this._IsSlipping = new ExplicitSignal<bool>(Initializer, State.IsSlipping);

            Property.Introduce(Ref.Create(this, _LocalToWorldRef));
            Property.Introduce(Ref.Create(this, _SurfaceRef));
            Property.Introduce(Ref.Create(this, _LoadRef));
            Property.Introduce(Ref.Create(this, _OffsetRef));
            Property.Introduce(Ref.Create(this, _BaseStaticMuRef));
            Property.Introduce(Ref.Create(this, _BaseKineticMuRef));
        }

        /// <summary>
        /// Gets or sets the surface the contact is on.
        /// </summary>
        public Property<Signal<Surface>> Surface
        {
            get
            {
                return Property.Get(Ref.Create(this, _SurfaceRef), ref this._Surface);
            }
            set
            {
                Property.Define(Ref.Create(this, _SurfaceRef), ref this._Surface, value);
            }
        }

        /// <summary>
        /// Set the physics world this contact is in.
        /// </summary>
        public Property<World> World
        {
            set
            {
                this.Surface = Property.From(delegate(out Signal<Surface> res)
                {
                    World world;
                    if (value.TryGetValue(out world))
                    {
                        Surface surface;
                        if (world.Surface.TryGetValue(out surface))
                        {
                            res = Signal.Static(surface);
                            return true;
                        }
                    }
                    res = default(Signal<Surface>);
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets or sets the transform from the local space of the body this contact is on to world space.
        /// </summary>
        public Property<Signal<FrameTransform>> LocalToWorld
        {
            get
            {
                return Property.Get(Ref.Create(this, _LocalToWorldRef), ref this._LocalToWorld);
            }
            set
            {
                Property.Define(Ref.Create(this, _LocalToWorldRef), ref this._LocalToWorld, value);
            }
        }

        /// <summary>
        /// Gets or sets the load on the contact.
        /// </summary>
        public Property<Signal<double>> Load
        {
            get
            {
                return Property.Get(Ref.Create(this, _LoadRef), ref this._Load);
            }
            set
            {
                Property.Define(Ref.Create(this, _LoadRef), ref this._Load, value);
            }
        }

        /// <summary>
        /// Sets the environment for this contact. This may be done in place of setting all environmental properties
        /// individually.
        /// </summary>
        public ContactEnvironment Environment
        {
            set
            {
                this.World = value.World;
                this.LocalToWorld = value.LocalToWorld;
                this.Load = value.Load;
            }
        }

        /// <summary>
        /// Gets or sets the offset of the contact on the body it is attached to.
        /// </summary>
        public Property<Vector> Offset
        {
            get
            {
                return Property.Get(Ref.Create(this, _OffsetRef), ref this._Offset);
            }
            set
            {
                Property.Define(Ref.Create(this, _OffsetRef), ref this._Offset, value);
            }
        }

        /// <summary>
        /// Gets or sets the coefficient of static friction of this contact prior to attenuated by the underlying
        /// surface.
        /// </summary>
        public Property<double> BaseStaticMu
        {
            get
            {
                return Property.Get(Ref.Create(this, _BaseStaticMuRef), ref this._BaseStaticMu);
            }
            set
            {
                Property.Define(Ref.Create(this, _BaseStaticMuRef), ref this._BaseStaticMu, value);
            }
        }

        /// <summary>
        /// Gets or sets the coefficient of kinetic friction of this contact prior to attenuation by the underlying
        /// surface.
        /// </summary>
        public Property<double> BaseKineticMu
        {
            get
            {
                return Property.Get(Ref.Create(this, _BaseKineticMuRef), ref this._BaseKineticMu);
            }
            set
            {
                Property.Define(Ref.Create(this, _BaseKineticMuRef), ref this._BaseKineticMu, value);
            }
        }

        /// <summary>
        /// Indicates whether this contact is slipping.
        /// </summary>
        public Signal<bool> IsSlipping
        {
            get
            {
                return this._IsSlipping;
            }
        }

        /// <summary>
        /// Gets the position of this contact in world space.
        /// </summary>
        public Property<Signal<Vector>> Position
        {
            get
            {
                if (this._Position != null)
                    return this._Position;
                return Property.From(delegate(out Signal<Vector> res)
                {
                    res = this._Position;
                    if (res != null)
                        return true;
                    Vector offset;
                    if (!this.Offset.TryGetValue(out offset))
                        return false;
                    Signal<FrameTransform> localToWorld;
                    if (!this.LocalToWorld.TryGetValue(out localToWorld))
                        return false;
                    res = localToWorld.Map(t => t.Spatial * offset);
                    res = Interlocked.CompareExchange(ref this._Position, res, null) ?? res;
                    return true;
                });
            }
        }

        /// <summary>
        /// Gets the impulse produced by this contact.
        /// </summary>
        /// <param name="Time">The amount of time that has passed since the last update</param>
        /// <param name="Angle">The angle of the body in world space</param>
        /// <param name="DeltaVelocity">The velocity of the contact in relation to the surface below it, in
        /// world space.</param>
        /// <param name="Mass">The mass of the attached body.</param>
        /// <param name="MomentOfInertia">The moment of inertia of the attached body.</param>
        /// <param name="Point">The point, in relation to the center of mass, where the impulse exerted by the
        /// contact will be applied.</param>
        /// <returns>The target impulse to be applied by the contact. This is subject to the maximum friction force the
        /// contact can exert.</returns>
        public abstract Vector GetImpulse(
            Transaction Transaction, double Time, Vector DeltaVelocity,
            double Mass, double MomentOfInertia, Vector Point);

        /// <summary>
        /// Advances the state of the contact and applies its effect on its containing body's motion.
        /// </summary>
        /// <param name="Angle">The angle of the attached body.</param>
        /// <param name="MassDistribution">The mass distribution of the attached body, in world space.</param>
        /// <param name="Vortex">A vortex, with origin at the center of mass of the body, that describes the
        /// velocity of all points on the body. This may be updated to apply the effect of the contact.</param>
        public void Advance(
            Transaction Transaction, double Time,
            MassDistribution MassDistribution, ref Vortex Vortex)
        {
            Vector pos = this.Position.RuntimeValue[Transaction];
            Vector point = pos - MassDistribution.Center;
            Surface surface = this.Surface.RuntimeValue[Transaction];
            Vector targetVel = surface.Motion[Transaction].GetVelocityAt(pos);
            Vector deltaVel = Vortex[point] - targetVel;

            // Compute effect of the contact
            Vector impulse = this.GetImpulse(
                Transaction, Time, deltaVel,
                MassDistribution.TotalMass, MassDistribution.MomentOfInertia, point);

            // Apply effect to motion
            bool isSlippingCur = this._IsSlipping[Transaction];
            double maxImpulse = Time * surface.MuAttenuation * this.Load.RuntimeValue[Transaction] *
                (isSlippingCur ? this.BaseKineticMu.RuntimeValue : this.BaseStaticMu.RuntimeValue);
            double sqrLen = impulse.SquareLength;
            bool isSlippingNext;
            if (sqrLen <= maxImpulse * maxImpulse)
            {
                isSlippingNext = false;
            }
            else
            {
                impulse *= maxImpulse / Math.Sqrt(sqrLen);
                isSlippingNext = true;
            }
            Vortex.ApplyImpulse(MassDistribution.TotalMass, MassDistribution.MomentOfInertia, point, impulse);

            // Update slipping
            if (isSlippingCur != isSlippingNext) this._IsSlipping[Transaction] = isSlippingNext;

            // TODO: apply opposite impulse to surface
        }
    }

    /// <summary>
    /// Describes the environment for a contact.
    /// </summary>
    public struct ContactEnvironment
    {
        /// <summary>
        /// The world the contact is in.
        /// </summary>
        public Property<World> World;

        /// <summary>
        /// The transform from the local space of the body the contact is attached to to world space.
        /// </summary>
        public Property<Signal<FrameTransform>> LocalToWorld;

        /// <summary>
        /// The load on the contact.
        /// </summary>
        public Property<Signal<double>> Load;
    }

    /// <summary>
    /// Describes the state of a contact at a particular reactive state.
    /// </summary>
    public struct ContactState
    {
        /// <summary>
        /// Indicates whether the contact is slipping. If so, kinetic friction will be applied instead of static
        /// friction.
        /// </summary>
        public bool IsSlipping;
    }

    /// <summary>
    /// A symmetrical contact that has no means of propulsion. This type of contact can be used to approximate
    /// flat surfaces and supports.
    /// </summary>
    public sealed class SimpleContact : Contact
    {
        public SimpleContact(Initializer Initializer, ContactState State)
            : base(Initializer, State)
        { }

        public override Vector GetImpulse(
            Transaction Transaction, double Time, Vector DeltaVelocity,
            double Mass, double MomentOfInertia, Vector Point)
        {
            return Vortex.ComputeImpulseAt(Mass, MomentOfInertia, Point, -DeltaVelocity);
        }
    }

    /// <summary>
    /// A contact that is rolling in one direction. Friction will only be applied in the perpendicular direction by
    /// default, but a propulsive force can be applied in the parallel direction.
    /// </summary>
    public sealed class WheelContact : Contact
    {
        private Signal<double> _Angle;
        private Signal<double> _Propulsion;
        private Signal<double> _Resistance;
        private static readonly InteriorRef<Signal<double>> _AngleRef =
            (obj, act) => act(ref ((WheelContact)obj)._Angle);
        private static readonly InteriorRef<Signal<double>> _PropulsionRef =
            (obj, act) => act(ref ((WheelContact)obj)._Propulsion);
        private static readonly InteriorRef<Signal<double>> _ResistanceRef =
            (obj, act) => act(ref ((WheelContact)obj)._Resistance);
        public WheelContact(
            Initializer Initializer,
            ContactState State)
            : base(Initializer, State)
        {
            Property.Introduce(Ref.Create(this, _AngleRef));
            Property.Introduce(Ref.Create(this, _PropulsionRef));
            Property.Introduce(Ref.Create(this, _ResistanceRef));
        }

        /// <summary>
        /// Gets or sets the relative angle of the wheel's parallel axis in relation to the body.
        /// </summary>
        public Property<Signal<double>> Angle
        {
            get
            {
                return Property.Get(Ref.Create(this, _AngleRef), ref this._Angle);
            }
            set
            {
                Property.Define(Ref.Create(this, _AngleRef), ref this._Angle, value);
            }
        }

        /// <summary>
        /// Gets or sets the target propulsive force applied by the wheel along its parallel direction. The actual
        /// applied force will never exceed the maximum allowed by friction.
        /// </summary>
        public Property<Signal<double>> Propulsion
        {
            get
            {
                return Property.Get(Ref.Create(this, _PropulsionRef), ref this._Propulsion);
            }
            set
            {
                Property.Define(Ref.Create(this, _PropulsionRef), ref this._Propulsion, value);
            }
        }

        /// <summary>
        /// Gets or sets the resistive force applied by the axle and brakes while the wheel is in motion. This should
        /// always be positive and will always be applied against the direction of motion. This should be set
        /// only once, before the contact is in use.
        /// </summary>
        public Property<Signal<double>> Resistance
        {
            get
            {
                return Property.Get(Ref.Create(this, _ResistanceRef), ref this._Resistance);
            }
            set
            {
                Property.Define(Ref.Create(this, _ResistanceRef), ref this._Resistance, value);
            }
        }

        public override Vector GetImpulse(
            Transaction Transaction, double Time, Vector DeltaVelocity,
            double Mass, double MomentOfInertia, Vector Point)
        {
            Vector worldDir = this.LocalToWorld.RuntimeValue[Transaction]
                .Spatial.ApplyVector(Vector.FromAngle(this._Angle[Transaction]));

            // Project delta velocity to be perpendicular to the wheel (we want to stop movement
            // along this direction)
            Vector worldAcrossDir = worldDir.Cross;
            Vector deltaVelAcross = worldAcrossDir * Vector.Dot(worldAcrossDir, DeltaVelocity);

            // Determine the impulse needed to stop the wheel's perpendicular motion
            Vector stopImpluseAcross = Vortex.ComputeImpulseAt(Mass, MomentOfInertia, Point, -deltaVelAcross);
            double across = Vector.Dot(worldAcrossDir, stopImpluseAcross);

            // Apply propulsion
            double along = this.Propulsion.RuntimeValue[Transaction] * Time;

            // Apply resistance
            double maxAlong = this.Resistance.RuntimeValue[Transaction] * Time;
            if (maxAlong > 0.0)
            {
                Vector deltaVelAlong = DeltaVelocity - deltaVelAcross;
                Vector stopImpulseAlong = Vortex.ComputeImpulseAt(Mass, MomentOfInertia, Point, -deltaVelAlong);
                double stopAlong = Vector.Dot(worldDir, stopImpulseAlong) - along;
                if (Math.Abs(stopAlong) <= maxAlong)
                    along += stopAlong;
                else
                    along += stopAlong > 0.0 ? maxAlong : -maxAlong;
            }

            return worldDir * along + worldAcrossDir * across;
        }
    }
}
