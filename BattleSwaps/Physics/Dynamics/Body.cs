﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Physics.Dynamics
{
    /// <summary>
    /// Describes an object moving through 2D space to which impulses and forces can be applied. Different kinds of
    /// bodies may have different constraints on motion and internal representations of motion properties.
    /// </summary>
    public abstract class Body
    {
        private Attachment _Attachment;

        /// <summary>
        /// Gets or sets the attachment for this body. This should be set immediately after construction of the body.
        /// The attachment specifies the mass and weight distribution of this body, and may transmit impulses and
        /// forces.
        /// </summary>
        public Attachment Attachment
        {
            get
            {
                Debug.Assert(this._Attachment != null);
                return this._Attachment;
            }
            set
            {
                Debug.Assert(this._Attachment == null);
                this._Attachment = value;
                value.Parent = this;
            }
        }

        /// <summary>
        /// Sets the attachments for this body. This is a shortcut for setting the attachment of this body
        /// to a compound attachment.
        /// </summary>
        public Attachment[] Attachments
        {
            set
            {
                this.Attachment = new CompoundAttachment { Components = value };
            }
        }

        /// <summary>
        /// Gets the mass distribution of the contents of this body in world space.
        /// </summary>
        public Property<Signal<MassDistribution>> MassDistribution
        {
            get
            {
                return this.Attachment.TransmittedMassDistribution;
            }
        }

        /// <summary>
        /// Gets the weight distribution of the contents of this body in local space.
        /// </summary>
        public Property<Signal<WeightDistribution>> LocalWeightDistribution
        {
            get
            {
                return this.Attachment.TransmittedWeightDistribution;
            }
        }

        /// <summary>
        /// Gets the transform from the local frame of reference of the body to the frame of reference
        /// of the world (which is inertial).
        /// </summary>
        public abstract Property<Signal<FrameTransform>> LocalToWorld { get; }

        /// <summary>
        /// Gets the world this body is in.
        /// </summary>
        public abstract Property<World> ContainingWorld { get; }

        /// <summary>
        /// Applies an impulse to this body at some point in world space.
        /// </summary>
        public abstract void ApplyImpulse(Transaction Transaction, PointImpulse Impulse);
    }

    /// <summary>
    /// Describes a physics object with inertial mass and weight that has its motion constrained in some way to
    /// follow a body.
    /// </summary>
    public abstract class Attachment
    {
        private Body _Parent;
        private static readonly InteriorRef<Body> _ParentRef =
            (obj, act) => act(ref ((Attachment)obj)._Parent);
        public Attachment()
        {
            Property.Introduce(Ref.Create(this, _ParentRef));
        }

        /// <summary>
        /// Gets or sets the parent body for this attachment.
        /// </summary>
        public Property<Body> Parent
        {
            get
            {
                return Property.Get(Ref.Create(this, _ParentRef), ref this._Parent);
            }
            set
            {
                Property.Define(Ref.Create(this, _ParentRef), ref this._Parent, value);
            }
        }

        /// <summary>
        /// Gets the mass distribution of this attachment as felt by its parent body, in world space.
        /// </summary>
        public abstract Property<Signal<MassDistribution>> TransmittedMassDistribution { get; }

        /// <summary>
        /// Gets the weight distribution of this attachment as felt by its parent body, in the local space of the
        /// body.
        /// </summary>
        public abstract Property<Signal<WeightDistribution>> TransmittedWeightDistribution { get; }

        /// <summary>
        /// Creates an attachment for a fixed mass with the given distribution.
        /// </summary>
        public static Attachment CreateFixedMass(MassDistribution MassDistribution)
        {
            return new MassAttachment { MassDistribution = MassDistribution };
        }
    }

    /// <summary>
    /// An attachment consisting of a fixed set of component attachments.
    /// </summary>
    public sealed class CompoundAttachment : Attachment
    {
        private Attachment[] _Components;
        private Signal<MassDistribution> _TransmittedMassDistribution;
        private Signal<WeightDistribution> _TransmittedWeightDistribution;

        /// <summary>
        /// Gets or sets the components of this compound attachment body. This should be set immediately after
        /// construction of the attachment.
        /// </summary>
        public Attachment[] Components
        {
            get
            {
                Debug.Assert(this._Components != null);
                return this._Components;
            }
            set
            {
                Debug.Assert(this._Components == null);
                this._Components = value;
                foreach (var comp in this._Components)
                    comp.Parent = this.Parent;
            }
        }

        public override Property<Signal<MassDistribution>> TransmittedMassDistribution
        {
            get
            {
                if (this._TransmittedMassDistribution != null)
                    return this._TransmittedMassDistribution;
                return Property.From(delegate(out Signal<MassDistribution> res)
                {
                    res = this._TransmittedMassDistribution;
                    if (res != null)
                        return true;

                    List<Signal<MassDistribution>> massDists = new List<Signal<MassDistribution>>();
                    foreach (var comp in this.Components)
                    {
                        Signal<MassDistribution> massDist;
                        if (!comp.TransmittedMassDistribution.TryGetValue(out massDist))
                            return false;
                        massDists.Add(massDist);
                    }

                    res = Signal.Reduce((a, b) => a + b, massDists);
                    res = Interlocked.CompareExchange(ref this._TransmittedMassDistribution, res, null) ?? res;
                    return true;
                });
            }
        }

        public override Property<Signal<WeightDistribution>> TransmittedWeightDistribution
        {
            get
            {
                if (this._TransmittedWeightDistribution != null)
                    return this._TransmittedWeightDistribution;
                return Property.From(delegate(out Signal<WeightDistribution> res)
                {
                    res = this._TransmittedWeightDistribution;
                    if (res != null)
                        return true;

                    List<Signal<WeightDistribution>> weightDists = new List<Signal<WeightDistribution>>();
                    foreach (var comp in this.Components)
                    {
                        Signal<WeightDistribution> weightDist;
                        if (!comp.TransmittedWeightDistribution.TryGetValue(out weightDist))
                            return false;
                        weightDists.Add(weightDist);
                    }

                    res = Signal.Reduce((a, b) => a + b, weightDists);
                    res = Interlocked.CompareExchange(ref this._TransmittedWeightDistribution, res, null) ?? res;
                    return true;
                });
            }
        }
    }

    /// <summary>
    /// An attachment for a fixed mass distribution.
    /// </summary>
    public sealed class MassAttachment : Attachment
    {
        private MassDistribution _MassDistribution;
        private Signal<MassDistribution> _TransmittedMassDistribution;
        private Signal<WeightDistribution> _TransmittedWeightDistribution;
        private static readonly InteriorRef<MassDistribution> _MassDistributionRef =
            (obj, act) => act(ref ((MassAttachment)obj)._MassDistribution);
        public MassAttachment()
        {
            Property.Introduce(Ref.Create(this, _MassDistributionRef));
        }

        /// <summary>
        /// Gets or sets the mass distribution of this attachment in the local space of its attached body.
        /// </summary>
        public Property<MassDistribution> MassDistribution
        {
            get
            {
                return Property.Get(Ref.Create(this, _MassDistributionRef), ref this._MassDistribution);
            }
            set
            {
                Property.Define(Ref.Create(this, _MassDistributionRef), ref this._MassDistribution, value);
            }
        }

        public override Property<Signal<MassDistribution>> TransmittedMassDistribution
        {
            get
            {
                if (this._TransmittedMassDistribution != null)
                    return this._TransmittedMassDistribution;
                return Property.From(delegate(out Signal<MassDistribution> res)
                {
                    res = this._TransmittedMassDistribution;
                    if (res != null)
                        return true;

                    MassDistribution localMassDist;
                    if (!this.MassDistribution.TryGetValue(out localMassDist))
                        return false;

                    Body parent;
                    if (!this.Parent.TryGetValue(out parent))
                        return false;

                    Signal<FrameTransform> localToWorld;
                    if (!parent.LocalToWorld.TryGetValue(out localToWorld))
                        return false;

                    res = localToWorld.Map(f => f.Spatial * localMassDist);
                    res = Interlocked.CompareExchange(ref this._TransmittedMassDistribution, res, null) ?? res;
                    return true;
                });
            }
        }

        public override Property<Signal<WeightDistribution>> TransmittedWeightDistribution
        {
            get
            {
                if (this._TransmittedWeightDistribution != null)
                    return this._TransmittedWeightDistribution;
                return Property.From(delegate(out Signal<WeightDistribution> res)
                {
                    res = this._TransmittedWeightDistribution;
                    if (res != null)
                        return true;

                    MassDistribution localMassDist;
                    if (!this.MassDistribution.TryGetValue(out localMassDist))
                        return false;

                    Body parent;
                    if (!this.Parent.TryGetValue(out parent))
                        return false;

                    World world;
                    if (!parent.ContainingWorld.TryGetValue(out world))
                        return false;

                    double gravity;
                    if (!world.Gravity.TryGetValue(out gravity))
                        return false;

                    res = WeightDistribution.FromGravity(gravity, localMassDist);
                    res = Interlocked.CompareExchange(ref this._TransmittedWeightDistribution, res, null) ?? res;
                    return true;
                });
            }
        }
    }

    /// <summary>
    /// Describes the distribution of mass in a collection of matter.
    /// </summary>
    public struct MassDistribution
    {
        public MassDistribution(double TotalMass, double MomentOfInertia, Vector CenterOfMass)
        {
            this.TotalMass = TotalMass;
            this.MomentOfInertia = MomentOfInertia;
            this.Center = CenterOfMass;
        }

        /// <summary>
        /// A mass distribution with no mass.
        /// </summary>
        public static readonly MassDistribution Zero = new MassDistribution(0.0, 0.0, Vector.Zero);

        /// <summary>
        /// The total mass in this distribution.
        /// </summary>
        public double TotalMass;

        /// <summary>
        /// The moment of inertia for this distribution about the center of mass.
        /// </summary>
        public double MomentOfInertia;

        /// <summary>
        /// The location of the center of mass for the distribution.
        /// </summary>
        public Vector Center;

        /// <summary>
        /// Shrinks this mass distribution to a point.
        /// </summary>
        public MassDistribution AsPoint()
        {
            return new MassDistribution(this.TotalMass, 0.0, this.Center);
        }

        /// <summary>
        /// Gets the moment of inertia of this mass distribution when rotating about the given point.
        /// </summary>
        public double GetMomentOfInertia(Vector Point)
        {
            return this.MomentOfInertia + this.TotalMass * (this.Center - Point).SquareLength;
        }

        /// <summary>
        /// Gets the mass distribution for the given rectangle with the given total mass.
        /// </summary>
        public static MassDistribution Rectangle(double TotalMass, Rectangle Rectangle)
        {
            return new MassDistribution(TotalMass,
                (TotalMass / 12.0) * (Rectangle.Transform.X.SquareLength + Rectangle.Transform.Y.SquareLength),
                Rectangle.Center);
        }

        public static MassDistribution operator *(Transform Transform, MassDistribution Mass)
        {
            Debug.Assert(Transform.IsIsometric);
            return new MassDistribution(Mass.TotalMass, Mass.MomentOfInertia, Transform * Mass.Center);
        }

        public static MassDistribution operator +(MassDistribution A, MassDistribution B)
        {
            double mass = A.TotalMass + B.TotalMass;
            Vector center = (A.Center * A.TotalMass + B.Center * B.TotalMass) / mass;
            double moment = A.GetMomentOfInertia(center) + B.GetMomentOfInertia(center);
            return new MassDistribution(mass, moment, center);
        }
    }

    /// <summary>
    /// Describes the distribution of weight (downward force) in a collection of matter.
    /// </summary>
    public struct WeightDistribution
    {
        public WeightDistribution(double TotalWeight, Vector Center)
        {
            this.TotalWeight = TotalWeight;
            this.Center = Center;
        }

        /// <summary>
        /// A weight distribution with no weight.
        /// </summary>
        public static readonly WeightDistribution Zero = new WeightDistribution(0.0, Vector.Zero);

        /// <summary>
        /// The total downward force in this distribution.
        /// </summary>
        public double TotalWeight;

        /// <summary>
        /// The location of the center of this weight distribution.
        /// </summary>
        public Vector Center;

        /// <summary>
        /// Gets the weight distribution of the given mass distribution in a world with the given gravitional
        /// constant.
        /// </summary>
        public static WeightDistribution FromGravity(double Gravity, MassDistribution Mass)
        {
            return new WeightDistribution(Mass.TotalMass * Gravity, Mass.Center);
        }

        public static WeightDistribution operator +(WeightDistribution A, WeightDistribution B)
        {
            double weight = A.TotalWeight + B.TotalWeight;
            Vector center = (A.Center * A.TotalWeight + B.Center * B.TotalWeight) / weight;
            return new WeightDistribution(weight, center);
        }

        public static WeightDistribution operator *(Transform Transform, WeightDistribution Weight)
        {
            Debug.Assert(Transform.IsIsometric);
            return new WeightDistribution(Weight.TotalWeight, Weight.Center);
        }
    }
}
