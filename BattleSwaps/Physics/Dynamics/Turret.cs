﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Physics.Dynamics
{
    /// <summary>
    /// A rigid physics body that is constrained to rotate about its local origin which is mapped to a fixed point
    /// (the pivot) on a parent body.
    /// </summary>
    public sealed class TurretBody : Body, IActor<TurretBodyState>
    {
        private readonly ExplicitSignal<TurretMotionSpan> _MotionSpan;
        private Name _Name;
        private Body _Parent;
        private Vector _Offset;
        private Signal<double> _Propulsion;
        private Signal<double> _Resistance;
        private Signal<FrameVector> _Pivot;
        private Signal<TurretMotion> _Motion;
        private Signal<FrameTransform> _LocalToWorld;
        private Bag<Event> _PendingEvents;
        private static readonly InteriorRef<Name> _NameRef =
            (obj, act) => act(ref ((TurretBody)obj)._Name);
        private static readonly InteriorRef<Body> _ParentRef =
            (obj, act) => act(ref ((TurretBody)obj)._Parent);
        private static readonly InteriorRef<Vector> _OffsetRef =
            (obj, act) => act(ref ((TurretBody)obj)._Offset);
        private static readonly InteriorRef<Signal<double>> _PropulsionRef =
            (obj, act) => act(ref ((TurretBody)obj)._Propulsion);
        private static readonly InteriorRef<Signal<double>> _ResistanceRef =
            (obj, act) => act(ref ((TurretBody)obj)._Resistance);
        public TurretBody(Initializer Initializer, TurretBodyState State)
        {
            this._MotionSpan = new ExplicitSignal<TurretMotionSpan>(Initializer, State.MotionSpan);

            Property.Introduce(Ref.Create(this, _NameRef));
            Property.Introduce(Ref.Create(this, _ParentRef));
            Property.Introduce(Ref.Create(this, _OffsetRef));
            Property.Introduce(Ref.Create(this, _PropulsionRef));
            Property.Introduce(Ref.Create(this, _ResistanceRef));
        }

        /// <summary>
        /// Gets or sets the name of this turret body for tie-breaking purposes.
        /// </summary>
        public Property<Name> Name
        {
            get
            {
                return Property.Get(Ref.Create(this, _NameRef), ref this._Name);
            }
            set
            {
                Property.Define(Ref.Create(this, _NameRef), ref this._Name, value);
            }
        }

        /// <summary>
        /// Gets or sets the parent body for this turret.
        /// </summary>
        public Property<Body> Parent
        {
            get
            {
                return Property.Get(Ref.Create(this, _ParentRef), ref this._Parent);
            }
            set
            {
                Property.Define(Ref.Create(this, _ParentRef), ref this._Parent, value);
            }
        }

        /// <summary>
        /// Gets or sets the offset of the pivot of this turret on its parent body.
        /// </summary>
        public Property<Vector> Offset
        {
            get
            {
                return Property.Get(Ref.Create(this, _OffsetRef), ref this._Offset);
            }
            set
            {
                Property.Define(Ref.Create(this, _OffsetRef), ref this._Offset, value);
            }
        }

        /// <summary>
        /// Sets the environment for this turret. This can be used in place of setting environmental properties
        /// individually.
        /// </summary>
        public TurretEnvironment Environment
        {
            set
            {
                this.Parent = value.Parent;
                this.Offset = value.Offset;
            }
        }

        /// <summary>
        /// Gets the position of the pivot for this turret in world space.
        /// </summary>
        public Property<Signal<FrameVector>> Pivot
        {
            get
            {
                if (this._Pivot != null)
                    return this._Pivot;
                return Property.From(delegate(out Signal<FrameVector> res)
                {
                    res = this._Pivot;
                    if (res != null)
                        return true;

                    Vector offset;
                    if (!this.Offset.TryGetValue(out offset))
                        return false;

                    Body parent;
                    Signal<FrameTransform> parentLocalToWorld;
                    if (!this.Parent.TryGetValue(out parent) ||
                        !parent.LocalToWorld.TryGetValue(out parentLocalToWorld))
                        return false;

                    res = parentLocalToWorld.Map(f => f * FrameVector.Static(offset));
                    res = Interlocked.CompareExchange(ref this._Pivot, res, null) ?? res;
                    return true;
                });
            }
        }
        
        /// <summary>
        /// Gets or sets the propulsive torque applied to the turret.
        /// </summary>
        public Property<Signal<double>> Propulsion
        {
            get
            {
                return Property.Get(Ref.Create(this, _PropulsionRef), ref this._Propulsion);
            }
            set
            {
                Property.Define(Ref.Create(this, _PropulsionRef), ref this._Propulsion, value);
            }
        }

        /// <summary>
        /// Gets or sets the maximum resistive torque applied to the turret. This should be positive and will always be
        /// applied against the direction of motion.
        /// </summary>
        public Property<Signal<double>> Resistance
        {
            get
            {
                return Property.Get(Ref.Create(this, _ResistanceRef), ref this._Resistance);
            }
            set
            {
                Property.Define(Ref.Create(this, _ResistanceRef), ref this._Resistance, value);
            }
        }

        /// <summary>
        /// Gets the motion properties of this body over time.
        /// </summary>
        public Signal<TurretMotion> Motion
        {
            get
            {
                Signal<TurretMotion> value = this._Motion;
                if (value == null)
                {
                    value = Signal.Sample<TurretMotionSpan, TurretMotion>(this._MotionSpan);
                    value = Interlocked.CompareExchange(ref this._Motion, value, null) ?? value;
                };
                return value;
            }
        }

        /// <summary>
        /// Given the name of a turret body, gets the name of the update events it produces.
        /// </summary>
        private static readonly Func<Name, Name> _UpdateName =
            BattleSwaps.Name.GetDecorator(new Name(5003902450103674366));

        /// <summary>
        /// Gets the event source for this body.
        /// </summary>
        public Property<Bag<Event>> PendingEvents
        {
            get
            {
                if (this._PendingEvents != null)
                    return this._PendingEvents;
                return Property.From(delegate(out Bag<Event> res)
                {
                    res = this._PendingEvents;
                    if (res != null)
                        return true;

                    Name bodyName;
                    if (this.Name.TryGetValue(out bodyName))
                    {
                        var updateName = _UpdateName(bodyName);
                        res = Bag.Singleton(this._MotionSpan.Map(
                            s => new Event(s.Start + Time.FromSeconds(0.05),
                                updateName, this._Advance)));
                        res = Interlocked.CompareExchange(ref this._PendingEvents, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        /// <summary>
        /// Advances the internal state for the body.
        /// </summary>
        public void _Advance(Transaction Transaction)
        {
            // TODO: Respond to impulses of parent
            TurretMotionSpan curSpan = this._MotionSpan[Transaction];
            TurretMotion cur = curSpan[Transaction.Time];
            Vector curPivot = this.Pivot.RuntimeValue[Transaction].Spatial;
            MassDistribution curMassDist = this.MassDistribution.RuntimeValue[Transaction];
            double baseAngularVel = this._Parent.LocalToWorld.RuntimeValue[Transaction].AngularVelocity;
            double moment = curMassDist.GetMomentOfInertia(curPivot);
            double delta = (Transaction.Time - curSpan.Start).Seconds;

            // Apply propulsion and resistance
            double dangVelProp = this.Propulsion.RuntimeValue[Transaction] * (delta / moment);
            double dangVelRes = this.Resistance.RuntimeValue[Transaction] * (delta / moment);
            cur.AngularVelocity += dangVelProp;
            if (Math.Abs(cur.AngularVelocity - baseAngularVel) < dangVelRes)
                cur.AngularVelocity = baseAngularVel;
            else if (cur.AngularVelocity > baseAngularVel)
                cur.AngularVelocity -= dangVelRes;
            else
                cur.AngularVelocity += dangVelRes;

            // TODO: Reactionary torque

            Debug.Assert(!double.IsNaN(cur.Angle));
            Debug.Assert(!double.IsNaN(cur.AngularVelocity));
            this._MotionSpan[Transaction] = new TurretMotionSpan(Transaction.Time, cur);
        }

        /// <summary>
        /// Creates an attachment for this turret. This allows the turret to be placed on a body.
        /// </summary>
        public TurretAttachment CreateAttachment(Vector Offset)
        {
            return new TurretAttachment
            {
                Turret = this,
                Offset = Offset
            };
        }

        public override Property<Signal<FrameTransform>> LocalToWorld
        {
            get
            {
                if (this._LocalToWorld != null)
                    return this._LocalToWorld;
                return Property.From(delegate(out Signal<FrameTransform> res)
                {
                    res = this._LocalToWorld;
                    if (res != null)
                        return true;

                    Vector offset;
                    Body parent;
                    Signal<FrameTransform> parentLocalToWorld;
                    if (!this.Offset.TryGetValue(out offset) ||
                        !this.Parent.TryGetValue(out parent) ||
                        !parent.LocalToWorld.TryGetValue(out parentLocalToWorld))
                        return false;

                    res = Signal.Lift(this.Motion, parentLocalToWorld, (m, p) => m.GetLocalToWorld(p, offset));
                    res = Interlocked.CompareExchange(ref this._LocalToWorld, res, null) ?? res;
                    return true;
                });
            }
        }

        public override Property<World> ContainingWorld
        {
            get
            {
                return Property.From(delegate(out World world)
                {
                    Body parent;
                    if (this.Parent.TryGetValue(out parent) && parent.ContainingWorld.TryGetValue(out world))
                        return true;
                    world = default(World);
                    return false;
                });
            }
        }

        public override void ApplyImpulse(Transaction Transaction, PointImpulse Impulse)
        {
            // TODO: Change angular velocity due to impulse
            this.Parent.RuntimeValue.ApplyImpulse(Transaction,
                new PointImpulse(this.Pivot.RuntimeValue[Transaction].Spatial, Impulse.Impulse));
        }
    }

    /// <summary>
    /// The environment for a turret body.
    /// </summary>
    public struct TurretEnvironment
    {
        /// <summary>
        /// The parent body the turret is attached to.
        /// </summary>
        public Property<Body> Parent;

        /// <summary>
        /// The offset of the turret on its body.
        /// </summary>
        public Property<Vector> Offset;
    }

    /// <summary>
    /// Describes the state of a turret body at a particular reactive state.
    /// </summary>
    public struct TurretBodyState
    {
        public TurretBodyState(TurretMotionSpan MotionSpan)
        {
            this.MotionSpan = MotionSpan;
        }

        /// <summary>
        /// The span that describes the motion of the turret body.
        /// </summary>
        public TurretMotionSpan MotionSpan;

        /// <summary>
        /// Constructs a state for a motionless turret body.
        /// </summary>
        public static TurretBodyState Static(double Angle)
        {
            return new TurretBodyState(new TurretMotionSpan(Time.Zero, new TurretMotion(Angle, 0.0)));
        }
    }

    /// <summary>
    /// An attachment consisting of a turret body.
    /// </summary>
    public sealed class TurretAttachment : Attachment
    {
        private TurretBody _Turret;
        private Vector _Offset;
        private Signal<MassDistribution> _TransmittedMassDistribution;
        private Signal<WeightDistribution> _TransmittedWeightDistribution;
        private static readonly InteriorRef<Vector> _OffsetRef =
            (obj, act) => act(ref ((TurretAttachment)obj)._Offset);
        public TurretAttachment()
        {
            Property.Introduce(Ref.Create(this, _OffsetRef));
        }

        /// <summary>
        /// Gets or sets the turret body for this attachment. This should be set immediately after creation of
        /// the attachment.
        /// </summary>
        public TurretBody Turret
        {
            get
            {
                Debug.Assert(this._Turret != null);
                return this._Turret;
            }
            set
            {
                Debug.Assert(this._Turret == null);
                this._Turret = value;
                this._Turret.Environment = new TurretEnvironment
                {
                    Parent = this.Parent,
                    Offset = this.Offset
                };
            }
        }

        /// <summary>
        /// Gets or sets the offset of the pivot of the turret in the local space of the parent body.
        /// </summary>
        public Property<Vector> Offset
        {
            get
            {
                return Property.Get(Ref.Create(this, _OffsetRef), ref this._Offset);
            }
            set
            {
                Property.Define(Ref.Create(this, _OffsetRef), ref this._Offset, value);
            }
        }

        public override Property<Signal<MassDistribution>> TransmittedMassDistribution
        {
            get
            {
                if (this._TransmittedMassDistribution != null)
                    return this._TransmittedMassDistribution;
                return Property.From(delegate(out Signal<MassDistribution> res)
                {
                    res = this._TransmittedMassDistribution;
                    if (res != null)
                        return true;

                    Signal<MassDistribution> source;
                    if (this.Turret.MassDistribution.TryGetValue(out source))
                    {
                        res = source.Map(m => m.AsPoint());
                        res = Interlocked.CompareExchange(ref this._TransmittedMassDistribution, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        public override Property<Signal<WeightDistribution>> TransmittedWeightDistribution
        {
            get
            {
                if (this._TransmittedWeightDistribution != null)
                    return this._TransmittedWeightDistribution;
                return Property.From(delegate(out Signal<WeightDistribution> res)
                {
                    res = this._TransmittedWeightDistribution;
                    if (res != null)
                        return true;

                    Vector offset;
                    Signal<WeightDistribution> source;
                    if (this.Offset.TryGetValue(out offset) &&
                        this.Turret.LocalWeightDistribution.TryGetValue(out source))
                    {
                        res = source.Map(weightDist => new WeightDistribution(weightDist.TotalWeight, offset));
                        res = Interlocked.CompareExchange(ref this._TransmittedWeightDistribution, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }
    }

    /// <summary>
    /// Describes the motion properties of a turret.
    /// </summary>
    public struct TurretMotion
    {
        public TurretMotion(double Angle, double AngularVelocity)
        {
            this.Angle = Angle;
            this.AngularVelocity = AngularVelocity;
        }

        /// <summary>
        /// The angle of the turret.
        /// </summary>
        public double Angle;

        /// <summary>
        /// The angular velocity of the turret.
        /// </summary>
        public double AngularVelocity;

        /// <summary>
        /// Gets the transform from the local frame of the turret to the world frame.
        /// </summary>
        public FrameTransform GetLocalToWorld(Vector Pivot, Vector PivotVelocity)
        {
            return new FrameTransform(
                Transform.Translation(Pivot) * Transform.Rotation(this.Angle),
                new Vortex(PivotVelocity, this.AngularVelocity));
        }

        /// <summary>
        /// Gets the transform from the local frame of the turret to the world frame.
        /// </summary>
        public FrameTransform GetLocalToWorld(FrameTransform ParentToWorld, Vector PivotOffset)
        {
            Vector pivot = ParentToWorld.Spatial * PivotOffset;
            return this.GetLocalToWorld(pivot, ParentToWorld.GetVelocityAt(PivotOffset));
        }
    }

    /// <summary>
    /// Describes the progression of turret motion properties over time.
    /// </summary>
    public struct TurretMotionSpan : ISpan<TurretMotion>
    {
        public TurretMotionSpan(Time Start, TurretMotion Motion)
        {
            this.Start = Start;
            this.Motion = Motion;
        }

        /// <summary>
        /// The start time for this span.
        /// </summary>
        public Time Start;

        /// <summary>
        /// The motion properties of this span at the reference time.
        /// </summary>
        public TurretMotion Motion;

        /// <summary>
        /// Gets the motion properties of this span at the given time.
        /// </summary>
        public TurretMotion this[Time Time]
        {
            get
            {
                double delta = (Time - this.Start).Seconds;
                return new TurretMotion(
                    Vector.NormalizeAngle(this.Motion.Angle + delta * this.Motion.AngularVelocity),
                    this.Motion.AngularVelocity);
            }
        }

        /// <summary>
        /// Gets the end time for this span.
        /// </summary>
        public Time End
        {
            get
            {
                return Time.MaxValue;
            }
        }

        Time ISpan<TurretMotion>.Start
        {
            get { return this.Start; }
        }
    }
}
