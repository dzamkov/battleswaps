﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Reactive;

namespace BattleSwaps.Physics.Dynamics
{
    /// <summary>
    /// Identifies and describes a dynamics-related physics world.
    /// </summary>
    public sealed class World
    {
        private double _Gravity;
        private Surface _Surface;
        private Bag<CollisionSource> _CollisionSources;
        private Bag<Event> _PendingEvents;
        private static readonly InteriorRef<double> _GravityRef =
            (obj, act) => act(ref ((World)obj)._Gravity);
        private static readonly InteriorRef<Surface> _SurfaceRef =
            (obj, act) => act(ref ((World)obj)._Surface);
        private static readonly InteriorRef<Bag<CollisionSource>> _CollisionSourcesRef =
            (obj, act) => act(ref ((World)obj)._CollisionSources);
        public World(Initializer Initializer)
        {
            Property.Introduce(Ref.Create(this, _GravityRef));
            Property.Introduce(Ref.Create(this, _SurfaceRef));
            Property.Introduce(Ref.Create(this, _CollisionSourcesRef));
        }

        /// <summary>
        /// Gets or sets the gravitational acceleration for the world.
        /// </summary>
        public Property<double> Gravity
        {
            get
            {
                return Property.Get(Ref.Create(this, _GravityRef), ref this._Gravity);
            }
            set
            {
                Property.Define(Ref.Create(this, _GravityRef), ref this._Gravity, value);
            }
        }
        
        /// <summary>
        /// Gets or sets the surface for the world.
        /// </summary>
        public Property<Surface> Surface
        {
            get
            {
                return Property.Get(Ref.Create(this, _SurfaceRef), ref this._Surface);
            }
            set
            {
                Property.Define(Ref.Create(this, _SurfaceRef), ref this._Surface, value);
            }
        }

        /// <summary>
        /// Gets or sets the collision sources for the world.
        /// </summary>
        public Property<Bag<CollisionSource>> CollisionSources
        {
            get
            {
                return Property.Get(Ref.Create(this, _CollisionSourcesRef), ref this._CollisionSources);
            }
            set
            {
                Property.Define(Ref.Create(this, _CollisionSourcesRef), ref this._CollisionSources, value);
            }
        }

        /// <summary>
        /// Gets the pending events for this world.
        /// </summary>
        public Property<Bag<Event>> PendingEvents
        {
            get
            {
                if (this._PendingEvents != null)
                    return this._PendingEvents;
                return Property.From(delegate(out Bag<Event> res)
                {
                    res = this._PendingEvents;
                    if (res != null)
                        return true;
                    Bag<CollisionSource> collisionSources;
                    if (!this.CollisionSources.TryGetValue(out collisionSources))
                        return false;
                    res = Collision.ResolutionsWithin(collisionSources);
                    res = Interlocked.CompareExchange(ref this._PendingEvents, res, null) ?? res;
                    return true;
                });
            }
        }
    }

    /// <summary>
    /// A full static description of a dynamics world at a particular reactive state. This can be used to serialize
    /// worlds.
    /// </summary>
    public struct WorldPrototype
    {
        /// <summary>
        /// The gravitational acceleration of the world.
        /// </summary>
        public double Gravity;

        /// <summary>
        /// The surface of the world.
        /// </summary>
        public Surface Surface;

        /// <summary>
        /// Creates an instance of the world described by this prototype.
        /// </summary>
        public World Instantiate(Initializer Initializer)
        {
            return new World(Initializer)
            {
                Gravity = this.Gravity,
                Surface = this.Surface
            };
        }
    }
}
