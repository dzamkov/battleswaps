﻿using System;
using System.Collections.Generic;

using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Physics.Dynamics
{
    /// <summary>
    /// Describes the motion properties of a surface.
    /// </summary>
    public struct SurfaceMotion
    {
        /// <summary>
        /// The absolute velocity of the surface.
        /// </summary>
        public Vector Velocity;

        /// <summary>
        /// The location of the center of the surface. This is only relevant when the surface is rotating.
        /// </summary>
        public Vector Center;

        /// <summary>
        /// The angular velocity of the surface about its center.
        /// </summary>
        public double AngularVelocity;

        /// <summary>
        /// Gets the velocity of a point on the surface.
        /// </summary>
        public Vector GetVelocityAt(Vector Point)
        {
            return this.Velocity - (Point - this.Center).Cross * this.AngularVelocity;
        }

        /// <summary>
        /// Motion for a surface that doesn't move.
        /// </summary>
        public static readonly SurfaceMotion Zero = new SurfaceMotion
        {
            Velocity = Vector.Zero,
            Center = Vector.Zero,
            AngularVelocity = 0.0
        };
    }

    /// <summary>
    /// Describes a surface that unconstrained rigid bodies can rest on.
    /// </summary>
    public struct Surface
    {
        /// <summary>
        /// The motion properties of this surface.
        /// </summary>
        public Signal<SurfaceMotion> Motion;

        /// <summary>
        /// The attenuation factor applied to the friction coefficients of contacts on this surface. This
        /// should be between 0 and 1.
        /// </summary>
        public double MuAttenuation;

        /// <summary>
        /// A surface that has perfect traction and doesn't move.
        /// </summary>
        public static readonly Surface Default = new Surface
        {
            Motion = SurfaceMotion.Zero,
            MuAttenuation = 1.0
        };
    }
}
