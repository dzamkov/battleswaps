﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Physics.Dynamics
{
    /// <summary>
    /// A rigid physics body that is free to move in 2D space using contacts to interact with the surface below it.
    /// </summary>
    public sealed class FreeBody : Body, IActor<FreeBodyState>
    {
        private readonly ExplicitSignal<FreeMotionSpan> _MotionSpan;
        private Name _Name;
        private World _World;
        private Contact[] _Contacts;
        private Signal<FrameTransform> _LocalToWorld;
        private Signal<double[]> _ContactLoads;
        private Signal<IFrameTransformSpan> _FrameTransformSpan;
        private Bag<Event> _PendingEvents;
        private static readonly InteriorRef<Name> _NameRef =
            (obj, act) => act(ref ((FreeBody)obj)._Name);
        private static readonly InteriorRef<World> _WorldRef =
            (obj, act) => act(ref ((FreeBody)obj)._World);
        public FreeBody(Initializer Initializer, FreeBodyState State)
        {
            this._MotionSpan = new ExplicitSignal<FreeMotionSpan>(Initializer, State.MotionSpan);

            Property.Introduce(Ref.Create(this, _NameRef));
            Property.Introduce(Ref.Create(this, _WorldRef));
        }

        /// <summary>
        /// Gets or sets the name of this free body for tie-breaking purposes.
        /// </summary>
        public Property<Name> Name
        {
            get
            {
                return Property.Get(Ref.Create(this, _NameRef), ref this._Name);
            }
            set
            {
                Property.Define(Ref.Create(this, _NameRef), ref this._Name, value);
            }
        }

        /// <summary>
        /// Gets or sets the world for this free body.
        /// </summary>
        public Property<World> World
        {
            get
            {
                return Property.Get(Ref.Create(this, _WorldRef), ref this._World);
            }
            set
            {
                Property.Define(Ref.Create(this, _WorldRef), ref this._World, value);
            }
        }

        /// <summary>
        /// Gets or sets the set of contacts on this body. This should be set immediately after construction of
        /// the body.
        /// </summary>
        public Contact[] Contacts
        {
            get
            {
                Debug.Assert(this._Contacts != null);
                return this._Contacts;
            }
            set
            {
                Debug.Assert(this._Contacts == null);
                this._Contacts = value;
                for (int i = 0; i < value.Length; i++)
                {
                    int index = i;
                    value[i].Environment = new ContactEnvironment
                    {
                        World = this.World,
                        LocalToWorld = this.LocalToWorld,
                        Load = Property.From(delegate(out Signal<double> load)
                        {
                            Signal<double[]> contactLoads;
                            if (this.ContactLoads.TryGetValue(out contactLoads))
                            {
                                load = contactLoads.Map(l => l[index]);
                                return true;
                            }
                            load = default(double);
                            return false;
                        })
                    };
                }
            }
        }

        /// <summary>
        /// Gets the loads on the contacts of this body.
        /// </summary>
        public Property<Signal<double[]>> ContactLoads
        {
            get
            {
                if (this._ContactLoads != null)
                    return this._ContactLoads;
                return Property.From(delegate(out Signal<double[]> res)
                {
                    res = this._ContactLoads;
                    if (res != null)
                        return true;
                    Signal<WeightDistribution> localWeightDistribution;
                    if (!this.LocalWeightDistribution.TryGetValue(out localWeightDistribution))
                        return false;
                    var contacts = this.Contacts;
                    res = localWeightDistribution.Map(weightDist =>
                    {
                        double[] loads;
                        _ComputeLoads(weightDist, contacts, out loads);
                        return loads;
                    });
                    res = Interlocked.CompareExchange(ref this._ContactLoads, res, null) ?? res;
                    return true;
                });
            }
        }

        /// <summary>
        /// Computes the loads on a set of contacts when the attached body has the given weight distribution.
        /// </summary>
        private static void _ComputeLoads(
            WeightDistribution WeightDistribution,
            Contact[] Contacts, out double[] Loads)
        {
            // TODO: Bogus solution, please fix
            Loads = new double[Contacts.Length];
            for (int i = 0; i < Contacts.Length; i++)
                Loads[i] = WeightDistribution.TotalWeight / (double)Contacts.Length;
        }

        /// <summary>
        /// Given the name of a free body, gets the name of the update events it produces.
        /// </summary>
        private static readonly Func<Name, Name> _UpdateName =
            BattleSwaps.Name.GetDecorator(new Name(7439199903483068519));

        /// <summary>
        /// Gets the event source for this body.
        /// </summary>
        public Property<Bag<Event>> PendingEvents
        {
            get
            {
                if (this._PendingEvents != null)
                    return this._PendingEvents;
                return Property.From(delegate(out Bag<Event> res)
                {
                    res = this._PendingEvents;
                    if (res != null)
                        return true;

                    Name bodyName;
                    if (this.Name.TryGetValue(out bodyName))
                    {
                        var updateName = _UpdateName(bodyName);
                        res = Bag.Singleton(this._MotionSpan.Map(
                            s => new Event(s.Start + Time.FromSeconds(0.05),
                                updateName, this._Advance)));
                        res = Interlocked.CompareExchange(ref this._PendingEvents, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets the updated velocities for the body at the given transaction.
        /// </summary>
        private void _Advance(
            Transaction Transaction,
            out MassDistribution MassDistribution,
            out Vector Position, out double Angle,
            out Vortex CenterVortex, out Vector CenterOffset)
        {
            MassDistribution = this.MassDistribution.RuntimeValue[Transaction];
            FreeMotionSpan curSpan = this._MotionSpan[Transaction];
            FreeMotion curMotion = curSpan[Transaction.Time];
            double delta = (Transaction.Time - curSpan.Start).Seconds;

            Position = curMotion.Position;
            Angle = curMotion.Angle;
            CenterOffset = MassDistribution.Center - curMotion.Position;
            CenterVortex = curMotion.Vortex.Translate(-CenterOffset);
            foreach (var contact in this._Contacts)
                contact.Advance(Transaction, delta, MassDistribution, ref CenterVortex);
        }

        /// <summary>
        /// Advances the internal state for the body.
        /// </summary>
        public void _Advance(Transaction Transaction)
        {
            MassDistribution massDist;
            Vector position; double angle;
            Vortex centerVortex; Vector centerOffset;
            this._Advance(Transaction, out massDist, out position, out angle, out centerVortex, out centerOffset);
            FreeMotion motion = new FreeMotion(position, angle, centerVortex.Translate(centerOffset));
            this._MotionSpan[Transaction] = new FreeMotionSpan(Transaction.Time, motion, centerOffset);
        }

        /// <summary>
        /// Responds to a collision hit.
        /// </summary>
        private void _Hit(
            Transaction Transaction, Vector Point, Vector Normal, out Vector Velocity,
            out double InverseMass, out double InverseInertia, out Stimulus<CollisionResponse> Resolve)
        {
            // Update, but don't write to motion span yet
            MassDistribution massDist;
            Vector position; double angle;
            Vortex centerVortex; Vector centerOffset;
            this._Advance(Transaction, out massDist, out position, out angle, out centerVortex, out centerOffset);

            // Compute velocity and inertia
            Vector offset = Point - centerOffset - position;
            InverseMass = (1.0 / massDist.TotalMass);
            InverseInertia = InverseMass + Vector.Dot(
                offset.Cross * (Vector.Det(offset, Normal) / massDist.MomentOfInertia),
                Normal);
            Velocity = centerVortex[offset];

            // Resolve writes to motion span
            Resolve = delegate(Transaction trans, CollisionResponse resp)
            {
                centerVortex.ApplyImpulse(massDist.TotalMass, massDist.MomentOfInertia, offset, resp.Impulse);
                position += resp.Seperation;
                FreeMotion motion = new FreeMotion(position, angle, centerVortex.Translate(centerOffset));
                this._MotionSpan[trans] = new FreeMotionSpan(trans.Time, motion, centerOffset);
            };
        }

        /// <summary>
        /// Gets the frame transform span for this body.
        /// </summary>
        public Signal<IFrameTransformSpan> FrameTransformSpan
        {
            get
            {
                Signal<IFrameTransformSpan> value = this._FrameTransformSpan;
                if (value == null)
                {
                    value = this._MotionSpan.Map(m => (IFrameTransformSpan)m.LocalToWorld);
                    value = Interlocked.CompareExchange(ref this._FrameTransformSpan, value, null) ?? value;
                }
                return value;
            }
        }
        
        /// <summary>
        /// Creates a collision source for this body.
        /// </summary>
        public CollisionSource GetCollisionSource(Name Name, CollisionHull Hull)
        {
            return new CollisionSource(Name, Hull, this.FrameTransformSpan, this._Hit);
        }

        public override Property<World> ContainingWorld
        {
            get
            {
                return this.World;
            }
        }

        public override Property<Signal<FrameTransform>> LocalToWorld
        {
            get
            {
                if (this._LocalToWorld != null)
                    return this._LocalToWorld;
                return Property.From(delegate(out Signal<FrameTransform> res)
                {
                    res = this._LocalToWorld;
                    if (res != null)
                        return true;

                    res = Signal.Sample<FreeMotionSpan, FreeMotion>(this._MotionSpan).Map(m => m.LocalToWorld);
                    res = Interlocked.CompareExchange(ref this._LocalToWorld, res, null) ?? res;
                    return true;
                });
            }
        }

        public override void ApplyImpulse(Transaction Transaction, PointImpulse Impulse)
        {
            MassDistribution massDist;
            Vector position; double angle;
            Vortex centerVortex; Vector centerOffset;
            this._Advance(Transaction, out massDist, out position, out angle, out centerVortex, out centerOffset);
            centerVortex.ApplyImpulse(massDist.TotalMass, massDist.MomentOfInertia,
                FreeImpulse.FromPointImpulse(massDist.Center, Impulse));
            FreeMotion motion = new FreeMotion(position, angle, centerVortex.Translate(centerOffset));
            this._MotionSpan[Transaction] = new FreeMotionSpan(Transaction.Time, motion, centerOffset);
        }
    }

    /// <summary>
    /// Describes the state of a free body at a particular reactive state.
    /// </summary>
    public struct FreeBodyState
    {
        public FreeBodyState(FreeMotionSpan MotionSpan)
        {
            this.MotionSpan = MotionSpan;
        }

        /// <summary>
        /// The span that describes the motion of the free body.
        /// </summary>
        public FreeMotionSpan MotionSpan;

        /// <summary>
        /// Constructs a state for a motionless free body.
        /// </summary>
        public static FreeBodyState Static(Vector Position, double Angle)
        {
            return new FreeBodyState(
                new FreeMotionSpan(Time.Zero,
                    new FreeMotion(Position, Angle, Vortex.Zero),
                    Vector.Zero));
        }
    }

    /// <summary>
    /// Describes the motion properties of a free rigid body at a particular moment in time.
    /// </summary>
    public struct FreeMotion
    {
        public FreeMotion(Vector Position, double Angle, Vortex Vortex)
        {
            this.Position = Position;
            this.Angle = Angle;
            this.Vortex = Vortex;
        }

        /// <summary>
        /// The position of the local origin of the body.
        /// </summary>
        public Vector Position;

        /// <summary>
        /// The angle of the body in world space.
        /// </summary>
        public double Angle;

        /// <summary>
        /// The velocity field of the body. This maps points in the local space of the body to their velocity in
        /// world space.
        /// </summary>
        public Vortex Vortex;

        /// <summary>
        /// Gets or sets the absolute velocity of the local origin of the body.
        /// </summary>
        public Vector Velocity
        {
            get
            {
                return this.Vortex.Velocity;
            }
            set
            {
                this.Vortex.Velocity = value;
            }
        }

        /// <summary>
        /// Gets or sets the angular velocity of the body.
        /// </summary>
        public double AngularVelocity
        {
            get
            {
                return this.Vortex.AngularVelocity;
            }
            set
            {
                this.Vortex.AngularVelocity = value;
            }
        }

        /// <summary>
        /// The transform from the local frame of the body to the world frame.
        /// </summary>
        public FrameTransform LocalToWorld
        {
            get
            {
                return new FrameTransform(
                    Transform.Translation(this.Position) * Transform.Rotation(this.Angle),
                    this.Vortex);
            }
        }

        /// <summary>
        /// Gets the velocity of this body at the given point in world space.
        /// </summary>
        public Vector GetVelocityAt(Vector Point)
        {
            return this.Vortex[Point - this.Position];
        }

        /// <summary>
        /// Advances this motion state, assuming that the local origin is the center of rotation.
        /// </summary>
        public FreeMotion Advance(double Time)
        {
            return new FreeMotion(
                this.Position + this.Velocity * Time,
                this.Angle + this.AngularVelocity * Time,
                this.Vortex);
        }
    }

    /// <summary>
    /// Describes an inertial progression of free motion properties over time.
    /// </summary>
    public struct FreeMotionSpan : ISpan<FreeMotion>
    {
        public FreeMotionSpan(Time Start, Vector Pivot, double Angle, Vortex Vortex, Vector Offset)
        {
            this.Start = Start;
            this.Pivot = Pivot;
            this.Angle = Angle;
            this.Vortex = Vortex;
            this.Offset = Offset;
        }

        public FreeMotionSpan(Time Start, FreeMotion Initial, Vector CenterOfMassOffset)
        {
            this.Start = Start;
            this.Pivot = Initial.Position + CenterOfMassOffset;
            this.Angle = Initial.Angle;
            this.Vortex = Initial.Vortex.Translate(-CenterOfMassOffset);
            this.Offset = -CenterOfMassOffset;
        }

        /// <summary>
        /// The start time for this span.
        /// </summary>
        public Time Start;

        /// <summary>
        /// The position of the center of rotation for the body at the start time. If the body is rotating,
        /// this should be the center of mass. Otherwise, this is arbitrary.
        /// </summary>
        public Vector Pivot;

        /// <summary>
        /// The angle of the body at the reference time.
        /// </summary>
        public double Angle;

        /// <summary>
        /// The velocity field of the body with the pivot as the origin. This will remain the same for all times.
        /// </summary>
        public Vortex Vortex;

        /// <summary>
        /// The offset of the local origin of the body from the center of mass at the reference time.
        /// </summary>
        public Vector Offset;

        /// <summary>
        /// Gets the motion properties of this span at the given time.
        /// </summary>
        public FreeMotion this[Time Time]
        {
            get
            {
                double delta = (Time - this.Start).Seconds;
                Vector dPivot = Vortex.Velocity * delta;
                double dAngle = Vortex.AngularVelocity * delta;
                Vector nOffset = Transform.Rotation(dAngle) * this.Offset;
                return new FreeMotion(
                    this.Pivot + dPivot + nOffset,
                    Vector.NormalizeAngle(this.Angle + dAngle),
                    this.Vortex.Translate(-nOffset));
            }
        }

        /// <summary>
        /// Gets the time at which this span ceases to be applicable.
        /// </summary>
        public Time End
        {
            get
            {
                return Time.MaxValue;
            }
        }

        /// <summary>
        /// Gets the transform from the local frame of the body to the world frame corresponding to this span.
        /// </summary>
        public FreeFrameTransformSpan LocalToWorld
        {
            get
            {
                return new FreeFrameTransformSpan(this);
            }
        }

        Time ISpan<FreeMotion>.Start
        {
            get { return this.Start; }
        }
    }

    /// <summary>
    /// Describes a frame transform space produced by free body.
    /// </summary>
    public struct FreeFrameTransformSpan : IFrameTransformSpan
    {
        public FreeFrameTransformSpan(FreeMotionSpan Source)
        {
            this.Source = Source;
        }

        /// <summary>
        /// The source span for this span.
        /// </summary>
        public FreeMotionSpan Source;

        /// <summary>
        /// Gets the start time for this span.
        /// </summary>
        public Time Start
        {
            get
            {
                return this.Source.Start;
            }
        }

        /// <summary>
        /// Gets the end time for this span.
        /// </summary>
        public Time End
        {
            get
            {
                return this.Source.End;
            }
        }

        /// <summary>
        /// Gets the value of this span at the given time.
        /// </summary>
        public FrameTransform this[Time Time]
        {
            get
            {
                return this.Source[Time].LocalToWorld;
            }
        }

        /// <summary>
        /// Given a circle in the pre-transformed space of the frame, returns a tight approximation of the
        /// circle in the transformed space that is valid between the given times. The resulting ray uses the "from"
        /// time as the initial parameter and seconds as its parameter units.
        /// </summary>
        public ThickRay TightApply(Time From, Time To, Circle Local)
        {
            double startDelta = (From - this.Start).Seconds;
            double fromAngle = this.Source.Angle + this.Source.Vortex.AngularVelocity * startDelta;
            Vector fromPivot = this.Source.Pivot + this.Source.Vortex.Velocity * startDelta;
            Vector circleOffset = Transform.Rotation(fromAngle) * Local.Center;

            double delta = (To - From).Seconds;
            double dAngle = this.Source.Vortex.AngularVelocity * delta;
            if (double.IsNaN(dAngle)) dAngle = 0.0;
            return ThickRay.TightArc(circleOffset, dAngle, delta).Pad(Local.Radius) +
                new Ray(fromPivot, this.Source.Vortex.Velocity);
        }
    }

    /// <summary>
    /// Describes an impulse applied at a point.
    /// </summary>
    public struct PointImpulse
    {
        public PointImpulse(Vector Point, Vector Impulse)
        {
            this.Point = Point;
            this.Impulse = Impulse;
        }

        /// <summary>
        /// The point the impulse is applied at.
        /// </summary>
        public Vector Point;

        /// <summary>
        /// The impulse applied at the point.
        /// </summary>
        public Vector Impulse;

        /// <summary>
        /// A zero-strength point impulse.
        /// </summary>
        public static readonly PointImpulse Zero = new PointImpulse(Vector.Zero, Vector.Zero);

        public static PointImpulse operator -(PointImpulse Impulse)
        {
            return new PointImpulse(Impulse.Point, -Impulse.Impulse);
        }
    }

    /// <summary>
    /// Describes the effect of a point impulse on a free body.
    /// </summary>
    public struct FreeImpulse
    {
        public FreeImpulse(Vector CenterImpulse, double AngularImpulse)
        {
            this.CenterImpulse = CenterImpulse;
            this.AngularImpulse = AngularImpulse;
        }

        /// <summary>
        /// The impulse applied to the center of mass.
        /// </summary>
        public Vector CenterImpulse;

        /// <summary>
        /// The angular impulse applied about the center of mass.
        /// </summary>
        public double AngularImpulse;

        /// <summary>
        /// A zero-strength free impulse.
        /// </summary>
        public static readonly FreeImpulse Zero = new FreeImpulse(Vector.Zero, 0.0);

        /// <summary>
        /// Constructs a free impulse from the given point impulse applied to a body with the given center of mass.
        /// </summary>
        public static FreeImpulse FromPointImpulse(Vector CenterOfMass, PointImpulse PointImpulse)
        {
            return new FreeImpulse(PointImpulse.Impulse,
                Vector.Dot((CenterOfMass - PointImpulse.Point).Cross, PointImpulse.Impulse));
        }

        public static FreeImpulse operator +(FreeImpulse A, FreeImpulse B)
        {
            return new FreeImpulse(
                A.CenterImpulse + B.CenterImpulse,
                A.AngularImpulse + B.AngularImpulse);
        }
    }
}
