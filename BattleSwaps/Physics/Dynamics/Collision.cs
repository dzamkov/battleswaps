﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Physics.Dynamics
{
    /// <summary>
    /// Describes the shape of the collision boundary of a physical object.
    /// </summary>
    public struct CollisionHull
    {
        public CollisionHull(Polygon Shape, double Restitution)
        {
            this.Shape = Shape;
            this.Restitution = Restitution;
        }

        /// <summary>
        /// The shape of this collision hull in the local space of its frame of reference.
        /// </summary>
        public Polygon Shape;

        /// <summary>
        /// The coefficient of restitution for this collision hull. This determines how much energy is preserved
        /// in a collision involving this hull.
        /// </summary>
        public double Restitution;

        /// <summary>
        /// Gets the coefficient of restitution in a collision involving the given hulls.
        /// </summary>
        public static double GetRestitution(CollisionHull A, CollisionHull B)
        {
            return Math.Min(A.Restitution, B.Restitution);
        }
    }

    /// <summary>
    /// Describes an object that can participate in collisions.
    /// </summary>
    public struct CollisionSource
    {
        public CollisionSource(Name Name,
            CollisionHull Hull,
            Signal<IFrameTransformSpan> Span,
            CollisionStimulus OnHit)
        {
            this.Name = Name;
            this.Hull = Hull;
            this.Span = Span;
            this.OnCollision = OnHit;
        }

        /// <summary>
        /// The name of this collision source, used for tie-breaking purposes.
        /// </summary>
        public Name Name;

        /// <summary>
        /// The collision hull for this source.
        /// </summary>
        public CollisionHull Hull;

        /// <summary>
        /// Describes the motion of the body associated with this collision source.
        /// </summary>
        public Signal<IFrameTransformSpan> Span;

        /// <summary>
        /// The stimulus to apply when there is a collision involving this source.
        /// </summary>
        public CollisionStimulus OnCollision;
    }

    /// <summary>
    /// A stimulus which responds to a collision.
    /// </summary>
    /// <param name="Point">The point where the collision occured in world space.</param>
    /// <param name="Normal">The normal of the surface at the point of collision.</param>
    /// <param name="Velocity">The velocity of the body at the point.</param>
    /// <param name="Resolve">The stimulus to resolve the collision. This will be applied soon after
    /// this stimulus returns.</param>
    public delegate void CollisionStimulus(
        Transaction Transaction,
        Vector Point, Vector Normal, out Vector Velocity,
        out double InverseMass, out double InverseInertia,
        out Stimulus<CollisionResponse> Resolve);

    /// <summary>
    /// Contains functions for detecting and resolving collisions.
    /// </summary>
    public static class Collision
    {
        /// <summary>
        /// Determines whether the moving circle described by the given thick ray ever overlaps with the
        /// origin. If so, takes the minimum of the given parameter with the smallest non-negative parameter where
        /// overlap occurs.
        /// </summary>
        public static void Intersect(ThickRay Ray, ref double Param)
        {
            double start, end;
            if (Ray.TryWhenContainsZero(out start, out end))
            {
                if (start < 0.0)
                {
                    if (end >= 0.0)
                        Param = 0.0;
                }
                else
                {
                    Param = start;
                }
            }
        }

        /// <summary>
        /// Determines whether the point described by ray P hits the oriented line segment between the points
        /// described by rays A and B. If so, and the parameter at which this occurs is non-negative and smaller
        /// than the current parameter, updates the given parameter and returns true.
        /// </summary>
        public static bool Hit(Ray A, Ray B, Ray P, ref double Param)
        {
            Ray dir = B - A;
            Ray offset = P - A;
            double a = Vector.Det(offset.Derivative, dir.Derivative);
            double b = Vector.Det(offset.Derivative, dir.Initial) + Vector.Det(offset.Initial, dir.Derivative);
            double c = Vector.Det(offset.Initial, dir.Initial);

            bool hit = false;
            double disc = b * b - 4.0 * a * c;
            if (disc >= 0)
            {
                double discSqrt = Math.Sqrt(disc);
                double first = (2.0 * c) / (-b - discSqrt * Math.Sign(b));
                if (0.0 <= first && first < Param)
                {
                    Vector dirAt = dir[first];
                    Vector offsetAt = offset[first];
                    double rel = Vector.Dot(offsetAt, dirAt);
                    if (rel >= 0.0 && rel <= dirAt.SquareLength)
                    {
                        Param = first;
                        hit = true;
                    }
                }

                double second = -(b / a) - first;
                if (0.0 <= second && second < Param)
                {
                    Vector dirAt = dir[second];
                    Vector offsetAt = offset[second];
                    double rel = Vector.Dot(offsetAt, dirAt);
                    if (rel >= 0.0 && rel <= dirAt.SquareLength)
                    {
                        Param = second;
                        hit = true;
                    }
                }
            }
            return hit;
        }

        /// <summary>
        /// Determines whether any of the points on the given "bullet" will hit the given "wall" shape. If so,
        /// and the parameter at which this occurs is smaller than the given parameter, updates the
        /// parameter, point and normal to reflect the hit. The resulting parameter will be negative if
        /// the bullet is inside the wall, and the magnitude of the parameter will be the penetration depth.
        /// </summary>
        private static bool _HitAsymmetric(
            FrameTransform MotionWall, Polygon Wall,
            FrameTransform MotionBullet, Vector[] Bullet,
            bool InvertNormal, ref double Param, ref Vector Point, ref Vector Normal)
        {
            Transform wallTransform = MotionWall.Spatial;
            Transform bulletTransform = MotionBullet.Spatial;
            bool hit = false;

            // Find penetration depth
            Transform bulletToWall = wallTransform.Inverse * bulletTransform;
            for (int i = 0; i < Bullet.Length; i++)
            {
                Vector normal;
                double dis = Wall.GetDistance(bulletToWall * Bullet[i], out normal);
                if (dis <= 0.0 && dis < Param)
                {
                    Param = dis;
                    Point = bulletTransform.ApplyPoint(Bullet[i]);
                    Normal = wallTransform.ApplyVector(normal);
                    if (InvertNormal) Normal *= -1.0;
                    hit = true;
                }
            }

            // If there is no penetration, find time to hit
            if (Param > 0.0)
            {
                for (int i = 0; i < Wall.Points.Length; i++)
                {
                    Vector localA = Wall.Points[i];
                    Vector localB = Wall.Points[(i + 1) % Wall.Points.Length];
                    Ray a = new Ray(wallTransform * localA, MotionWall.GetVelocityAt(localA));
                    Ray b = new Ray(wallTransform * localB, MotionWall.GetVelocityAt(localB));
                    for (int j = 0; j < Bullet.Length; j++)
                    {
                        Vector localP = Bullet[j];
                        Ray p = new Ray(bulletTransform * localP, MotionBullet.GetVelocityAt(localP));
                        if (0.0 < Param && Hit(a, b, p, ref Param))
                        {
                            Point = p[Param];
                            Normal = Vector.FromAngle(
                                wallTransform.ApplyVector((localB - localA).Cross).Angle +
                                MotionWall.AngularVelocity * Param);
                            if (InvertNormal) Normal *= -1.0;
                            hit = true;
                        }
                    }
                }
            }

            return hit;
        }

        /// <summary>
        /// Determines whether the given moving shapes will hit each other and, If so,
        /// and the parameter at which this occurs is closer to zero than the given parameter, updates the
        /// parameter, point and normal to reflect the hit. The resulting parameter will be negative if
        /// the bullet is inside the wall, and the magnitude of the parameter will be the penetration depth.
        /// </summary>
        public static bool Hit(
            FrameTransform MotionA, Polygon ShapeA,
            FrameTransform MotionB, Polygon ShapeB,
            ref double Param, ref Vector Point, ref Vector Normal)
        {
            // TODO: More accurate penetration depth test
            return
                _HitAsymmetric(MotionA, ShapeA, MotionB, ShapeB.Points, false, ref Param, ref Point, ref Normal) |
                _HitAsymmetric(MotionB, ShapeB, MotionA, ShapeA.Points, true, ref Param, ref Point, ref Normal);
        }

        /// <summary>
        /// The smallest amount of time that can pass between the latest update of an object and the next
        /// collision it is involved in. This prevents infinitely recurring events due to sustained collisions.
        /// </summary>
        public static readonly Time MinCollisionDelay = Time.FromSeconds(0.01);

        /// <summary>
        /// The minimum amount of seperation to apply to colliding bodies.
        /// </summary>
        public const double MinSeperation = 0.001;

        /// <summary>
        /// Used to generate the name for a collision resolution event.
        /// </summary>
        private static readonly Func<Name, Name> _ResolutionName =
            BattleSwaps.Name.GetDecorator(new Name(5006746774977578164));

        /// <summary>
        /// Gets an event source that detects collisions between the given sources and provides events
        /// to resolve them.
        /// </summary>
        public static Bag<Event> ResolutionsBetween(CollisionSource A, CollisionSource B)
        {
            // Response
            double restitution = CollisionHull.GetRestitution(A.Hull, B.Hull);
            CollisionStimulus hitA = A.OnCollision;
            CollisionStimulus hitB = B.OnCollision;
            Func<Vector, Vector, double, Stimulus> respond = delegate(
                Vector point, Vector normal, double depth)
            {
                return delegate(Transaction trans)
                {
                    Vector velA, velB;
                    double invMassA, invMassB;
                    double invInertiaA, invInertiaB;
                    Stimulus<CollisionResponse> resolveA, resolveB;
                    hitA(trans, point, normal, out velA, out invMassA, out invInertiaA, out resolveA);
                    hitB(trans, point, normal, out velB, out invMassB, out invInertiaB, out resolveB);
                    double invInertia = invInertiaA + invInertiaB;
                    double normalRelVel = Vector.Dot(velB - velA, normal);
                    Vector impulse = normalRelVel < 0.0 ?
                        normal * (normalRelVel * (1.0 + restitution) / invInertia) :
                        Vector.Zero;
                    double seperation = Math.Max(MinSeperation, depth);
                    double seperationA = seperation * (invMassA / (invMassA + invMassB));
                    double seperationB = seperation - seperationA;
                    resolveA(trans, new CollisionResponse(impulse, normal * -seperationA));
                    resolveB(trans, new CollisionResponse(-impulse, normal * seperationB));
                };
            };

            // Detection
            Name name = _ResolutionName(A.Name * B.Name);
            Polygon localShapeA = A.Hull.Shape;
            Polygon localShapeB = B.Hull.Shape;
            Circle localCircleA = Circle.Tight(localShapeA);
            Circle localCircleB = Circle.Tight(localShapeB);
            return Bag.Switch(Signal.Lift(A.Span, B.Span, (a, b) =>
            {
                Time startTime = Time.Later(a.Start, b.Start);
                Time endTime = Time.Earlier(a.End, b.End);
                ThickRay pathA = a.TightApply(startTime, endTime, localCircleA);
                ThickRay pathB = b.TightApply(startTime, endTime, localCircleB);
                ThickRay pathDiff = pathA - pathB;
                double paramBroad = double.PositiveInfinity;
                Intersect(pathDiff, ref paramBroad);
                if (paramBroad < double.PositiveInfinity)
                {
                    Time broadHitTime = startTime + Time.FromSeconds(paramBroad);
                    if (broadHitTime <= endTime)
                    {
                        FrameTransform broadTransformA = a[broadHitTime];
                        FrameTransform broadTransformB = b[broadHitTime];
                        double paramNarrow = double.PositiveInfinity;
                        Vector point = Vector.Zero;
                        Vector normal = Vector.Zero;
                        if (Hit(
                            broadTransformA, localShapeA,
                            broadTransformB, localShapeB,
                            ref paramNarrow, ref point, ref normal))
                        {
                            // TODO: Make collision resolution fair when multiple sustained collisions are involved

                            Time hitTime = Time.Later(
                                broadHitTime + Time.FromSeconds(paramNarrow),
                                startTime + MinCollisionDelay);
                            return Bag.Singleton(
                                new Event(hitTime, name,
                                    respond(point, normal, Math.Max(0.0, -paramNarrow))));
                        }
                    }
                }
                return Bag<Event>.Empty;
            }));
        }

        /// <summary>
        /// Gets a set of events which resolve collisions within the given sources.
        /// </summary>
        public static Bag<Event> ResolutionsWithin(Bag<CollisionSource> Sources)
        {
            return Bag.Union(Bag.Lift(Sources, Sources, delegate(CollisionSource a, CollisionSource b)
            {
                if (a.Name < b.Name)
                    return Collision.ResolutionsBetween(a, b);
                else
                    return Bag<Event>.Empty;
            }));
        }
    }

    /// <summary>
    /// Describes an impulse and seperation that needs to be applied to resolve a collision.
    /// </summary>
    public struct CollisionResponse
    {
        public CollisionResponse(Vector Impulse, Vector Seperation)
        {
            this.Impulse = Impulse;
            this.Seperation = Seperation;
        }

        /// <summary>
        /// The impulse to apply to the colliding body at the point of collision.
        /// </summary>
        public Vector Impulse;
            
        /// <summary>
        /// The seperation to apply to the colliding body.
        /// </summary>
        public Vector Seperation;
    }
}