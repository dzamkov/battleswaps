﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using BattleSwaps.Geometry;
using BattleSwaps.Reactive;

namespace BattleSwaps.Physics
{
    /// <summary>
    /// Describes an isometric transform from one moving frame of reference to another. This encodes their immediate
    /// spatial relationship, as well as their instantaneous motion with respect to each other.
    /// </summary>
    public struct FrameTransform
    {
        public FrameTransform(Transform Spatial, Vortex Vortex)
        {
            this.Spatial = Spatial;
            this.Vortex = Vortex;
        }

        /// <summary>
        /// The immediate spatial transform between the frames of reference.
        /// </summary>
        public Transform Spatial;

        /// <summary>
        /// A vortex which describes the velocity of all points in the pre-transformed frame within the
        /// post-transformed frame.
        /// </summary>
        public Vortex Vortex;

        /// <summary>
        /// Gets the angular velocity of the pre-transformed frame within the post-transformed frame.
        /// </summary>
        public double AngularVelocity
        {
            get
            {
                return this.Vortex.AngularVelocity;
            }
        }

        /// <summary>
        /// Gets the velocity of the given point in the pre-transformed frame within the post-transformed frame.
        /// </summary>
        public Vector GetVelocityAt(Vector Local)
        {
            return this.Vortex[Local];
        }

        public static FrameVector operator *(FrameTransform Transform, FrameVector Vector)
        {
            return new FrameVector(
                Transform.Spatial * Vector.Spatial,
                Transform.GetVelocityAt(Vector.Spatial) + Transform.Spatial.ApplyVector(Vector.Velocity));
        }

        public static FrameTransform operator *(FrameTransform A, FrameTransform B)
        {
            return new FrameTransform(
                A.Spatial * B.Spatial,
                A.Vortex * B.Spatial + A.Spatial * B.Vortex);
        }

        public static FrameTransform operator *(Transform A, FrameTransform B)
        {
            return new FrameTransform(
                A * B.Spatial,
                A * B.Vortex);
        }

        public static FrameTransform operator *(FrameTransform A, Transform B)
        {
            return new FrameTransform(
                A.Spatial * B,
                A.Vortex * B);
        }

        public static DetailFrameTransform operator *(FrameTransform A, UprightTransform B)
        {
            return new DetailFrameTransform(A * B.Lateral, B.Height);
        }

        public static DetailFrameTransform operator *(UprightTransform A, FrameTransform B)
        {
            return new DetailFrameTransform(A.Lateral * B, A.Height);
        }

        public static implicit operator FrameTransform(Transform Transform)
        {
            return new FrameTransform(Transform, Vortex.Zero);
        }
    }

    /// <summary>
    /// Describes the velocities of all points in a space containing a rigid-body-like vortex.
    /// </summary>
    public struct Vortex
    {
        public Vortex(Vector Velocity, double AngularVelocity)
        {
            this.Velocity = Velocity;
            this.AngularVelocity = AngularVelocity;
        }

        /// <summary>
        /// The velocity at the origin.
        /// </summary>
        public Vector Velocity;

        /// <summary>
        /// The angular velocity of the vortex.
        /// </summary>
        public double AngularVelocity;

        /// <summary>
        /// A vortex whose velocity is zero at all points.
        /// </summary>
        public static Vortex Zero = new Vortex(Vector.Zero, 0.0);

        /// <summary>
        /// Gets the velocity at a point on this vortex.
        /// </summary>
        public Vector this[Vector Point]
        {
            get
            {
                return this.Velocity - Point.Cross * this.AngularVelocity;
            }
        }

        /// <summary>
        /// Gets the center of the vortex, the only point where the velocity is zero. Note that this is undefined
        /// if the angular velocity of the vortex is zero.
        /// </summary>
        public Vector Center
        {
            get
            {
                return this.Velocity.Cross / this.AngularVelocity;
            }
        }

        /// <summary>
        /// Translates this vortex by the given amount.
        /// </summary>
        public Vortex Translate(Vector Offset)
        {
            return new Vortex(this[-Offset], AngularVelocity);
        }

        /// <summary>
        /// Assuming that there is a body with the given mass and moment of inertia centered at the origin of
        /// a vortex, and all points in the body have velocity given by the vortex, determines the impulse
        /// that needs to be applied at the given point to change the velocity at that point by the given amount.
        /// </summary>
        public static Vector ComputeImpulseAt(double Mass, double MomentOfInertia, Vector Point, Vector DeltaVelocity)
        {
            Vector r = -Point.Cross;
            double dAngularMomentum = Vector.Dot(r, DeltaVelocity) / (1.0 / Mass + r.SquareLength / MomentOfInertia);
            return (DeltaVelocity - r * (dAngularMomentum / MomentOfInertia)) * Mass;
        }

        /// <summary>
        /// Assuming that there is a body with the given mass and moment of inertia centered at the origin of
        /// this vortex, and all points in the body have velocity given by the vortex, updates the vortex to
        /// reflect the given impulse applied at the given point.
        /// </summary>
        public void ApplyImpulse(double Mass, double MomentOfInertia, Vector Point, Vector Impulse)
        {
            this.Velocity += Impulse / Mass;
            this.AngularVelocity -= Vector.Dot(Point.Cross, Impulse) / MomentOfInertia;
        }

        /// <summary>
        /// Assuming that there is a body with the given mass and moment of inertia centered at the origin of
        /// this vortex, and all points in the body have velocity given by the vortex, updates the vortex to
        /// reflect the given impulse applied at the given point.
        /// </summary>
        public void ApplyImpulse(double Mass, double MomentOfInertia, Dynamics.PointImpulse Impulse)
        {
            this.ApplyImpulse(Mass, MomentOfInertia, Impulse.Point, Impulse.Impulse);
        }

        /// <summary>
        /// Assuming that there is a body with the given mass and moment of inertia centered at the origin of
        /// this vortex, and all points in the body have velocity given by the vortex, updates the vortex to
        /// reflect the application of the given impulse.
        /// </summary>
        public void ApplyImpulse(double Mass, double MomentOfInertia, Dynamics.FreeImpulse Impulse)
        {
            this.Velocity += Impulse.CenterImpulse / Mass;
            this.AngularVelocity += Impulse.AngularImpulse / MomentOfInertia;
        }

        public static Vortex operator +(Vortex A, Vortex B)
        {
            return new Vortex(A.Velocity + B.Velocity, A.AngularVelocity + B.AngularVelocity);
        }

        public static Vortex operator -(Vortex A, Vortex B)
        {
            return new Vortex(A.Velocity - B.Velocity, A.AngularVelocity - B.AngularVelocity);
        }

        public static Vortex operator *(Transform Transform, Vortex Vortex)
        {
            return new Vortex(Transform.ApplyVector(Vortex.Velocity), Vortex.AngularVelocity);
        }

        public static Vortex operator *(Vortex Vortex, Transform Transform)
        {
            return new Vortex(Vortex[Transform.Offset], Vortex.AngularVelocity);
        }
    }

    /// <summary>
    /// Combines a frame transform with additional information placing it in 3D space.
    /// </summary>
    public struct DetailFrameTransform
    {
        public DetailFrameTransform(FrameTransform Lateral, HeightTransform Height)
        {
            this.Lateral = Lateral;
            this.Height = Height;
        }

        /// <summary>
        /// The source frame transform describing the lateral component of this frame transform.
        /// </summary>
        public FrameTransform Lateral;

        /// <summary>
        /// The height transform for this frame transform.
        /// </summary>
        public HeightTransform Height;

        /// <summary>
        /// Gets the immediate spatial transform for this frame transform.
        /// </summary>
        public UprightTransform Spatial
        {
            get
            {
                return new UprightTransform(this.Lateral.Spatial, this.Height);
            }
        }

        public static DetailFrameVector operator *(DetailFrameTransform Transform, DetailFrameVector Vector)
        {
            return (Transform.Lateral * Vector.Lateral).WithHeight(Transform.Height * Vector.Height);
        }
    }

    /// <summary>
    /// Describes a smooth progression of a frame transform over time.
    /// </summary>
    public interface IFrameTransformSpan : ISpan<FrameTransform>
    {
        /// <summary>
        /// Given a circle in the pre-transformed space of the frame, returns a tight approximation of the
        /// circle in the transformed space that is valid between the given times. The resulting ray uses the "from"
        /// time as the initial parameter and seconds as its parameter units.
        /// </summary>
        ThickRay TightApply(Time From, Time To, Circle Local);
    }

    /// <summary>
    /// Describes a smooth progression of a 3D frame transform over time.
    /// </summary>
    public interface IDetailFrameTransformSpan : ISpan<DetailFrameTransform>
    {
        /// <summary>
        /// Gets the lateral component of this frame transform span.
        /// </summary>
        IFrameTransformSpan Lateral { get; }
    }

    /// <summary>
    /// A 3D frame transform span constructed by ammending a constant height transform to a regular frame transform
    /// span.
    /// </summary>
    public struct LevelDetailFrameTransformSpan : IDetailFrameTransformSpan
    {
        public LevelDetailFrameTransformSpan(IFrameTransformSpan Lateral, HeightTransform Height)
        {
            this.Lateral = Lateral;
            this.Height = Height;
        }

        /// <summary>
        /// The lateral component of this span.
        /// </summary>
        public IFrameTransformSpan Lateral;

        /// <summary>
        /// The height component of this span.
        /// </summary>
        public HeightTransform Height;

        /// <summary>
        /// Gets the start time for this span.
        /// </summary>
        public Time Start
        {
            get
            {
                return this.Lateral.Start;
            }
        }

        /// <summary>
        /// Gets the end time for this span.
        /// </summary>
        public Time End
        {
            get
            {
                return this.Lateral.End;
            }
        }

        /// <summary>
        /// Gets the value of this span at the given time.
        /// </summary>
        public DetailFrameTransform this[Time Time]
        {
            get
            {
                return new DetailFrameTransform(this.Lateral[Time], this.Height);
            }
        }

        IFrameTransformSpan IDetailFrameTransformSpan.Lateral
        {
            get
            { 
                return this.Lateral;
            }
        }
    }
}
