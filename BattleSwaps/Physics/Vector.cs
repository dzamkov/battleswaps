﻿using System;
using System.Collections.Generic;

using BattleSwaps.Geometry;

namespace BattleSwaps.Physics
{
    /// <summary>
    /// A spatial vector paired with a velocity. This describes the motion of a point on a moving frame of reference.
    /// </summary>
    public struct FrameVector
    {
        public FrameVector(Vector Spatial, Vector Velocity)
        {
            this.Spatial = Spatial;
            this.Velocity = Velocity;
        }

        /// <summary>
        /// The spatial component of this frame vector.
        /// </summary>
        public Vector Spatial;

        /// <summary>
        /// The velocity component of this frame vector.
        /// </summary>
        public Vector Velocity;

        /// <summary>
        /// Gives this vector a height, converting it into a detail frame vector.
        /// </summary>
        public DetailFrameVector WithHeight(double Height)
        {
            return new DetailFrameVector(new DetailVector(this.Spatial, Height), this.Velocity);
        }

        /// <summary>
        /// Constructs a frame vector that is not moving.
        /// </summary>
        public static FrameVector Static(Vector Spatial)
        {
            return new FrameVector(Spatial, Vector.Zero);
        }

        public static FrameVector operator +(FrameVector A, FrameVector B)
        {
            return new FrameVector(A.Spatial + B.Spatial, A.Velocity + B.Velocity);
        }

        public static FrameVector operator -(FrameVector A, FrameVector B)
        {
            return new FrameVector(A.Spatial - B.Spatial, A.Velocity - B.Velocity);
        }
    }

    /// <summary>
    /// Combines a frame vector with additional information placing it in 3D space.
    /// </summary>
    public struct DetailFrameVector
    {
        public DetailFrameVector(DetailVector Spatial, Vector Velocity)
        {
            this.Spatial = Spatial;
            this.Velocity = Velocity;
        }

        /// <summary>
        /// The spatial component of this frame vector.
        /// </summary>
        public DetailVector Spatial;

        /// <summary>
        /// The velocity component of this frame vector.
        /// </summary>
        public Vector Velocity;

        /// <summary>
        /// Gets the lateral component of this frame vector.
        /// </summary>
        public FrameVector Lateral
        {
            get
            {
                return new FrameVector(this.Spatial.Lateral, this.Velocity);
            }
        }

        /// <summary>
        /// Gets the height component of this frame vector.
        /// </summary>
        public double Height
        {
            get
            {
                return this.Spatial.Height;
            }
        }

        /// <summary>
        /// Constructs a frame vector that is not moving.
        /// </summary>
        public static DetailFrameVector Static(DetailVector Spatial)
        {
            return new DetailFrameVector(Spatial, Vector.Zero);
        }

        public static DetailFrameVector operator +(DetailFrameVector A, DetailFrameVector B)
        {
            return new DetailFrameVector(A.Spatial + B.Spatial, A.Velocity + B.Velocity);
        }

        public static DetailFrameVector operator -(DetailFrameVector A, DetailFrameVector B)
        {
            return new DetailFrameVector(A.Spatial - B.Spatial, A.Velocity - B.Velocity);
        }
    }
}
