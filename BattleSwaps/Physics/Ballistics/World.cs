﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Physics.Ballistics
{
    /// <summary>
    /// Identifies and describes the projectile-related component of a game world.
    /// </summary>
    public sealed class World
    {
        private double _Gravity;
        private Bag<ImpactSource> _ImpactSources;
        private static readonly InteriorRef<double> _GravityRef =
            (obj, act) => act(ref ((World)obj)._Gravity);
        private static readonly InteriorRef<Bag<ImpactSource>> _ImpactSourcesRef =
            (obj, act) => act(ref ((World)obj)._ImpactSources);
        public World()
        {
            Property.Introduce(Ref.Create(this, _GravityRef));
            Property.Introduce(Ref.Create(this, _ImpactSourcesRef));
        }

        /// <summary>
        /// Gets or sets the (positive) gravitational acceleration of the world.
        /// </summary>
        public Property<double> Gravity
        {
            get
            {
                return Property.Get(Ref.Create(this, _GravityRef), ref this._Gravity);
            }
            set
            {
                Property.Define(Ref.Create(this, _GravityRef), ref this._Gravity, value);
            }
        }

        /// <summary>
        /// Gets or sets the set of impact sources in the world.
        /// </summary>
        public Property<Bag<ImpactSource>> ImpactSources
        {
            get
            {
                return Property.Get(Ref.Create(this, _ImpactSourcesRef), ref this._ImpactSources);
            }
            set
            {
                Property.Define(Ref.Create(this, _ImpactSourcesRef), ref this._ImpactSources, value);
            }
        }

        /// <summary>
        /// Gets the motion span of an unpowered projectile launched at the given moment with the given initial
        /// motion properties.
        /// </summary>
        public ProjectileMotionSpan GetMotionSpan(Transaction Transaction, ProjectileMotion Initial)
        {
            return new ProjectileMotionSpan(Transaction.Time, Initial, this._Gravity);
        }

        /// <summary>
        /// Tries solving the firing parameters needed to hit a target. This will prefer solutions that minimize
        /// travel time, and will yield an approximate solution and return false if no exact solution can be
        /// found. This assumes a fixed firing speed.
        /// </summary>
        public bool Solve(
            double MinElevation, double MaxElevation, double Speed,
            DetailFrameVector Source, DetailFrameVector Target,
            out double Angle, out double Elevation)
        {
            DetailFrameVector diff = Target - Source;
            double gravity = this.Gravity.RuntimeValue;

            // Compute a lower bound for the time to impact based on the min elevation and time it would take
            // for the projectile to fall to the correct height
            double minInitialVerticalSpeed = Speed * Math.Sin(MinElevation);
            double tMin = (minInitialVerticalSpeed +
                Math.Sqrt(minInitialVerticalSpeed * minInitialVerticalSpeed - 2.0 * gravity * diff.Height)) / gravity;

            // Compute an upper bound for the time to impact based on the max elevation and time it would take
            // for the projectile to fall to the correct height
            double maxInitialVerticalSpeed = Speed * Math.Sin(MaxElevation);
            double tMax = (maxInitialVerticalSpeed +
                Math.Sqrt(maxInitialVerticalSpeed * maxInitialVerticalSpeed - 2.0 * gravity * diff.Height)) / gravity;

            // Solve for the time to impact (a t^4 + b t^2 + c t + d = 0)
            double a = gravity * gravity / 4.0;
            double b = diff.Velocity.SquareLength + diff.Height * gravity - Speed * Speed;
            double c = 2.0 * Vector.Dot(diff.Spatial.Lateral, diff.Velocity);
            double d = diff.Height * diff.Height + diff.Spatial.Lateral.SquareLength;

            // Get an initial estimate by assuming c = 0 and using the quadratic formula
            bool exact = true;
            double disc = b * b - 4.0 * a * d;
            double t;
            if (disc >= 0.0)
            {
                double discRt = Math.Sqrt(disc);
                double tSqrEst = (-b - discRt) / (2.0 * a);
                if (tSqrEst < tMin * tMin)
                    tSqrEst = (-b + discRt) / (2.0 * a);
                t = Math.Sqrt(tSqrEst);

                // Apply Newton's method to improve accuracy when c != 0
                if (Math.Abs(c) >= 1.0e-5)
                {
                    t = t - (a * t * t * t * t + b * t * t + c * t + d) / (4.0 * a * t * t * t + 2.0 * b * t + c);
                    t = t - (a * t * t * t * t + b * t * t + c * t + d) / (4.0 * a * t * t * t + 2.0 * b * t + c);
                }

                // Check range
                if (t < tMin)
                {
                    t = tMin;
                    exact = false;
                }
                if (t > tMax)
                {
                    t = tMax;
                    exact = false;
                }
            }
            else
            {
                exact = false;
                t = tMax;
            }

            // Compute angle and elevation based on time to impact
            Vector diffAtImpact = diff.Spatial.Lateral + t * diff.Velocity;
            Angle = diffAtImpact.Angle;
            Elevation = Math.Asin((diff.Height + gravity * t * t / 2.0) / (Speed * t));
            return exact;
        }
    }

    /// <summary>
    /// A full static description of a ballistics world at a particular reactive state. This can be used to serialize
    /// worlds.
    /// </summary>
    public struct WorldPrototype
    {
        /// <summary>
        /// The (positive) gravitational acceleration of the world.
        /// </summary>
        public double Gravity;

        /// <summary>
        /// Creates an instance of the world described by this prototype.
        /// </summary>
        public World Instantiate(Initializer Initializer)
        {
            return new World
            {
                Gravity = this.Gravity
            };
        }
    }
}