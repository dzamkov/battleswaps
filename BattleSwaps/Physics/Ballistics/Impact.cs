﻿using System;
using System.Collections.Generic;

using BattleSwaps.Reactive;
using BattleSwaps.Geometry;

namespace BattleSwaps.Physics.Ballistics
{
    /// <summary>
    /// Describes an object that can be hit by a projectile.
    /// </summary>
    public struct ImpactSource
    {
        /// <summary>
        /// The name of this impact source, used for tie-breaking purposes.
        /// </summary>
        public Name Name;

        /// <summary>
        /// The mesh that defines the shape of the impact source in the local space of its frame of reference.
        /// </summary>
        public Mesh Mesh;

        /// <summary>
        /// Describes the motion of the body associated with this collision source.
        /// </summary>
        public Signal<IDetailFrameTransformSpan> Span;

        /// <summary>
        /// The stimulus to apply when there is an impact on this source which results in the application of the given
        /// impulse.
        /// </summary>
        public ImpulseStimulus OnImpulse;
    }

    /// <summary>
    /// Describes a projectile.
    /// </summary>
    public struct ProjectileSource
    {
        /// <summary>
        /// The name of this projectile source, used for tie-breaking purposes.
        /// </summary>
        public Name Name;

        /// <summary>
        /// Describes the motion of the projectile.
        /// </summary>
        public ProjectileMotionSpan Span;

        /// <summary>
        /// The stimulus to apply when this projectile impacts on something.
        /// </summary>
        public ImpactStimulus OnImpact;
    }

    /// <summary>
    /// A stimulus which applies an impulse to an impact source in response to an impact.
    /// </summary>
    public delegate void ImpulseStimulus(Transaction Transaction, DetailVector Point, DetailVector Impulse);

    /// <summary>
    /// A stimulus which responds to an impact.
    /// </summary>
    public delegate void ImpactStimulus(
        Transaction Transaction, ImpulseStimulus OnImpulse,
        ProjectileMotion Motion);

    /// <summary>
    /// Contains functions for detecting and resolving impacts.
    /// </summary>
    public static class Impact
    {
        /// <summary>
        /// Constructs an impulse stimulus which applies the appropriate impulse to the body that was hit.
        /// </summary>
        public static ImpulseStimulus Impulse(Dynamics.Body Body)
        {
            return delegate(Transaction trans, DetailVector point, DetailVector impulse)
            {
                Body.ApplyImpulse(trans, new Dynamics.PointImpulse(point.Lateral, impulse.Lateral));
            };
        }

        /// <summary>
        /// Used to generate the name for an impact resolution event.
        /// </summary>
        private static readonly Func<Name, Name> _ResolutionName =
            BattleSwaps.Name.GetDecorator(new Name(11619155610867691856));

        /// <summary>
        /// Gets an event source that detects and resolves impacts between a given projectile and the given
        /// impact source.
        /// </summary>
        public static Bag<Event> ResolutionsBetween(ProjectileSource Projectile, ImpactSource Impact)
        {
            var name = _ResolutionName(Projectile.Name * Impact.Name);
            Circle localCircle = Impact.Mesh.TightCircle;
            return Impact.Span.Map(impactSpan =>
            {
                Time startTime = Time.Later(Projectile.Span.Start, impactSpan.Start);
                Time endTime = impactSpan.End;
                ThickRay path = impactSpan.Lateral.TightApply(startTime, endTime, localCircle);
                ThickRay pathDiff = path - Projectile.Span.GetApproximateLateralAt(startTime);
                double broadStart, broadEnd;
                if (pathDiff.TryWhenContainsZero(out broadStart, out broadEnd) && broadEnd > 0.0)
                {
                    double paramBroad = Math.Max(broadStart, 0.0);
                    Time approxTime = startTime + Time.FromSeconds(paramBroad);
                    UprightTransform approxLocalToWorld = impactSpan[approxTime].Spatial;
                    DetailRay approx = approxLocalToWorld.Inverse * Projectile.Span.GetApproximateAt(approxTime);

                    double paramNarrow = double.PositiveInfinity;
                    if (Impact.Mesh.TryIntersect(approx, ref paramNarrow))
                    {
                        Time hitTime = approxTime + Time.FromSeconds(paramNarrow);
                        return Bag.Singleton(new Event(hitTime, name, delegate(Transaction trans)
                        {
                            Projectile.OnImpact(trans, Impact.OnImpulse, Projectile.Span[hitTime]);
                        }));
                    }
                }
                return Bag<Event>.Empty;
            }).Switch();
        }

        /// <summary>
        /// Gets an event source that detects and resolves impacts between the given projectile and impact sources.
        /// </summary>
        public static Bag<Event> ResolutionsBetween(ProjectileSource Projectile, Bag<ImpactSource> Impacts)
        {
            return Bag.Union(Impacts.Map(impact => ResolutionsBetween(Projectile, impact)));
        }
    }
}