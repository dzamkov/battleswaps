﻿using System;
using System.Collections.Generic;

using BattleSwaps.Geometry;
using BattleSwaps.Reactive;

namespace BattleSwaps.Physics.Ballistics
{
    /// <summary>
    /// Describes the motion of a projectile at a particular moment in time.
    /// </summary>
    public struct ProjectileMotion
    {
        public ProjectileMotion(DetailVector Position, DetailVector Velocity)
        {
            this.Position = Position;
            this.Velocity = Velocity;
        }

        /// <summary>
        /// The position of the projectile.
        /// </summary>
        public DetailVector Position;

        /// <summary>
        /// The velocity of the projectile.
        /// </summary>
        public DetailVector Velocity;

        /// <summary>
        /// Gets the motion of a projectile as it is launched from a barrel.
        /// </summary>
        public static ProjectileMotion Launch(
            DetailFrameTransform BarrelToWorld,
            double Elevation, double Speed)
        {
            double lateralSpeed = Math.Cos(Elevation) * Speed;
            double verticalSpeed = Math.Sin(Elevation) * Speed;
            return new ProjectileMotion(
                BarrelToWorld.Spatial.Offset,
                BarrelToWorld.Lateral.Vortex.Velocity.WithHeight(0.0) +
                BarrelToWorld.Spatial.ApplyVector(new DetailVector(lateralSpeed, 0.0, verticalSpeed)));
        }
    }

    /// <summary>
    /// Describes a progression of projectile motion properties over time.
    /// </summary>
    public struct ProjectileMotionSpan : ISpan<ProjectileMotion>
    {
        public ProjectileMotionSpan(Time Start, ProjectileMotion Initial, double Gravity)
        {
            this.Start = Start;
            this.Initial = Initial;
            this.Gravity = Gravity;
        }

        /// <summary>
        /// The start time for this span.
        /// </summary>
        public Time Start;

        /// <summary>
        /// The motion of the projectile at the start time.
        /// </summary>
        public ProjectileMotion Initial;

        /// <summary>
        /// The downward acceleration due to gravity applied to the projectile.
        /// </summary>
        public double Gravity;

        /// <summary>
        /// Gets the motion properties of this span at a particular time.
        /// </summary>
        public ProjectileMotion this[Time Time]
        {
            get
            {
                double delta = (Time - this.Start).Seconds;
                return new ProjectileMotion(
                    this.Initial.Position + this.Initial.Velocity * delta +
                    DetailVector.Down * (this.Gravity * delta * delta / 2.0),
                    this.Initial.Velocity + DetailVector.Down * (this.Gravity * delta));
            }
        }

        /// <summary>
        /// Gets a ray approximating the lateral component of this trajectory at the given time, using
        /// seconds as parameter units.
        /// </summary>
        public Ray GetApproximateLateralAt(Time Time)
        {
            double delta = (Time - this.Start).Seconds;
            return new Ray(
                this.Initial.Position.Lateral + this.Initial.Velocity.Lateral * delta,
                this.Initial.Velocity.Lateral);
        }

        /// <summary>
        /// Gets a ray approximating this trajectory at the given time, using seconds as parameter units.
        /// </summary>
        public DetailRay GetApproximateAt(Time Time)
        {
            ProjectileMotion motion = this[Time];
            return new DetailRay(motion.Position, motion.Velocity);
        }

        /// <summary>
        /// Gets the end time for this span.
        /// </summary>
        public Time End
        {
            get
            {
                return Time.MaxValue;
            }
        }

        Time ISpan<ProjectileMotion>.Start
        {
            get { return this.Start; }
        }
    }
}