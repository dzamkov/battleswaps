﻿using System;
using System.Collections.Generic;

using BattleSwaps.Util;
using BattleSwaps.Reactive;

namespace BattleSwaps
{
    /// <summary>
    /// Identifies and describes a running instance of a reactive actor.
    /// </summary>
    public interface IActor<TState>
    {

    }
}