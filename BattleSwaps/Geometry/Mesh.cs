﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// Describes a triangular mesh in 3D space.
    /// </summary>
    public struct Mesh
    {
        public Mesh(DetailVector[] Points, Triangle[] Triangles)
        {
            this.Points = Points;
            this.Triangles = Triangles;
        }

        /// <summary>
        /// The points in this mesh. Each point should be referenced by at least one triangle.
        /// </summary>
        public DetailVector[] Points;

        /// <summary>
        /// The triangles in this mesh.
        /// </summary>
        public Triangle[] Triangles;

        /// <summary>
        /// Describes a triangle in a mesh by referencing three points.
        /// </summary>
        public struct Triangle
        {
            public uint A;
            public uint B;
            public uint C;
            public Triangle(uint A, uint B, uint C)
            {
                this.A = A;
                this.B = B;
                this.C = C;
            }

            /// <summary>
            /// Tries intersecting a ray with this triangle. If there is an intersection and the ray parameter at the
            /// intersection is smaller than the given parameter, this will update the parameter and return true.
            /// Otherwise, this will return false.
            /// </summary>
            /// <param name="Points">The points array this triangle uses.</param>
            public bool TryIntersect(DetailVector[] Points, DetailRay Ray, ref double Param)
            {
                // From https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
                DetailVector a = Points[this.A];
                DetailVector b = Points[this.B];
                DetailVector c = Points[this.C];

                DetailVector ab = b - a;
                DetailVector ac = c - a;
                DetailVector p = DetailVector.Cross(Ray.Derivative, ac);
                double det = DetailVector.Dot(ab, p);

                if (Math.Abs(det) < 1.0e-5) return false;
                double invDet = 1.0 / det;

                DetailVector t = Ray.Initial - a;
                double u = DetailVector.Dot(t, p) * invDet;
                if (u < 0.0 || u > 1.0) return false;

                DetailVector q = DetailVector.Cross(t, ab);
                double v = DetailVector.Dot(Ray.Derivative, q) * invDet;
                if (v < 0.0 || u + v > 1.0) return false;

                double cand = DetailVector.Dot(ac, q) * invDet;
                if (cand > 0.0 && cand < Param)
                {
                    Param = cand;
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Computes the tightest approximation of the lateral bounds of this mesh.
        /// </summary>
        public Bounds TightBounds
        {
            get
            {
                Bounds bounds = Bounds.Empty;
                foreach (var point in this.Points)
                    bounds = bounds.TightUnionWith(point.Lateral);
                return bounds;
            }
        }

        /// <summary>
        /// Computes the tightest approximation of the lateral bounds of this mesh as a circle.
        /// </summary>
        public Circle TightCircle
        {
            get
            {
                Vector[] points = new Vector[this.Points.Length];
                for (int i = 0; i < points.Length; i++)
                    points[i] = this.Points[i].Lateral;
                return Circle.Tight(points);
            }
        }

        /// <summary>
        /// Tries intersecting a ray with this mesh. If there is an intersection and the ray parameter at the
        /// intersection is smaller than the given parameter, this will update the parameter and return true.
        /// Otherwise, this will return false.
        /// </summary>
        public bool TryIntersect(DetailRay Ray, ref double Param)
        {
            bool intersect = false;
            foreach (var tri in this.Triangles)
                intersect |= tri.TryIntersect(this.Points, Ray, ref Param);
            return intersect;
        }
    }
}
