﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// Describes a vector that varies linearly over a scalar input (e.g. time).
    /// </summary>
    public struct Ray
    {
        public Ray(Vector Initial, Vector Derivative)
        {
            this.Initial = Initial;
            this.Derivative = Derivative;
        }

        /// <summary>
        /// The initial point of the ray.
        /// </summary>
        public Vector Initial;

        /// <summary>
        /// The derivative of the ray with respect to the input.
        /// </summary>
        public Vector Derivative;

        /// <summary>
        /// Gets the value of this ray at the given parameter.
        /// </summary>
        public Vector this[double Param]
        {
            get
            {
                return this.Initial + Param * this.Derivative;
            }
        }

        public static Ray operator +(Ray A, Ray B)
        {
            return new Ray(A.Initial + B.Initial, A.Derivative + B.Derivative);
        }

        public static Ray operator -(Ray A, Ray B)
        {
            return new Ray(A.Initial - B.Initial, A.Derivative - B.Derivative);
        }

        public static implicit operator Ray(Vector Value)
        {
            return new Ray(Value, Vector.Zero);
        }

        public override string ToString()
        {
            return String.Format("{0} + t{1}", this.Initial, this.Derivative);
        }
    }

    /// <summary>
    /// Describes a ray in 3D space.
    /// </summary>
    public struct DetailRay
    {
        public DetailRay(DetailVector Initial, DetailVector Derivative)
        {
            this.Initial = Initial;
            this.Derivative = Derivative;
        }

        /// <summary>
        /// The initial point of the ray.
        /// </summary>
        public DetailVector Initial;

        /// <summary>
        /// The derivative of the ray with respect to the input.
        /// </summary>
        public DetailVector Derivative;

        /// <summary>
        /// Gets the value of this ray at the given parameter.
        /// </summary>
        public DetailVector this[double Param]
        {
            get
            {
                return this.Initial + Param * this.Derivative;
            }
        }

        public static DetailRay operator +(DetailRay A, DetailRay B)
        {
            return new DetailRay(A.Initial + B.Initial, A.Derivative + B.Derivative);
        }

        public static DetailRay operator -(DetailRay A, DetailRay B)
        {
            return new DetailRay(A.Initial - B.Initial, A.Derivative - B.Derivative);
        }

        public static DetailRay operator *(UprightTransform Transform, DetailRay Ray)
        {
            return new DetailRay(Transform.ApplyPoint(Ray.Initial), Transform.ApplyVector(Ray.Derivative));
        }

        public static DetailRay operator *(Transform Transform, DetailRay Ray)
        {
            return (UprightTransform)Transform * Ray;
        }

        public static implicit operator DetailRay(DetailVector Value)
        {
            return new DetailRay(Value, DetailVector.Zero);
        }

        public override string ToString()
        {
            return String.Format("{0} + t{1}", this.Initial, this.Derivative);
        }
    }

    /// <summary>
    /// Describes a circle that moves linearly over a scalar input (e.g. time).
    /// </summary>
    public struct ThickRay
    {
        public ThickRay(Ray Center, double Radius)
        {
            this.Center = Center;
            this.Radius = Radius;
        }

        /// <summary>
        /// The location at the center of the circle.
        /// </summary>
        public Ray Center;

        /// <summary>
        /// The radius of the circle.
        /// </summary>
        public double Radius;

        /// <summary>
        /// Gets the value of this thick ray at the given parameter.
        /// </summary>
        public Circle this[double Param]
        {
            get
            {
                return new Circle(this.Center[Param], this.Radius);
            }
        }

        /// <summary>
        /// Translates this thick ray by the given offset.
        /// </summary>
        public ThickRay Translate(Ray Offset)
        {
            return new ThickRay(this.Center + Offset, this.Radius);
        }

        /// <summary>
        /// Applies the given padding to the boundary of this thick ray.
        /// </summary>
        public ThickRay Pad(double Amount)
        {
            return new ThickRay(this.Center, this.Radius + Amount);
        }

        /// <summary>
        /// Constructs the tighest thick ray which at all parameters in the interval [0, 1] contains the point
        /// traced by a parameteric arc starting at the given point and going around the origin by the given signed
        /// angle in one parametric unit.
        /// </summary>
        public static ThickRay TightArc(Vector Start, double Angle)
        {
            // If the arc contains a full circle, just return the circle, as this is the tightest bounds for all points
            // on the arc.
            if (Math.Abs(Angle) >= 2.0 * Math.PI)
                return new ThickRay(Vector.Zero, Start.Length);

            // For small angles, approximate with a thin line.
            if (Math.Abs(Angle) <= 0.001)
                return new ThickRay(new Ray(Start, -Start.Cross * Angle), 0.0);

            // Find the end point, compute the derivative of the ray
            Transform rot = Transform.Rotation(Angle);
            Vector end = rot * Start;
            Vector d = end - Start;

            // Find the direction perpendicular to the derivative, and radius of the thick ray
            Vector normal = d.Cross.Normal;
            if (Angle < 0.0) normal = -normal;
            double rad = (Start.Length - Vector.Dot(normal, Start)) / 2.0;

            // Compute initial offset of the ray
            Vector offset = Start + rad * normal;
            return new ThickRay(new Ray(offset, d), rad);
        }

        /// <summary>
        /// Constructs the tighest thick ray which at all parameters contains the point traced by a parameteric arc
        /// starting at the given point and going around the origin by the given signed angle in the given amount of
        /// parameteric units.
        /// </summary>
        /// <param name="Length">The amount of parameteric units it takes to trace out the arc.</param>
        public static ThickRay TightArc(Vector Start, double Angle, double Length)
        {
            ThickRay unit = TightArc(Start, Angle);
            return new ThickRay(new Ray(unit.Center.Initial, unit.Center.Derivative / Length), unit.Radius);
        }

        /// <summary>
        /// Tries getting the interval at which this thick ray contains zero, or returns false if this never happens.
        /// </summary>
        public bool TryWhenContainsZero(out double Start, out double End)
        {
            double deriSqrLen = Center.Derivative.SquareLength;
            if (deriSqrLen > 0.0)
            {
                double nearParam = -Vector.Dot(Center.Initial, Center.Derivative) / deriSqrLen;
                double sqrDis = Center[nearParam].SquareLength;
                double remDis = Radius * Radius - sqrDis;
                if (remDis >= 0.0)
                {
                    double paramOffset = Math.Sqrt(remDis / deriSqrLen);
                    Start = nearParam - paramOffset;
                    End = nearParam + paramOffset;
                    return true;
                }
            }
            else
            {
                if (Center.Initial.SquareLength < Radius * Radius)
                {
                    Start = double.NegativeInfinity;
                    End = double.PositiveInfinity;
                    return true;
                }
            }
            Start = End = 0.0;
            return false;
        }

        public static ThickRay operator +(ThickRay A, ThickRay B)
        {
            return new ThickRay(A.Center + B.Center, A.Radius + B.Radius);
        }

        public static ThickRay operator +(ThickRay A, Ray B)
        {
            return new ThickRay(A.Center + B, A.Radius);
        }

        public static ThickRay operator +(Ray A, ThickRay B)
        {
            return new ThickRay(A + B.Center, B.Radius);
        }

        public static ThickRay operator -(ThickRay A, ThickRay B)
        {
            return new ThickRay(A.Center - B.Center, A.Radius + B.Radius);
        }

        public static ThickRay operator -(ThickRay A, Ray B)
        {
            return new ThickRay(A.Center - B, A.Radius);
        }

        public static ThickRay operator -(Ray A, ThickRay B)
        {
            return new ThickRay(A - B.Center, B.Radius);
        }

        public static implicit operator ThickRay(Circle Value)
        {
            return new ThickRay(Value.Center, Value.Radius);
        }
    }
}