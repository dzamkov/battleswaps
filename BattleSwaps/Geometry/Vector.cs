﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using BattleSwaps.Data;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// Describes a vector in 2D space.
    /// </summary>
    public struct Vector
    {
        public double X;
        public double Y;

        [DefaultSerializer]
        public static readonly Serializer<Vector> Serializer = Data.Serializer.Struct<Vector>();
        public Vector(double X, double Y)
        {
            this.X = X;
            this.Y = Y;
        }

        /// <summary>
        /// The zero vector.
        /// </summary>
        public static readonly Vector Zero = new Vector(0.0, 0.0);

        /// <summary>
        /// Gets the angle, in radians, between two vectors.
        /// </summary>
        public static double AngleBetween(Vector From, Vector To)
        {
            return (To - From).Angle;
        }

        /// <summary>
        /// Gets the unit vector for the given angle.
        /// </summary>
        public static Vector FromAngle(double Angle)
        {
            return new Vector(Math.Cos(Angle), -Math.Sin(Angle));
        }

        /// <summary>
        /// Normalizes an angle to the range [-pi, pi].
        /// </summary>
        public static double NormalizeAngle(double Angle)
        {
            const double tau = 2.0 * Math.PI;
            return ((Angle + Math.PI) % tau + tau) % tau - Math.PI;
        }

        /// <summary>
        /// Gets the dot product of two vectors.
        /// </summary>
        public static double Dot(Vector A, Vector B)
        {
            return A.X * B.X + A.Y * B.Y;
        }

        /// <summary>
        /// Gets the determinant of two vectors. This is equal to the signed area of the parallelogram with
        /// sides determined by the given vectors.
        /// </summary>
        public static double Det(Vector A, Vector B)
        {
            return A.X * B.Y - A.Y * B.X;
        }

        /// <summary>
        /// Gives this vector a height and converts it to a detail vector.
        /// </summary>
        public DetailVector WithHeight(double Height)
        {
            return new DetailVector(this, Height);
        }

        /// <summary>
        /// Gets a vector perpendicular to the right of this vector.
        /// </summary>
        public Vector Cross
        {
            get
            {
                return new Vector(-this.Y, this.X);
            }
        }

        /// <summary>
        /// Gets the square of the length of this vector.
        /// </summary>
        public double SquareLength
        {
            get
            {
                return this.X * this.X + this.Y * this.Y;
            }
        }

        /// <summary>
        /// Gets the length of this vector.
        /// </summary>
        public double Length
        {
            get
            {
                return Math.Sqrt(this.SquareLength);
            }
        }

        /// <summary>
        /// Gets the angle of this vector.
        /// </summary>
        public double Angle
        {
            get
            {
                return Math.Atan2(-this.Y, this.X);
            }
        }

        /// <summary>
        /// Gets the unit vector with the same direction as this vector.
        /// </summary>
        public Vector Normal
        {
            get
            {
                return this / this.Length;
            }
        }

        public static Vector operator *(Vector A, double B)
        {
            return new Vector(A.X * B, A.Y * B);
        }

        public static Vector operator *(double A, Vector B)
        {
            return new Vector(A * B.X, A * B.Y);
        }

        public static Vector operator /(Vector A, double B)
        {
            return A * (1.0 / B);
        }

        public static Vector operator +(Vector A, Vector B)
        {
            return new Vector(A.X + B.X, A.Y + B.Y);
        }

        public static Vector operator -(Vector A, Vector B)
        {
            return new Vector(A.X - B.X, A.Y - B.Y);
        }

        public static Vector operator -(Vector A)
        {
            return new Vector(-A.X, -A.Y);
        }

        public static implicit operator System.Drawing.PointF(Vector Vector)
        {
            return new System.Drawing.PointF((float)Vector.X, (float)Vector.Y);
        }

        public static implicit operator Vector(System.Drawing.Point Point)
        {
            return new Vector(Point.X, Point.Y);
        }

        public static implicit operator OpenTK.Vector2(Vector Vector)
        {
            return new OpenTK.Vector2((float)Vector.X, (float)Vector.Y);
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", this.X, this.Y);
        }
    }

    /// <summary>
    /// Combines a vector in 2D space with a height scalar.
    /// </summary>
    public struct DetailVector
    {
        [DefaultSerializer]
        public static readonly Serializer<DetailVector> Serializer =
            Data.Serializer.Struct<DetailVector>();
        public DetailVector(Vector Lateral, double Height)
        {
            this.Lateral = Lateral;
            this.Height = Height;
        }

        public DetailVector(double X, double Y, double Z)
        {
            this.Lateral = new Vector(X, Y);
            this.Height = Z;
        }

        /// <summary>
        /// The lateral component of this vector.
        /// </summary>
        public Vector Lateral;

        /// <summary>
        /// Gets or sets the lateral "X" component of this vector.
        /// </summary>
        public double X
        {
            get
            {
                return this.Lateral.X;
            }
            set
            {
                this.Lateral.X = value;
            }
        }

        /// <summary>
        /// Gets or sets the lateral "Y" component of this vector.
        /// </summary>
        public double Y
        {
            get
            {
                return this.Lateral.Y;
            }
            set
            {
                this.Lateral.Y = value;
            }
        }

        /// <summary>
        /// The height component of this vector.
        /// </summary>
        public double Height;

        /// <summary>
        /// Gets or sets the vertical "Z" component of this vector.
        /// </summary>
        public double Z
        {
            get
            {
                return this.Height;
            }
            set
            {
                this.Height = value;
            }
        }

        /// <summary>
        /// The zero vector.
        /// </summary>
        public static readonly DetailVector Zero = new DetailVector(0.0, 0.0, 0.0);

        /// <summary>
        /// A unit vector that points upward.
        /// </summary>
        public static readonly DetailVector Up = new DetailVector(0.0, 0.0, 1.0);

        /// <summary>
        /// A unit vector that points down.
        /// </summary>
        public static readonly DetailVector Down = new DetailVector(0.0, 0.0, -1.0);

        /// <summary>
        /// Gets the square of the length of this vector.
        /// </summary>
        public double SquareLength
        {
            get
            {
                return this.Lateral.SquareLength + this.Height * this.Height;
            }
        }

        /// <summary>
        /// Gets the length of this vector.
        /// </summary>
        public double Length
        {
            get
            {
                return Math.Sqrt(this.SquareLength);
            }
        }

        /// <summary>
        /// Gets the unit vector with the same direction as this vector.
        /// </summary>
        public DetailVector Normal
        {
            get
            {
                return this / this.Length;
            }
        }

        /// <summary>
        /// Gets the cross product of two vectors.
        /// </summary>
        public static DetailVector Cross(DetailVector A, DetailVector B)
        {
            return new DetailVector(
                A.Y * B.Z - A.Z * B.Y,
                A.Z * B.X - A.X * B.Z,
                A.X * B.Y - A.Y * B.X);
        }

        /// <summary>
        /// Gets the dot product of two vectors.
        /// </summary>
        public static double Dot(DetailVector A, DetailVector B)
        {
            return A.X * B.X + A.Y * B.Y + A.Z * B.Z;
        }

        public static DetailVector operator *(DetailVector A, double B)
        {
            return new DetailVector(A.Lateral * B, A.Height * B);
        }

        public static DetailVector operator *(double A, DetailVector B)
        {
            return new DetailVector(A * B.Lateral, A * B.Height);
        }

        public static DetailVector operator /(DetailVector A, double B)
        {
            return A * (1.0 / B);
        }

        public static DetailVector operator +(DetailVector A, DetailVector B)
        {
            return new DetailVector(A.Lateral + B.Lateral, A.Height + B.Height);
        }

        public static DetailVector operator -(DetailVector A, DetailVector B)
        {
            return new DetailVector(A.Lateral - B.Lateral, A.Height - B.Height);
        }

        public static DetailVector operator -(DetailVector A)
        {
            return new DetailVector(-A.Lateral, -A.Height);
        }

        public static implicit operator OpenTK.Vector3(DetailVector Vector)
        {
            return new OpenTK.Vector3((float)Vector.X, (float)Vector.Y, (float)Vector.Height);
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}, {2})", this.Lateral.X, this.Lateral.Y, this.Height);
        }
    }
}
