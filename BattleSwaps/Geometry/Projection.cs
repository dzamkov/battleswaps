﻿using System;
using System.Collections.Generic;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// A projection from three-dimensional <see cref="DetailVector"/>s to two-dimensional <see cref="Vector"/>s
    /// which applies an affine transform in lateral space followed by height-based perspective scaling centered
    /// about the origin in two-dimensional space.
    /// </summary>
    public struct FixedProjection
    {
        public FixedProjection(Transform Lateral, double InverseHeight)
        {
            this.Lateral = Lateral;
            this.InverseHeight = InverseHeight;
        }

        /// <summary>
        /// The lateral component of this projection. This will be applied as-is for all points on the
        /// zero-height plane.
        /// </summary>
        public Transform Lateral;

        /// <summary>
        /// The reciprocal of the height of the perspective's apex. If this is 0, then the resulting projection will
        /// be orthogonal and equivalent to <see cref="Lateral"/>.
        /// </summary>
        public double InverseHeight;

        /// <summary>
        /// Constructs a perspective projection where the perspective's apex is at the given height above the
        /// zero-height plane, and the given transform is applied at the zero-height plane.
        /// </summary>
        public static Projection Perspective(Transform Lateral, double Height)
        {
            return new FixedProjection(Lateral, 1.0 / Height);
        }

        /// <summary>
        /// Constructs an orthogonal projection that applies the given transform in lateral space.
        /// </summary>
        public static FixedProjection Orthogonal(Transform Lateral)
        {
            return new FixedProjection(Lateral, 0.0);
        }

        /// <summary>
        /// Gets the transform this projection applies to lateral coordinates at the height plane with the given
        /// height.
        /// </summary>
        public Transform AtHeight(double Height)
        {
            return (1.0 / (1.0 - this.InverseHeight * Height)) * this.Lateral;
        }

        /// <summary>
        /// Applies this projection to a point.
        /// </summary>
        public Vector ApplyPoint(DetailVector Point)
        {
            return this.Lateral.ApplyPoint(Point.Lateral) / (1.0 - this.InverseHeight * Point.Height);
        }

        public static Vector operator *(FixedProjection Projection, DetailVector Point)
        {
            return Projection.ApplyPoint(Point);
        }

        public static FixedProjection operator *(FixedProjection Projection, UprightTransform Transform)
        {
            double scale = 1.0 / (1.0 - Projection.InverseHeight * Transform.Height.Offset);
            return new FixedProjection(
                scale * Projection.Lateral * Transform.Lateral,
                scale * Projection.InverseHeight * Transform.Height.Scale);
        }
    }

    /// <summary>
    /// A projection from three-dimensional <see cref="DetailVector"/>s to two-dimensional <see cref="Vector"/>s
    /// which applies an affine transform in lateral space along with height-based perspective scaling centered
    /// about some point.
    /// </summary>
    public struct Projection
    {
        public Projection(FixedProjection Fixed, Vector Offset)
        {
            this.Fixed = Fixed;
            this.Offset = Offset;
        }

        /// <summary>
        /// The transform from the original space to a two-dimensional space with perspective effects centered
        /// about the origin.
        /// </summary>
        public FixedProjection Fixed;

        /// <summary>
        /// The translational offset of the centered perspective space from the origin of the final space.
        /// </summary>
        public Vector Offset;

        /// <summary>
        /// Gets the reciprocal of the height of the perspective's apex.
        /// </summary>
        public double InverseHeight
        {
            get
            {
                return this.Fixed.InverseHeight;
            }
        }

        /// <summary>
        /// Gets the transform this projection applies to lateral coordinates at the height plane with the given
        /// height.
        /// </summary>
        public Transform AtHeight(double Height)
        {
            return Transform.Translation(this.Offset) * this.Fixed.AtHeight(Height);
        }

        /// <summary>
        /// Applies this transform to a point.
        /// </summary>
        public Vector ApplyPoint(DetailVector Point)
        {
            return this.Fixed.ApplyPoint(Point) + this.Offset;
        }

        /// <summary>
        /// Constructs a centered perspective projection where the perspective's apex is at the given height above the
        /// zero-height plane, and the given transform is applied at the zero-height plane.
        /// </summary>
        public static Projection Perspective(Transform Lateral, double Height)
        {
            return FixedProjection.Perspective(Lateral, Height);
        }

        /// <summary>
        /// Constructs an orthogonal projection that applies the given transform in lateral space.
        /// </summary>
        public static Projection Orthogonal(Transform Lateral)
        {
            return FixedProjection.Orthogonal(Lateral);
        }

        public static Vector operator *(Projection Projection, DetailVector Point)
        {
            return Projection.ApplyPoint(Point);
        }

        public static Projection operator *(Projection Projection, UprightTransform Transform)
        {
            return new Projection(Projection.Fixed * Transform, Projection.Offset);
        }

        public static Projection operator *(Transform Transform, Projection Projection)
        {
            Transform lateral = Projection.Fixed.Lateral;
            Transform nLateral = new Transform(
                Transform.ApplyVector(lateral.Offset),
                Transform.ApplyVector(lateral.X),
                Transform.ApplyVector(lateral.Y));
            return new Projection(
                new FixedProjection(nLateral, Projection.Fixed.InverseHeight),
                Transform.ApplyPoint(Projection.Offset));
        }

        public static implicit operator Projection(FixedProjection Fixed)
        {
            return new Projection(Fixed, Vector.Zero);
        }
    }
}
