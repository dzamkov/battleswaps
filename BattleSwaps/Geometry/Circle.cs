﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// Describes a circle in 2D space.
    /// </summary>
    public struct Circle
    {
        public Circle(Vector Center, double Radius)
        {
            this.Center = Center;
            this.Radius = Radius;
        }

        /// <summary>
        /// The point at the center of the circle.
        /// </summary>
        public Vector Center;

        /// <summary>
        /// The radius of the circle.
        /// </summary>
        public double Radius;

        /// <summary>
        /// Indicates whether this circle contains the given point.
        /// </summary>
        public bool Contains(Vector Point)
        {
            return (Point - this.Center).SquareLength <= this.Radius * this.Radius;
        }

        /// <summary>
        /// Translates this circle by the given offset.
        /// </summary>
        public Circle Translate(Vector Offset)
        {
            return new Circle(this.Center + Offset, this.Radius);
        }

        /// <summary>
        /// Applies the given padding to the boundary of this circle
        /// </summary>
        public Circle Pad(double Amount)
        {
            return new Circle(this.Center, this.Radius + Amount);
        }

        /// <summary>
        /// Gets the smallest circle with the given center that contains the given points.
        /// </summary>
        public static Circle TightAbout(Vector Center, IEnumerable<Vector> Points)
        {
            double sqrRad = 0.0;
            foreach (var point in Points)
                sqrRad = Math.Max(sqrRad, (point - Center).SquareLength);
            return new Circle(Center, Math.Sqrt(sqrRad));
        }

        /// <summary>
        /// Gets the smallest circle with the given center that contains the given polygon.
        /// </summary>
        public static Circle TightAbout(Vector Center, Polygon Polygon)
        {
            return TightAbout(Center, Polygon.Points);
        }

        /// <summary>
        /// Gets the smallest circle that contains the given points.
        /// </summary>
        public static Circle Tight(Vector A, Vector B)
        {
            return new Circle((A + B) / 2.0, (A - B).Length / 2.0);
        }

        /// <summary>
        /// Gets the smallest circle that contains the given points.
        /// </summary>
        public static Circle Tight(Vector A, Vector B, Vector C)
        {
            // From https://en.wikipedia.org/wiki/Circumscribed_circle
            double aSqrLen = A.SquareLength;
            double bSqrLen = B.SquareLength;
            double cSqrLen = C.SquareLength;
            double d = 2.0 * (A.X * (B.Y - C.Y) + B.X * (C.Y - A.Y) + C.X * (A.Y - B.Y));
            double centerX = (aSqrLen * (B.Y - C.Y) + bSqrLen * (C.Y - A.Y) + cSqrLen * (A.Y - B.Y)) / d;
            double centerY = (aSqrLen * (C.X - B.X) + bSqrLen * (A.X - C.X) + cSqrLen * (B.X - A.X)) / d;
            Vector center = new Vector(centerX, centerY);
            double radius = (A - center).Length;
            return new Circle(center, radius);
        }

        /// <summary>
        /// Gets the smallest circle that contains the points in the given array before the given indices, with the
        /// points at the given indices on the boundary.
        /// </summary>
        private static Circle _TightWith(Vector[] Points, int IndexA, int IndexB)
        {
            Circle cur = Tight(Points[IndexA], Points[IndexB]);
            for (int i = 0; i < IndexA; i++)
            {
                if (!cur.Contains(Points[i]))
                    cur = Tight(Points[i], Points[IndexA], Points[IndexB]);
            }
            return cur;
        }

        /// <summary>
        /// Gets the smallest circle that contains the points in the given array before the given index, with the
        /// point at the given index on the boundary.
        /// </summary>
        private static Circle _TightWith(Vector[] Points, int Index)
        {
            Circle cur = Tight(Points[0], Points[Index]);
            for (int i = 1; i < Index; i++)
            {
                if (!cur.Contains(Points[i]))
                    cur = _TightWith(Points, i, Index);
            }
            return cur;
        }

        /// <summary>
        /// Gets the smallest circle that contains the given points.
        /// </summary>
        public static Circle Tight(params Vector[] Points)
        {
            // From http://www.cs.uu.nl/docs/vakken/ga/slides4b.pdf
            Circle cur = Tight(Points[0], Points[1]);
            for (int i = 2; i < Points.Length; i++)
            {
                if (!cur.Contains(Points[i]))
                    cur = _TightWith(Points, i);
            }
            return cur;
        }

        /// <summary>
        /// Gets the smallest circle that contains the given polygon.
        /// </summary>
        public static Circle Tight(Polygon Polygon)
        {
            return Tight(Polygon.Points);
        }
    }
}