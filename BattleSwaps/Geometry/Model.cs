﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using BattleSwaps.Util;
using BattleSwaps.Data;

namespace BattleSwaps.Geometry
{
    using Material = BattleSwaps.Graphics.Scene.Material;

    /// <summary>
    /// Identifies and describes a named set of 3D meshes with possible material information.
    /// </summary>
    public struct Model
    {
        public Model(Resource<Data> Source,
            Maybe<string[]> FilterObjects,
            KeyValuePair<string, Material>[] Substitutions)
        {
            this.Source = Source;
            this.FilterObjects = FilterObjects;
            this.Substitutions = Substitutions;
        }

        /// <summary>
        /// The source data for this model.
        /// </summary>
        public Resource<Data> Source;

        /// <summary>
        /// The names for the objects considered by this model, or nothing if all objects are to be included.
        /// </summary>
        public Maybe<string[]> FilterObjects;

        /// <summary>
        /// The material substitutions applied to the model.
        /// </summary>
        public KeyValuePair<string, Material>[] Substitutions;

        /// <summary>
        /// Excludes all objects from this model except those with the given names.
        /// </summary>
        public Model Filter(params string[] Objects)
        {
            if (this.FilterObjects.IsNothing)
            {
                return new Model(this.Source, Maybe.Just(Objects), this.Substitutions);
            }
            else
            {
                HashSet<string> filterObjects = new HashSet<string>(this.FilterObjects.Value);
                filterObjects.IntersectWith(Objects);
                return new Model(this.Source, Maybe.Just(filterObjects.ToArray()), this.Substitutions);
            }
        }

        /// <summary>
        /// Substitutes a material in this model.
        /// </summary>
        public Model Substitute(string Name, Material Material)
        {
            if (this.Substitutions == null)
            {
                return new Model(this.Source, this.FilterObjects,
                    new[] { new KeyValuePair<string, Material>(Name, Material) });
            }
            else
            {
                // TODO: Easy task but I'm lazy right now
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Describes a geometry group for a model.
        /// </summary>
        public struct Group
        {
            public Group(Index[] Indices)
            {
                this.Indices = Indices;
            }

            /// <summary>
            /// The face indices for this group. All faces are assumed to be triangles.
            /// </summary>
            public Index[] Indices;
        }

        /// <summary>
        /// A multi-part vertex index.
        /// </summary>
        public struct Index
        {
            public Index(uint Position, uint UV)
            {
                this.Position = Position;
                this.UV = UV;
            }

            /// <summary>
            /// The index of the position of the vertex.
            /// </summary>
            public uint Position;

            /// <summary>
            /// The index of the UV for the vertex.
            /// </summary>
            public uint UV;

            public static bool operator ==(Index A, Index B)
            {
                return A.Position == B.Position && A.UV == B.UV;
            }

            public static bool operator !=(Index A, Index B)
            {
                return !(A == B);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Index))
                    return false;
                return this == (Index)obj;
            }

            public override int GetHashCode()
            {
                return this.Position.GetHashCode() + 257 * this.UV.GetHashCode();
            }

            public override string ToString()
            {
                return string.Format("{0}/{1}", this.Position, this.UV);
            }
        }

        /// <summary>
        /// Describes an object within a model.
        /// </summary>
        public struct Object
        {
            public Object(Dictionary<string, Group> Groups)
            {
                this.Groups = Groups;
            }

            /// <summary>
            /// The material-specific geometry groups in this object.
            /// </summary>
            public Dictionary<string, Group> Groups;
        }

        /// <summary>
        /// Describes a model.
        /// </summary>
        public struct Data
        {
            public Data(
                DetailVector[] Pos, Vector[] UV,
                Dictionary<string, Graphics.Scene.Material> Materials,
                Dictionary<string, Object> Objects)
            {
                this.Pos = Pos;
                this.UV = UV;
                this.Materials = Materials;
                this.Objects = Objects;
            }

            /// <summary>
            /// The vertex position data for this model.
            /// </summary>
            public DetailVector[] Pos;

            /// <summary>
            /// The vertex UV data for this model.
            /// </summary>
            public Vector[] UV;

            /// <summary>
            /// The default named materials for this model.
            /// </summary>
            public Dictionary<string, Material> Materials;

            /// <summary>
            /// The objects for this model table.
            /// </summary>
            public Dictionary<string, Object> Objects;

            /// <summary>
            /// Reads material data from the given text.
            /// </summary>
            private static void _ReadMaterials(
                AbsolutePath ResourcePath,
                Dictionary<string, Material> Materials,
                TextReader Reader)
            {
                string name = null;
                Material cur = Material.Default;

                // Read line by line
                string line;
                while ((line = Reader.ReadLine()) != null)
                {
                    // Skip empty lines and comments
                    if (line.Length == 0 || line[0] == '#')
                        continue;

                    // Parse
                    Parser parser = Parser.Create(line);
                    if (parser.TryAcceptItem("newmtl"))
                    {
                        // Write current material
                        if (name != null) Materials[name] = cur;

                        // Start new material
                        name = parser.ExpectString();
                        cur = Material.Default;
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("d"))
                    {
                        // Read opacity
                        double opacity = parser.ExpectDoubleItem();
                        if (opacity == 1.0)
                            cur.Visible = true;
                        else if (opacity == 0.0)
                            cur.Visible = false;
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("Kd"))
                    {
                        // Read diffuse color
                        cur.Diffuse.Base = new Graphics.Color(
                            parser.ExpectDoubleItem(),
                            parser.ExpectDoubleItem(),
                            parser.ExpectDoubleItem());
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("map_Kd"))
                    {
                        // Read texture
                        cur.Diffuse.Texture = Graphics.Image.Load(ResourcePath / parser.ExpectStringItem());
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("Ks"))
                    {
                        cur.Specular = new Graphics.Color(
                            parser.ExpectDoubleItem(),
                            parser.ExpectDoubleItem(),
                            parser.ExpectDoubleItem());
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("Ns"))
                    {
                        cur.Shininess = parser.ExpectDoubleItem();
                        parser.ExpectEOF();
                    }
                }

                // Write final material
                if (name != null) Materials[name] = cur;
            }

            /// <summary>
            /// Reads model data from the given text in the Wavefront object format.
            /// </summary>
            public static Data ReadObj(AbsolutePath ResourcePath, TextReader Reader)
            {
                List<DetailVector> pos = new List<DetailVector>();
                List<Vector> uv = new List<Vector>();
                Dictionary<string, Object> objects = new Dictionary<string, Object>();
                Dictionary<string, Material> materials = new Dictionary<string, Material>();

                List<Model.Index> groupInds = new List<Model.Index>();
                string groupMat = null;

                Dictionary<string, Group> objectGroups = new Dictionary<string, Group>();
                string objectName = null;

                // Read line by line
                string line;
                while ((line = Reader.ReadLine()) != null)
                {
                    // Skip empty lines and comments
                    if (line.Length == 0 || line[0] == '#')
                        continue;

                    // Parse
                    Parser parser = Parser.Create(line);
                    if (parser.TryAcceptItem("mtllib"))
                    {
                        // Read material library
                        AbsolutePath matpath = ResourcePath / parser.ExpectStringItem();
                        using (var usage = Blob.Load(matpath).Use())
                        {
                            Blob matblob = usage.Wait();
                            matblob.Use(null, reader => _ReadMaterials(
                                matpath / RelativePath.GoUp,
                                materials, reader));
                        }
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("v"))
                    {
                        // Read position data
                        double x = parser.ExpectDoubleItem();
                        double y = parser.ExpectDoubleItem();
                        double z = parser.ExpectDoubleItem();
                        pos.Add(new DetailVector(x, z, y));
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("vt"))
                    {
                        // Read UV data
                        uv.Add(new Vector(parser.ExpectDoubleItem(), 1.0 - parser.ExpectDoubleItem()));
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("o"))
                    {
                        if (groupInds.Count > 0)
                        {
                            objectGroups.Add(groupMat, new Model.Group(groupInds.ToArray()));
                            groupInds.Clear();
                        }
                        if (objectGroups.Count > 0)
                        {
                            objects.Add(objectName, new Object(objectGroups));
                            objectGroups = new Dictionary<string, Group>();
                        }
                        objectName = parser.ExpectStringItem();
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("g"))
                    {
                        if (groupInds.Count > 0)
                        {
                            objectGroups.Add(groupMat, new Model.Group(groupInds.ToArray()));
                            groupInds.Clear();
                        }
                        parser.ExpectStringItem();
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("usemtl"))
                    {
                        groupMat = parser.ExpectStringItem();
                        parser.ExpectEOF();
                    }
                    else if (parser.TryAcceptItem("f"))
                    {
                        // For each index
                        ulong posInd;
                        while (parser.TryAcceptIntegral(out posInd))
                        {
                            ulong uvInd = posInd;
                            if (parser.TryAccept('/'))
                            {
                                ulong integral;
                                if (parser.TryAcceptIntegral(out integral))
                                {
                                    uvInd = integral;
                                }
                                if (parser.TryAccept('/'))
                                    parser.TryAcceptIntegral(out integral);
                            }
                            parser.AcceptWhiteSpace();
                            groupInds.Add(new Model.Index((uint)posInd - 1, (uint)uvInd - 1));
                        }
                        parser.ExpectEOF();
                    }
                }

                // Emit current group and object
                if (groupInds.Count > 0) objectGroups.Add(groupMat, new Model.Group(groupInds.ToArray()));
                if (objectGroups.Count > 0) objects.Add(objectName, new Object(objectGroups));

                // Build data
                return new Data(pos.ToArray(), uv.ToArray(), materials, objects);
            }

            /// <summary>
            /// Reads model data from a blob.
            /// </summary>
            public static Data ReadObj(AbsolutePath ResourcePath, Blob Blob)
            {
                Data data = default(Data);
                Blob.Use(null, reader => data = ReadObj(ResourcePath, reader));
                return data;
            }

            /// <summary>
            /// Gets the index of a point when producing a mesh.
            /// </summary>
            private uint _GetMeshIndex(List<DetailVector> Points, Dictionary<uint, uint> Map, Index Index)
            {
                uint res;
                if (!Map.TryGetValue(Index.Position, out res))
                {
                    res = (uint)Points.Count;
                    Points.Add(this.Pos[Index.Position]);
                    Map[Index.Position] = res;
                }
                return res;
            }

            /// <summary>
            /// Adds an object to a mesh under construction.
            /// </summary>
            private void _AddMesh(
                List<DetailVector> Points,
                Dictionary<uint, uint> Inds,
                List<Mesh.Triangle> Tris,
                Object Object)
            {
                foreach (var kvp in Object.Groups)
                {
                    Group group = kvp.Value;
                    for (int i = 0; i < group.Indices.Length; i += 3)
                    {
                        uint indA = this._GetMeshIndex(Points, Inds, group.Indices[i + 0]);
                        uint indB = this._GetMeshIndex(Points, Inds, group.Indices[i + 1]);
                        uint indC = this._GetMeshIndex(Points, Inds, group.Indices[i + 2]);
                        Tris.Add(new Mesh.Triangle(indA, indB, indC));
                    }
                }
            }

            /// <summary>
            /// Converts this model into a mesh by extracting the geometry of the given named objects.
            /// </summary>
            public Mesh ToMesh(IEnumerable<string> Objects)
            {
                List<DetailVector> points = new List<DetailVector>();
                Dictionary<uint, uint> inds = new Dictionary<uint, uint>();
                List<Mesh.Triangle> tris = new List<Mesh.Triangle>();
                foreach (var objectName in Objects)
                {
                    Object obj;
                    if (this.Objects.TryGetValue(objectName, out obj))
                    {
                        this._AddMesh(points, inds, tris, obj);
                    }
                }
                return new Mesh(points.ToArray(), tris.ToArray());
            }

            /// <summary>
            /// Converts this model into a mesh by extracting the geometry of all objects.
            /// </summary>
            public Mesh ToMesh()
            {
                List<DetailVector> points = new List<DetailVector>();
                Dictionary<uint, uint> inds = new Dictionary<uint, uint>();
                List<Mesh.Triangle> tris = new List<Mesh.Triangle>();
                foreach (var obj in this.Objects)
                    this._AddMesh(points, inds, tris, obj.Value);
                return new Mesh(points.ToArray(), tris.ToArray());
            }
        }

        /// <summary>
        /// A resource for a model loaded from a blob.
        /// </summary>
        [Tag(typeof(Resource<Model.Data>), (uint)3609290138)]
        public sealed class LoadResource : Resource<Model.Data>
        {
            [DefaultSerializer]
            public static readonly Serializer<LoadResource> Serializer =
                BattleSwaps.Data.Serializer.Immutable<LoadResource>();
            public readonly AbsolutePath ResourcePath;
            public readonly Resource<Blob> Source;
            private LoadResource(AbsolutePath ResourcePath, Resource<Blob> Source)
            {
                this.ResourcePath = ResourcePath;
                this.Source = Source;
            }

            public static LoadResource Get(AbsolutePath ResourcePath, Resource<Blob> Source)
            {
                return (LoadResource)Resource.Coalesce(new LoadResource(ResourcePath, Source));
            }

            protected override void Load()
            {
                using (var sourceUsage = this.Source.Use())
                {
                    this.Provide(Data.ReadObj(this.ResourcePath, sourceUsage.Wait()));
                }
            }
        }

        /// <summary>
        /// Loads a named model as a resource.
        /// </summary>
        public static Model Load(AbsolutePath Path)
        {
            return new Model(
                LoadResource.Get(Path / RelativePath.GoUp, Blob.Load(Path)),
                Maybe<string[]>.Nothing, null);
        }

        /// <summary>
        /// A resource for a mesh loaded from a model.
        /// </summary>
        [Tag(typeof(Resource<Mesh>), (uint)506115369)]
        public sealed class MeshResource : Resource<Mesh>
        {
            [DefaultSerializer]
            public static readonly Serializer<MeshResource> Serializer =
                BattleSwaps.Data.Serializer.Immutable<MeshResource>();
            public readonly Resource<Model.Data> Source;
            public readonly Maybe<string[]> FilterObjects;
            private MeshResource(Resource<Model.Data> Source, Maybe<string[]> FilterObjects)
            {
                this.Source = Source;
                this.FilterObjects = FilterObjects;
            }

            public static MeshResource Get(Resource<Model.Data> Source, Maybe<string[]> FilterObjects)
            {
                return (MeshResource)Resource.Coalesce(new MeshResource(Source, FilterObjects));
            }

            protected override void Load()
            {
                using (var sourceUsage = this.Source.Use())
                {
                    if (this.FilterObjects.IsJust)
                        this.Provide(sourceUsage.Wait().ToMesh(this.FilterObjects.Value));
                    else
                        this.Provide(sourceUsage.Wait().ToMesh());
                }
            }
        }

        /// <summary>
        /// Converts this model into a mesh.
        /// </summary>
        public Resource<Mesh> ToMesh()
        {
            return MeshResource.Get(this.Source, this.FilterObjects);
        }
    }
}
