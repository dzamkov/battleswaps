﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// Describes the size of an image.
    /// </summary>
    public struct ImageSize
    {
        public ImageSize(uint Width, uint Height)
        {
            this.Width = Width;
            this.Height = Height;
        }

        /// <summary>
        /// The width, in pixels, of the image.
        /// </summary>
        public uint Width;

        /// <summary>
        /// The height, in pixels, of the image.
        /// </summary>
        public uint Height;

        /// <summary>
        /// An image size with zero components.
        /// </summary>
        public static readonly ImageSize Zero = new ImageSize(0, 0);

        /// <summary>
        /// Indicates whether both components of this size are zero.
        /// </summary>
        public bool IsZero
        {
            get
            {
                return this.Width == 0 && this.Height == 0;
            }
        }

        /// <summary>
        /// Adds to this image size.
        /// </summary>
        public ImageSize Pad(uint Width, uint Height)
        {
            return new ImageSize(Width + this.Width, Height + this.Height);
        }

        /// <summary>
        /// Adds to this image size.
        /// </summary>
        public ImageSize Pad(uint Amount)
        {
            return this.Pad(Amount, Amount);
        }

        public static bool operator ==(ImageSize A, ImageSize B)
        {
            return A.Width == B.Width && A.Height == B.Height;
        }

        public static bool operator !=(ImageSize A, ImageSize B)
        {
            return A.Width != B.Width || A.Height != B.Height;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ImageSize))
                return false;
            return this == (ImageSize)obj;
        }

        public override int GetHashCode()
        {
            return this.Width.GetHashCode() ^ this.Height.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}x{1}", this.Width, this.Height);
        }

        public static implicit operator ImageSize(System.Drawing.Size Size)
        {
            return new ImageSize((uint)Size.Width, (uint)Size.Height);
        }

        public static explicit operator Vector(ImageSize Size)
        {
            return new Vector(Size.Width, Size.Height);
        }
    }

    /// <summary>
    /// A point on an image.
    /// </summary>
    public struct ImagePoint
    {
        public uint X;
        public uint Y;
        public ImagePoint(uint X, uint Y)
        {
            this.X = X;
            this.Y = Y;
        }

        /// <summary>
        /// The point (0, 0).
        /// </summary>
        public static readonly ImagePoint Zero = new ImagePoint(0, 0);

        public static implicit operator ImagePoint(System.Drawing.Point Point)
        {
            return new ImagePoint((uint)Point.X, (uint)Point.Y);
        }

        public static explicit operator Vector(ImagePoint Point)
        {
            return new Vector(Point.X, Point.Y);
        }

        public static explicit operator ImagePoint(Vector Vector)
        {
            return new ImagePoint((uint)Vector.X, (uint)Vector.Y);
        }

        public static ImagePoint operator +(ImagePoint A, ImagePoint B)
        {
            return new ImagePoint(A.X + B.X, A.Y + B.Y);
        }

        public override string ToString()
        {
            return String.Format("({0}, {1})", this.X, this.Y);
        }
    }

    /// <summary>
    /// Identifies a 2D rectangular region on an image, such as a texture.
    /// </summary>
    public struct ImageBounds
    {
        public ImagePoint Min;
        public ImagePoint Max;
        public ImageBounds(ImagePoint Min, ImagePoint Max)
        {
            Debug.Assert(Max.X >= Min.X);
            Debug.Assert(Max.Y >= Min.Y);
            this.Min = Min;
            this.Max = Max;
        }

        public ImageBounds(uint MinX, uint MaxX, uint MinY, uint MaxY)
            : this(new ImagePoint(MinX, MinY), new ImagePoint(MaxX, MaxY))
        { }

        public ImageBounds(ImagePoint Min, ImageSize Size)
            : this(Min, new ImagePoint(Min.X + Size.Width - 1, Min.Y + Size.Height - 1))
        { }

        public ImageBounds(uint MinX, uint MinY, ImageSize Size)
            : this(new ImagePoint(MinX, MinY), Size)
        { }

        public ImageBounds(ImageSize Size)
            : this(0, 0, Size)
        { }

        /// <summary>
        /// Gets the width of these bounds.
        /// </summary>
        public uint Width
        {
            get
            {
                return this.Max.X - this.Min.X + 1;
            }
        }

        /// <summary>
        /// Gets the height of these bounds.
        /// </summary>
        public uint Height
        {
            get
            {
                return this.Max.Y - this.Min.Y + 1;
            }
        }

        /// <summary>
        /// Gets the size of these bounds.
        /// </summary>
        public ImageSize Size
        {
            get
            {
                return new ImageSize(this.Width, this.Height);
            }
        }

        /// <summary>
        /// Gets the relative bounds of the given inner bounds inside this container. This will return
        /// the unit bounds, ([0, 1], [0, 1]), if the bounds are the same.
        /// </summary>
        public Bounds Relative(ImageBounds Inner)
        {
            Vector containerSize = (Vector)this.Size;
            return new Bounds(
                ((double)Inner.Min.X - this.Min.X) / containerSize.X,
                ((double)(Inner.Max.X + 1) - this.Min.X) / containerSize.X,
                ((double)Inner.Min.Y - this.Min.Y) / containerSize.Y,
                ((double)(Inner.Max.Y + 1) - this.Min.Y) / containerSize.Y);
        }

        /// <summary>
        /// Gets the bounds of a rectangle of the given size embedded in the center of these bounds.
        /// </summary>
        public ImageBounds Embed(ImageSize Size)
        {
            Debug.Assert(Size.Width <= this.Width);
            Debug.Assert(Size.Height <= this.Height);
            uint hWidth = Size.Width / 2;
            uint hHeight = Size.Height / 2;
            uint cx = (this.Min.X + this.Max.X + 1) / 2;
            uint cy = (this.Min.Y + this.Max.Y + 1) / 2;
            return new ImageBounds(cx - hWidth, cy - hHeight, Size);
        }

        public override string ToString()
        {
            return String.Format("([{0}, {1}], [{2}, {3}])", this.Min.X, this.Max.X, this.Min.Y, this.Max.Y);
        }

        public static implicit operator Bounds(ImageBounds Bounds)
        {
            return new Bounds(Bounds.Min.X, Bounds.Max.X + 1, Bounds.Min.Y, Bounds.Max.Y + 1);
        }
    }
}
