﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// Describes a rectangular region in 2D space.
    /// </summary>
    public struct Rectangle
    {
        public Rectangle(Transform Transform)
        {
            this.Transform = Transform;
        }

        public Rectangle(Vector Center, double Angle, double Length, double Width)
        {
            this.Transform = 
                Transform.Translation(Center) *
                Transform.Rotation(Angle) *
                Transform.Scale(Length, Width) *
                Transform.Translation(-0.5, -0.5);
        }

        /// <summary>
        /// The transform from the unit bounds to this rectangle.
        /// </summary>
        public Transform Transform;

        /// <summary>
        /// Indicates whether this rectangle contain the given point.
        /// </summary>
        public bool Contains(Vector Point)
        {
            return Bounds.Unit.Contains(this.Transform.Inverse.ApplyPoint(Point));
        }

        /// <summary>
        /// Gets the signed area of this rectangle.
        /// </summary>
        public double Area
        {
            get
            {
                return Vector.Det(this.Transform.X, this.Transform.Y);
            }
        }

        /// <summary>
        /// Gets the point at the center of this rectangle.
        /// </summary>
        public Vector Center
        {
            get
            {
                return this.Transform * new Vector(0.5, 0.5);
            }
        }

        /// <summary>
        /// Creates a convex polygon representation of this rectangle.
        /// </summary>
        public Convex ToConvex()
        {
            return Convex.FromArray(
                this.Transform.Offset,
                this.Transform.Offset + this.Transform.Y,
                this.Transform.Offset + this.Transform.Y + this.Transform.X,
                this.Transform.Offset + this.Transform.X);
        }

        /// <summary>
        /// Applies the given padding to all edges of this rectangle.
        /// </summary>
        public Rectangle Pad(double Amount)
        {
            double length = this.Transform.X.Length;
            double width = this.Transform.Y.Length;
            return new Rectangle(this.Transform * Transform.Between(Bounds.Unit, new Bounds(
                -Amount / length, 1.0 + Amount / length,
                -Amount / width, 1.0 + Amount / width)));
        }

        /// <summary>
        /// The rectangle for the bounds ([0, 1], [0, 1]).
        /// </summary>
        public static readonly Rectangle Unit = new Rectangle(Transform.Identity);

        /// <summary>
        /// The rectangle for the bounds ([-1, 1], [-1, 1]).
        /// </summary>
        public static readonly Rectangle Device = FromBounds(Bounds.Device);

        /// <summary>
        /// Constructs a rectangle containing all and only points in the given bounds.
        /// </summary>
        public static Rectangle FromBounds(Bounds Bounds)
        {
            return new Rectangle(Transform.Translation(Bounds.Min) * Transform.Scale(Bounds.Size));
        }

        public static implicit operator Rectangle(Bounds Bounds)
        {
            return FromBounds(Bounds);
        }

        public static implicit operator Convex(Rectangle Rectangle)
        {
            return Rectangle.ToConvex();
        }

        public static Rectangle operator *(Transform Transform, Rectangle Rectangle)
        {
            return new Rectangle(Transform * Rectangle.Transform);
        }

        public override string ToString()
        {
            return this.Transform.ToString();
        }
    }
}
