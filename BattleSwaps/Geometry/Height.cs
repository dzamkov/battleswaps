﻿using System;
using System.Diagnostics;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// Describes the range of heights in a scene.
    /// </summary>
    public struct HeightRange
    {
        public HeightRange(double Min, double Max)
        {
            this.Min = Min;
            this.Max = Max;
        }

        /// <summary>
        /// The minimum height.
        /// </summary>
        public double Min;

        /// <summary>
        /// The maximum height.
        /// </summary>
        public double Max;

        /// <summary>
        /// Gets the size of this range.
        /// </summary>
        public double Size
        {
            get
            {
                Debug.Assert(!this.IsEmpty);
                return this.Max - this.Min;
            }
        }

        /// <summary>
        /// Indicates whether this height range is empty.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this.Max <= this.Min;
            }
        }

        /// <summary>
        /// The empty height range.
        /// </summary>
        public static readonly HeightRange Empty = new HeightRange(double.PositiveInfinity, double.NegativeInfinity);

        /// <summary>
        /// The height range of [0, 1].
        /// </summary>
        public static readonly HeightRange Unit = new HeightRange(0.0, 1.0);

        /// <summary>
        /// Gets the union of two height ranges.
        /// </summary>
        public static HeightRange Union(HeightRange A, HeightRange B)
        {
            return new HeightRange(
                Math.Min(A.Min, B.Min),
                Math.Max(A.Max, B.Max));
        }

        public static HeightRange operator *(HeightTransform Transform, HeightRange Range)
        {
            return new HeightRange(Transform * Range.Min, Transform * Range.Max);
        }

        public override string ToString()
        {
            if (this.IsEmpty) return "Empty";
            return string.Format("[{0}, {1}]", this.Min, this.Max);
        }
    }

    /// <summary>
    /// Transforms the height of a scene.
    /// </summary>
    public struct HeightTransform
    {
        public HeightTransform(double Offset, double Scale)
        {
            this.Offset = Offset;
            this.Scale = Scale;
        }

        /// <summary>
        /// The height added to the scene.
        /// </summary>
        public double Offset;

        /// <summary>
        /// The scaling factor applied to the height of the scene.
        /// </summary>
        public double Scale;

        /// <summary>
        /// The identity height transform.
        /// </summary>
        public static readonly HeightTransform Identity = new HeightTransform(0.0, 1.0);

        /// <summary>
        /// Constructs a transform that adds an offset to all heights.
        /// </summary>
        public static HeightTransform Translation(double Offset)
        {
            return new HeightTransform(Offset, 1.0);
        }

        /// <summary>
        /// Gets the transform between two height ranges.
        /// </summary>
        public static HeightTransform Between(HeightRange From, HeightRange To)
        {
            double scale = To.Size / From.Size;
            return new HeightTransform(To.Min - From.Min * scale, scale);
        }

        /// <summary>
        /// Gets the inverse of this transform.
        /// </summary>
        public HeightTransform Inverse
        {
            get
            {
                return new HeightTransform(-this.Offset / this.Scale, 1.0 / this.Scale);
            }
        }

        /// <summary>
        /// Applies a height transform to a height.
        /// </summary>
        public static double operator *(HeightTransform Transform, double Height)
        {
            return Transform.Offset + Transform.Scale * Height;
        }

        /// <summary>
        /// Composes two height transforms.
        /// </summary>
        public static HeightTransform operator *(HeightTransform A, HeightTransform B)
        {
            return new HeightTransform(A * B.Offset, A.Scale * B.Scale);
        }

        public static UprightTransform operator *(HeightTransform A, Transform B)
        {
            return new UprightTransform(B, A);
        }

        public static UprightTransform operator *(Transform A, HeightTransform B)
        {
            return new UprightTransform(A, B);
        }

        public static implicit operator OpenTK.Vector2(HeightTransform Transform)
        {
            return new OpenTK.Vector2((float)Transform.Scale, (float)Transform.Offset);
        }
    }

    /// <summary>
    /// Combines a height transform with a regular 2D transform.
    /// </summary>
    public struct UprightTransform
    {
        public UprightTransform(Transform Lateral, HeightTransform Height)
        {
            this.Lateral = Lateral;
            this.Height = Height;
        }

        /// <summary>
        /// The lateral component of this transform.
        /// </summary>
        public Transform Lateral;

        /// <summary>
        /// The height component of this transform.
        /// </summary>
        public HeightTransform Height;

        /// <summary>
        /// Gets the offset of the origin when this transform is applied.
        /// </summary>
        public DetailVector Offset
        {
            get
            {
                return new DetailVector(this.Lateral.Offset, this.Height.Offset);
            }
        }

        /// <summary>
        /// Gets the direction of the X vector when this transform is applied.
        /// </summary>
        public DetailVector X
        {
            get
            {
                return new DetailVector(this.Lateral.X, 0.0);
            }
        }

        /// <summary>
        /// Gets the direction of the Y vector when this transform is applied.
        /// </summary>
        public DetailVector Y
        {
            get
            {
                return new DetailVector(this.Lateral.Y, 0.0);
            }
        }

        /// <summary>
        /// Gets the direction of the Z vector when this transform is applied.
        /// </summary>
        public DetailVector Z
        {
            get
            {
                return new DetailVector(Vector.Zero, this.Height.Scale);
            }
        }

        /// <summary>
        /// The identity transform.
        /// </summary>
        public static readonly UprightTransform Identity = new UprightTransform
        {
            Lateral = Transform.Identity,
            Height = HeightTransform.Identity
        };

        /// <summary>
        /// Gets the inverse of this transform.
        /// </summary>
        public UprightTransform Inverse
        {
            get
            {
                return new UprightTransform(this.Lateral.Inverse, this.Height.Inverse);
            }
        }

        /// <summary>
        /// Applies this transform to a point. This will apply the offset and linear transform.
        /// </summary>
        public DetailVector ApplyPoint(DetailVector Point)
        {
            return new DetailVector(this.Lateral.ApplyPoint(Point.Lateral), this.Height * Point.Height);
        }

        /// <summary>
        /// Applies this transform to a vector. This will apply only the linear transform.
        /// </summary>
        public DetailVector ApplyVector(DetailVector Vector)
        {
            return new DetailVector(this.Lateral.ApplyVector(Vector.Lateral), this.Height.Scale * Vector.Height);
        }

        /// <summary>
        /// Composes two transforms.
        /// </summary>
        public static UprightTransform operator *(UprightTransform A, UprightTransform B)
        {
            return new UprightTransform(A.Lateral * B.Lateral, A.Height * B.Height);
        }

        public static implicit operator UprightTransform(Transform Transform)
        {
            return new UprightTransform(Transform, HeightTransform.Identity);
        }

        public static implicit operator UprightTransform(HeightTransform HeightTransform)
        {
            return new UprightTransform(Transform.Identity, HeightTransform);
        }

        public static implicit operator OpenTK.Matrix4x3(UprightTransform Transform)
        {
            float a = (float)Transform.Lateral.X.X;
            float b = (float)Transform.Lateral.X.Y;
            float c = (float)Transform.Lateral.Y.X;
            float d = (float)Transform.Lateral.Y.Y;
            float e = (float)Transform.Lateral.Offset.X;
            float f = (float)Transform.Lateral.Offset.Y;
            float g = (float)Transform.Height.Offset;
            float h = (float)Transform.Height.Scale;
            return new OpenTK.Matrix4x3(
                a, b, 0,
                c, d, 0,
                0, 0, h,
                e, f, g);
        }
    }
}
