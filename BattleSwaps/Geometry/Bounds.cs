﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// Identifies an axis-aligned rectangular region in 2D space.
    /// </summary>
    public struct Bounds
    {
        public Vector Min;
        public Vector Max;

        public Bounds(Vector Min, Vector Max)
        {
            this.Min = Min;
            this.Max = Max;
        }

        public Bounds(double MinX, double MaxX, double MinY, double MaxY)
            : this(new Vector(MinX, MinY), new Vector(MaxX, MaxY))
        { }

        /// <summary>
        /// Gets the point at the center of these bounds.
        /// </summary>
        public Vector Center
        {
            get
            {
                Debug.Assert(!this.IsEmpty);
                return new Vector(
                    (this.Min.X + this.Max.X) / 2.0,
                    (this.Min.Y + this.Max.Y) / 2.0);
            }
        }

        /// <summary>
        /// The empty region.
        /// </summary>
        public static readonly Bounds Empty = new Bounds(
            double.PositiveInfinity, double.NegativeInfinity,
            double.PositiveInfinity, double.NegativeInfinity);

        /// <summary>
        /// The bounds ([-1, 1], [-1, 1]).
        /// </summary>
        public static readonly Bounds Device = new Bounds(-1.0, 1.0, -1.0, 1.0);

        /// <summary>
        /// The bounds ([0, 1], [0, 1]).
        /// </summary>
        public static readonly Bounds Unit = new Bounds(0.0, 1.0, 0.0, 1.0);

        /// <summary>
        /// Gets the intersection of two bounds.
        /// </summary>
        public static Bounds Intersection(Bounds A, Bounds B)
        {
            return new Bounds(
                new Vector(Math.Max(A.Min.X, B.Min.X), Math.Max(A.Min.Y, B.Min.Y)),
                new Vector(Math.Min(A.Max.X, B.Max.X), Math.Min(A.Max.Y, B.Max.Y)));
        }

        /// <summary>
        /// Gets the tightest approximation of the given bounds transformed by the given transform.
        /// </summary>
        public static Bounds TightTransform(Transform Transform, Bounds Bounds)
        {
            return Tight(
                Transform * Bounds.Min,
                Transform * Bounds.Max,
                Transform * new Vector(Bounds.Min.X, Bounds.Max.Y),
                Transform * new Vector(Bounds.Max.X, Bounds.Min.Y));
        }

        /// <summary>
        /// Gets the tightest bounds that contain the given vectors.
        /// </summary>
        public static Bounds Tight(params Vector[] Vectors)
        {
            Bounds bounds = Empty;
            foreach (Vector vec in Vectors)
                bounds = bounds.TightUnionWith(vec);
            return bounds;
        }

        /// <summary>
        /// Gets the tightest bounds containing these bounds and the given vector.
        /// </summary>
        public Bounds TightUnionWith(Vector Vector)
        {
            return new Bounds(
                Math.Min(this.Min.X, Vector.X),
                Math.Max(this.Max.X, Vector.X),
                Math.Min(this.Min.Y, Vector.Y),
                Math.Max(this.Max.Y, Vector.Y));
        }

        /// <summary>
        /// Gets the relative bounds of the given inner bounds inside this container. This will return
        /// the unit bounds, ([0, 1], [0, 1]), if the bounds are the same.
        /// </summary>
        public Bounds Relative(Bounds Inner)
        {
            Vector containerSize = this.Size;
            return new Bounds(
                (Inner.Min.X - this.Min.X) / containerSize.X,
                (Inner.Max.X - this.Min.X) / containerSize.X,
                (Inner.Min.Y - this.Min.Y) / containerSize.Y,
                (Inner.Max.Y - this.Min.Y) / containerSize.Y);
        }

        /// <summary>
        /// Applies uniform padding to all edges of these bounds. Padding may be negative to create a margin.
        /// </summary>
        public Bounds Pad(double Amount)
        {
            return new Bounds(
                this.Min.X - Amount, this.Max.X + Amount,
                this.Min.Y - Amount, this.Max.Y + Amount);
        }

        /// <summary>
        /// Gets the size of these bounds.
        /// </summary>
        public Vector Size
        {
            get
            {
                return this.Max - this.Min;
            }
        }

        /// <summary>
        /// Indicates whether these bounds are empty.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this.Max.X <= this.Min.X || this.Max.Y <= this.Min.Y;
            }
        }

        /// <summary>
        /// Rounds all coordinates of these bounds to integers. The resulting bounds will still contain these
        /// bounds.
        /// </summary>
        public Bounds Round
        {
            get
            {
                return new Bounds(
                    Math.Floor(this.Min.X), Math.Ceiling(this.Max.X),
                    Math.Floor(this.Min.Y), Math.Ceiling(this.Max.Y));
            }
        }

        /// <summary>
        /// Indicates whether these bounds contain the given point.
        /// </summary>
        public bool Contains(Vector Point)
        {
            return
                Point.X >= this.Min.X &&
                Point.Y >= this.Min.Y &&
                Point.X <= this.Max.X &&
                Point.Y <= this.Max.Y;
        }

        /// <summary>
        /// Creates a convex polygon representation of these bounds
        /// </summary>
        public Convex ToConvex()
        {
            return Convex.FromArray(
                new Vector(this.Max.X, this.Min.Y),
                new Vector(this.Min.X, this.Min.Y),
                new Vector(this.Min.X, this.Max.Y),
                new Vector(this.Max.X, this.Max.Y));
        }

        public static implicit operator Convex(Bounds Bounds)
        {
            return Bounds.ToConvex();
        }

        public static implicit operator Polygon(Bounds Bounds)
        {
            return Bounds.ToConvex();
        }

        public static Rectangle operator *(Transform Transform, Bounds Bounds)
        {
            return Transform * Rectangle.FromBounds(Bounds);
        }

        public static Bounds operator *(OrthoTransform Transform, Bounds Bounds)
        {
            return new Bounds(
                Transform.Offset.X + Transform.Scale * Bounds.Min.X,
                Transform.Offset.X + Transform.Scale * Bounds.Max.X,
                Transform.Offset.Y + Transform.Scale * Bounds.Min.Y,
                Transform.Offset.Y + Transform.Scale * Bounds.Max.Y);
        }

        public override string ToString()
        {
            if (this.IsEmpty) return "Empty";
            return String.Format("([{0}, {1}], [{2}, {3}])", this.Min.X, this.Max.X, this.Min.Y, this.Max.Y);
        }
    }
}
