﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// Identifies a possible bounding size of a container.
    /// </summary>
    public struct Size
    {
        public int Width;
        public int Length;

        public Size(int Width, int Length)
        {
            Debug.Assert(Width >= 1);
            Debug.Assert(Length >= 1);
            this.Width = Width;
            this.Length = Length;
        }

        public override string ToString()
        {
            return String.Format("{0}x{1}", this.Width, this.Length);
        }

        public static bool operator ==(Size A, Size B)
        {
            return A.Width == B.Width && A.Length == B.Length;
        }

        public static bool operator !=(Size A, Size B)
        {
            return A.Width != B.Width || A.Length != B.Length;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Size)) return false;
            return this == (Size)obj;
        }

        public override int GetHashCode()
        {
            return this.Length + 65536 * this.Width;
        }
    }

    /// <summary>
    /// Identifies a cardinal direction.
    /// </summary>
    public enum Direction
    {
        East,
        North,
        West,
        South
    }

    /// <summary>
    /// Identifies a tile offset within a container.
    /// </summary>
    public struct Tile
    {
        public int X;
        public int Y;

        public Tile(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }

        /// <summary>
        /// The tile at (0, 0).
        /// </summary>
        public static readonly Tile Origin = new Tile(0, 0);

        /// <summary>
        /// Gets the point at the center of this tile.
        /// </summary>
        public Vector Center
        {
            get
            {
                return new Vector(this.X + 0.5, this.Y + 0.5);
            }
        }

        /// <summary>
        /// Finds the tile that the given point belongs in.
        /// </summary>
        public static Tile FromPoint(Vector Point)
        {
            return new Tile((int)Math.Floor(Point.X), (int)Math.Floor(Point.Y));
        }

        public static bool operator ==(Tile A, Tile B)
        {
            return A.X == B.X && A.Y == B.Y;
        }

        public static bool operator !=(Tile A, Tile B)
        {
            return A.X != B.X || A.Y != B.Y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Tile)) return false;
            return this == (Tile)obj;
        }

        public override int GetHashCode()
        {
            return this.X + 65536 * this.Y;
        }

        public override string ToString()
        {
            return String.Format("({0}, {1})", this.X, this.Y);
        }
    }

    /// <summary>
    /// Identifies a rectangular region in a tile grid.
    /// </summary>
    public struct TileBounds
    {
        public Tile Min;
        public Tile Max;

        public TileBounds(Tile Min, Tile Max)
        {
            this.Min = Min;
            this.Max = Max;
        }

        public TileBounds(Tile Offset, Size Size)
        {
            this.Min = Offset;
            this.Max = new Tile(Offset.X + Size.Length - 1, Offset.Y + Size.Width - 1);
        }

        /// <summary>
        /// The empty region.
        /// </summary>
        public static readonly TileBounds Empty = new TileBounds(
            new Tile(int.MaxValue, int.MaxValue),
            new Tile(int.MinValue, int.MinValue));

        /// <summary>
        /// Constructs tile bounds containing only the given tile.
        /// </summary>
        public static TileBounds FromTile(Tile Tile)
        {
            return new TileBounds(Tile, Tile);
        }

        /// <summary>
        /// Gets the intersection of two tile bounds.
        /// </summary>
        public static TileBounds Intersection(TileBounds A, TileBounds B)
        {
            if (A.IsEmpty || B.IsEmpty)
                return Empty;
            return new TileBounds(
                new Tile(Math.Max(A.Min.X, B.Min.X), Math.Max(A.Min.Y, B.Min.Y)),
                new Tile(Math.Min(A.Max.X, B.Max.X), Math.Min(A.Max.Y, B.Max.Y)));
        }

        /// <summary>
        /// Gets the tightest approximation of the union of two tile bounds.
        /// </summary>
        public static TileBounds TightUnion(TileBounds A, TileBounds B)
        {
            return new TileBounds(
                new Tile(Math.Min(A.Min.X, B.Min.X), Math.Min(A.Min.Y, B.Min.Y)),
                new Tile(Math.Max(A.Max.X, B.Max.X), Math.Max(A.Max.Y, B.Max.Y)));
        }

        /// <summary>
        /// Gets the tightest bounds containing the given tiles.
        /// </summary>
        public static TileBounds Tight(Tile A, Tile B)
        {
            return new TileBounds(
                new Tile(Math.Min(A.X, B.X), Math.Min(A.Y, B.Y)),
                new Tile(Math.Max(A.X, B.X), Math.Max(A.Y, B.Y)));
        }

        /// <summary>
        /// Indicates whether these bounds are empty.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this.Max.X < this.Min.X;
            }
        }

        /// <summary>
        /// Indicates whether these bounds contain the given tile.
        /// </summary>
        public bool Contains(Tile Tile)
        {
            return
                Tile.X >= this.Min.X &&
                Tile.Y >= this.Min.Y &&
                Tile.X <= this.Max.X &&
                Tile.Y <= this.Max.Y;
        }

        /// <summary>
        /// Gets the number of tiles in these bounds.
        /// </summary>
        public int Area
        {
            get
            {
                return (this.Max.X - this.Min.X) * (this.Max.Y - this.Min.Y);
            }
        }

        /// <summary>
        /// Gets the set of tiles that these bounds contain.
        /// </summary>
        public IEnumerable<Tile> Tiles
        {
            get
            {
                for (int y = this.Min.Y; y <= this.Max.Y; y++)
                    for (int x = this.Min.X; x <= this.Max.X; x++)
                        yield return new Tile(x, y);
            }
        }

        public override string ToString()
        {
            if (this.IsEmpty) return "Empty";
            return String.Format("([{0}, {1}], [{2}, {3}]", this.Min.X, this.Max.X, this.Min.Y, this.Max.Y);
        }
    }

    /// <summary>
    /// Identifies a discrete rotation in tile space.
    /// </summary>
    public enum Turn : uint
    {
        None = 0,
        Left = 1,
        Full = 2,
        Right = 3,
    }

    /// <summary>
    /// A linear transform for tiles; describes a way of moving a component or complex.
    /// </summary>
    public struct TileTransform
    {
        public TileTransform(Turn Turn, int X, int Y)
        {
            this.Turn = Turn;
            this.X = X;
            this.Y = Y;
        }

        /// <summary>
        /// The turn applied by this transform.
        /// </summary>
        public Turn Turn;

        /// <summary>
        /// The number of tiles offset in the horizontal (forward) direction.
        /// </summary>
        public int X;

        /// <summary>
        /// The number of tiles offset in the vertical (side) direction.
        /// </summary>
        public int Y;

        /// <summary>
        /// The identity tile transform.
        /// </summary>
        public static readonly TileTransform Identity = new TileTransform(Turn.None, 0, 0);

        /// <summary>
        /// Constructs a tile transform that offsets to the given tile.
        /// </summary>
        public static TileTransform Translation(Tile Tile)
        {
            return new TileTransform(Turn.None, Tile.X, Tile.Y);
        }

        /// <summary>
        /// Constructs a tile transform for a rotation.
        /// </summary>
        public static TileTransform Rotate(Turn Turn)
        {
            return new TileTransform(Turn, 0, 0);
        }

        /// <summary>
        /// Gets the vector transform for this tile transform.
        /// </summary>
        public Transform Transform
        {
            get
            {
                return
                    Transform.Translation(0.5 + this.X, 0.5 + this.Y) *
                    Transform.Rotation((uint)this.Turn * Math.PI / 2.0) *
                    Transform.Translation(-0.5, -0.5);
            }
        }

        /// <summary>
        /// Gets the inverse of this tile transform.
        /// </summary>
        public TileTransform Inverse
        {
            get
            {
                switch (this.Turn)
                {
                    case Turn.None: return new TileTransform(Turn.None, -this.X, -this.Y);
                    case Turn.Left: return new TileTransform(Turn.Right, this.Y, -this.X);
                    case Turn.Full: return new TileTransform(Turn.Full, this.X, this.Y);
                    case Turn.Right: return new TileTransform(Turn.Left, -this.Y, this.X);

                    default: Debug.Fail("Invalid turn"); throw new Exception();
                }
            }
        }

        public static Tile operator *(TileTransform Transform, Tile Tile)
        {
            switch (Transform.Turn)
            {
                case Turn.None: return new Tile(Transform.X + Tile.X, Transform.Y + Tile.Y);
                case Turn.Left: return new Tile(Transform.X + Tile.Y, Transform.Y - Tile.X);
                case Turn.Full: return new Tile(Transform.X - Tile.X, Transform.Y - Tile.Y);
                case Turn.Right: return new Tile(Transform.X - Tile.Y, Transform.Y + Tile.X);
                default: Debug.Fail("Invalid turn"); throw new Exception();
            }
        }

        public static TileBounds operator *(TileTransform Transform, TileBounds Bounds)
        {
            return TileBounds.Tight(Transform * Bounds.Min, Transform * Bounds.Max);
        }

        public static TileTransform operator *(TileTransform A, TileTransform B)
        {
            Tile offset = A * new Tile(B.X, B.Y);
            return new TileTransform((Turn)(((uint)A.Turn + (uint)B.Turn) % 4), offset.X, offset.Y);
        }
    }
}
