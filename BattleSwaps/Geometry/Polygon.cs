﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// Describes a convex polygon in 2D space.
    /// </summary>
    public struct Convex
    {
        public Convex(Vector[] Points)
        {
            this.Points = Points;
        }

        /// <summary>
        /// The points in the polygon, in counter-clockwise order.
        /// </summary>
        public Vector[] Points;

        /// <summary>
        /// Creates a convex polygon defined by the given points in counter-clockwise order.
        /// </summary>
        public static Convex FromArray(params Vector[] Points)
        {
            return new Convex(Points);
        }
    }

    /// <summary>
    /// Describes a polygon in 2D space.
    /// </summary>
    public struct Polygon
    {
        public Polygon(Vector[] Points)
        {
            this.Points = Points;
        }

        /// <summary>
        /// The points in the polygon, in counter-clockwise order.
        /// </summary>
        public Vector[] Points;

        /// <summary>
        /// Updates the signed distance of the given point inside a segment of a polygon.
        /// </summary>
        private void _GetDistance(
            Vector A, Vector B, Vector Point,
            out Vector Span, ref double Distance, ref Vector Normal)
        {
            Span = B - A;
            Vector offset = Point - A;
            double rel = Vector.Dot(Span, offset);
            if (0.0 <= rel && rel <= Span.SquareLength)
            {
                Vector normal = Span.Cross.Normal;
                double dis = Vector.Dot(normal, offset);
                if (Math.Abs(dis) < Math.Abs(Distance))
                {
                    Distance = dis;
                    Normal = normal;
                }
            }
        }

        /// <summary>
        /// Updates the signed distance of the given point inside a corner a polygon.
        /// </summary>
        private void _GetDistance(
            Vector PrevSpan, Vector NextSpan,
            Vector Corner, Vector Point,
            ref double Distance, ref Vector Normal)
        {
            Vector offset = Point - Corner;
            double adis = offset.Length;
            if (adis < Math.Abs(Distance))
            {
                bool cornerInside = Vector.Det(-PrevSpan, NextSpan) < 0.0;
                bool inside = cornerInside ^
                    (Vector.Det(-PrevSpan, offset) < 0.0 == cornerInside &&
                    Vector.Det(offset, NextSpan) < 0.0 == cornerInside);
                Distance = inside ? -adis : adis;
                Normal = offset / Distance;
            }
        }

        /// <summary>
        /// Gets the signed distance of the given point inside (negative) or outside (positive) the polygon.
        /// </summary>
        /// <param name="Normal">The normal of the nearest segment on the polygon.</param>
        public double GetDistance(Vector Point, out Vector Normal)
        {
            double dis = double.PositiveInfinity;
            Normal = Vector.Zero;
            Vector firstSpan;
            _GetDistance(this.Points[0], this.Points[1], Point, out firstSpan, ref dis, ref Normal);
            Vector prevSpan = firstSpan;
            Vector prevPoint = this.Points[1];
            for (int i = 2; i < this.Points.Length; i++)
            {
                Vector curPoint = this.Points[i];
                Vector curSpan;
                _GetDistance(prevPoint, curPoint, Point, out curSpan, ref dis, ref Normal);
                _GetDistance(prevSpan, curSpan, prevPoint, Point, ref dis, ref Normal);
                prevSpan = curSpan;
                prevPoint = curPoint;
            }
            Vector lastSpan;
            _GetDistance(prevPoint, this.Points[0], Point, out lastSpan, ref dis, ref Normal);
            _GetDistance(prevSpan, lastSpan, prevPoint, Point, ref dis, ref Normal);
            _GetDistance(lastSpan, firstSpan, this.Points[0], Point, ref dis, ref Normal);
            return dis;
        }

        /// <summary>
        /// Gets the signed distance of the given point inside (negative) or outside (positive) the polygon.
        /// </summary>
        public double GetDistance(Vector Point)
        {
            Vector normal;
            return this.GetDistance(Point, out normal);
        }

        /// <summary>
        /// Creates a polygon defined by the given points in counter-clockwise order.
        /// </summary>
        public static Polygon FromArray(params Vector[] Points)
        {
            return new Polygon(Points);
        }

        /// <summary>
        /// Computes the tightest approximation of the lateral bounds of this polygon.
        /// </summary>
        public Bounds TightBounds
        {
            get
            {
                return Bounds.Tight(this.Points);
            }
        }

        /// <summary>
        /// Computes the tightest approximation of the lateral bounds of this mesh as a circle.
        /// </summary>
        public Circle TightCircle
        {
            get
            {
                return Circle.Tight(this.Points);
            }
        }

        public static Polygon operator *(Transform Transform, Polygon Polygon)
        {
            Vector[] nPoints = new Vector[Polygon.Points.Length];
            for (int i = 0; i < nPoints.Length; i++)
                nPoints[i] = Transform * Polygon.Points[i];
            return new Polygon(nPoints);
        }

        public static implicit operator Polygon(Convex Shape)
        {
            return new Polygon(Shape.Points);
        }
    }
}