﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleSwaps.Geometry
{
    /// <summary>
    /// A 2D affine transform.
    /// </summary>
    public struct Transform
    {
        public Vector Offset;
        public Vector X;
        public Vector Y;
        public Transform(Vector Offset, Vector X, Vector Y)
        {
            this.Offset = Offset;
            this.X = X;
            this.Y = Y;
        }

        /// <summary>
        /// Determines whether this is the identity transform.
        /// </summary>
        public bool IsIdentity
        {
            get
            {
                return this.X.X == 1.0 && this.X.Y == 0.0 && this.Y.X == 0.0 && this.Y.Y == 1.0;
            }
        }

        /// <summary>
        /// Determines whether this transform is shape-preserving. This is the case for rotation and translation
        /// transforms, and all combinations of them.
        /// </summary>
        public bool IsIsometric
        {
            get
            {
                return
                    this.X.SquareLength - 1 < 1.0e-12 &&
                    (this.X.Cross - this.Y).SquareLength < 1.0e-12;
            }
        }

        /// <summary>
        /// Gets the angle this transform rotates by, provided that it is isometric.
        /// </summary>
        public double Angle
        {
            get
            {
                Debug.Assert(this.IsIsometric);
                return this.X.Angle;
            }
        }

        /// <summary>
        /// The identity transform.
        /// </summary>
        public static readonly Transform Identity = new Transform(Vector.Zero,
            new Vector(1.0, 0.0),
            new Vector(0.0, 1.0));

        /// <summary>
        /// Constructs a translation transform applying the given offset.
        /// </summary>
        public static Transform Translation(Vector Offset)
        {
            return new Transform(Offset,
                new Vector(1.0, 0.0),
                new Vector(0.0, 1.0));
        }

        /// <summary>
        /// Constructs a translation transform applying the given offset.
        /// </summary>
        public static Transform Translation(double X, double Y)
        {
            return Translation(new Vector(X, Y));
        }

        /// <summary>
        /// Constructs a translation to the given tile.
        /// </summary>
        public static Transform Translation(Tile Tile)
        {
            return Translation(Tile.X, Tile.Y);
        }

        /// <summary>
        /// Constructs a scale transform.
        /// </summary>
        public static Transform Scale(Vector Size)
        {
            return new Transform(Vector.Zero,
                new Vector(Size.X, 0.0),
                new Vector(0.0, Size.Y));
        }

        /// <summary>
        /// Constructs a scale transform.
        /// </summary>
        public static Transform Scale(double Length, double Width)
        {
            return Scale(new Vector(Length, Width));
        }

        /// <summary>
        /// Constructs a uniform scale transform.
        /// </summary>
        public static Transform Scale(double Factor)
        {
            return Scale(Factor, Factor);
        }

        /// <summary>
        /// Constructs a counterclockwise rotation transform about the origin using the given angle in radians.
        /// </summary>
        public static Transform Rotation(double Angle)
        {
            double sin = Math.Sin(Angle);
            double cos = Math.Cos(Angle);
            return new Transform(Vector.Zero,
                new Vector(cos, -sin),
                new Vector(sin, cos));
        }

        /// <summary>
        /// Constructs a counterclockwise rotation transform about the given point using the given angle in radians.
        /// </summary>
        public static Transform Rotation(double Angle, Vector Center)
        {
            return
                Transform.Translation(Center) *
                Transform.Rotation(Angle) *
                Transform.Translation(-Center);
        }

        /// <summary>
        /// Gets the transform from the given bounds to the given bounds. The axes may optionally be flipped.
        /// </summary>
        public static Transform Between(Bounds From, Bounds To, bool FlipX, bool FlipY)
        {
            Vector fromSize = From.Size;
            Vector toSize = To.Size;
            return
                Transform.Translation(To.Center) *
                Transform.Scale(toSize.X / 2.0, toSize.Y / 2.0) *
                Transform.Scale(FlipX ? -1.0 : 1.0, FlipY ? -1.0 : 1.0) *
                Transform.Scale(2.0 / fromSize.X, 2.0 / fromSize.Y) *
                Transform.Translation(-From.Center);
        }

        /// <summary>
        /// Gets the transform from the given bounds to the given bounds.
        /// </summary>
        public static Transform Between(Bounds From, Bounds To)
        {
            Vector fromSize = From.Size;
            Vector toSize = To.Size;
            return
                Transform.Translation(To.Min) *
                Transform.Scale(toSize.X / fromSize.X, toSize.Y / fromSize.Y) *
                Transform.Translation(-From.Min);
        }

        /// <summary>
        /// Applies this transform to a point. This will apply the offset and linear transform.
        /// </summary>
        public Vector ApplyPoint(Vector Point)
        {
            return this.Offset + this.ApplyVector(Point);
        }

        /// <summary>
        /// Applies this transform to a vector. This will apply only the linear transform.
        /// </summary>
        public Vector ApplyVector(Vector Vector)
        {
            return this.X * Vector.X + this.Y * Vector.Y;
        }

        /// <summary>
        /// Gets the inverse of this transform.
        /// </summary>
        public Transform Inverse
        {
            get
            {
                double det = Vector.Det(this.X, this.Y);
                Debug.Assert(det != 0.0);
                double idet = 1.0 / det;
                return new Transform(
                    new Vector(
                        this.Offset.Y * this.Y.X - this.Offset.X * this.Y.Y,
                        this.Offset.X * this.X.Y - this.Offset.Y * this.X.X) * idet,
                    new Vector(this.Y.Y, -this.X.Y) * idet,
                    new Vector(-this.Y.X, this.X.X) * idet);
            }
        }

        public static Vector operator *(Transform Transform, Vector Point)
        {
            return Transform.ApplyPoint(Point);
        }

        public static Transform operator *(Transform A, Transform B)
        {
            return new Transform(
                A.ApplyPoint(B.Offset),
                A.ApplyVector(B.X),
                A.ApplyVector(B.Y));
        }
        public static Transform operator *(double Scale, Transform Transform)
        {
            return new Transform(
                Transform.Offset * Scale,
                Transform.X * Scale,
                Transform.Y * Scale);
        }

        public override string ToString()
        {
            return string.Format("Offset: {0}, X: {1}, Y : {2}", this.Offset, this.X, this.Y);
        }

        public static implicit operator System.Drawing.Drawing2D.Matrix(Transform Transform)
        {
            return new System.Drawing.Drawing2D.Matrix(
                (float)Transform.X.X, (float)Transform.X.Y,
                (float)Transform.Y.X, (float)Transform.Y.Y,
                (float)Transform.Offset.X, (float)Transform.Offset.Y);
        }

        public static implicit operator OpenTK.Matrix3(Transform Transform)
        {
            float a = (float)Transform.X.X;
            float b = (float)Transform.X.Y;
            float c = (float)Transform.Y.X;
            float d = (float)Transform.Y.Y;
            float e = (float)Transform.Offset.X;
            float f = (float)Transform.Offset.Y;
            return new OpenTK.Matrix3(
                a, b, 0,
                c, d, 0,
                e, f, 1);
        }

        public static implicit operator OpenTK.Matrix3x2(Transform Transform)
        {
            float a = (float)Transform.X.X;
            float b = (float)Transform.X.Y;
            float c = (float)Transform.Y.X;
            float d = (float)Transform.Y.Y;
            float e = (float)Transform.Offset.X;
            float f = (float)Transform.Offset.Y;
            return new OpenTK.Matrix3x2(
                a, b,
                c, d,
                e, f);
        }
    }

    /// <summary>
    /// A 3D affine transform.
    /// </summary>
    public struct DetailTransform
    {
        public DetailVector Offset;
        public DetailVector X;
        public DetailVector Y;
        public DetailVector Z;
        public DetailTransform(DetailVector Offset, DetailVector X, DetailVector Y, DetailVector Z)
        {
            this.Offset = Offset;
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        /// <summary>
        /// The identity transform.
        /// </summary>
        public static readonly DetailTransform Identity = new DetailTransform(DetailVector.Zero,
            new DetailVector(1.0, 0.0, 0.0),
            new DetailVector(0.0, 1.0, 0.0),
            new DetailVector(0.0, 0.0, 1.0));

        /// <summary>
        /// Applies this transform to a point. This will apply the offset and linear transform.
        /// </summary>
        public DetailVector ApplyPoint(DetailVector Point)
        {
            return this.Offset + this.ApplyVector(Point);
        }

        /// <summary>
        /// Applies this transform to a vector. This will apply only the linear transform.
        /// </summary>
        public DetailVector ApplyVector(DetailVector Vector)
        {
            return this.X * Vector.X + this.Y * Vector.Y + this.Z * Vector.Z;
        }

        public static DetailVector operator *(DetailTransform Transform, DetailVector Point)
        {
            return Transform.ApplyPoint(Point);
        }

        public static DetailTransform operator *(DetailTransform A, DetailTransform B)
        {
            return new DetailTransform(
                A.ApplyPoint(B.Offset),
                A.ApplyVector(B.X),
                A.ApplyVector(B.Y),
                A.ApplyVector(B.Z));
        }

        public static DetailTransform operator *(double Scale, DetailTransform Transform)
        {
            return new DetailTransform(
                Transform.Offset * Scale,
                Transform.X * Scale,
                Transform.Y * Scale,
                Transform.Z * Scale);
        }

        public static implicit operator DetailTransform(Transform Source)
        {
            return new DetailTransform(
                new DetailVector(Source.Offset, 0.0),
                new DetailVector(Source.X, 0.0),
                new DetailVector(Source.Y, 0.0),
                new DetailVector(0.0, 0.0, 1.0));
        }

        public static implicit operator DetailTransform(UprightTransform Source)
        {
            return new DetailTransform(Source.Offset, Source.X, Source.Y, Source.Z);
        }

        public override string ToString()
        {
            return string.Format("Offset: {0}, X: {1}, Y : {2}, Z : {3}", this.Offset, this.X, this.Y, this.Z);
        }

        public static implicit operator OpenTK.Matrix4(DetailTransform Transform)
        {
            float a = (float)Transform.X.X;
            float b = (float)Transform.X.Y;
            float c = (float)Transform.X.Z;
            float d = (float)Transform.Y.X;
            float e = (float)Transform.Y.Y;
            float f = (float)Transform.Y.Z;
            float g = (float)Transform.Z.X;
            float h = (float)Transform.Z.Y;
            float i = (float)Transform.Z.Z;
            float j = (float)Transform.Offset.X;
            float k = (float)Transform.Offset.Y;
            float l = (float)Transform.Offset.Z;
            return new OpenTK.Matrix4(
                a, b, c, 0,
                d, e, f, 0,
                g, h, i, 0,
                j, k, l, 1);
        }

        public static implicit operator OpenTK.Matrix4x3(DetailTransform Transform)
        {
            float a = (float)Transform.X.X;
            float b = (float)Transform.X.Y;
            float c = (float)Transform.X.Z;
            float d = (float)Transform.Y.X;
            float e = (float)Transform.Y.Y;
            float f = (float)Transform.Y.Z;
            float g = (float)Transform.Z.X;
            float h = (float)Transform.Z.Y;
            float i = (float)Transform.Z.Z;
            float j = (float)Transform.Offset.X;
            float k = (float)Transform.Offset.Y;
            float l = (float)Transform.Offset.Z;
            return new OpenTK.Matrix4x3(
                a, b, c,
                d, e, f,
                g, h, i,
                j, k, l);
        }
    }

    /// <summary>
    /// An orthogonal transform which preserves the directions of vectors.
    /// </summary>
    public struct OrthoTransform
    {
        public OrthoTransform(Vector Offset, double Scale)
        {
            this.Offset = Offset;
            this.Scale = Scale;
        }

        /// <summary>
        /// The post-transformed position of the pre-transformed zero vector.
        /// </summary>
        public Vector Offset;

        /// <summary>
        /// The scale factor applied by this transform.
        /// </summary>
        public double Scale;

        /// <summary>
        /// Gets the inverse of this transform.
        /// </summary>
        public OrthoTransform Inverse
        {
            get
            {
                double iScale = 1.0 / this.Scale;
                return new OrthoTransform(this.Offset * -iScale, iScale);
            }
        }

        public override string ToString()
        {
            return string.Format("Offset: {0}, Scale: {1}", this.Offset, this.Scale);
        }

        public static Vector operator *(OrthoTransform Transform, Vector Point)
        {
            return Transform.Offset + Transform.Scale * Point;
        }

        public static OrthoTransform operator *(OrthoTransform A, OrthoTransform B)
        {
            return new OrthoTransform(A * B.Offset, A.Scale * B.Scale);
        }

        public static implicit operator OpenTK.Matrix3x2(OrthoTransform Transform)
        {
            float a = (float)Transform.Scale;
            float b = (float)Transform.Offset.X;
            float c = (float)Transform.Offset.Y;
            return new OpenTK.Matrix3x2(
                a, 0,
                0, a,
                b, c);
        }
    }
}
