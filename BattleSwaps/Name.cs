﻿using System;
using System.Collections.Generic;

using BattleSwaps.Data;

namespace BattleSwaps
{
    /// <summary>
    /// Persistently identifies an object.
    /// </summary>
    /// <remarks>
    /// Helpful link for generating random static names:
    /// http://www.wolframalpha.com/input/?i=random+from+0+to+2%5E64-1
    /// </remarks>
    public struct Name : IEquatable<Name>
    {
        public Name(ulong Label)
        {
            this.Label = Label;
        }

        /// <summary>
        /// The numeric label for this name.
        /// </summary>
        public ulong Label;

        /// <summary>
        /// The random source used to generate name.
        /// </summary>
        private static readonly Random _Random = new Random();

        /// <summary>
        /// Creates a new name, ideally distinct from all existing names in any program run.
        /// </summary>
        public static Name Generate()
        {
            byte[] buffer = new byte[sizeof(ulong)];
            _Random.NextBytes(buffer);
            return new Name((ulong)BitConverter.ToInt64(buffer, 0));
        }

        /// <summary>
        /// Creates a name based on the given comparable object or value. The same name will be returned for
        /// identical calls.
        /// </summary>
        public static Name Create<T>(IEqualityComparer<T> Comparer, T Value)
        {
            // TODO: Use full space of name for hash
            return new Name(unchecked((ulong)Comparer.GetHashCode(Value)));
        }

        /// <summary>
        /// Gets a function which applies a consistent, but arbitrary, function to given names. Calls with identical
        /// names will yield identical decorators.
        /// </summary>
        public static Func<Name, Name> GetDecorator(Name Name)
        {
            ulong salt = Name.Label * 4294967231;
            return n => new Name((n.Label * 4294967291) ^ salt);
        }

        /// <summary>
        /// Combines two names with a non-associative, non-commutative hashing operation.
        /// </summary>
        public static Name operator *(Name A, Name B)
        {
            return new Name((A.Label * 4294967291) ^ (B.Label * 4294967279));
        }

        /// <summary>
        /// Combines two names with an associative, commutative hashing operation.
        /// </summary>
        public static Name operator |(Name A, Name B)
        {
            return new Name(A.Label ^ B.Label);
        }

        public static bool operator ==(Name A, Name B)
        {
            return A.Label == B.Label;
        }

        public static bool operator !=(Name A, Name B)
        {
            return A.Label != B.Label;
        }

        public static bool operator <(Name A, Name B)
        {
            return A.Label < B.Label;
        }

        public static bool operator >(Name A, Name B)
        {
            return A.Label > B.Label;
        }

        public static bool operator <=(Name A, Name B)
        {
            return A.Label <= B.Label;
        }

        public static bool operator >=(Name A, Name B)
        {
            return A.Label >= B.Label;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Name))
                return false;
            return this == (Name)obj;
        }

        public override int GetHashCode()
        {
            return this.Label.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("<{0}>", this.Label);
        }

        bool IEquatable<Name>.Equals(Name Other)
        {
            return this == Other;
        }
    }
}
