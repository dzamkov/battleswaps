﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Physics.Dynamics;
using BattleSwaps.Physics.Ballistics;
using BattleSwaps.Intel;
using BattleSwaps.Graphics.Scene;

namespace BattleSwaps.Game.Asset
{
    using Dynamics = BattleSwaps.Physics.Dynamics;

    /// <summary>
    /// An entity for a truck vehicle, including attachments.
    /// </summary>
    public sealed class Truck : Entity
    {
        private MaterialMap _Pattern;
        private Assemblage _Assemblage;
        public Truck(Initializer Initializer, NameRef<Intel.Unit> NameRef, TruckState State)
        {
            // Create unit
            DriveController controller;
            this.Unit = new Intel.Unit(Initializer, NameRef)
            {
                Controller = controller = new DriveController(Initializer, State.Controller)
            };
            Property<Signal<double>> angle = controller.Input.Map(input => input.Map(i => 0.8 * i.Turn));
            Property<Signal<double>> prop = controller.Input.Map(input => input.Map(i => 6000.0 * i.Forward));

            // Create attachment
            this.Slot = new Slot(Initializer, State.Attachment)
            {
                World = this.World,
                Offset = new Vector(-1.0, 0.0),
                HeightTransform = Signal.Static(HeightTransform.Translation(1.3))
            };

            // Create truck body
            const double wheelBaseStaticMu = 0.7;
            const double wheelBaseKineticMu = 0.5;
            Signal<double> resistance = 100.0;
            this.Body = new FreeBody(Initializer, State.Body)
            {
                Name = _BodyName(NameRef.Name),
                World = Property.From(delegate(out Physics.Dynamics.World dynamics)
                {
                    World world;
                    if (this.World.TryGetValue(out world) &&
                        world.Dynamics.TryGetValue(out dynamics))
                        return true;
                    dynamics = default(Physics.Dynamics.World);
                    return false;
                }),
                Attachments = new Dynamics.Attachment[]
                {
                    Dynamics.Attachment.CreateFixedMass(new MassDistribution
                    {
                        TotalMass = 2500.0,
                        MomentOfInertia = 2900.0,
                        Center = new Vector(0.5, 0.0),
                    }),
                    this.Slot.Physical
                },
                Contacts = new Contact[]
                {
                    new WheelContact(Initializer, State.FrontLeftWheel)
                    {
                        Offset = new Vector(1.5, -0.8),
                        BaseStaticMu = wheelBaseStaticMu,
                        BaseKineticMu = wheelBaseKineticMu,
                        Angle = angle,
                        Propulsion = prop,
                        Resistance = resistance
                    },
                    new WheelContact(Initializer, State.FrontRightWheel)
                    {
                        Offset = new Vector(1.5, 0.8),
                        BaseStaticMu = wheelBaseStaticMu,
                        BaseKineticMu = wheelBaseKineticMu,
                        Angle = angle,
                        Propulsion = prop,
                        Resistance = resistance
                    },
                    new WheelContact(Initializer, State.BackRightWheel)
                    {
                        Offset = new Vector(-1.5, 0.8),
                        BaseStaticMu = wheelBaseStaticMu,
                        BaseKineticMu = wheelBaseKineticMu,
                        Angle = Signal.Static(0.0),
                        Propulsion = prop,
                        Resistance = resistance
                    },
                    new WheelContact(Initializer, State.BackLeftWheel)
                    {
                        Offset = new Vector(-1.5, -0.8),
                        BaseStaticMu = wheelBaseStaticMu,
                        BaseKineticMu = wheelBaseKineticMu,
                        Angle = Signal.Static(0.0),
                        Propulsion = prop,
                        Resistance = resistance
                    },
                },
            };
            this.Unit.Rectangle = this.Body.LocalToWorld.Map(localToWorld =>
                localToWorld.Map(t => t.Spatial * new Bounds(-2.0, 2.23, -1.1, 1.1)));
        }

        /// <summary>
        /// The intel unit for this truck.
        /// </summary>
        public readonly Intel.Unit Unit;

        /// <summary>
        /// The attachment slot for this truck.
        /// </summary>
        public readonly Slot Slot;

        /// <summary>
        /// The body for this truck.
        /// </summary>
        public readonly FreeBody Body;

        /// <summary>
        /// Given the name of a truck, gets the name of its turret body.
        /// </summary>
        private static readonly Func<Name, Name> _BodyName =
            BattleSwaps.Name.GetDecorator(new Name(841716029650782336));

        /// <summary>
        /// Gets or sets the pattern displayed on this truck.
        /// </summary>
        public MaterialMap Pattern
        {
            get
            {
                return this._Pattern;
            }
            set
            {
                this._Pattern = value;
            }
        }

        private static readonly Model _Model = Model.Load("Part/Vehicle/Pickup/Model.obj");
        private static readonly Data.Resource<Mesh> _CollisionMesh = _Model.Filter("body", "mount").ToMesh();

        /// <summary>
        /// Used to generate the name for the collision source for a truck.
        /// </summary>
        private static readonly Func<Name, Name> _CollisionName =
            BattleSwaps.Name.GetDecorator(new Name(16704402495544633800));

        /// <summary>
        /// Used to generate the name for the impact source for a truck.
        /// </summary>
        private static readonly Func<Name, Name> _ImpactName =
            BattleSwaps.Name.GetDecorator(new Name(13631669233826963312));

        public override Property<Assemblage> Assemblage
        {
            get
            {
                if (this._Assemblage.IsValid)
                    return this._Assemblage;
                return Property.From(delegate(out Assemblage res)
                {
                    lock (this)
                    {
                        res = this._Assemblage;
                        if (res.IsValid)
                            return true;

                        Signal<Physics.FrameTransform> localToWorld;
                        Assemblage slotAssemblage;
                        if (this.Body.LocalToWorld.TryGetValue(out localToWorld) &&
                            this.Slot.Assemblage.TryGetValue(out slotAssemblage))
                        {
                            // Create collision source
                            CollisionSource collisionSource = this.Body.GetCollisionSource(
                                _CollisionName(this.Unit.NameRef.Name),
                                new CollisionHull
                                {
                                    Shape = Polygon.FromArray(
                                        new Vector(2.14, -0.71),
                                        new Vector(1.41, -1.00),
                                        new Vector(-1.95, -1.04),
                                        new Vector(-1.95, 1.04),
                                        new Vector(1.41, 1.00),
                                        new Vector(2.14, 0.71)),
                                    Restitution = 0.2
                                });

                            // Create impact source
                            ImpactSource impactSource = new ImpactSource
                            {
                                Name = _ImpactName(this.Unit.NameRef.Name),
                                Mesh = _CollisionMesh.Peek(),
                                Span = this.Body.FrameTransformSpan.Map(s =>
                                    (Physics.IDetailFrameTransformSpan)
                                    new Physics.LevelDetailFrameTransformSpan(s, HeightTransform.Identity)),
                                OnImpulse = Impact.Impulse(this.Body)
                            };

                            // Create composition
                            Content content =
                                localToWorld.Map(t => t.Spatial) *
                                new ModelContent(_Model.Substitute(
                                    "pattern", Attachment.GetMetalMaterial(this.Pattern)));

                            // Create assemblage
                            this._Assemblage = res = new Assemblage
                            {
                                CollisionSources = Bag.Static(collisionSource),
                                ImpactSources = Bag.Static(impactSource),
                                Units = Bag.Static<Intel.Unit>(this.Unit),
                                Content = content,
                            } + slotAssemblage;
                            return true;
                        }
                    }
                    return false;
                });
            }
        }

        public override Property<Bag<Event>> PendingEvents
        {
            get
            {
                return Property.From(delegate(out Bag<Event> events)
                {
                    Bag<Event> bodyEvents;
                    Bag<Event> slotEvents;
                    if (this.Body.PendingEvents.TryGetValue(out bodyEvents) &&
                        this.Slot.PendingEvents.TryGetValue(out slotEvents))
                    {
                        events = bodyEvents + slotEvents;
                        return true;
                    }
                    events = default(Bag<Event>);
                    return false;
                });
            }
        }

        public override EntityPrototype GetPrototype(State State)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Describes the state of a truck entity at a particular reactive state.
    /// </summary>
    public struct TruckState
    {
        /// <summary>
        /// The state of the controller for the truck.
        /// </summary>
        public ControllerState Controller;

        /// <summary>
        /// The state of the main body of the truck.
        /// </summary>
        public FreeBodyState Body;

        /// <summary>
        /// The state of the front-left wheel of the truck.
        /// </summary>
        public ContactState FrontLeftWheel;

        /// <summary>
        /// The state of the front-right wheel of the truck.
        /// </summary>
        public ContactState FrontRightWheel;

        /// <summary>
        /// The state of the back-left wheel of the truck.
        /// </summary>
        public ContactState BackLeftWheel;

        /// <summary>
        /// The state of the back-right wheel of the truck.
        /// </summary>
        public ContactState BackRightWheel;

        /// <summary>
        /// Sets whether the all of the wheels of the truck are slipping at once.
        /// </summary>
        public bool IsSlipping
        {
            set
            {
                this.FrontLeftWheel.IsSlipping = value;
                this.FrontRightWheel.IsSlipping = value;
                this.BackLeftWheel.IsSlipping = value;
                this.BackRightWheel.IsSlipping = value;
            }
        }

        /// <summary>
        /// The attachment for the truck.
        /// </summary>
        public AttachmentPrototype Attachment;

        /// <summary>
        /// Converts this state into a prototype by providing additional information.
        /// </summary>
        public TruckPrototype ToPrototype(NameRef<Intel.Unit> NameRef, MaterialMap Pattern)
        {
            return new TruckPrototype(NameRef, Pattern, this);
        }
    }

    /// <summary>
    /// A prototype for a truck entity.
    /// </summary>
    public sealed class TruckPrototype : EntityPrototype
    {
        public TruckPrototype(NameRef<Intel.Unit> NameRef, MaterialMap Pattern, TruckState State)
        {
            this.NameRef = NameRef;
            this.Pattern = Pattern;
            this.State = State;
        }

        /// <summary>
        /// The name of the intel unit for the truck.
        /// </summary>
        public readonly NameRef<Intel.Unit> NameRef;

        /// <summary>
        /// The pattern displayed by the truck.
        /// </summary>
        public readonly MaterialMap Pattern;

        /// <summary>
        /// The state of the truck associated with this prototype.
        /// </summary>
        public readonly TruckState State;

        public override Entity Instantiate(Initializer Initializer)
        {
            Truck truck = new Truck(Initializer, this.NameRef, this.State);
            truck.Pattern = this.Pattern;
            return truck;
        }
    }
}
