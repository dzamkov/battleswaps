﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics.Scene;
using BattleSwaps.Intel;
using BattleSwaps.Physics;
using BattleSwaps.Physics.Dynamics;
using BattleSwaps.Physics.Ballistics;

namespace BattleSwaps.Game.Asset
{
    using Dynamics = BattleSwaps.Physics.Dynamics;

    /// <summary>
    /// An attachment representing a conventional direct-fire cannon.
    /// </summary>
    public sealed class Cannon : Attachment
    {
        private readonly ExplicitSignal<Firing> _LastFiring;
        private readonly TurretAttachment _Physical;
        private MaterialMap _Pattern;
        private Assemblage _Assemblage;
        private Signal<FiringParameters?> _FiringParameters;
        public Cannon(Initializer Initializer, NameRef<Intel.Unit> NameRef, CannonState State)
        {
            // Internal state
            this._LastFiring = new ExplicitSignal<Firing>(Initializer, State.LastFiring);

            // Create control
            WeaponController controller;
            this.Unit = new Intel.Unit(Initializer, NameRef)
            {
                Controller = controller = new WeaponController(Initializer, State.Controller)
            };

            // Create body
            double momentOfInertia = 400.0;
            double resist = 100.0;
            double maxProp = 400.0;
            this.Body = new TurretBody(Initializer, State.Body)
            {
                Name = _BodyName(NameRef.Name),
                Resistance = Signal.Static(resist),
                Attachment = Dynamics.Attachment.CreateFixedMass(new MassDistribution
                {
                    TotalMass = 1000.0,
                    MomentOfInertia = momentOfInertia,
                    Center = new Vector(0.0, 0.0)
                })
            };
            this.Unit.Rectangle = this.Body.LocalToWorld.Map(localToWorld =>
                localToWorld.Map(t => t.Spatial * new Bounds(-1.0, 1.3, -0.8, 0.8)));
            this._Physical = new TurretAttachment
            {
                Turret = this.Body,
                Offset = this.Offset
            };

            // Turret control
            this.Body.Propulsion = this.FiringParameters.Map(firingParams =>
            {
                double stopTorque = resist + maxProp;
                double stopConst = 1.1 * (0.5 * momentOfInertia / stopTorque);
                return Signal.Lift(this.Body.Motion, firingParams, (m, f) =>
                {
                    if (f == null) return 0.0;
                    double angVel = m.AngularVelocity;
                    double estStopAngle = m.Angle + stopConst * angVel * angVel * Math.Sign(angVel);
                    double dAng = Vector.NormalizeAngle(f.Value.Angle - estStopAngle);
                    return maxProp * Math.Sign(dAng);
                });
            });
                
                

            // Firing
            /* EventSource fireEventSource = EventSource.FromNext(Signal.CustomDiscrete(
                delegate(State state, OrLatchBuilder invalidate)
            {
                Firing lastFiring = this._LastFiring.Get(state, invalidate);
                if (lastFiring.Pending)
                {
                    // Emit shell
                    return new Event(lastFiring.Time + FireDelay, delegate(Transaction trans)
                    {
                        Firing last = this._LastFiring[trans];
                        last.Pending = false;
                        this._LastFiring[trans] = last;

                        FiringParameters? param = this.FiringParameters.Value[trans];
                        double elevation = param.HasValue ? param.Value.Elevation : 0.0;

                        World world = this.Slot.Value.Environment.Value;
                        world.Spawn(trans, new Shell(trans.Initializer, new ShellState
                        {
                            Span = world.Ballistics.Value.GetMotionSpan(trans,
                                ProjectileMotion.Launch(this._GetBarrelToWorld(trans), elevation, Speed))
                        }));
                    });
                }
                else
                {
                    WeaponBeat beat = this.Input.Get(state, invalidate);
                    if (beat.Repeat)
                    {
                        Time nextPossibleFire = lastFiring.Time + ReloadDelay;
                        return new Event(nextPossibleFire, delegate(Transaction trans)
                        {
                            this._LastFiring[trans] = new Firing(trans.Time, true);
                            DetailFrameTransform barrelToWorld = this._GetBarrelToWorld(trans);

                            // Recoil
                            this.Body.ApplyImpulse(trans, new PointImpulse(
                                barrelToWorld.Lateral.Spatial.Offset,
                                Speed * 25.0 * -barrelToWorld.Lateral.Spatial.X));

                            // Effect
                            World world = this.Slot.Value.Environment.Value;
                            world.Spawn(trans, new Fireball
                            {
                                Start = trans.Time,
                                Position = barrelToWorld.Spatial.Offset,
                                Direction = barrelToWorld.Spatial.X
                            });
                        });
                    }
                }
                return Event.Never;
            })); */
        }

        /// <summary>
        /// The minimum elevation of the barrel.
        /// </summary>
        public static readonly double MinElevation = -0.1;

        /// <summary>
        /// The maximum elevation of the barrel.
        /// </summary>
        public static readonly double MaxElevation = 0.2;

        /// <summary>
        /// The speed of the launched projectile.
        /// </summary>
        public static readonly double Speed = 300.0;

        /// <summary>
        /// The minimum time between cannon firings.
        /// </summary>
        public static readonly Time ReloadDelay = Time.FromSeconds(4.0);

        /// <summary>
        /// The time between the cannon being ordered to fire and it actually firing a shell.
        /// </summary>
        public static readonly Time FireDelay = Time.FromSeconds(0.1);

        /// <summary>
        /// Given the name of a cannon, gets the name of its turret body.
        /// </summary>
        private static readonly Func<Name, Name> _BodyName =
            BattleSwaps.Name.GetDecorator(new Name(9841903596632239796));
        
        /// <summary>
        /// Gets the firing parameters for the aiming of this cannon.
        /// </summary>
        public Property<Signal<FiringParameters?>> FiringParameters
        {
            get
            {
                if (this._FiringParameters != null)
                    return this._FiringParameters;
                return Property.From(delegate(out Signal<FiringParameters?> res)
                {
                    res = this._FiringParameters;
                    if (res != null)
                        return true;

                    Slot slot;
                    World world;
                    Physics.Ballistics.World ballistics;
                    Signal<FrameVector> pivot;
                    Signal<WeaponBeat> input;
                    if (this.Slot.TryGetValue(out slot) &&
                        slot.World.TryGetValue(out world) &&
                        world.Ballistics.TryGetValue(out ballistics) &&
                        this.Body.Pivot.TryGetValue(out pivot) &&
                        ((WeaponController)this.Unit.Controller).Input.TryGetValue(out input))
                    {
                        res = Signal.Custom(delegate(Moment moment, OrLatchBuilder invalidate, ref bool isDiscrete)
                        {
                            WeaponBeat inputBeat = input.Get(moment, invalidate, ref isDiscrete);
                            if (inputBeat.Aim == null) return (FiringParameters?)null;
                            double angle, elevation;

                            // TODO: no hardcoded height
                            DetailFrameVector source = pivot.Get(moment, invalidate, ref isDiscrete).WithHeight(1.8);
                            DetailFrameVector target = DetailFrameVector.Static(inputBeat.Aim.Value);
                            ballistics.Solve(
                                MinElevation, MaxElevation,
                                Speed, source, target,
                                out angle, out elevation);
                            return new FiringParameters(angle, elevation);
                        });
                        res = Interlocked.CompareExchange(ref this._FiringParameters, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets the frame transform between the barrel of the cannon and the world frame at the given moment.
        /// </summary>
        private DetailFrameTransform _GetBarrelToWorld(Transaction Transaction)
        {
            return this.Body.LocalToWorld.RuntimeValue[Transaction] *
                (this.Slot.RuntimeValue.HeightTransform.RuntimeValue[Transaction] * HeightTransform.Translation(0.3) *
                Transform.Translation(1.3, 0.0));
        }

        /// <summary>
        /// The intel unit for this cannon.
        /// </summary>
        public readonly Intel.Unit Unit;

        /// <summary>
        /// The body for this cannon.
        /// </summary>
        public readonly TurretBody Body;

        /// <summary>
        /// Gets or sets the pattern displayed on this cannon.
        /// </summary>
        public MaterialMap Pattern
        {
            get
            {
                return this._Pattern;
            }
            set
            {
                this._Pattern = value;
            }
        }

        public override Property<Bag<Event>> PendingEvents
        {
            get
            {
                return this.Body.PendingEvents;
            }
        }

        public override Dynamics.Attachment Physical
        {
            get
            {
                return this._Physical;
            }
        }

        public override Property<Assemblage> Assemblage
        {
            get
            {
                if (this._Assemblage.IsValid)
                    return this._Assemblage;
                return Property.From(delegate(out Assemblage res)
                {
                    lock (this)
                    {
                        res = this._Assemblage;
                        if (res.IsValid)
                            return true;

                        Slot slot;
                        Signal<HeightTransform> heightTransform;
                        Signal<FrameTransform> localToWorld;
                        if (this.Slot.TryGetValue(out slot) &&
                            slot.HeightTransform.TryGetValue(out heightTransform) &&
                            this.Body.LocalToWorld.TryGetValue(out localToWorld))
                        {

                            Signal<double> dampenerOffset = Signal.Lift(
                                Signal.Clock, this._LastFiring,
                            delegate(Time current, Firing lastFiring)
                            {
                                Time last = lastFiring.Time;
                                if (current - last >= ReloadDelay) return 0.0;
                                double diff = (current - last).Seconds;
                                const double back = 0.5;
                                double outward = 1 - Math.Exp(-Math.Sqrt(4.0 * diff));
                                double rel = diff / ReloadDelay.Seconds;
                                return back * (outward * (1.0 - rel));
                            });

                            // Create composition
                            Signal<DetailTransform> turretTransform = Signal.Lift(
                                heightTransform, localToWorld,
                                (h, t) => (DetailTransform)(h * t.Spatial));
                            Model model = Model
                                .Load("Part/Weapon/B/Cannon/Model.obj")
                                .Substitute("pattern", Attachment.GetMetalMaterial(this.Pattern));
                            Content content = turretTransform *
                                (new ModelContent(model.Filter("barrel", "body")) +
                                dampenerOffset.Map(o => Transform.Translation(-o, 0.0)) *
                                    new ModelContent(model.Filter("dampener")));

                            // Create assemblage
                            // TODO: Impact source
                            this._Assemblage = res = new Assemblage
                            {
                                CollisionSources = Bag.Empty<CollisionSource>(),
                                ImpactSources = Bag.Empty<ImpactSource>(),
                                Units = Bag.Static<Intel.Unit>(this.Unit),
                                Content = content,
                            };
                            return true;
                        }
                    }
                    return false;
                });
            }
        }

        public override AttachmentPrototype GetPrototype(State State)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Identifies and describes a firing of a cannon.
    /// </summary>
    public struct Firing
    {
        public Firing(Time Time, bool Pending)
        {
            this.Time = Time;
            this.Pending = Pending;
        }

        /// <summary>
        /// The time that this firing was ordered to begin.
        /// </summary>
        public Time Time;

        /// <summary>
        /// Indicates whether a shell is yet to have been emitted for this firing.
        /// </summary>
        public bool Pending;

        /// <summary>
        /// Identifies a "null" firing.
        /// </summary>
        public static readonly Firing None = new Firing(Time.Origin, false);
    }

    /// <summary>
    /// Describes the parameters for firing a cannon.
    /// </summary>
    public struct FiringParameters
    {
        public FiringParameters(double Angle, double Elevation)
        {
            this.Angle = Angle;
            this.Elevation = Elevation;
        }

        /// <summary>
        /// The lateral angle of the cannon.
        /// </summary>
        public double Angle;

        /// <summary>
        /// The elevation of the cannon.
        /// </summary>
        public double Elevation;
    }

    /// <summary>
    /// Describes the state of a cannon attachment at a particular reactive state.
    /// </summary>
    public struct CannonState
    {
        /// <summary>
        /// The state for the controller for this cannon.
        /// </summary>
        public ControllerState Controller;

        /// <summary>
        /// The state of the main body of the cannon.
        /// </summary>
        public TurretBodyState Body;

        /// <summary>
        /// The last firing for this cannon.
        /// </summary>
        public Firing LastFiring;

        /// <summary>
        /// Converts this state into a prototype by providing additional information.
        /// </summary>
        public CannonPrototype ToPrototype(NameRef<Intel.Unit> NameRef, MaterialMap Pattern)
        {
            return new CannonPrototype(NameRef, Pattern, this);
        }
    }

    /// <summary>
    /// A prototype for a cannon attachment.
    /// </summary>
    public sealed class CannonPrototype : AttachmentPrototype
    {
        public CannonPrototype(NameRef<Intel.Unit> NameRef,  MaterialMap Pattern, CannonState State)
        {
            this.NameRef = NameRef;
            this.Pattern = Pattern;
            this.State = State;
        }

        /// <summary>
        /// The name for the intel unit for this cannon.
        /// </summary>
        public readonly NameRef<Intel.Unit> NameRef;

        /// <summary>
        /// The pattern map to apply to the cannon.
        /// </summary>
        public readonly MaterialMap Pattern;

        /// <summary>
        /// The state of the cannon associated with this prototype.
        /// </summary>
        public readonly CannonState State;

        public override Attachment Instantiate(Initializer Initializer)
        {
            Cannon cannon = new Cannon(Initializer, this.NameRef, this.State);
            cannon.Pattern = this.Pattern;
            return cannon;
        }
    }

    /*
    /// <summary>
    /// An entity for a fireball effect emitted from a cannon.
    /// </summary>
    public sealed class Fireball : Entity
    {
        private readonly ExplicitLatch _Dissolve;
        private Delay<Time> _Start;
        private Delay<DetailVector> _Position;
        private Delay<DetailVector> _Direction;
        private Assemblage _Assemblage;
        public Fireball()
        {
            this._Dissolve = new ExplicitLatch();
            this._Start = Delay<Time>.Undefined;
            this._Position = Delay<DetailVector>.Undefined;
            this._Direction = Delay<DetailVector>.Undefined;
        }

        /// <summary>
        /// The time when this fireball was emitted.
        /// </summary>
        public Delay<Time> Start
        {
            get
            {
                Time value;
                if (this._Start.TryGetImmediate(out value))
                    return value;
                else
                    return Delay.FromStable(() => this._Start.Value);
            }
            set
            {
                Debug.Assert(this._Start.IsUndefined);
                this._Start = value;
            }
        }

        /// <summary>
        /// The position of the end of the barrel when the fireball was emitted.
        /// </summary>
        public Delay<DetailVector> Position
        {
            get
            {
                DetailVector value;
                if (this._Position.TryGetImmediate(out value))
                    return value;
                else
                    return Delay.FromStable(() => this._Position.Value);
            }
            set
            {
                Debug.Assert(this._Position.IsUndefined);
                this._Position = value;
            }
        }

        /// <summary>
        /// The direction of the barrel when the fireball was emitted.
        /// </summary>
        public Delay<DetailVector> Direction
        {
            get
            {
                DetailVector value;
                if (this._Direction.TryGetImmediate(out value))
                    return value;
                else
                    return Delay.FromStable(() => this._Direction.Value);
            }
            set
            {
                Debug.Assert(this._Direction.IsUndefined);
                this._Direction = value;
            }
        }

        private static readonly Graphics.Image _Image =
            Graphics.Image.Load("Effects/Fireball.png");

        private static readonly Graphics.Image _EmissionImage =
            Graphics.Image.Load("Effects/FireballEmission.png");

        private static readonly Graphics.Image _DiffuseImage =
            Graphics.Image.Load("Effects/FireballDiffuse.png");

        private static Composition _CreateSprite(
            Random Random, double Offset,
            Time Start, DetailVector Position, DetailVector Direction)
        {
            Offset -= 0.5;
            Offset += Random.NextDouble() * 0.1;
            double startAng = Math.PI * 2.0 * Random.NextDouble();
            double speed = 0.7 * Random.NextDouble() + 0.3 * (1 - 2.0 * Math.Abs(Offset));
            DetailVector move = new DetailVector(
                (1.0 + 6.5 * speed) * Direction.Lateral +
                (2.0 * Offset) * Direction.Lateral.Cross,
                (0.5 + 3.0 * speed) * Direction.Height);
            double startScale = 1.0;
            double rotSpeed = 12.0 * -Offset / (1.0 + Random.NextDouble() + 2.0 * Math.Abs(Offset));
            double deltaScale = 6.0 + 1.0 * Random.NextDouble();
            Signal<OrthoTransform> trans = Signal.Clock.Map(delegate(Time time)
            {
                double delta = (time - Start).Seconds;
                double sqrRtDelta = Math.Sqrt(delta);
                double ang = startAng + sqrRtDelta * rotSpeed;
                DetailVector pos = Position + sqrRtDelta * move;
                double scale = startScale + delta * deltaScale;
                return
                    Transform.Translation(pos.Lateral) *
                    Transform.Scale(scale) *
                    new HeightTransform(pos.Height, 0.5 * scale) *
                    Transform.Rotation(ang) *
                    Transform.Translation(-0.5, -0.5);
            });
            double start = 0.15 * (1.0 - speed);
            double length = 1.0 + 0.5 * speed;
            return trans * new GradientVolume(_Image, _EmissionImage, _DiffuseImage,
                Signal.Clock.Map(t => start + (t - Start).Seconds / length));
        }

        public override Delay<Assemblage> Assemblage
        {
            get
            {
                if (this._Assemblage.Composition != null)
                    return this._Assemblage;
                else
                    return Delay.FromStable<Assemblage>(this._GetAssemblage);
            }
        }

        /// <summary>
        /// Gets the assemblage for this entity.
        /// </summary>
        private Assemblage _GetAssemblage()
        {
            lock (this)
            {
                if (this._Assemblage.Composition == null)
                {
                    // Create composition
                    Composition comp = Composition.Empty;
                    Random rand = new Random();
                    const int sprites = 10;
                    for (int i = 0; i < sprites; i++)
                        comp += _CreateSprite(rand, (double)i / sprites,
                            this.Start.Value, this.Position.Value, this.Direction.Value);
                    comp = comp.DissolveAt(this._Dissolve);

                    // Create assemblage
                    this._Assemblage = Game.Assemblage.Empty;
                    this._Assemblage.Composition = comp;
                }
            }
            return this._Assemblage;
        }

        public override EventSource EventSource
        {
            get
            {
                // Dissolve entity
                return EventSource.FromNext(new Event
                {
                    Time = this.Start.Value + Time.FromSeconds(2.0),
                    Stimulus = trans => this._Dissolve.Trigger(trans)
                });
            }
        }

        public override Delay<Latch> Dissolve
        {
            get
            {
                return this._Dissolve;
            }
        }

        public override EntityPrototype GetPrototype(State State)
        {
            // TODO
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// An entity for a shell fired from a cannon.
    /// </summary>
    public sealed class Shell : Entity
    {
        private readonly ExplicitSignal<ShellState> _State;
        private readonly ExplicitLatch _Dissolve;
        private Assemblage _Assemblage;
        public Shell(Initializer Initializer, ShellState State)
        {
            this._State = new ExplicitSignal<ShellState>(Initializer, State);
            this._Dissolve = new ExplicitLatch();
        }

        /// <summary>
        /// The time between a shell's impact and its dissolution.
        /// </summary>
        public static readonly Time DissolveDelay = Time.FromSeconds(2.0);

        /// <summary>
        /// The maximum time a shell can remain active.
        /// </summary>
        public static readonly Time MaxLife = Time.FromSeconds(5.0);

        public override EventSource EventSource
        {
            get
            {
                ImpactStimulus onImpact = delegate(
                    Transaction trans, ImpulseStimulus onImpulse,
                    ProjectileMotion motion)
                {
                    onImpulse(trans, motion.Position, motion.Velocity * 25.0);
                    ShellState cur = this._State[trans];
                    cur.Impact = trans.Time;
                    this._State[trans] = cur;
                };
                return EventSource.Union(Bag.Singleton<ShellState>(this._State).Map<EventSource>(state =>
                {
                    if (state.Impact == null)
                        return Impact.ResolutionsBetween(state.Span, onImpact,
                            this.World.Value.Ballistics.Value.ImpactSources.Value);
                    else
                        return EventSource.Empty;
                })) + EventSource.FromNext(this._State.Map(state =>
                {
                    // Dissolve after delay
                    if (state.Impact != null)
                        return new Event(state.Impact.Value + DissolveDelay, this._Dissolve.Trigger);
                    return Event.Never;
                })) + EventSource.FromNext(this._State.Map(state =>
                {
                    // Impact after lifetime expires
                    if (state.Impact == null)
                    {
                        return new Event(state.Start + MaxLife, delegate(Transaction trans)
                        {
                            ShellState cur = this._State[trans];
                            cur.Impact = trans.Time;
                            this._State[trans] = cur;
                        });
                    }
                    return Event.Never;
                }));
            }
        }

        public override Delay<Latch> Dissolve
        {
            get
            {
                return this._Dissolve;
            }
        }

        public override Delay<Assemblage> Assemblage
        {
            get
            {
                if (this._Assemblage.Composition != null)
                    return this._Assemblage;
                else
                    return Delay.FromStable<Assemblage>(this._GetAssemblage);
            }
        }

        /// <summary>
        /// Gets the assemblage for this attachment.
        /// </summary>
        private Assemblage _GetAssemblage()
        {
            lock (this)
            {
                if (this._Assemblage.Composition == null)
                {
                    // Create composition
                    Composition comp = Composition.Empty;
                    comp = Volume.Union(Bag.Singleton<ShellState>(this._State).Map<Volume>(state =>
                    {
                        return new Graphics.Scene.Effects.BulletTrailVolume(0.3, state.Span,
                            state.Impact.GetValueOrDefault(Time.MaxValue));
                    }));
                    comp = comp.DissolveAt(this._Dissolve);

                    // Create assemblage
                    this._Assemblage = Game.Assemblage.Empty;
                    this._Assemblage.Composition = comp;
                }
            }
            return this._Assemblage;
        }

        public override EntityPrototype GetPrototype(State State)
        {
            // TODO
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Describes the state of a shell at a moment.
    /// </summary>
    public struct ShellState
    {
        /// <summary>
        /// The span describing the motion of the shell.
        /// </summary>
        public ProjectileMotionSpan Span;

        /// <summary>
        /// Gets or sets the time at which this shell was fired.
        /// </summary>
        public Time Start
        {
            get
            {
                return this.Span.Start;
            }
            set
            {
                this.Span.Start = value;
            }
        }

        /// <summary>
        /// The time at which this shell impacted.
        /// </summary>
        public Time? Impact;
    } */
}
