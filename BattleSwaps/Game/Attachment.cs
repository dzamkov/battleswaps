﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Graphics;
using BattleSwaps.Graphics.Scene;

namespace BattleSwaps.Game
{
    using Dynamics = BattleSwaps.Physics.Dynamics;

    /// <summary>
    /// Identifies and describes an assemblage within a world that is attached to (and owned by) a base entity through
    /// a circular slot.
    /// </summary>
    public abstract class Attachment
    {
        private Slot _Slot;
        private static readonly InteriorRef<Slot> _SlotRef =
            (obj, act) => act(ref ((Attachment)obj)._Slot);
        public Attachment()
        {
            Property.Introduce(Ref.Create(this, _SlotRef));
        }

        /// <summary>
        /// Gets or sets the slot this attachment is in.
        /// </summary>
        public Property<Slot> Slot
        {
            get
            {
                return Property.Get(Ref.Create(this, _SlotRef), ref this._Slot);
            }
            set
            {
                Property.Define(Ref.Create(this, _SlotRef), ref this._Slot, value);
            }
        }

        /// <summary>
        /// Sets the environment for this attachment.
        /// </summary>
        public Slot Environment
        {
            set
            {
                this.Slot = value;
            }
        }

        /// <summary>
        /// Gets the offset of this attachment on its parent body.
        /// </summary>
        public Property<Vector> Offset
        {
            get
            {
                return Property.From(delegate(out Vector offset)
                {
                    Slot slot;
                    if (this.Slot.TryGetValue(out slot) && slot.Offset.TryGetValue(out offset))
                        return true;
                    offset = default(Vector);
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets the material that makes the given pattern appear as metal.
        /// </summary>
        public static Material GetMetalMaterial(MaterialMap Pattern)
        {
            // TODO: Move elsewhere
            return new Material(Pattern, new Color(0.4, 0.4, 0.5), 12.0);
        }

        /// <summary>
        /// Gets the event source for this attachment.
        /// </summary>
        public virtual Property<Bag<Event>> PendingEvents
        {
            get
            {
                return Bag<Event>.Empty;
            }
        }

        /// <summary>
        /// Gets the assemblage of game objects presented by this attachment.
        /// </summary>
        public abstract Property<Assemblage> Assemblage { get; }

        /// <summary>
        /// Gets the physical representation of this attachment.
        /// </summary>
        public abstract Dynamics.Attachment Physical { get; }

        /// <summary>
        /// Gets a static description of this attachment at the given reactive state.
        /// </summary>
        public abstract AttachmentPrototype GetPrototype(State State);
    }

    /// <summary>
    /// A full static description of an attachment at a particular reactive state. This can be used to serialize
    /// attachments.
    /// </summary>
    public abstract class AttachmentPrototype
    {
        /// <summary>
        /// Creates an instance of the attachment described by this prototype. The only missing information that
        /// needs to be supplied before the attachment can be put to use is its environment.
        /// </summary>
        public abstract Attachment Instantiate(Initializer Initializer);
    }

    /// <summary>
    /// Describes a slot for an attachment.
    /// </summary>
    public sealed class Slot
    {
        private readonly Attachment _Attachment;
        private World _World;
        private Vector _Offset;
        private Signal<HeightTransform> _HeightTransform;
        private static readonly InteriorRef<World> _WorldRef =
            (obj, act) => act(ref ((Slot)obj)._World);
        private static readonly InteriorRef<Vector> _OffsetRef =
            (obj, act) => act(ref ((Slot)obj)._Offset);
        private static readonly InteriorRef<Signal<HeightTransform>> _HeightTransformRef =
            (obj, act) => act(ref ((Slot)obj)._HeightTransform);
        public Slot(Initializer Initializer, AttachmentPrototype State)
        {
            this._Attachment = State.Instantiate(Initializer);
            this._Attachment.Environment = this;

            Property.Introduce(Ref.Create(this, _WorldRef));
            Property.Introduce(Ref.Create(this, _OffsetRef));
            Property.Introduce(Ref.Create(this, _HeightTransformRef));
        }

        /// <summary>
        /// The kind of this slot.
        /// </summary>
        public readonly SlotKind Kind;

        /// <summary>
        /// Gets or sets the world for this slot.
        /// </summary>
        public Property<World> World
        {
            get
            {
                return Property.Get(Ref.Create(this, _WorldRef), ref this._World);
            }
            set
            {
                Property.Define(Ref.Create(this, _WorldRef), ref this._World, value);
            }
        }

        /// <summary>
        /// Gets or sets the offset of this slot on its parent physical body.
        /// </summary>
        public Property<Vector> Offset
        {
            get
            {
                return Property.Get(Ref.Create(this, _OffsetRef), ref this._Offset);
            }
            set
            {
                Property.Define(Ref.Create(this, _OffsetRef), ref this._Offset, value);
            }
        }

        /// <summary>
        /// Gets or sets the height transform for this slot.
        /// </summary>
        public Property<Signal<HeightTransform>> HeightTransform
        {
            get
            {
                return Property.Get(Ref.Create(this, _HeightTransformRef), ref this._HeightTransform);
            }
            set
            {
                Property.Define(Ref.Create(this, _HeightTransformRef), ref this._HeightTransform, value);
            }
        }

        /// <summary>
        /// Gets the physical component of this attachment.
        /// </summary>
        public Dynamics.Attachment Physical
        {
            get
            {
                return this._Attachment.Physical;
            }
        }

        /// <summary>
        /// Gets the event source for this slot.
        /// </summary>
        public Property<Bag<Event>> PendingEvents
        {
            get
            {
                return this._Attachment.PendingEvents;
            }
        }

        /// <summary>
        /// Gets the assemblage of game objects presented by this slot.
        /// </summary>
        public Property<Assemblage> Assemblage
        {
            get
            {
                return this._Attachment.Assemblage;
            }
        }
    }

    /// <summary>
    /// Identifies a kind of circular slot for attachments.
    /// </summary>
    public sealed class SlotKind
    {
        private SlotKind(double Radius)
        {
            this.Radius = Radius;
        }

        /// <summary>
        /// The radius of the slot, in meters.
        /// </summary>
        public readonly double Radius;

        /// <summary>
        /// The smallest slot, for minor utilities.
        /// </summary>
        public static readonly SlotKind A = new SlotKind(0.5);

        /// <summary>
        /// The second smallest slot, for light weapons and most utilities.
        /// </summary>
        public static readonly SlotKind B = new SlotKind(0.8);
    }
}
