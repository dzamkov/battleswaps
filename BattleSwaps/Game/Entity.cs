﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;

namespace BattleSwaps.Game
{
    /// <summary>
    /// Identifies and describes an assemblage within a world that is emitted, serialized and deserialized as a group.
    /// Entities should be independent, with no direct references or communication between each others. Entities
    /// may only interact indirectly through game systems.
    /// </summary>
    public abstract class Entity
    {
        private World _World;
        private static readonly InteriorRef<World> _WorldRef =
            (obj, act) => act(ref ((Entity)obj)._World);
        public Entity()
        {
            Property.Introduce(Ref.Create(this, _WorldRef));
        }

        /// <summary>
        /// Gets or sets the world this entity is in.
        /// </summary>
        public Property<World> World
        {
            get
            {
                return Property.Get(Ref.Create(this, _WorldRef), ref this._World);
            }
            set
            {
                Property.Define(Ref.Create(this, _WorldRef), ref this._World, value);
            }
        }

        /// <summary>
        /// Sets the environment for this entity.
        /// </summary>
        public World Environment
        {
            set
            {
                this.World = value;
            }
        }

        /// <summary>
        /// Gets the assemblage of game objects presented by this entity.
        /// </summary>
        public virtual Property<Assemblage> Assemblage
        {
            get
            {
                return Game.Assemblage.Empty;
            }
        }

        /// <summary>
        /// Gets a latch such that is triggered when the entity is to be "dissolved", i.e. when it will no longer
        /// interact with the world.
        /// </summary>
        public virtual Property<Latch> Dissolve
        {
            get
            {
                return Property.From(delegate(out Latch res)
                {
                    Assemblage assem;
                    if (this.Assemblage.TryGetValue(out assem))
                    {
                        res = assem.Dissolve;
                        return true;
                    }
                    res = default(Latch);
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets the event source for this entity.
        /// </summary>
        public virtual Property<Bag<Event>> PendingEvents
        {
            get
            {
                return Bag<Event>.Empty;
            }
        }

        /// <summary>
        /// Gets a static description of this entity at the given reactive state.
        /// </summary>
        public abstract EntityPrototype GetPrototype(State State);
    }

    /// <summary>
    /// A full static description of an entity at a particular reactive state. This can be used to serialize entities.
    /// </summary>
    public abstract class EntityPrototype
    {
        /// <summary>
        /// Creates an instance of the entity described by this prototype. The only missing information that
        /// needs to be supplied before the entity can be put to use is its environment.
        /// </summary>
        public abstract Entity Instantiate(Initializer Initializer);

        /// <summary>
        /// Creates an instance of the entity described by this prototype.
        /// </summary>
        public Entity Instantiate(Initializer Initializer, World Environment)
        {
            Entity entity = this.Instantiate(Initializer);
            entity.Environment = Environment;
            return entity;
        }
    }
}
