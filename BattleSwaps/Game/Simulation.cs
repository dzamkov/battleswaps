﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;
using BattleSwaps.Intel;

namespace BattleSwaps.Game
{
    /// <summary>
    /// Manages the reactive system for a game simulation. This will control the evaluation of the simulation and
    /// apply player commands as they become available.
    /// </summary>
    public sealed class Simulation
    {
        private readonly Integrator _Integrator;
        private readonly ExplicitBag<Event> _Pending;
        public Simulation(WorldPrototype Initial)
        {
            Initializer initializer = Initializer.Create();
            this.World = Initial.Instantiate(initializer);
            this._Pending = new ExplicitBag<Event>(initializer);
            State initial = initializer.FinishInitialize();

            this._Integrator = new Integrator(16,
                this.World.PendingEvents.RuntimeValue + this._Pending,
                initial);
        }

        /// <summary>
        /// The world for this simulation.
        /// </summary>
        public readonly World World;

        /// <summary>
        /// Gets the last moment successfully integrated for this simulation.
        /// </summary>
        public Moment Head
        {
            get
            {
                return this._Integrator.Head;
            }
        }

        /// <summary>
        /// Gets or sets the clock for this simulation. This provides the time that the simulation can safely be
        /// integrated up to without missing any <see cref="Perturbation"/>s. No <see cref="Perturbation"/>s may be
        /// issued before the clock time. This is initially set to <see cref="Clock.StaticZero"/>.
        /// </summary>
        public Clock Clock
        {
            get
            {
                return this._Integrator.Clock;
            }
            set
            {
                this._Integrator.Clock = value;
            }
        }

        /// <summary>
        /// Adds a perturbation for consideration by this simulation. The time for the perturbation must be at
        /// or after the current clock time.
        /// </summary>
        public void Perturb(Perturbation Perturbation)
        {
            Debug.Assert(this.Clock.Current <= Perturbation.Time);

            // Create event to apply perturbation
            World world = this.World;
            ExplicitLatch applied = new ExplicitLatch();
            Name name = Name.Create(Perturbation.Serializer.Comparer, Perturbation);
            Event evnt = new Event(Perturbation.Time, name, delegate(Transaction trans)
            {
                Perturbation.Apply(trans, world);
                applied.Trigger(trans);
            });

            // Add as pending event
            this._Integrator.ApplyHead(this._Pending.Add, new Entry<Event>(evnt, applied));
        }

        /// <summary>
        /// Creates an observer for this simulation.
        /// </summary>
        public Observer CreateObserver(Initializer Initializer, Signal<Time> Lag)
        {
            return new Observer(this._Integrator, Initializer, Lag);
        }
    }

    /// <summary>
    /// Describes an external stimulus to a simulation which sets it off its normal deterministic progression.
    /// </summary>
    public abstract class Perturbation
    {
        [DefaultSerializer]
        public static readonly Serializer<Perturbation> Serializer = Data.Serializer.Open<byte, Perturbation>();
        public Perturbation() { }
        public Perturbation(Time Time)
        {
            this.Time = Time;
        }

        /// <summary>
        /// The simulation time that this perturbation is to be applied at.
        /// </summary>
        public Time Time;

        /// <summary>
        /// Applies this perturbation to the given world.
        /// </summary>
        public abstract void Apply(Transaction Transaction, World World);
    }

    /// <summary>
    /// A perturbation which applies a command using a faction interface.
    /// </summary>
    [Tag(typeof(Perturbation), (byte)(0))]
    public sealed class CommandPerturbation : Perturbation
    {
        [DefaultSerializer]
        public static readonly new Serializer<CommandPerturbation> Serializer =
            Data.Serializer.Struct<CommandPerturbation>();
        public CommandPerturbation() { }
        public CommandPerturbation(Time Time, NameRef<Faction> Faction, Command Command)
        {
            this.Time = Time;
            this.Faction = Faction;
            this.Command = Command;
        }

        /// <summary>
        /// The faction to apply the command for.
        /// </summary>
        public NameRef<Faction> Faction;

        /// <summary>
        /// The command to apply.
        /// </summary>
        public Command Command;

        public override void Apply(Transaction Transaction, World World)
        {
            this.Command.Apply(Transaction, this.Faction[Transaction].Interface);
        }
    }

    /// <summary>
    /// Reads the data from a simulation into an "external" reactive system, following a linear time progression.
    /// </summary>
    public sealed class Observer
    {
        // TODO: Variable time rate
        private readonly Action<Moment> _Listener;
        private readonly ExplicitBridge _Bridge;
        private readonly ExplicitSignal<TimeMark> _TimeMark;
        private readonly ExplicitSignal<Time> _InputTime;
        private Queue<Moment> _Queue;
        public Observer(Integrator Integrator, Initializer Initializer, Signal<Time> Lag)
        {
            this.Integrator = Integrator;
            this.Lag = Lag;

            // Set up listener
            this._Queue = new Queue<Moment>();
            this._Listener = new Action<Moment>(this._Enqueue);
            Moment head = Integrator.Listen(this._Listener);

            // Set up bridge and time mark
            this._Bridge = new ExplicitBridge(Initializer, head);
            this._InputTime = new ExplicitSignal<Time>(Initializer, head.Time);
            this._TimeMark = new ExplicitSignal<TimeMark>(Initializer, new TimeMark(head.Time, null));
        }

        /// <summary>
        /// The integrator for the simulation this observer is "observing".
        /// </summary>
        public readonly Integrator Integrator;

        /// <summary>
        /// The amount of time the observer should try to stay behind the simulation clock.
        /// </summary>
        public readonly Signal<Time> Lag;

        /// <summary>
        /// Correlates internal simulation times with external times.
        /// </summary>
        public struct TimeMark
        {
            public TimeMark(Time Internal, Time? External)
            {
                this.Internal = Internal;
                this.External = External;
            }

            /// <summary>
            /// The simulation time associated with this time mark.
            /// </summary>
            public Time Internal;

            /// <summary>
            /// The external time associated with this time mark.
            /// </summary>
            public Time? External;
        }

        /// <summary>
        /// The bridge between the "external" (i.e. UI) reactive system and the simulation reactive system.
        /// </summary>
        public Bridge Bridge
        {
            get
            {
                return this._Bridge;
            }
        }

        /// <summary>
        /// A signal in the external reactive system for this observer which provides the simulation time that
        /// input given through the observer should be applied at, taking into account the lag of the observer.
        /// This should be non-decreasing.
        /// </summary>
        public Signal<Time> InputTime
        {
            get
            {
                return this._InputTime;
            }
        }

        /// <summary>
        /// Checks if the simulation state is ready for the external moment of the given transaction, and if so,
        /// updates the observer with the appropriate mapping between states.
        /// </summary>
        public void Poll(Transaction Transaction)
        {
            TimeMark mark = this._TimeMark[Transaction];
            Time lag = this.Lag[Transaction];
            Time internalTime = mark.Internal;
            Time headTime = this.Integrator.Head.Time;
            if (mark.External.HasValue)
            {
                internalTime = mark.Internal + (Transaction.Time - mark.External.Value);
                if (internalTime <= headTime)
                {
                    // Advance bridge
                    State last = this._Bridge.Apply(Transaction.Base).State;
                    lock (this)
                    {
                        while (this._Queue.Count > 0 && this._Queue.Peek().Time <= internalTime)
                            last = this._Queue.Dequeue().State;
                    }
                    this._Bridge.Advance(Transaction, new Moment(last, internalTime));
                }
                else
                {
                    // Create discontinuity due to integrator underrun
                    internalTime = headTime;
                    mark.Internal = headTime;
                    mark.External = null;
                    this._TimeMark[Transaction] = mark;
                }
            }
            else if (mark.Internal + lag <= headTime)
            {
                // (Re-)start simulation here
                mark.External = Transaction.Time;
                this._TimeMark[Transaction] = mark;
            }

            // Update input time
            Time inputTime = internalTime + lag;
            if (this._InputTime[Transaction] < inputTime)
                this._InputTime[Transaction] = inputTime;
        }

        /// <summary>
        /// Gets a stimulus which, when given a command, will construct and apply the appropriate perturbation to
        /// the underlying simulation. This will handle input lag as if the command was issued by a player watching
        /// the simulation through this observer.
        /// </summary>
        public Stimulus<Command> GetRelay(Stimulus<Perturbation> Perturb, NameRef<Faction> Faction)
        {
            return delegate(Transaction trans, Command command)
            {
                Time inputTime = this._InputTime[trans];
                this._InputTime.TryUpdate(trans, Time.Next(inputTime));
                Perturb(trans, new CommandPerturbation(inputTime, Faction, command));
            };
        }

        /// <summary>
        /// Adds a new generated simulation moment to the queue for this observer.
        /// </summary>
        private void _Enqueue(Moment Moment)
        {
            lock (this)
            {
                this._Queue.Enqueue(Moment);
            }
        }
    }
}