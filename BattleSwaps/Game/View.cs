﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using BattleSwaps;
using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Geometry;
using BattleSwaps.Intel;
using BattleSwaps.Graphics;
using BattleSwaps.Graphics.UI;

namespace BattleSwaps.Game
{
    using UI = BattleSwaps.Graphics.UI;

    /// <summary>
    /// A widget that allows the user to interact with a game world as a player.
    /// </summary>
    public sealed class View : Widget, IActor<ViewState>
    {
        private readonly ExplicitSignal<CameraViewState> _CameraState;
        private readonly ExplicitSignal<Mouse<ImagePoint>> _Mouse;
        private readonly ExplicitSignal<StaticGroup> _Selection;
        private readonly Capture<DriveBeat> _DriveCapture;
        private readonly Capture<WeaponBeat> _WeaponCapture;
        private Interface _Interface;
        private Bag<Event> _PendingEvents;
        private Signal<Projection> _Projection;
        private Signal<DriveBeat> _DriveCaptureInput;
        private Signal<WeaponBeat> _WeaponCaptureInput;
        private Display _Display;
        private static readonly InteriorRef<Interface> _InterfaceRef =
            (obj, act) => act(ref ((View)obj)._Interface);
        public View(Initializer Initializer, ViewState State)
        {
            this._CameraState = new ExplicitSignal<CameraViewState>(Initializer, State.Camera);
            this._Mouse = new ExplicitSignal<Mouse<ImagePoint>>(Initializer, State.Mouse);
            this._Selection = new ExplicitSignal<StaticGroup>(Initializer, State.Selection);
            this.Camera = this._CameraState.Map(s => s.Value);

            Property.Introduce(Ref.Create(this, _InterfaceRef));

            // Set up capture
            this._DriveCapture = new Capture<DriveBeat>(DriveController.CaptureParameter, Initializer)
            {
                Interface = this.Interface,
                Input = this.DriveCaptureInput,
                UpdateRate = Time.FromSeconds(1.0 / 120.0),
                PollRate = Time.Zero
            };
            this._WeaponCapture = new Capture<WeaponBeat>(WeaponController.CaptureParameter, Initializer)
            {
                Interface = this.Interface,
                Input = this.WeaponCaptureInput,
                UpdateRate = Time.FromSeconds(1.0 / 120.0),
                PollRate = Time.FromSeconds(1.0 / 120.0)
            };
        }

        /// <summary>
        /// Gets or sets the interface this view uses to control units in the world.
        /// </summary>
        public Property<Interface> Interface
        {
            get
            {
                return Property.Get(Ref.Create(this, _InterfaceRef), ref this._Interface);
            }
            set
            {
                Property.Define(Ref.Create(this, _InterfaceRef), ref this._Interface, value);
            }
        }

        /// <summary>
        /// The camera used for this view.
        /// </summary>
        public readonly Signal<Camera> Camera;

        /// <summary>
        /// Gets the projection from world space to widget space.
        /// </summary>
        public Property<Signal<Projection>> Projection
        {
            get
            {
                if (this._Projection != null)
                    return this._Projection;
                return Property.From(delegate(out Signal<Projection> res)
                {
                    res = this._Projection;
                    if (res != null)
                        return true;

                    Signal<ImageSize> size;
                    if (this.Size.TryGetValue(out size))
                    {
                        res = Signal.Lift(size, this.Camera, (s, c) => c.GetProjection(s.Width, s.Height));
                        res = Interlocked.CompareExchange(ref this._Projection, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets the control signal provided by this view for a captured vehicle.
        /// </summary>
        public Property<Signal<DriveBeat>> DriveCaptureInput
        {
            get
            {
                if (this._DriveCaptureInput != null)
                    return this._DriveCaptureInput;
                return Property.From(delegate(out Signal<DriveBeat> res)
                {
                    res = this._DriveCaptureInput;
                    if (res != null)
                        return true;

                    Domain domain;
                    Signal<bool> isWDown;
                    Signal<bool> isSDown;
                    Signal<bool> isADown;
                    Signal<bool> isDDown;
                    if (this.Domain.TryGetValue(out domain) &&
                        domain.IsKeyDown(OpenTK.Input.Key.W).TryGetValue(out isWDown) &&
                        domain.IsKeyDown(OpenTK.Input.Key.S).TryGetValue(out isSDown) &&
                        domain.IsKeyDown(OpenTK.Input.Key.A).TryGetValue(out isADown) &&
                        domain.IsKeyDown(OpenTK.Input.Key.D).TryGetValue(out isDDown))
                    {
                        res = Signal.Lift(isWDown, isSDown, isADown, isDDown,
                            (w, s, a, d) => new Intel.DriveBeat(
                                (w ? 1 : 0) - (s ? 1 : 0),
                                (a ? 1 : 0) - (d ? 1 : 0)));
                        res = Interlocked.CompareExchange(ref this._DriveCaptureInput, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets the control signal provided by this view for a captured weapon.
        /// </summary>
        public Property<Signal<WeaponBeat>> WeaponCaptureInput
        {
            get
            {
                if (this._WeaponCaptureInput != null)
                    return this._WeaponCaptureInput;
                return Property.From(delegate(out Signal<WeaponBeat> res)
                {
                    res = this._WeaponCaptureInput;
                    if (res != null)
                        return true;

                    Signal<Projection> proj;
                    if (this.Projection.TryGetValue(out proj))
                    {
                        res = Signal.Custom<WeaponBeat>(
                            delegate(Moment moment, OrLatchBuilder invalidate, ref bool isDiscrete)
                        {
                            Mouse<ImagePoint> mouse = this._Mouse.Get(moment.State, invalidate);
                            return new WeaponBeat(
                                new DetailVector(
                                    proj.Get(moment, invalidate, ref isDiscrete).AtHeight(0.0).Inverse *
                                        (Vector)mouse.Position.Get(moment, invalidate, ref isDiscrete),
                                    1.0),
                                mouse.IsRightDown.Get(moment, invalidate, ref isDiscrete));
                        });
                        res = Interlocked.CompareExchange(ref this._WeaponCaptureInput, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        /// <summary>
        /// Advances the internal state of this view.
        /// </summary>
        private void _Advance(Transaction Transaction)
        {
            CameraViewState cur = this._CameraState[Transaction];
            double delta = (Transaction.Time - cur.LastUpdate).Seconds;
            cur.Value.Update(delta);
            cur.LastUpdate = Transaction.Time;
            this._CameraState[Transaction] = cur;
        }

        public override Property<Display> Display
        {
            get
            {
                if (this._Display != null)
                    return this._Display;
                return Property.From(delegate(out Display res)
                {
                    res = this._Display;
                    if (res != null)
                        return true;

                    Interface intelface;
                    Signal<Projection> projection;
                    if (this.Interface.TryGetValue(out intelface) &&
                        this.Projection.TryGetValue(out projection))
                    {
                        var world = intelface.World;

                        // Create selection markers
                        var markers = Figure.Union(Bag.Union(world.Units.Map<Bag<Figure>>(delegate(IUnit unit)
                        {
                            IController controller = unit.Controller;
                            return Signal.Lift<bool, StaticGroup, Bag<Figure>>(
                                controller.IsCaptured.AsSignalWithDefault(false), this._Selection,
                                delegate(bool isCaptured, StaticGroup selection)
                                {
                                    bool isSelected = selection.StaticContains(unit);
                                    Signal<Rectangle> rect = unit.Rectangle.Map(r => r.Pad(0.2));
                                    // TODO: Projection height
                                    if (isCaptured || isSelected)
                                        return Bag.Singleton<Figure>(
                                            projection.Map(p => p.AtHeight(0.0) * new MarkerFigure(
                                                isSelected ?
                                                    new Paint(1.0, 0.8, 0.5, 1.0) :
                                                    new Paint(0.7, 0.7, 0.7, 0.5),
                                                0.5, unit.Rectangle.Map(r => r.Pad(0.2)))));
                                    return Bag.Empty<Figure>();
                                }).Switch();
                        })));

                        // Create display
                        var comp = world.Scene;
                        res =
                            Graphics.Display.FromFigure(comp.GetFigure(projection)).Dither(16) +
                            Graphics.Display.FromFigure(markers);
                        res = Interlocked.CompareExchange(ref this._Display, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }


        /// <summary>
        /// Given the name of a view, gets the name of the update events it produces.
        /// </summary>
        private static readonly Func<Name, Name> _UpdateName =
            BattleSwaps.Name.GetDecorator(new Name(11950305940577315816));

        public override Property<Bag<Event>> PendingEvents
        {
            get
            {
                if (this._PendingEvents != null)
                    return this._PendingEvents;
                return Property.From(delegate(out Bag<Event> res)
                {
                    res = this._PendingEvents;
                    if (res != null)
                        return true;

                    Name name;
                    Bag<Event> driveCaptureEvents;
                    Bag<Event> weaponCaptureEvents;
                    if (this.Name.TryGetValue(out name) &&
                        this._DriveCapture.PendingEvents.TryGetValue(out driveCaptureEvents) &&
                        this._WeaponCapture.PendingEvents.TryGetValue(out weaponCaptureEvents))
                    {
                        Name updateName = _UpdateName(name);
                        res = Bag.Singleton(this._CameraState.Map(
                             old => new Event(old.LastUpdate + Time.FromSeconds(1.0 / 60.0),
                                 updateName, this._Advance)));
                        res = res + driveCaptureEvents + weaponCaptureEvents;
                        res = Interlocked.CompareExchange(ref this._PendingEvents, res, null) ?? res;
                    }
                    return false;
                });
            }
        }

        public override void OnKeyDown(Transaction Transaction, OpenTK.Input.Key Key)
        {
            if (Key == OpenTK.Input.Key.Enter)
            {
                var selection = this._Selection[Transaction];
                this._DriveCapture.Acquire(Transaction, selection);
                this._WeaponCapture.Acquire(Transaction, selection);
                this._Selection[Transaction] = StaticGroup.Empty;
            }
            else if (Key == OpenTK.Input.Key.Escape)
            {
                this._DriveCapture.Release(Transaction);
                this._WeaponCapture.Release(Transaction);
            }
        }

        public override void OnMouseScroll(Transaction Transaction, Mouse<ImagePoint> Mouse, double Amount)
        {
            CameraViewState cur = this._CameraState[Transaction];
            cur.Value.ZoomTo(
                this.Projection.RuntimeValue[Transaction].AtHeight(0.0).Inverse *
                (Vector)Mouse.Position[Transaction],
                -Amount * 0.5);
            this._CameraState[Transaction] = cur;
        }

        /// <summary>
        /// Gets the unit under the given point in the given world.
        /// </summary>
        private static IUnit _PickUnit(IWorld World, Transaction Transaction, Vector Point)
        {
            IUnit best = null;
            double bestArea = double.PositiveInfinity;
            World.Units.Iterate(Transaction, delegate(IUnit unit)
            {
                Rectangle rect = unit.Rectangle[Transaction];
                if (rect.Pad(0.2).Contains(Point))
                {
                    double area = rect.Area;
                    if (area < bestArea)
                    {
                        best = unit;
                        bestArea = area;
                    }
                }
            });
            return best;
        }

        public override void OnMouseDown(Transaction Transaction, MouseButton Button, Mouse<ImagePoint> Mouse)
        {
            Vector pos = this.Projection.RuntimeValue[Transaction].AtHeight(0.0).Inverse *
                (Vector)Mouse.Position[Transaction];
            if (Button == MouseButton.Left)
            {
                var domain = this.Domain.RuntimeValue;
                bool isShiftDown = domain.IsKeyDown(OpenTK.Input.Key.ShiftLeft).RuntimeValue[Transaction];
                bool isCtrlDown = domain.IsKeyDown(OpenTK.Input.Key.ControlLeft).RuntimeValue[Transaction];
                IUnit unit = _PickUnit(this.Interface.RuntimeValue.World, Transaction, pos);
                StaticGroup cur = this._Selection[Transaction];
                if (isShiftDown)
                {
                    if (unit != null)
                    {
                        if (cur.StaticContains(unit))
                            cur -= StaticGroup.Singleton(unit);
                        else
                            cur += StaticGroup.Singleton(unit);
                    }
                }
                else
                {
                    if (unit == null || (cur.IsSingleton && cur.StaticContains(unit)))
                        cur = StaticGroup.Empty;
                    else
                        cur = StaticGroup.Singleton(unit);
                }
                this._Selection[Transaction] = cur;
                this._Mouse[Transaction] = Mouse;
            }
        }
    }
    
    /// <summary>
    /// Describes the camera-related state of a view widget at a reactive state.
    /// </summary>
    public struct CameraViewState
    {
        // TODO: Camera span

        /// <summary>
        /// The time when the camera was last updated.
        /// </summary>
        public Time LastUpdate;

        /// <summary>
        /// Thelast value for the camera for the view.
        /// </summary>
        public Camera Value;
    }

    /// <summary>
    /// Describes the state of a view widget at a reactive state.
    /// </summary>
    public struct ViewState
    {
        /// <summary>
        /// The camera for the view.
        /// </summary>
        public CameraViewState Camera;

        /// <summary>
        /// The mouse that the view is currently responding to.
        /// </summary>
        public Mouse<ImagePoint> Mouse;

        /// <summary>
        /// The current selection for the view.
        /// </summary>
        public StaticGroup Selection;
    }
}
