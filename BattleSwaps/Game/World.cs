﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Data;
using BattleSwaps.Reactive;

namespace BattleSwaps.Game
{
    /// <summary>
    /// Identifies and describes a game world. This actor is a fully self-contained simulation of the game.
    /// </summary>
    public sealed class World
    {
        private readonly ExplicitBag<Entity> _Entities;
        private Intel.World _Intel;
        private Physics.Dynamics.World _Dynamics;
        private Physics.Ballistics.World _Ballistics;
        private Terrain.World _Terrain;
        private Assemblage _Assemblage;
        private Bag<Event> _PendingEvents;
        private static readonly InteriorRef<Intel.World> _IntelRef =
            (obj, act) => act(ref ((World)obj)._Intel);
        private static readonly InteriorRef<Physics.Dynamics.World> _DynamicsRef =
            (obj, act) => act(ref ((World)obj)._Dynamics);
        private static readonly InteriorRef<Physics.Ballistics.World> _BallisticsRef =
            (obj, act) => act(ref ((World)obj)._Ballistics);
        private static readonly InteriorRef<Terrain.World> _TerrainRef =
            (obj, act) => act(ref ((World)obj)._Terrain);
        public World(Initializer Initializer, WorldState State)
        {
            Property.Introduce(Ref.Create(this, _IntelRef));
            Property.Introduce(Ref.Create(this, _DynamicsRef));
            Property.Introduce(Ref.Create(this, _BallisticsRef));
            Property.Introduce(Ref.Create(this, _TerrainRef));

            // Set up entities
            ExplicitBagSetup<Entity> setup;
            this._Entities = new ExplicitBag<Entity>(Initializer, out setup);
            for (int i = 0; i < State.Entities.Length; i++)
            {
                Entity entity = State.Entities[i].Instantiate(Initializer);
                entity.Environment = this;
                setup.LazyAdd(Initializer, () => new Entry<Entity>(entity, entity.Dissolve.RuntimeValue));
            }

            // Set up relationships between properties
            Property.Defer(this.Intel, delegate(Intel.World intel)
            {
                intel.Terrain = this.Terrain;
                intel.Assemblage = Property.From(delegate(out Intel.Assemblage assem)
                {
                    Assemblage source;
                    if (this.Assemblage.TryGetValue(out source))
                    {
                        assem = source.Intel;
                        return true;
                    }
                    assem = default(Intel.Assemblage);
                    return false;
                });
            });
            Property.Defer(this.Dynamics, delegate(Physics.Dynamics.World dynamics)
            {
                dynamics.CollisionSources = Property.From(
                    delegate(out Bag<Physics.Dynamics.CollisionSource> res)
                    {
                        Assemblage source;
                        if (this.Assemblage.TryGetValue(out source))
                        {
                            res = source.CollisionSources;
                            return true;
                        }
                        res = default(Bag<Physics.Dynamics.CollisionSource>);
                        return false;
                    });
            });
            Property.Defer(this.Ballistics, delegate(Physics.Ballistics.World ballistics)
            {
                ballistics.ImpactSources = Property.From(
                    delegate(out Bag<Physics.Ballistics.ImpactSource> res)
                    {
                        Assemblage source;
                        if (this.Assemblage.TryGetValue(out source))
                        {
                            res = source.ImpactSources;
                            return true;
                        }
                        res = default(Bag<Physics.Ballistics.ImpactSource>);
                        return false;
                    });
            });
        }

        /// <summary>
        /// Gets or sets the intel-related component of this world.
        /// </summary>
        public Property<Intel.World> Intel
        {
            get
            {
                return Property.Get(Ref.Create(this, _IntelRef), ref this._Intel);
            }
            set
            {
                Property.Define(Ref.Create(this, _IntelRef), ref this._Intel, value);
            }
        }

        /// <summary>
        /// Gets or sets the physical dynamics-related component of this world.
        /// </summary>
        public Property<Physics.Dynamics.World> Dynamics
        {
            get
            {
                return Property.Get(Ref.Create(this, _DynamicsRef), ref this._Dynamics);
            }
            set
            {
                Property.Define(Ref.Create(this, _DynamicsRef), ref this._Dynamics, value);
            }
        }

        /// <summary>
        /// Gets or sets the ballistics-related component of this world. This must be set once, immediately
        /// after the world is created.
        /// </summary>
        public Property<Physics.Ballistics.World> Ballistics
        {
            get
            {
                return Property.Get(Ref.Create(this, _BallisticsRef), ref this._Ballistics);
            }
            set
            {
                Property.Define(Ref.Create(this, _BallisticsRef), ref this._Ballistics, value);
            }
        }

        /// <summary>
        /// Gets or sets the terrain-related component of this world.
        /// </summary>
        public Property<Terrain.World> Terrain
        {
            get
            {
                return Property.Get(Ref.Create(this, _TerrainRef), ref this._Terrain);
            }
            set
            {
                Property.Define(Ref.Create(this, _TerrainRef), ref this._Terrain, value);
            }
        }

        /// <summary>
        /// Gets the event source for this world.
        /// </summary>
        public Property<Bag<Event>> PendingEvents
        {
            get
            {
                if (this._PendingEvents != null)
                    return this._PendingEvents;
                return Property.From(delegate(out Bag<Event> res)
                {
                    res = this._PendingEvents;
                    if (res != null)
                        return true;

                    Physics.Dynamics.World dynamics;
                    Bag<Event> dynamicsSource;
                    if (this.Dynamics.TryGetValue(out dynamics) &&
                        dynamics.PendingEvents.TryGetValue(out dynamicsSource))
                    {
                        res = Bag.Union(this._Entities.Map(e => e.PendingEvents.RuntimeValue)) + dynamicsSource;
                        res = Interlocked.CompareExchange(ref this._PendingEvents, res, null) ?? res;
                        return true;
                    }
                    return false;
                });
            }
        }

        /// <summary>
        /// Gets the full assemblage of content in this world.
        /// </summary>
        public Property<Assemblage> Assemblage
        {
            get
            {
                if (this._Assemblage.IsValid)
                    return this._Assemblage;
                return Property.From(delegate(out Assemblage res)
                {
                    lock (this)
                    {
                        res = this._Assemblage;
                        if (res.IsValid)
                            return true;

                        this._Assemblage = res = Game.Assemblage.Union(
                            this._Entities.Map(e => e.Assemblage.RuntimeValue));
                        return true;
                    }
                });
            }
        }

        /// <summary>
        /// Adds an entity to this world.
        /// </summary>
        public void Spawn(Transaction Transaction, Entity Entity)
        {
            Entity.Environment = this;
            this._Entities.Add(Transaction, new Entry<Game.Entity>(Entity, Entity.Dissolve.RuntimeValue));
        }
    }

    /// <summary>
    /// Describes the state of a world at a particular reactive state.
    /// </summary>
    public struct WorldState
    {
        /// <summary>
        /// The prototypes for the entities in the world.
        /// </summary>
        public EntityPrototype[] Entities;
    }

    /// <summary>
    /// A full static description of a world at a particular reactive state. This can be used to serialize worlds.
    /// </summary>
    public struct WorldPrototype
    {
        /// <summary>
        /// The state of the world.
        /// </summary>
        public WorldState State;

        /// <summary>
        /// The prototype for the dynamics-related aspect of the world.
        /// </summary>
        public Physics.Dynamics.WorldPrototype Dynamics;

        /// <summary>
        /// The prototype for the ballistics-related aspect of the world.
        /// </summary>
        public Physics.Ballistics.WorldPrototype Ballistics;

        /// <summary>
        /// The prototype for the terrain-related aspected of the world.
        /// </summary>
        public Terrain.WorldPrototype Terrain;

        /// <summary>
        /// Sets the gravity of the world for both dynamics and ballistics.
        /// </summary>
        public double Gravity
        {
            set
            {
                this.Dynamics.Gravity = value;
                this.Ballistics.Gravity = value;
            }
        }

        /// <summary>
        /// Gets or sets the dynamics surface of the world.
        /// </summary>
        public Physics.Dynamics.Surface Surface
        {
            get
            {
                return this.Dynamics.Surface;
            }
            set
            {
                this.Dynamics.Surface = value;
            }
        }

        /// <summary>
        /// The prototypes for the factions in the world.
        /// </summary>
        public Intel.FactionPrototype[] Factions;

        /// <summary>
        /// Gets or sets the prototypes for the entities in the world.
        /// </summary>
        public EntityPrototype[] Entities
        {
            get
            {
                return this.State.Entities;
            }
            set
            {
                this.State.Entities = value;
            }
        }

        /// <summary>
        /// Creates an instance of the world described by this prototype.
        /// </summary>
        public World Instantiate(Initializer Initializer)
        {
            World world = new World(Initializer, this.State);
            Intel.World intelWorld = new Intel.World();
            world.Intel = intelWorld;
            world.Dynamics = this.Dynamics.Instantiate(Initializer);
            world.Ballistics = this.Ballistics.Instantiate(Initializer);
            world.Terrain = this.Terrain.Instantiate(Initializer);
            foreach (var faction in this.Factions)
                faction.Instantiate(Initializer, intelWorld);
            return world;
        }
    }

    /// <summary>
    /// Describes the contents of a game world, or a portion of a game world. This is a collection of game objects
    /// that interact in the game systems, such as physics or intel.
    /// </summary>
    public struct Assemblage
    {
        /// <summary>
        /// The collision sources in this assemblage.
        /// </summary>
        public Bag<Physics.Dynamics.CollisionSource> CollisionSources;

        /// <summary>
        /// The impact sources in this assemblage.
        /// </summary>
        public Bag<Physics.Ballistics.ImpactSource> ImpactSources;

        /// <summary>
        /// The intel-related contents of this assemblage.
        /// </summary>
        public Intel.Assemblage Intel;

        /// <summary>
        /// Gets or sets the controllable units presented by this assemblage.
        /// </summary>
        public Bag<Intel.Unit> Units
        {
            get
            {
                return this.Intel.Units;
            }
            set
            {
                this.Intel.Units = value;
            }
        }

        /// <summary>
        /// Gets or sets graphical contributions of this assemblage.
        /// </summary>
        public Graphics.Scene.Content Content
        {
            get
            {
                return this.Intel.Content;
            }
            set
            {
                this.Intel.Content = value;
            }
        }

        /// <summary>
        /// Gets a latch such that this assemblage is empty at all states where the latch is closed.
        /// </summary>
        public Latch Dissolve
        {
            get
            {
                return
                    this.CollisionSources.Dissolve &
                    this.ImpactSources.Dissolve &
                    this.Intel.Dissolve;
            }
        }

        /// <summary>
        /// Indicates whether this structure contains a valid assemblage.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return
                    this.CollisionSources != null &&
                    this.ImpactSources != null &&
                    this.Intel.IsValid;
            }
        }

        /// <summary>
        /// The empty assemblage.
        /// </summary>
        public static readonly Assemblage Empty = new Assemblage
        {
            CollisionSources = Bag<Physics.Dynamics.CollisionSource>.Empty,
            ImpactSources = Bag<Physics.Ballistics.ImpactSource>.Empty,
            Intel = BattleSwaps.Intel.Assemblage.Empty
        };

        /// <summary>
        /// Gets the union of a bag of assemblages.
        /// </summary>
        public static Assemblage Union(Bag<Assemblage> Sources)
        {
            return new Assemblage
            {
                CollisionSources = Bag.Union(Sources.Map(a => a.CollisionSources)),
                ImpactSources = Bag.Union(Sources.Map(a => a.ImpactSources)),
                Intel = BattleSwaps.Intel.Assemblage.Union(Sources.Map(a => a.Intel))
            };
        }

        public static Assemblage operator +(Assemblage A, Assemblage B)
        {
            return new Assemblage
            {
                CollisionSources = A.CollisionSources + B.CollisionSources,
                ImpactSources = A.ImpactSources + B.ImpactSources,
                Intel = A.Intel + B.Intel
            };
        }
    }
}