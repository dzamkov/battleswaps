﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using BattleSwaps.Util;
using BattleSwaps.Reactive;
using BattleSwaps.Intel;

namespace BattleSwaps.Game
{
    /// <summary>
    /// Manages the control capture of a set (possibly empty) of units with control input signals of the given type.
    /// </summary>
    public sealed class Capture<T>
    {
        private readonly ExplicitSignal<_Link> _State;
        private Interface _Interface;
        private Signal<T> _Input;
        private Time _UpdateRate;
        private Time _PollRate;
        private Bag<Event> _PendingEvents;
        private static readonly InteriorRef<Interface> _InterfaceRef =
            (obj, act) => act(ref ((Capture<T>)obj)._Interface);
        private static readonly InteriorRef<Signal<T>> _InputRef =
            (obj, act) => act(ref ((Capture<T>)obj)._Input);
        private static readonly InteriorRef<Time> _UpdateRateRef =
            (obj, act) => act(ref ((Capture<T>)obj)._UpdateRate);
        private static readonly InteriorRef<Time> _PollRateRef =
            (obj, act) => act(ref ((Capture<T>)obj)._PollRate);
        public Capture(CaptureParameter<T> Parameter, Initializer Initializer)
        {
            this.Parameter = Parameter;
            this._State = new ExplicitSignal<_Link>(Initializer, null);

            Property.Introduce(Ref.Create(this, _InterfaceRef));
            Property.Introduce(Ref.Create(this, _InputRef));
            Property.Introduce(Ref.Create(this, _UpdateRateRef));
            Property.Introduce(Ref.Create(this, _PollRateRef));
        }

        /// <summary>
        /// Contains the information needed to access and update a capture source through an interface.
        /// </summary>
        private sealed class _Link
        {
            public readonly NameRef<Intel.Capture> Capture;
            public readonly Listener<T> Listener;
            public _Link(NameRef<Capture> Capture, Listener<T> Listener)
            {
                this.Capture = Capture;
                this.Listener = Listener;
            }
        }

        /// <summary>
        /// The capture parameter this capture influences.
        /// </summary>
        public readonly CaptureParameter<T> Parameter;

        /// <summary>
        /// Gets or sets the interface this capture uses to communicate with the game simulation.
        /// </summary>
        public Property<Interface> Interface
        {
            get
            {
                return Property.Get(Ref.Create(this, _InterfaceRef), ref this._Interface);
            }
            set
            {
                Property.Define(Ref.Create(this, _InterfaceRef), ref this._Interface, value);
            }
        }

        /// <summary>
        /// Gets or sets the input signal provided for this capture. This signal is not actually evaluated unless
        /// there are units responding to the capture.
        /// </summary>
        public Property<Signal<T>> Input
        {
            get
            {
                return Property.Get(Ref.Create(this, _InputRef), ref this._Input);
            }
            set
            {
                Property.Define(Ref.Create(this, _InputRef), ref this._Input, value);
            }
        }

        /// <summary>
        /// Gets or sets the minimum time between transmitted signal updates for this captures source.
        /// </summary>
        public Property<Time> UpdateRate
        {
            get
            {
                return Property.Get(Ref.Create(this, _UpdateRateRef), ref this._UpdateRate);
            }
            set
            {
                Property.Define(Ref.Create(this, _UpdateRateRef), ref this._UpdateRate, value);
            }
        }

        /// <summary>
        /// Gets or sets the minimum time between pollings for the input signal for this capture source.
        /// </summary>
        public Property<Time> PollRate
        {
            get
            {
                return Property.Get(Ref.Create(this, _PollRateRef), ref this._PollRate);
            }
            set
            {
                Property.Define(Ref.Create(this, _PollRateRef), ref this._PollRate, value);
            }
        }

        /// <summary>
        /// Attempts to acquire control capture over the given group of units.
        /// </summary>
        public void Acquire(Transaction Transaction, Group Group)
        {
            Interface intelface = this.Interface.RuntimeValue;
            CaptureParameter<T> param = this.Parameter;
            _Link link = this._State[Transaction];
            if (link == null)
            {
                Listener<T> listener;
                NameRef<Capture> capture = NameRef<Capture>.Generate();
                T initial = Listener<T>.Listen(Transaction, new ListenerParameters<T>
                {
                    Comparer = this.Parameter.BeatSerializer.Comparer,
                    Signal = this.Input.RuntimeValue,
                    Update = (trans, beat) => intelface.UpdateCapture(trans, param, capture, beat),
                    UpdateRate = this.UpdateRate.RuntimeValue,
                    PollRate = this.PollRate.RuntimeValue,
                    EventName = Name.Generate()
                }, out listener);
                intelface.BeginCapture(Transaction, Group, param, capture, initial);
                this._State[Transaction] = new Capture<T>._Link(capture, listener);
            }
            else
            {
                intelface.OverrideBehavior(Transaction, Behavior.Define(param, link.Capture).For(Group));
            }
        }

        /// <summary>
        /// Releases control of all units associated with this capture.
        /// </summary>
        public void Release(Transaction Transaction)
        {
            _Link link = this._State[Transaction];
            if (link != null)
            {
                this.Interface.RuntimeValue.CancelCapture(Transaction, link.Capture);
                this._State[Transaction] = null;
            }
        }

        /// <summary>
        /// Gets the event source for this capture.
        /// </summary>
        public Property<Bag<Event>> PendingEvents
        {
            get
            {
                if (this._PendingEvents != null)
                    return this._PendingEvents;
                return Property.From(delegate(out Bag<Event> res)
                {
                    res = this._PendingEvents;
                    if (res != null)
                        return true;

                    res = this._State.Map(link => link != null ?
                        link.Listener.PendingEvents :
                        Bag<Event>.Empty).Switch();
                    res = Interlocked.CompareExchange(ref this._PendingEvents, res, null) ?? res;
                    return true;
                });
            }
        }
    }
}