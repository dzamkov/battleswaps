﻿using System;

using BattleSwaps.Geometry;

namespace BattleSwaps.Game
{
    /// <summary>
    /// Describes the state of the user's camera at a given moment.
    /// </summary>
    public struct Camera
    {
        public Vector Center;
        public Vector Velocity;
        public double Zoom;
        public double ZoomVelocity;

        public Camera(Vector Center, double Zoom)
        {
            this.Center = Center;
            this.Zoom = Zoom;
            this.Velocity = Vector.Zero;
            this.ZoomVelocity = 0.0;
        }

        /// <summary>
        /// Gets the projection from world space to view space, given the size of the view.
        /// </summary>
        public Projection GetProjection(double Width, double Height)
        {
            double perspHeight = Math.Exp(1.1 * this.Zoom);
            return
                Transform.Translation(new Vector(Width / 2.0, Height / 2.0)) *
                Transform.Scale(Math.Min(Width, Height) / Math.Exp(this.Zoom)) *
                Projection.Perspective(Transform.Translation(-this.Center), perspHeight);
        }

        private const double _VelocityDampening = 0.001;
        private const double _ZoomDampening = 0.001;

        /// <summary>
        /// Updates the camera as time passes.
        /// </summary>
        public void Update(double Seconds)
        {
            this.Center += Velocity * (Math.Exp(this.Zoom) * Seconds);
            this.Zoom += ZoomVelocity * Seconds;

            this.Velocity *= Math.Pow(_VelocityDampening, Seconds);
            this.ZoomVelocity *= Math.Pow(_ZoomDampening, Seconds);
        }

        /// <summary>
        /// Zooms the camera in or out of the given point.
        /// </summary>
        public void ZoomTo(Vector Point, double Amount)
        {
            this.ZoomVelocity += Amount * -Math.Log(_ZoomDampening);
            Vector newPoint =
                Transform.Translation(this.Center) *
                Transform.Scale(Math.Exp(Amount)) *
                Transform.Translation(-this.Center) * Point;
            this.Velocity += (Point - newPoint) * 
                (1.0 / Math.Exp(this.Zoom + Amount / 2.0)) *
                -Math.Log(_VelocityDampening);
        }
    }
}