#version 130
precision highp float;

#ifdef VERTEX
uniform mat3 posTransform;
uniform mat3 uvTransform;
in vec2 pos;
out vec2 uv;

void main(void) {
	uv = (uvTransform * vec3(pos, 1)).xy;
	gl_Position = vec4((posTransform * vec3(pos, 1)).xy, 0, 1);
}
#elif FRAGMENT
uniform sampler2D texture;
uniform mat4 colorTransform;
uniform vec4 colorOffset;
in vec2 uv;
out vec4 color;

void main(void) {
	color = colorOffset + colorTransform * texture2D(texture, uv);
}
#endif
