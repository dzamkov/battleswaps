#version 130
precision highp float;

#ifdef VERTEX
uniform mat3 transform;
uniform mat3 figureToDevice;
uniform float size;
in vec2 pos;
in vec2 dir;

void main(void) {
	vec2 ndir = (transform * vec3(dir, 0)).xy;
	if (length(ndir) > 0) ndir /= length(ndir);
	vec2 figurePos = size * ndir + (transform * vec3(pos, 1)).xy;
	vec2 devicePos = (figureToDevice * vec3(figurePos, 1)).xy;
	gl_Position = vec4(devicePos, 0, 1);
}
#elif FRAGMENT
uniform vec4 color;
out vec4 ncolor;

void main(void) {
	ncolor = color;
}
#endif
