#version 330 core

#pragma include "SolidVisual.glsl"

#ifdef VERTEX
uniform mat3x2 modelToProjScene;
in vec2 pos;
in vec2 grass;
out vec2 projScenePos;
out float nGrass;

void solidMain(out vec3 scenePos) {
	projScenePos = modelToProjScene * vec3(pos, 1);
	nGrass = grass;
	scenePos = vec3(projScenePos, 0.01);
}

#elif FRAGMENT
uniform sampler2D groundTexture;
in vec2 projScenePos;
in float nGrass;

void solidMain(out float relativeHeight, out vec3 diffuse, out vec3 specular, out float shininess) {
	relativeHeight = transformSceneToRelativeHeight(0.01);

	vec3 color = texture(groundTexture, projScenePos / 10.0).rgb * nGrass + vec3(0.2, 0.2, 0.3) * (1 - nGrass);
	diffuse = color * 0.5;
	specular = color * 0.5;
	shininess = 3;
}
#endif