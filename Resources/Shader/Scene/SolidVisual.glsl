uniform vec2 sceneToRelativeHeight;
uniform mat3x2 sceneToDeviceLateral;
uniform float sceneToDeviceInverseHeight;
uniform vec2 sceneToDeviceOffset;

vec2 transformSceneToDevice(vec3 pos) {
	float perspScale = 1 / (1 - sceneToDeviceInverseHeight * pos.z);
	return (sceneToDeviceLateral * vec3(pos.xy, 1)) * perspScale + sceneToDeviceOffset;
}

float transformSceneToRelativeHeight(float height) {
	return dot(sceneToRelativeHeight, vec2(height, 1));
}

#ifdef VERTEX
void solidMain(out vec3 scenePos);

void main(void) {
	vec3 scenePos;
	solidMain(scenePos);
	gl_Position = vec4(transformSceneToDevice(scenePos), 0, 1);
}
#else
layout (location = 0) out vec3 solidDiffuse;
layout (location = 1) out vec4 solidSpecular;
void solidMain(out float relativeHeight, out vec3 diffuse, out vec3 specular, out float shininess);

void main(void) {
	float relativeHeight;
	vec3 diffuse;
	vec3 specular;
	float shininess;
	solidMain(relativeHeight, diffuse, specular, shininess);
	gl_FragDepth = 1 - relativeHeight;
	solidDiffuse = diffuse;
	solidSpecular = vec4(specular, log(shininess - 1) / 7.0);
}
#endif