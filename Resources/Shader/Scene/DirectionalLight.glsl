#version 330 core

uniform sampler2D depthTexture;

#ifdef VERTEX
in vec2 pos;
out vec2 projScenePos;
out vec2 uv;
out vec2 luv;
out vec2 uuv;
out vec2 ruv;
out vec2 duv;

void main(void) {
	uv = pos;
	gl_Position = vec4(2 * pos.xy - vec2(1, 1), 0, 1);
	
	ivec2 size = textureSize(depthTexture, 0);
	luv = uv - vec2(1 / float(size.x), 0);
	uuv = uv - vec2(0, 1 / float(size.y));
	ruv = uv + vec2(1 / float(size.x), 0);
	duv = uv + vec2(0, 1 / float(size.y));
}

#elif FRAGMENT
uniform vec2 relativeToSceneHeight;
uniform mat3x2 deviceToSceneLateral;
uniform float sceneToDeviceInverseHeight;
uniform vec2 sceneToDeviceOffset;
uniform vec3 lightRadiance;
uniform vec3 lightDirection;
uniform mat4 sceneToShadow;
uniform sampler2D shadowTexture;
uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
in vec2 uv;
in vec2 luv;
in vec2 uuv;
in vec2 ruv;
in vec2 duv;
out vec4 radiance;

void main(void) {
	float relativeHeight = 1 - texture(depthTexture, uv).r;
	float lRelativeHeight = 1 - texture(depthTexture, luv).r;
	float uRelativeHeight = 1 - texture(depthTexture, uuv).r;
	float rRelativeHeight = 1 - texture(depthTexture, ruv).r;
	float dRelativeHeight = 1 - texture(depthTexture, duv).r;
	
	float height = dot(relativeToSceneHeight, vec2(relativeHeight, 1));
	float perspScale = 1 - sceneToDeviceInverseHeight * height;
	vec2 sceneDepthX = deviceToSceneLateral * vec3(2.0 / textureSize(depthTexture, 0).x, 0, 0);
	vec2 sceneDepthY = deviceToSceneLateral * vec3(0, 2.0 / textureSize(depthTexture, 0).y, 0);
	vec3 scenePos = vec3(
		deviceToSceneLateral * vec3((2 * uv - vec2(1, 1) - sceneToDeviceOffset) * perspScale, 1),
		height);

	vec4 rawShadowPos = sceneToShadow * vec4(scenePos, 1);
	vec3 shadowPos = 0.5 * (rawShadowPos.xyz / rawShadowPos.w + vec3(1, 1, 1));
	if (shadowPos.z < texture(shadowTexture, shadowPos.xy).r) {
		vec3 norm = normalize(-cross(
			vec3(2 * sceneDepthX * perspScale, (rRelativeHeight - lRelativeHeight) * relativeToSceneHeight.x),
			vec3(2 * sceneDepthY * perspScale, (dRelativeHeight - uRelativeHeight) * relativeToSceneHeight.x)));

		vec3 diffuse = texture(diffuseTexture, uv).rgb;
		vec4 specularPack = texture(specularTexture, uv);
		vec3 specular = specularPack.rgb;
		float shininess = exp(specularPack.a * 7.0) + 1;

		radiance = vec4(lightRadiance *
			(diffuse * max(0, dot(lightDirection, norm)) +
			specular * pow(max(0, dot(reflect(-lightDirection, norm), vec3(0, 0, 1))), shininess)),
			1);
	} else {
		radiance = vec4(0, 0, 0, 1);
	}
}

#endif