#version 330 core

#pragma include "SolidShadow.glsl"

#ifdef VERTEX
uniform mat4x3 modelToScene;
in vec3 pos;

void main(void) {
	writeVertex(modelToScene * vec4(pos, 1));
}
#elif FRAGMENT

void main(void) {

}
#endif