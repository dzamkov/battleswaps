#version 330 core

#pragma include "SolidVisual.glsl"

#ifdef VERTEX
uniform mat4x3 modelToScene;
in vec3 pos;
#ifdef TEXTURE
in vec2 uv;
out vec2 nuv;
#endif
out float relativeHeight;

void solidMain(out vec3 scenePos) {
	#ifdef TEXTURE
	nuv = uv;
	#endif
	scenePos = modelToScene * vec4(pos, 1);
	relativeHeight = transformSceneToRelativeHeight(scenePos.z);
}
#elif FRAGMENT
uniform vec3 diffuse;
uniform vec3 specular;
uniform float shininess;
#ifdef TEXTURE
uniform sampler2D diffuseTexture;
in vec2 nuv;
#endif
in float relativeHeight;

void solidMain(out float a, out vec3 b, out vec3 c, out float d) {
	a = relativeHeight;
	b = diffuse;
	c = specular;
	d = shininess;
	#ifdef TEXTURE
	b *= texture(diffuseTexture, nuv).rgb;
	#endif
}
#endif