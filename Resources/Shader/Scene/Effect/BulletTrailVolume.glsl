#version 130
precision highp float;

#pragma include "../Volume.glsl"

const float life = 1;

#ifdef VERTEX
uniform float width;
uniform vec3 initialPosition;
uniform vec3 initialVelocity;
uniform float param;
uniform float end;
in vec2 pos;
out float side;
out float time;

void main(void) {
	time = clamp(param * pos.x, param - life, end);

	vec2 across = normalize(initialVelocity.xy);
	across = 0.5 * width * vec2(-across.y, across.x);
	side = 2 * pos.y - 1;
	
	volumeVertex(initialPosition.xy + time * initialVelocity.xy + side * across);
}
#elif FRAGMENT
uniform float width;
uniform vec3 initialPosition;
uniform vec3 initialVelocity;
uniform float gravity;
uniform float param;
uniform sampler2D texture;
in float side;
in float time;

void main(void) {
	float age = (param - time) / life;
	float dist = length(initialVelocity.xy) * time;
	float height = initialPosition.z + time * initialVelocity.z - 0.5 * time * time * gravity;
	float extent = width * sqrt(1 - side * side);
	float occlusion = getOcclusion() - height;
	float visible = clamp((1 - occlusion / extent) / 2, 0, 1);
	
	float density = visible * texture2D(texture, vec2(dist * width, clamp(0.5 * side / sqrt(age) + 0.5, 0, 1))).r;
	density *= clamp(time / 0.05, 0, 1);
	density *= 1 - clamp(sqrt(age), 0, 1);
	density *= 0.5;
	vec3 radiance = 0.8 * (ambientColor + lightColor) * vec3(density, density, density);

	volumeFrag(radiance, 1 - density);
}
#endif