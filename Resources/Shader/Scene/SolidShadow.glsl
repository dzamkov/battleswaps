uniform mat4 sceneToShadow;

void writeVertex(vec3 scenePos) {
	gl_Position = sceneToShadow * vec4(scenePos, 1);
}