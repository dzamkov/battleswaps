#version 330 core

#ifdef VERTEX
in vec2 pos;
out vec2 uv;

void main(void) {
	uv = pos;
	gl_Position = vec4(2 * pos.xy - vec2(1, 1), 0, 1);
}

#elif FRAGMENT
uniform vec3 lightRadiance;
uniform sampler2D diffuseTexture;
in vec2 uv;
out vec4 radiance;

void main(void) {
	radiance = vec4(lightRadiance * texture(diffuseTexture, uv).rgb, 1);
}

#endif