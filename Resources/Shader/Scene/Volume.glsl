uniform float objectToRelativeHeightOffset;
uniform float objectToRelativeHeightScale;
uniform mat3 objectToDevice;

#ifdef VERTEX
uniform mat3 deviceToDepth;
out vec2 depthPos;

void volumeVertex(vec2 pos) {
	vec2 devicePos = (objectToDevice * vec3(pos, 1)).xy;
	depthPos = (deviceToDepth * vec3(devicePos, 1)).xy;
	gl_Position = vec4(devicePos, 0, 1);
}

#elif FRAGMENT
uniform sampler2D depthTexture;
uniform vec3 ambientColor;
uniform vec3 lightColor;
uniform vec2 lightDirection;
in vec2 depthPos;
out vec4 color;

// Gets the relative height offset of the solid contents of the scene within this volume
float getOcclusion() {
	float solidRelHeight = 1 - texture2D(depthTexture, depthPos).r;
	return (solidRelHeight - objectToRelativeHeightOffset) / objectToRelativeHeightScale;
}

void volumeFrag(vec3 radiance, float transmittance) {
	transmittance = max(transmittance, 0.001);
	color = vec4((1 / transmittance) * radiance, 1 / transmittance - 1);
}

void volumeNullFrag() {
	color = vec4(0, 0, 0, 0);
}

#endif