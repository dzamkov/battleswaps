#version 330 core

#ifdef VERTEX
in vec2 pos;
out vec2 uv;

void main(void) {
	uv = pos;
	gl_Position = vec4(2 * pos.xy - vec2(1, 1), 0, 1);
}

#elif FRAGMENT
uniform sampler2D lightTexture;
in vec2 uv;
out vec4 color;

void main(void) {
	vec4 light = texture2D(lightTexture, uv);
	color = vec4(light.rgb, 1);
}

#endif
