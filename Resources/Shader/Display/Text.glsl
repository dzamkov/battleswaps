#version 130
precision highp float;

#ifdef VERTEX
uniform ivec2 viewOffset;
uniform ivec2 viewSize;
in vec2 uv;
in vec2 pos;
out vec2 atlasUV;

void main(void) {
	atlasUV = uv;
	vec2 viewPos = (pos + vec2(viewOffset)) / vec2(viewSize);
	gl_Position = vec4(2 * viewPos.x - 1, 1 - 2 * viewPos.y, 0, 1);
}
#elif FRAGMENT
uniform sampler2D atlasTexture;
uniform vec4 paint;
in vec2 atlasUV;
out vec4 color;

void main(void) {
	vec3 baseColor = texture2D(atlasTexture, atlasUV).rgb;
	color = vec4(paint.rgb, paint.a * baseColor.r);
}
#endif
