#version 130
precision highp float;

#ifdef VERTEX
uniform sampler2D backTexture;
uniform ivec2 size;
uniform ivec2 viewOffset;
uniform ivec2 viewSize;
in vec2 pos;
out vec2 relPos;
out vec2 pixPos;
out vec2 backPos;

void main(void) {
	relPos = pos;
	pixPos = pos * vec2(size);
	backPos = pixPos / textureSize(backTexture, 0);
	vec2 viewPos = (pixPos + vec2(viewOffset)) / vec2(viewSize);
	gl_Position = vec4(2 * viewPos.x - 1, 1 - 2 * viewPos.y, 0, 1);
}
#elif FRAGMENT
uniform sampler2D windowTexture;
uniform sampler2D backTexture;
uniform ivec2 size;
in vec2 relPos;
in vec2 pixPos;
in vec2 backPos;
out vec4 color;

void main(void) {
	vec2 windowSize = textureSize(windowTexture, 0);
	vec2 windowPos =
		(pixPos + windowSize / 2 - clamp(pixPos, windowSize / 2, vec2(size) - windowSize / 2))
		/ windowSize;
		
	vec3 backColor = texture2D(backTexture, backPos).rgb;
	vec4 windowColor = texture2D(windowTexture, windowPos);
	vec3 newColor = vec3(0.4, 0.7, 0.3) * windowColor.r + backColor * windowColor.g;
	color = vec4(newColor, windowColor.a);
}
#endif
