#version 130
precision highp float;

#ifdef VERTEX
in vec2 pos;
out vec2 uv;

void main(void) {
	uv = pos;
	gl_Position = vec4(2 * pos.xy - vec2(1, 1), 0, 1);
}
#elif FRAGMENT
uniform sampler2D texture;
in vec2 uv;
out vec4 color;

void main(void) {
	color = vec4(texture2D(texture, uv).rgb, 1);
}
#endif
