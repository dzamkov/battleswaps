#version 130
precision highp float;

#ifdef VERTEX
uniform mat3 posTransform;
uniform mat3 uvTransform;
in vec2 pos;
out vec2 uv;

void main(void) {
	uv = (uvTransform * vec3(pos, 1)).xy;
	gl_Position = vec4(2 * pos.xy - vec2(1, 1), 0, 1);
}
#elif FRAGMENT
uniform sampler2D source;
uniform int levels;
in vec2 uv;
out vec4 color;

float[] ditherKernel = float[](1, 9, 3, 11, 13, 5, 15, 7, 4, 12, 2, 10, 16, 8, 14, 6);
float ditherBase = 17;

void main(void) {
	float ditherRange = 1.0 / levels;
	int ditherX = int(gl_FragCoord.x) % 4;
	int ditherY = int(gl_FragCoord.y) % 4;
	int ditherIndex = ditherX + 4 * ditherY;
	float ditherValue = ditherKernel[ditherIndex] * ditherRange / ditherBase;

	vec4 baseColor = texture2D(source, uv);
	color = floor((baseColor + vec4(1, 1, 1, 1) * ditherValue) / ditherRange) * ditherRange;
}
#endif
